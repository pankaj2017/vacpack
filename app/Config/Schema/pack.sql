/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.7.28-0ubuntu0.16.04.2 : Database - vacpack
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`vacpack` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `vacpack`;

/*Table structure for table `acos` */

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_acos_lft_rght` (`lft`,`rght`),
  KEY `idx_acos_alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=latin1;

/*Table structure for table `admingroups` */

DROP TABLE IF EXISTS `admingroups`;

CREATE TABLE `admingroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `adminprofiles` */

DROP TABLE IF EXISTS `adminprofiles`;

CREATE TABLE `adminprofiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `gender` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `userimage` text CHARACTER SET utf8,
  `birthdate` date DEFAULT NULL,
  `messanger` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `msgtype` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `aboutme` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admingroup_id` smallint(6) NOT NULL,
  `firstname` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lastname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `email_address` varchar(150) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(25) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `aros` */

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aros_lft_rght` (`lft`,`rght`),
  KEY `idx_aros_alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `aros_acos` */

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`),
  KEY `idx_aco_id` (`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `articlecategories` */

DROP TABLE IF EXISTS `articlecategories`;

CREATE TABLE `articlecategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `articlecoverimages` */

DROP TABLE IF EXISTS `articlecoverimages`;

CREATE TABLE `articlecoverimages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `namesmall` varchar(256) NOT NULL,
  `namemedium` varchar(256) NOT NULL,
  `namebig` varchar(265) CHARACTER SET utf8 NOT NULL,
  `filesize` int(11) NOT NULL,
  `mimetype` varchar(50) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `cover` enum('Y','N') NOT NULL DEFAULT 'N',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `articles` */

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `articlecategory_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `meta_title` text,
  `meta_desc` text,
  `body` text CHARACTER SET utf8,
  `comment_count` int(11) NOT NULL DEFAULT '0',
  `is_fullwidth` tinyint(1) DEFAULT NULL,
  `articleview_count` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `articleviews` */

DROP TABLE IF EXISTS `articleviews`;

CREATE TABLE `articleviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `ipadds` varchar(40) CHARACTER SET utf8 NOT NULL,
  `realipadds` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `opsys` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

/*Table structure for table `comments` */

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `article_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email_address` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `firstname` varchar(25) CHARACTER SET utf8 NOT NULL,
  `website` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `about` text CHARACTER SET utf8,
  `iso_2` char(2) CHARACTER SET utf8 DEFAULT NULL,
  `iso_3` char(3) CHARACTER SET utf8 DEFAULT NULL,
  `isonumeric` varchar(4) CHARACTER SET utf8 DEFAULT NULL,
  `phonecode` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `lat` decimal(13,7) DEFAULT NULL,
  `long` decimal(13,7) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=latin1;

/*Table structure for table `destinations` */

DROP TABLE IF EXISTS `destinations`;

CREATE TABLE `destinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_id` smallint(6) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image` text,
  `imagename` text,
  `about` text,
  `localfood` text,
  `howtoreach` text,
  `besttime` text,
  `googleplaceid` varchar(100) DEFAULT NULL,
  `lat` decimal(11,8) DEFAULT NULL,
  `lon` decimal(11,8) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `holidaythemes` */

DROP TABLE IF EXISTS `holidaythemes`;

CREATE TABLE `holidaythemes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `holidaythemes_packags` */

DROP TABLE IF EXISTS `holidaythemes_packags`;

CREATE TABLE `holidaythemes_packags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packag_id` int(11) NOT NULL,
  `holidaytheme_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

/*Table structure for table `hotelroomtypes` */

DROP TABLE IF EXISTS `hotelroomtypes`;

CREATE TABLE `hotelroomtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `inquiries` */

DROP TABLE IF EXISTS `inquiries`;

CREATE TABLE `inquiries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `inquirytitle` varchar(150) NOT NULL,
  `inquiryremark` text,
  `status` char(2) NOT NULL DEFAULT 'nw',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `itineraries` */

DROP TABLE IF EXISTS `itineraries`;

CREATE TABLE `itineraries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packag_id` int(11) NOT NULL,
  `daynumber` smallint(6) DEFAULT '1',
  `titleofday` varchar(60) DEFAULT NULL,
  `abouttheday` text,
  `imageofday` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `packageincludes` */

DROP TABLE IF EXISTS `packageincludes`;

CREATE TABLE `packageincludes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amenity` varchar(20) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `packagetypes` */

DROP TABLE IF EXISTS `packagetypes`;

CREATE TABLE `packagetypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` text,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `packags` */

DROP TABLE IF EXISTS `packags`;

CREATE TABLE `packags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `travelagency_id` int(11) NOT NULL,
  `discounttype` char(2) NOT NULL DEFAULT 'FT',
  `title` varchar(100) NOT NULL,
  `description` text,
  `numberofdays` tinyint(4) NOT NULL DEFAULT '1',
  `numberofnights` tinyint(4) NOT NULL DEFAULT '1',
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `inclusions` text,
  `exclusions` text,
  `notes` text,
  `featured` tinyint(4) DEFAULT NULL,
  `sponsored` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `packags_packageincludes` */

DROP TABLE IF EXISTS `packags_packageincludes`;

CREATE TABLE `packags_packageincludes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packag_id` int(11) NOT NULL,
  `packageinclude_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `packags_packagetypes` */

DROP TABLE IF EXISTS `packags_packagetypes`;

CREATE TABLE `packags_packagetypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packag_id` int(11) NOT NULL,
  `packagetype_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

/*Table structure for table `places` */

DROP TABLE IF EXISTS `places`;

CREATE TABLE `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image` text,
  `about` text,
  `googleplaceid` varchar(100) DEFAULT NULL,
  `lat` decimal(11,8) DEFAULT NULL,
  `lon` decimal(11,8) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `profiles` */

DROP TABLE IF EXISTS `profiles`;

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `gender` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `userimage` text CHARACTER SET utf8,
  `birthdate` date DEFAULT NULL,
  `messanger` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `msgtype` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `aboutme` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `quotes` mediumtext CHARACTER SET utf8,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `ratings` */

DROP TABLE IF EXISTS `ratings`;

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foreign_model` varchar(100) CHARACTER SET utf8 NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `author_ip` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `rating` float NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `description` text,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `tagged` */

DROP TABLE IF EXISTS `tagged`;

CREATE TABLE `tagged` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foreign_key` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `model` varchar(255) CHARACTER SET utf8 NOT NULL,
  `language` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `times_tagged` int(11) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `keyname` varchar(30) CHARACTER SET utf8 NOT NULL,
  `weight` int(2) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Table structure for table `thingstodos` */

DROP TABLE IF EXISTS `thingstodos`;

CREATE TABLE `thingstodos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `about` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tofriends` */

DROP TABLE IF EXISTS `tofriends`;

CREATE TABLE `tofriends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `sender` varchar(50) CHARACTER SET utf8 NOT NULL,
  `senderemail` varchar(150) CHARACTER SET utf8 NOT NULL,
  `receiver` varchar(50) CHARACTER SET utf8 NOT NULL,
  `recemail` varchar(150) CHARACTER SET utf8 NOT NULL,
  `reccontact` varchar(25) CHARACTER SET utf8 NOT NULL,
  `link` text CHARACTER SET utf8 NOT NULL,
  `message` text CHARACTER SET utf8,
  `userip` varchar(40) CHARACTER SET utf8 NOT NULL,
  `mailsent` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `travelagencies` */

DROP TABLE IF EXISTS `travelagencies`;

CREATE TABLE `travelagencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8,
  `website` varchar(200) DEFAULT NULL,
  `teamsize` varchar(7) DEFAULT NULL,
  `phone` varchar(20) NOT NULL DEFAULT '0000000',
  `email_address` varchar(200) DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8 NOT NULL,
  `region` varchar(50) CHARACTER SET utf8 NOT NULL,
  `country_id` smallint(6) NOT NULL DEFAULT '226',
  `zipcode` varchar(10) NOT NULL,
  `imagesmall` text CHARACTER SET utf8,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `travelagentgroups` */

DROP TABLE IF EXISTS `travelagentgroups`;

CREATE TABLE `travelagentgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `travelagentprofiles` */

DROP TABLE IF EXISTS `travelagentprofiles`;

CREATE TABLE `travelagentprofiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `travelagent_id` int(11) NOT NULL,
  `gender` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `userimage` text CHARACTER SET utf8,
  `birthdate` date DEFAULT NULL,
  `messanger` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `msgtype` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `aboutme` text CHARACTER SET utf8,
  `quotes` mediumtext CHARACTER SET utf8,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `travelagents` */

DROP TABLE IF EXISTS `travelagents`;

CREATE TABLE `travelagents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `travelagency_id` int(11) NOT NULL,
  `travelagentgroup_id` int(11) NOT NULL,
  `country_id` mediumint(9) DEFAULT NULL,
  `firstname` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lastname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `email_address` varchar(150) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(25) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `zipcode` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `confirm` varchar(36) CHARACTER SET utf8 NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `firstname` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lastname` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `phone` varchar(25) CHARACTER SET utf8 NOT NULL,
  `confirmkey` char(36) CHARACTER SET utf8 NOT NULL,
  `src` varchar(5) CHARACTER SET utf8 NOT NULL DEFAULT 'site',
  `lastlogin` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `zones` */

DROP TABLE IF EXISTS `zones`;

CREATE TABLE `zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
