/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.7.21-0ubuntu0.16.04.1 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `articlecategories` (
	`id` int (11),
	`parent_id` int (11),
	`name` varchar (384),
	`slug` varchar (300),
	`active` tinyint (4),
	`lft` int (10),
	`rght` int (10),
	`created` datetime ,
	`modified` datetime 
); 
insert into `articlecategories` (`id`, `parent_id`, `name`, `slug`, `active`, `lft`, `rght`, `created`, `modified`) values('1',NULL,'Honeymoon','honeymoon','1','1','6','2019-05-15 12:34:56','2019-11-24 21:33:50');
insert into `articlecategories` (`id`, `parent_id`, `name`, `slug`, `active`, `lft`, `rght`, `created`, `modified`) values('2','1','asda','asda','0','2','3','2019-11-24 17:25:34','2019-11-24 17:25:34');
insert into `articlecategories` (`id`, `parent_id`, `name`, `slug`, `active`, `lft`, `rght`, `created`, `modified`) values('3',NULL,'asdasd','asdasd','0','7','10','2019-11-24 17:26:42','2019-11-24 21:39:26');
insert into `articlecategories` (`id`, `parent_id`, `name`, `slug`, `active`, `lft`, `rght`, `created`, `modified`) values('4','3','cdcdef','cdcdef','1','8','9','2019-11-24 18:30:27','2019-11-24 18:30:27');
insert into `articlecategories` (`id`, `parent_id`, `name`, `slug`, `active`, `lft`, `rght`, `created`, `modified`) values('5','1','Honey moon','honey-moon','1','4','5','2019-11-24 18:37:09','2019-11-24 21:25:10');
