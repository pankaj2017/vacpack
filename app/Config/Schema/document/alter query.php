ALTER TABLE `states` ADD `slug` VARCHAR(200) NOT NULL AFTER `name`, ADD `image` VARCHAR(255) NOT NULL AFTER `slug`;
ALTER TABLE `states` ADD `imagename` VARCHAR(255) NOT NULL AFTER `image`;

ALTER TABLE `destinations` ADD `slug` VARCHAR(200) NOT NULL AFTER `name`;



//date 19-01-2020
ALTER TABLE `packags` ADD `state _id` INT(10) NOT NULL AFTER `travelagency_id`;


//date 10-02-2020
ALTER TABLE `itineraries` ADD `destination_id` INT(10) NOT NULL AFTER `packag_id`;



//date 11-02-2019
ALTER TABLE `packags` ADD `pkgimage` VARCHAR(200) NOT NULL AFTER `state_id`;

//date 07-05-2020
ALTER TABLE `holidaythemes` ADD `holidayimage` VARCHAR(200) NULL AFTER `description`;