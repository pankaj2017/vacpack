-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 13, 2020 at 10:28 AM
-- Server version: 5.7.28-log
-- PHP Version: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insurance_advisor`
--

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `foreign_model` text NOT NULL,
  `ratingtype_id` int(11) NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `authorip` varchar(255) NOT NULL,
  `rating` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `foreign_model`, `ratingtype_id`, `foreign_id`, `authorip`, `rating`, `created`, `modified`) VALUES
(1, 'Insuranceprofessional', 1, 4, 'dasd32', 5, '2020-08-28 17:24:55', '2020-08-28 17:24:55'),
(2, 'Insuranceprofessional', 2, 4, '1', 2, '2020-08-31 12:54:31', '2020-08-31 12:54:31'),
(3, 'Insuranceprofessional', 1, 4, '::1', 5, '2020-09-02 17:47:21', '2020-09-02 17:47:21'),
(4, 'Insuranceprofessional', 2, 4, '::1', 5, '2020-09-02 17:47:24', '2020-09-02 17:47:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
