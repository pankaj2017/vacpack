-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 13, 2020 at 10:28 AM
-- Server version: 5.7.28-log
-- PHP Version: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insurance_advisor`
--

-- --------------------------------------------------------

--
-- Table structure for table `rating_types`
--

CREATE TABLE `rating_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `foreign_model` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedby` int(11) NOT NULL DEFAULT '0',
  `reasontodelete` varchar(200) NOT NULL DEFAULT 'NA'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating_types`
--

INSERT INTO `rating_types` (`id`, `name`, `foreign_model`, `status`, `created`, `modified`, `deletedon`, `deletedby`, `reasontodelete`) VALUES
(1, '2222', 'Insuranceprofessional', 1, '2019-12-23 09:58:39', '2020-08-28 14:44:26', '2019-12-23 09:58:39', 0, 'NA'),
(2, 'Facebook', 'Insuranceprofessional', 1, '2020-02-29 05:00:00', '2020-02-29 05:01:29', '2020-02-29 05:00:00', 0, 'NA');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rating_types`
--
ALTER TABLE `rating_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rating_types`
--
ALTER TABLE `rating_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
