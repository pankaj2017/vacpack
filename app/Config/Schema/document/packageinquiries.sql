-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2020 at 07:09 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vacpack`
--

-- --------------------------------------------------------

--
-- Table structure for table `packageinquiries`
--

CREATE TABLE `packageinquiries` (
  `id` int(10) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` int(20) NOT NULL,
  `message` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packageinquiries`
--

INSERT INTO `packageinquiries` (`id`, `fname`, `lname`, `email`, `phone`, `message`, `created`, `modified`) VALUES
(1, 'sa', 'asd', 'asds@gmail.com', 23423, '34324', '2020-06-07 01:34:05', '2020-06-07 01:34:05'),
(2, 'pankaj', 'bhaliya', 'pajka@gmail.com', 23423, '34233432', '2020-06-07 01:35:25', '2020-06-07 01:35:25'),
(3, 'dsa', 'asd', '2342@gmail.com', 23423, 'sdfsf', '2020-06-07 01:38:14', '2020-06-07 01:38:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `packageinquiries`
--
ALTER TABLE `packageinquiries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `packageinquiries`
--
ALTER TABLE `packageinquiries`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
