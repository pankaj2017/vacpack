<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
	Router::connect('/india', array('controller' => 'countries', 'action' => 'getCountry'));
	Router::connect('/india/:slug', array('controller' => 'states', 'action' => 'view'),['pass' => ['slug']]);
	Router::connect('/india/:slug1/:slug2', array('controller' => 'destinations', 'action' => 'placesList'),['pass' => ['slug1','slug2']]);
	Router::connect('/agency/:id',
		array('controller' => 'tagencies', 'action' => 'view'),
		array('pass' => array('id'))
	);
	//Router::connect('/admin', array('controller' => 'admins', 'action' => 'dashboard','admin'=>true));
	Router::connect('/admin', 
	array('plugin'=>'','controller'=>'admins','action'=>'login','admin'=>true)
	);
	
	Router::connect('/tour-package/:slug', array('controller' => 'packages', 'action' => 'filter'),['pass' => ['slug']]);
	Router::connect('/tour-package/:slug/str::str', array('controller' => 'packages', 'action' => 'filter'),['pass' => ['slug','str']]);
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));


	// Router::connect('/packages/:slug', array('controller' => 'packages', 'action' => 'getDetail'),['pass' => ['slug']]);
	Router::connect('/packages/:slug/:id',
	    array('controller' => 'packages', 'action' => 'view'),
	    array(
	        'slug' => '[A-Za-z0-9\._-]+',
	        'id'   => '[0-9]+',
	        'pass' => array('slug','id')
	    ));
	Router::connect('/article/:slug-:id',
		array('controller' => 'articles', 'action' => 'view'),
		array('pass' => array('slug','id'))
	);
	Router::connect('/articles/:slug/:id',
		array('controller' => 'articles', 'action' => 'index'),
		array('pass' => array('slug','id'))
	);
	Router::connect('/articles',
		array('controller' => 'articlecategories', 'action' => 'index')
	);
	
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
