<?php
App::uses('ClassRegistry', 'Utility');
App::uses('UserConfirmed', 'Lib/Event');
App::uses('CakeEventManager', 'Event');
CakeEventManager::instance()->attach(new UserConfirmed());
?>