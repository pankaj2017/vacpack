$(document).ready(function() {
	//common variable
	var width = $(window).width();
	var height = $(window).height();

	// select js
	$('select').niceSelect();

	// menu js
	if (width <= 991) {
		$('.bars-icon').on('click', function() {
			var ele = $('.mobile-navigation');
			if (ele.hasClass('open')) {
				ele.removeClass('open')
				$('body').removeClass('overflow-hidden')
			} else {
				ele.addClass('open')
				$('body').addClass('overflow-hidden')
			}
		})

		$('.menu').on('click', function() {
			var ele = $(this).next('.mob-submenu');
			var parantEle = $(this).closest('.mob-submenu');

			if (!parantEle.length && !$(this).hasClass('open')) {
				$('.menu').removeClass('open');
			}

			if ($(this).hasClass('open')) {
				$(this).removeClass('open');
			} else {
				parantEle.find('.menu').removeClass('open');
				$(this).addClass('open');
			}
		})
	}
})