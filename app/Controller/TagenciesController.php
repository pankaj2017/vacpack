<?php
App::uses('AppController', 'Controller');

/**
 * Profiles Controller
 *
 * @property Profile $Profile
 * @property PaginatorComponent $Paginator
 */
class TagenciesController extends AppController {
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Feedback.Ratings' => array('on' => array('view')),
        'Feedback.Comments' => ['on' => ['view']],
    );
	
	public $helpers = array('Paginator');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
	}
	public function index() {
		
		$conditions = array();//',(Agency.name LIKE \'%'.$this->request->data['query'].'%\' or Agency.name LIKE \''.$this->request->data['query'].'%\')');
		$this->Paginator->settings = array(
			'Travelagency'=>array(
				'contain'=>array(
					'Travelagent'=>array('fields'=>array('id','firstname','lastname'),
						'Agentprofile'=>array('fields'=>array('id','imagesmall')),
					),
					//'Bscoverimage'=>array('imagesmall','imagebig'),
					//'Bscategory'=>array('id','name'),
					//'Rating'=>array()
				),
			'fields'=>array('Travelagency.id','Travelagency.name','Travelagency.city','Travelagency.phone','Travelagency.modified'),
			'conditions' => array($conditions),
			'limit' => 20,
			'order'=>array('Travelagency.active','Travelagency.name'=>'asc'),
			)
		);
		$travelagencies = $this->Paginator->paginate();
		//pr($travelagencies);exit;
		$this->set('travelagencies', $travelagencies);
		//echo json_encode($travelagencies);
		//exit;
	}
	public function view($id = null,$admin=false) {
		if (!$this->Tagency->exists($id)) {
			throw new NotFoundException(__('Invalid Aagency'));
		}
		$conditions = array('Tagency.' . $this->Tagency->primaryKey => $id);

		$fields = array("Tagency.id","Tagency.name","Tagency.description","Tagency.city","Tagency.region","Tagency.zipcode","Tagency.phone","Tagency.imagesmall","Tagency.email_address",
		);
		$this->Tagency->Behaviors->load('Containable');
		$agency = $this->Tagency->find('first',array(
			"contain"=>array(
				"Packag"=>array(
					"State"=>array("fields"=>array("State.name")),
					"fields"=>array("Packag.id","Packag.title","Packag.description","Packag.price","Packag.numberofnights","Packag.numberofdays","Packag.pkgimage","Packag.active","Packag.modified")
				),
				"Travelagent"=>array(
					"Travelagentprofile"=>array("fields"=>array("Travelagentprofile.aboutme","Travelagentprofile.userimage")),
					"fields"=>array("Travelagent.id","Travelagent.firstname","Travelagent.lastname","Travelagent.phone","Travelagent.email_address")
				),
			),
		"fields"=>$fields,
		"conditions"=>$conditions)
		);
		$agency_rate=$this->Tagency->find('first',['conditions'=>$conditions]);
		$agency =array_replace_recursive($agency,$agency_rate);
		
		$this->set('agencyid', $agency['Tagency']['id']);
		$this->set('travelagency', $agency);
	}
	
}
