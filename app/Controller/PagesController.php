<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();
 	public $components = ['Feedback.Ratings' => array('on' => array('display'))];
/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function display() {
		$query = $this->request->data;
		
		if(isset($query['Searchform'])) {
			 $redirect=['controller'=>'packages','action'=>'filter','slug'=>$query['Searchform']['holidaythem']];
			 if(isset($query['days'][0]) || isset($query['price_ranges'][0]))
			 {
			 	$str = serialize($this->request->data);
			 	$search_data = (base64_encode(implode(Configure::read('Security.salt'),['str'=>$str])));
			 	$redirecs['str'] = $search_data;
			 	$redirect = array_merge($redirect,$redirecs);
			 }
			
			 $this->redirect($redirect);
		}
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		if (in_array('..', $path, true) || in_array('.', $path, true)) {
			throw new ForbiddenException();
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}

		$this->loadModel('Article');		
		$random_list_articles = $this->Article->getRandomListArticle();	

		$this->loadModel('Package');
		$random_list_packages = $this->Package->find('all',['conditions' => [],'recursive' =>1,'order' => 'rand()','limit' => 6]);

		/*Useed in home page popular destionation block*/
		$this->loadModel('Country');
		$this->Country->Behaviors->load('Containable');
		$countries_data = $this->Country->find('first',
			['contain'=>
				[
				'State'=>
					[
						'fields'=>['State.id','State.name','State.slug','State.description','State.imagebig'],
						'order'=>['State.name']
					],
					
				],
				'fields'=>['Country.id','Country.name'/*,'Country.about'*/],
				'conditions' => ['Country.' . $this->Country->primaryKey => 104],
				
			]
		);
		$this->set('popular_destinations',$countries_data);

		/*Used in home page search destination drop down*/
		$this->loadModel('State');
		$state_list = $this->State->find('list',['conditions' => ['State.country_id' => 104],'fields'=>['State.id','State.name']]);

		/*Used in home page search holiday theme drop down*/
		$this->loadModel('Holidaytheme');
		$holiday_thems = $this->Holidaytheme->find('list',['conditions' => [],'fields'=>['Holidaytheme.name','Holidaytheme.name']]);
		// pr($holiday_thems);die;
		

		/*Used in Travel agency home page list */
		$this->loadModel('Tagency');
		$fields = ['Tagency.*'];
		$tagency = $this->Tagency->find('all',['conditions' => [],'fields'=>$fields,'order' => 'rand()','limit' => 4]);
		// pr($tagency);die;
				
		$this->set(compact('page', 'subpage', 'title_for_layout','random_list_articles','random_list_packages','tagency','state_list','holiday_thems'));
		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
	
	public function privacypolicies(){
		
	}
	
	public function termsofuse(){
		
	}
	
	public function about(){
		
	}
}
