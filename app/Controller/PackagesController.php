<?php
App::uses('AppController', 'Controller');

/**
 * Profiles Controller
 *
 * @property Profile $Profile
 * @property PaginatorComponent $Paginator
 */
class PackagesController extends AppController {
/**
 * Components
 *
 * @var array
 */
	// public $components = array('Paginator');
	var $components = array('Paginator','Feedback.Ratings' => array('on' => array('view')),
        'Feedback.Comments' => ['on' => ['view']],
	);

	public $helpers = array('Paginator');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
	}

/**
 * index method
 *
 * @return void
 */
	public function index($packtype=null) {

		$postconditions = array();
		
		//$this->Package->recursive = 0;
		if($packtype){
			//$postconditions[] = array("Packagetype.id"=>$packtype);
		}
		$this->Package->Behaviors->load('Containable');
		$this->Paginator->settings=array(
				'contain'=>array(
					'Travelagency'=>array(
						'fields'=>array('Travelagency.id','Travelagency.name','Travelagency.phone')
					),
				),
				'fields'=>array('Packag.title','Packag.id','Packag.numberofdays','Packag.numberofdays','Packag.numberofnights','Packag.modified','Packag.price','Packag.active',),
				'conditions'=>$postconditions,
				'limit' => 10
		);
		$packags=$this->Paginator->paginate();
		//pr($packags);
		$this->set('packages',$packags);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($slug ='',$id = '') {
		if (!$this->Package->exists($id)) {
			throw new NotFoundException(__('Invalid profile'));
		}
		$conditions = array('Package.' . $this->Package->primaryKey => $id);
				
		$fields = array("Package.*","Travelagent.id","Travelagent.firstname","Travelagent.");
		$this->Package->Behaviors->load('Containable');
		$package=$this->Package->find('first',
			array(
				'contain'=>array(
					'Travelagency'=>array(
						'Travelagent'=>array(
							'fields'=>array('Travelagent.firstname','Travelagent.lastname','Travelagent.email_address','Travelagent.phone')
						),
						'fields'=>array('Travelagency.name','Travelagency.phone','Travelagency.description','Travelagency.modified','Travelagency.created','Travelagency.active')
					),'Itinerary'
				),
				'fields'=>array('Package.title','Package.id','Package.inclusions','Package.description','Package.numberofdays','Package.numberofnights','Package.pkgimage','Package.active','Package.modified','Package.price','Package.travelagency_id','Package.inclusions','Package.exclusions','Package.discount','Package.discounttype'),
				'conditions'=>$conditions,
				'limit' => 10,
				'order'=>array('')
			)
		);
		$package1=$this->Package->find('first',['conditions'=>$conditions]);
		$package =array_replace_recursive($package,$package1);
		// pr($package);exit;
		$this->set('packagid', $package['Package']['id']);
		$this->set('package', $package);
	}
	public function filter($slug,$fnd= array()){

		if ($this->request->is('post')) {
			$query = $this->request->data;

			$redirect=['controller'=>'packages','action'=>'filter','slug'=>$slug];
			 if(isset($query['Package']['days'][0]) || isset($query['Package']['price_ranges'][0]))
			 {
			 	$str = serialize($this->request->data['Package']);
			 	$search_data = (base64_encode(implode(Configure::read('Security.salt'),['str'=>$str])));
			 	$redirecs['str'] = $search_data;
			 	// pr($redirecs);die;
			 	$redirect = array_merge($redirect,$redirecs);
			 }
			
			 $this->redirect($redirect);
		}

		if(isset($fnd) && !empty($fnd)) {

			$encryptedarray= explode(Configure::read('Security.salt'),base64_decode($fnd));
			$query = unserialize($encryptedarray[0]);
			$this->set('query', $query);

		}else{
			$query = [];
		}
		// pr($query);die;
		// $query = $this->request->query;
		$postconditions =[];
		
		if (isset($query['price_ranges'][0]) && !empty($query['price_ranges'][0])) {
			foreach ($query['price_ranges'] as $key => $value) {
				$price  = explode("-", $value);
					$postconditions['OR'][$key] = ['Package.price BETWEEN ? and ?' => array($price[0], $price[1])];
			}
		}
		if (isset($query['days'][0]) && !empty($query['days'][0])) {
			foreach ($query['days'] as $key => $value) {
				$day  = explode("-", $value);
					$postconditions['OR'][$key] = ['Package.numberofdays BETWEEN ? and ?' => array($day[0], $day[1])];
			}
		}
		$conditions = array();
		$conditions['conditions'] = $postconditions;
		// print_r($price_con);
		// $postconditions[] =  array('OR'=>[
		// ]);
		// $foreach ($this->request->query['price_ranges'] as $price_key => $price_value) {
		// 	dd($s);
		// $postconditions = array('OR'=>[
		// 		['Package.price BETWEEN ? and ?' => array(1, 50)],
		// 		['Package.price BETWEEN ? and ?' => array(50, 100)]
		// ]);
		// }
		// use this "between" range

		// $this->Package->Behaviors->load('Containable');
		// $this->Paginator->settings = array(
		// 		'contain'=>array(
		// 			'Travelagency'=>array(
		// 				'fields'=>array('Travelagency.id','Travelagency.name','Travelagency.phone')
		// 			),
		// 			'Holidaytheme'=>array(
		// 				'fields'=>array('Holidaytheme.*'),
		// 				'conditions' => array(
		//                    "Holidaytheme.name" => "Family",
		//                ),
		// 			),
		// 		),
		// 		'fields'=>array('Package.title','Package.id','Package.pkgimage','Package.numberofdays','Package.numberofdays','Package.numberofnights','Package.modified','Package.price','Package.active','Package.description','Travelagency.name'),
		// 		'conditions'=>$postconditions,
		// 		'limit' => 10
		// );
		// $packags = $this->Paginator->paginate();
		// $this->set('package_data', $packags);
		
		$conditions['joins'] = array(
		    array(
                'table' => 'holidaythemes_packags', 
                'alias' => 'HolidaythemesPackag',
                'type' => 'INNER',
                'conditions' => array(
                    'HolidaythemesPackag.packag_id = Package.id'
                )
            ),
            array(
                'table' => 'holidaythemes',
                'alias' => 'Holidaytheme',
                'type' => 'INNER',
                'conditions' => array(
                    'Holidaytheme.id = HolidaythemesPackag.holidaytheme_id',
                    'Holidaytheme.name = "' . $slug . '"'
                )
            )
		);
		// pr($conditions);die;
        $packags = $this->Package->find('all',$conditions);
        // pr($p);die;
		$this->set('package_data', $packags);
		// pr($packags);die;
	}
	public function createpackageinquires()
	{
		$layout = 'ajax';
        $this->autoRender = false;
      
      	$this->loadModel('Packageinquiry');
      	if ($this->Packageinquiry->save($this->request->data)) {

        	$result = ['success' => true, 'message' => "Inquiry has been submited"];
            echo json_encode($result);
            exit;
        }else{
        	// pr($this->Packageinquiry->validationErrors);die;
			$this->set('errors',$this->Packageinquiry->validationErrors);die;
        }
		
	}
}
