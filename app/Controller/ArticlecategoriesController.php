<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 */
class ArticlecategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
		$this->set('masterclass','active');
		$this->set('usersclass','');
	}

	private function _details($id = null)
	{
		$options =['recursive'=>-1,'conditions' =>['Articlecategory.'.$this->Articlecategory->primaryKey => $id]];
		$data = $this->Articlecategory->find('first', $options);
		return $data ;
	}
	private function _treeFormCheck(array &$elements, $parentId = 0, $elkey = null) {
        $branch = array();

        foreach ($elements as $key=>$element) {
            if ($element[$elkey]['parent_id'] == $parentId) {
                $children = $this->_treeFormCheck($elements, $element[$elkey]['id'], $elkey);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$element[$elkey]['id']] = $element;
                unset($elements[$key]);
            }
        }
        return $branch;
    }
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$category_tree = $this->Articlecategory->find('threaded',['conditions'=>['Articlecategory.active'=>1]]);
		$this->set('categories', $category_tree);

		$category_list= $this->Articlecategory->find('all',
			[
				"conditions" => ['Articlecategory.parent_id' => ''],
				"fields"=>["Articlecategory.id","Articlecategory.name","Articlecategory.slug"],
				"contain"=>[
					"ChildArticlecategory"=>[
					"fields"=>["ChildArticlecategory.id","ChildArticlecategory.name","ChildArticlecategory.slug"]
					]
				]
			]
		);
		$this->set('categorylist',$category_list);
		
		//pr($category_list);exit;
		   
        $menuTreeCheck = $this->_treeFormCheck($category_tree, 0, 'Articlecategory');
    
        $this->set('menutreecheck', $menuTreeCheck);

		//Get all article with categories.
		$conditions = array('Articlecategory.active'=>1);		
		$this->Articlecategory->recursive = -1;
		$cat = $this->Articlecategory->find('all',
			array(
				'conditions'=>$conditions,
				'order'=>['Articlecategory.modified'=>'ASC'],
				'contain' => [
					'Article'=>['Articlecoverimage','order'=>['Article.modified'=>'DESC'],
					'limit'=>5
					],
				]
			)
		);
		// pr($cat);die;
		$this->set('cat',$cat);
	}
	
	public function admin_index(){
		$this->set('openarticle','has-class');
		$this->set('activearticlecategory','has-class');
		

		$category_tree = $this->Articlecategory->find('threaded');  
		$this->set('categories', $category_tree);
		//pr($category_tree);die;
		
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_create() {
		$this->set('openarticle','has-class');
		$this->set('activearticlecategory','has-class');
		if ($this->request->is('post')) {
		// 	$this->Articlecategory->create();
			
			if($this->request->data['Articlecategory']['slug'] == '') {
				$slug = Inflector::slug ($this->request->data['Articlecategory']['name'],'-');
			} else {
				$slug = Inflector::slug ($this->request->data['Articlecategory']['slug'],'-');
			}
			$slug = strtolower($slug);
			$this->request->data['Articlecategory']['slug'] = $slug;
			
			if($this->Articlecategory->save($this->request->data))
			{
				$this->Session->setFlash(__('Article Category created successfully.'), 'default',['class' => 'alert alert-success']);
				$this->redirect(['controller'=>'articlecategories', 'action'=>'index', 'admin'=>true]);
			}
			else 
			{
				$this->set('errors',$this->Articlecategory->validationErrors);
			}
		}
		$spacer="-";
		$parent = $this->Articlecategory->generateTreeList('', '', '', $spacer);
		$this->set(compact('parent'));
	}
	
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Articlecategory->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		$options = array('conditions' => array('Articlecategory.' . $this->Articlecategory->primaryKey => $id));
		$this->set('articlecategory', $this->Articlecategory->find('first', $options));
	}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Articlecategory->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		
		if ($this->request->is(['post', 'put'])) 
		{
			if($this->request->data['Articlecategory']['slug'] == '') {
				$slug = Inflector::slug ($this->request->data['Articlecategory']['name'],'-');
			} else {
				$slug = Inflector::slug ($this->request->data['Articlecategory']['slug'],'-');
			}
			$slug = strtolower($slug);
			$this->request->data['Articlecategory']['slug'] = $slug;

			if ($this->Articlecategory->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Article Category has been saved.'), 'default',['class' => 'alert alert-success']);
				return $this->redirect(['action' => 'index']);
			} 
			else 
			{
				$this->set('errors',$this->Articlecategory->validationErrors);
			}
		} 
		else 
		{
			$this->request->data = $this->_details($id);
		}
		$spacer="-";
		$parent = $this->Articlecategory->generateTreeList('', '', '', $spacer);
		$this->set(compact('parent'));
		$this->set('parent_category','disabled');
	}
	public function admin_edit($id = null){
		$this->set('openarticle','has-class');
		$this->set('activearticlecategory','has-class');
		$this->edit($id);
	}
	public function admin_delete($id = null) 
	{
		$this->Articlecategory->id = $id;
		// $this->set('openarticle','in');
		// $this->set('activearticlecategory','active');
		if (!$this->Articlecategory->exists($id)) 
		{
			throw new NotFoundException(__('Invalid Article Category'));
		}
		if ($this->Articlecategory->delete()) 
		{
			$this->Session->setFlash(__('Article Category  has been deleted.'), 'default', ['class' => 'alert alert-success']);
		} 
		else 
		{
			$this->set('errors',$this->Articlecategory->validationErrors);
		}
		return $this->redirect(['action' => 'index']);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Articlecategory->id = $id;
		if (!$this->Articlecategory->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Articlecategory->delete()) {
			$this->Flash->success(__('The category has been deleted.'));
		} else {
			$this->Flash->error(__('The category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
