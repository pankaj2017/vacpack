<?php
App::uses('AppController', 'Controller');
/**
 * Aagentgroups Controller
 *
 * @property Aagentgroup $Aagentgroup
 * @property PaginatorComponent $Paginator
 */
class PlacesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Flash');
	
	public function beforeFilter() {
		parent::beforeFilter();
		// For CakePHP 2.1 and up
		//$this->Auth->allow();
		// $this->Auth->authorize = array(
		// 	AuthComponent::ALL => array('actionPath' => 'controllers/', 'userModel' => 'Admin'),
		// 	'Actions',
		// 	'Controller'
		// );

		$this->set('masterclass','');
		$this->set('dashboardclass','');
		$this->set('aclclass','');
		$this->set('usersclass','');
		$this->set('groupsclass','active');
	}

/**
 * index method
 *
 * @return void
 */
	public function index($admin=null) {
		$this->Place->recursive = 0;
		$this->Paginator->settings=array(
			'Place' => array(
				'order'=>array('Place.name'=>'asc'),
			)
		);
		$places = $this->Paginator->paginate();
		// pr($places);die;
		$this->set('places', $places);
	}
	private function _ajaxCall() {
        if (isset($this->request->query['ajax'])) {
            if ($this->isajaxcallonly()) {
                $this->autoRender = false;
                $param = $this->request->query['ajax'];
                if($param == 'get-places-list') {
                    return $this->_getPlacesList();
                }
                return;    
            }            
        }
    }
    private function _getPlacesList() 
	{
		$data = $this->Place->getListingData();
        echo json_encode($data);
        exit; 
	}
	public function admin_index($admin=true) {
		$this->_ajaxCall();	

		$this->set('opendestination','has-class');
		$this->set('activeplace','has-class');
		// $this->index($admin);

	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		/*if (!$this->Place->exists($id)) {
			throw new NotFoundException(__('Invalid Place'));
		}*/
		$options = array('conditions' => array('Place.' . $this->Place->primaryKey => $id));
		$place = $this->Place->find('first', $options);
		// pr($place);
		$this->set('place',$place );
	}
	public function admin_view($id = null) {

		$this->set('opendestination','has-class');
		$this->set('activeplace','has-class');
		$this->view($id);
	}

/**
 * add method
 *
 * @return void
 */
	public function create() {
		if ($this->request->is(array('post','put'))) {
			// pr($this->request->data);exit;
			$this->Place->create();
			if ($this->Place->save($this->request->data)) {
				$this->Flash->success(__('The Place has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The Place could not be saved. Please, try again.'));
			}
		}else {
			// $this->loadModel('State');
			// $states = $this->State->find('list',['recursive'=>-1,'conditions'=>[]]);
			// $this->set(compact('states'));
			 // pr($states); die;
			$this->loadModel('Country');
			$countries = $this->Country->find('list',['recursive'=>-1,'conditions'=>['Country.active'=>1]]);
			$this->set(compact('countries'));
		}
		$this->loadModel('Country');
		$countries = $this->Country->find('list',['recursive'=>-1,'conditions'=>['Country.active'=>1]]);
		$this->set(compact('countries'));
	}
	
	public function admin_create() {
		$this->set('opendestination','has-class');
		$this->set('activeplace','has-class');
		
		$this->create();
	}
	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		if (!$this->Place->exists($id)) {
			throw new NotFoundException(__('Invalid Place'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if(empty($this->request->data['Place']['image']['name'])){
				
				unset($this->request->data['Place']['image']);
			}
			if ($this->Place->save($this->request->data)) {
				$this->Flash->success(__('The Place has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The Place could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Place.' . $this->Place->primaryKey => $id));
			$this->request->data = $this->Place->find('first', $options);
			$this->request->data['Place']['state_id'] = $this->request->data['Destination']['state_id'];
			
			// $this->request->data['Country']['country_id'] = $this->request->data['Destination']['state_id'];
			// pr($this->request->data['Destination']['state_id']); die;
			$this->loadModel('State');
			$country_id = $this->State->find('first',['conditions'=>['State.id'=>$this->request->data['Destination']['state_id']]]);
			
			$this->request->data['Place']['country_id'] =$country_id['Country']['id'];
			$this->loadModel('Destination');

			$destinations = $this->Destination->find('list',['recursive'=>-1,'conditions'=>['Destination.state_id'=> $this->request->data['Destination']['state_id']]]);
		
			$this->set(compact('destinations'));

			$this->loadModel('State');
			$states = $this->State->find('list',['recursive'=>-1,'conditions'=>['State.country_id'=>$country_id['Country']['id']]]);
			$this->set(compact('states'));
		

			$this->loadModel('Country');
			$countries = $this->Country->find('list',['recursive'=>-1,'conditions'=>['Country.active'=>1]]);
			$this->set(compact('countries'));

		}
	}

	public function admin_edit($id = null) {

		$this->set('opendestination','has-class');
		$this->set('activeplace','has-class');
		$this->edit($id);
	}
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Place->id = $id;
		if (!$this->Place->exists()) {
			throw new NotFoundException(__('Invalid Place'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Place->delete()) {
			$this->Flash->success(__('The Place has been deleted.'));
		} else {
			$this->Flash->error(__('The Place could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	public function getstates()
	{
		// pr('dd');die;
		$this->autoRender = false;
		$this->loadModel('State');
		$data = $this->State->find('list',['recursive'=>-1,'conditions'=>['State.country_id'=>$this->request->data['country_id']]]);
		// pr($data);die;
		echo json_encode($data);die;
	}
	public function getdestination()
	{
		// pr('dd');die;
		$this->autoRender = false;
		$this->loadModel('Destination');
		$data = $this->Destination->find('list',['recursive'=>-1,'conditions'=>['Destination.state_id'=>$this->request->data['state_id']]]);
		// pr($data);die;
		echo json_encode($data);die;
	}
}
