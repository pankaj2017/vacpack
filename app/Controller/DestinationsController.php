<?php
App::uses('AppController', 'Controller');
/**
 * Aagentgroups Controller
 *
 * @property Aagentgroup $Aagentgroup
 * @property PaginatorComponent $Paginator
 */
class DestinationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Flash');
	
	public function beforeFilter() {
		parent::beforeFilter();
		// For CakePHP 2.1 and up
		//$this->Auth->allow();
		// $this->Auth->authorize = array(
		// 	AuthComponent::ALL => array('actionPath' => 'controllers/', 'userModel' => 'Admin'),
		// 	'Actions',
		// 	'Controller'
		// );

		$this->set('masterclass','');
		$this->set('dashboardclass','');
		$this->set('aclclass','');
		$this->set('usersclass','');
		$this->set('groupsclass','active');
	}

	private function _otherdestinations($destinationid=null){
		
		if (!$this->Destination->exists($destinationid)) {
			throw new NotFoundException(__('Invalid Destination'));
		}

		$otherdestinations=$this->Destination->find('all',array($conditions=>array('Destination.id'<>$destinationid)));
		$this->set('otherdestinations',$otherdestinations);
		
	}
/**
 * index method
 *
 * @return void
 */
	public function index($stateid='',$admin=null) {
		//$this->Destination->recursive = 0;
		$conditions = array('Destination.active'=>1);
		if($stateid){
			$conditions = array_merge($conditions,array('Destination.state_id'=>$stateid));
		}
		$this->Destination->Behaviors->load('Containable');
		$this->Paginator->settings=array(
			'Destination' => array(
					'contain'=>array('State'=>array(
						'fields'=>array('State.id','State.name')
					)
				),
				'fields'=>array('Destination.id','Destination.name','Destination.slug','Destination.about','Destination.image','Destination.localfood','Destination.howtoreach','Destination.besttime','Destination.thingstodo'),
				'conditions'=>$conditions, //array(,'Destination.state_id'=>$stateid),
				'order'=>array('Destination.name'=>'asc'),
			)
		);
		$destinations = $this->Paginator->paginate();
		//pr($destinations);exit;
		$this->set('destinations', $destinations);
	}
	private function _ajaxCall() {
        if (isset($this->request->query['ajax'])) {
            if ($this->isajaxcallonly()) {
                $this->autoRender = false;
                $param = $this->request->query['ajax'];
                if($param == 'get-destinations-list') {
                    return $this->_getDestinationsList();
                }
                return;    
            }            
        }
    }
    private function _getDestinationsList() 
	{
		$data = $this->Destination->getListingData();
        echo json_encode($data);
        exit; 
	}
	public function admin_index($stateid='',$admin=true) {
		$this->_ajaxCall();	

		$this->set('opendestination','has-class');
		$this->set('activedestination','has-class');
		// $this->index($stateid,$admin);
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		/*if (!$this->Destination->exists($id)) {
			throw new NotFoundException(__('Invalid Destination'));
		}*/
		$options = array('conditions' => array('Destination.' . $this->Destination->primaryKey => $id));
		$destination = $this->Destination->find('first', $options);
		//pr($destination);
		$this->set('destination',$destination );
	}
	public function admin_view($id = null) {
		$this->set('opendestination','has-class');
		$this->set('activedestination','has-class');

		$this->view($id);
	}

/**
 * add method
 *
 * @return void
 */
	public function create() {
		if ($this->request->is(array('post','put'))) {
			// pr($this->request->data);exit;
			$this->Destination->create();
			if($this->request->data['Destination']['slug'] == '') {
				$slug = Inflector::slug ($this->request->data['Destination']['name'],'-');
				$slug = strtolower($slug);
				$this->request->data['Destination']['slug'] = $slug;
			} else {
				$slug = Inflector::slug ($this->request->data['Destination']['slug'],'-');
				$slug = strtolower($slug);
				$this->request->data['Destination']['slug'] = $slug;
			}
			if ($this->Destination->save($this->request->data)) {
				$this->Flash->success(__('The Destination has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The Destination could not be saved. Please, try again.'));
			}
		}
	}
public function admin_create() {

	$this->set('opendestination','has-class');
	$this->set('activedestination','has-class');

	$this->create();

	$this->loadModel('Country');
	$countries = $this->Country->find('list',['recursive'=>-1,'conditions'=>['Country.active'=>1]]);
	$this->set(compact('countries'));

	// pr($bverticals);die;
        // return $bverticals = $this->Bvertical->find('list',['recursive'=>-1,'conditions'=>['Bvertical.status'=>1]]);

}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Destination->exists($id)) {
			throw new NotFoundException(__('Invalid Destination'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if($this->request->data['Destination']['slug'] == '') {
				$slug = Inflector::slug ($this->request->data['Destination']['name'],'-');
				$slug = strtolower($slug);
				$this->request->data['Destination']['slug'] = $slug;
			} else {
				$slug = Inflector::slug ($this->request->data['Destination']['slug'],'-');
				$slug = strtolower($slug);
				$this->request->data['Destination']['slug'] = $slug;
			}
			if(empty($this->request->data['Destination']['image']['name'])){
				
				unset($this->request->data['Destination']['image']);
			}
			if ($this->Destination->save($this->request->data)) {
				$this->Flash->success(__('The Destination has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The Destination could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Destination.' . $this->Destination->primaryKey => $id));
			$this->request->data = $this->Destination->find('first', $options);
			$this->request->data['Destination']['country_id'] = $this->request->data['State']['country_id'];
			// pr($this->request->data); die;
			$this->loadModel('State');
			$states = $this->State->find('list',['recursive'=>-1,'conditions'=>[]]);
			$this->set(compact('states'));
			 // pr($states); die;
			$this->loadModel('Country');
			$countries = $this->Country->find('list',['recursive'=>-1,'conditions'=>['Country.active'=>1]]);
			$this->set(compact('countries'));
		}
	}

	public function admin_edit($id = null) {
		
		$this->set('opendestination','has-class');
		$this->set('activedestination','has-class');

		$this->edit($id);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Destination->id = $id;
		if (!$this->Destination->exists()) {
			throw new NotFoundException(__('Invalid Destination'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Destination->delete()) {
			$this->Flash->success(__('The Destination has been deleted.'));
		} else {
			$this->Flash->error(__('The Destination could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	public function getstates()
	{
		// pr('dd');die;
		$this->autoRender = false;
		$this->loadModel('State');
		$data = $this->State->find('list',['recursive'=>-1,'conditions'=>['State.country_id'=>$this->request->data['country_id']]]);
		// pr($data);die;
		echo json_encode($data);die;
	}
	public function placesList($slug1=null, $slug2=null)
	{
		$this->loadModel('State');
		$states_data = $this->State->find('first',['conditions' => ['State.slug' => $slug1]]);
		$this->set('states_data',$states_data);

		$cities_data = $this->Destination->find('first',['conditions' => ['Destination.name' => $slug2]]);
		$this->set('cities_data',$cities_data);

		$this->loadModel('Package');
		$package_list = $this->Package->find('all',['conditions' => ['Package.state_id'=>$states_data['State']['id']],'recursive' =>1]);
		
		$this->set('package_data',$package_list);
		// pr($package_list);die;
		// pr($slug2);die;
	}
}
