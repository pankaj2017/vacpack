<?php
App::uses('AppController', 'Controller');
/**
 * Aagentgroups Controller
 *
 * @property Aagentgroup $Aagentgroup
 * @property PaginatorComponent $Paginator
 */
class HolidaythemesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public function beforeFilter() {
		parent::beforeFilter();
		// For CakePHP 2.1 and up
		//$this->Auth->allow();
		// $this->Auth->authorize = array(
		// 	AuthComponent::ALL => array('actionPath' => 'controllers/', 'userModel' => 'Admin'),
		// 	'Actions',
		// 	'Controller'
		// );

		$this->set('masterclass','');
		$this->set('dashboardclass','');
		$this->set('aclclass','');
		$this->set('usersclass','');
		$this->set('groupsclass','active');
	}

/**
 * index method
 *
 * @return void
 */
	public function index($slug=null) {
		// pr('dd');die;
		$this->Holidaytheme->recursive = 0;
		$this->set('holidaythemes', $this->Paginator->paginate());
	}
	private function _ajaxCall() {
	
		if (isset($this->request->query['ajax'])) {
		// pr($this->request->query);die;
	        if ($this->isajaxcallonly()) {
	            $this->autoRender = false;
	            $param = $this->request->query['ajax'];
	            if($param == 'get-holidaythemes-list') {
	                return $this->_getHolidayThemeList();
	            }
	            return;    
	        }            
	    }
	}
	private function _getHolidayThemeList() 
	{
		$data = $this->Holidaytheme->getListingData();
	    echo json_encode($data);
	    exit; 
	}
	public function admin_index() {
		$this->_ajaxCall();	
		$this->set('openuser','has-class');
		$this->set('activeholidaytheme','has-class');
		// $this->index();
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Holidaytheme->exists($id)) {
			throw new NotFoundException(__('Invalid Holidaytheme'));
		}
		$options = array('conditions' => array('Holidaytheme.' . $this->Holidaytheme->primaryKey => $id));
		$this->set('holidaytheme', $this->Holidaytheme->find('first', $options));
	}
	public function admin_view($id = null) {
		
		$this->view($id);
	}

/**
 * add method
 *
 * @return void
 */
	public function create() {
		if ($this->request->is('post')) {
			$this->Holidaytheme->create();
			if ($this->Holidaytheme->save($this->request->data)) {
				$this->Flash->success(__('The Holidaytheme has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The Holidaytheme could not be saved. Please, try again.'));
			}
		}
	}
public function admin_create() {
	$this->create();
}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Holidaytheme->exists($id)) {
			throw new NotFoundException(__('Invalid Holidaytheme'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if($this->request->data['Holidaytheme']['holidayimage']['name'] == '') {
				$this->request->data['Holidaytheme']['holidayimage'] = null;
				unset($this->request->data['Holidaytheme']['holidayimage']);
			}
			if ($this->Holidaytheme->save($this->request->data)) {
				$this->Flash->success(__('The Holidaytheme has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The Holidaytheme could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Holidaytheme.' . $this->Holidaytheme->primaryKey => $id));
			$this->request->data = $this->Holidaytheme->find('first', $options);
		}
	}

	public function admin_edit($id = null) {
		$this->edit($id);
	}
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Holidaytheme->id = $id;
		if (!$this->Holidaytheme->exists()) {
			throw new NotFoundException(__('Invalid Holidaytheme'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Holidaytheme->delete()) {
			$this->Flash->success(__('The Holidaytheme has been deleted.'));
		} else {
			$this->Flash->error(__('The Holidaytheme could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
