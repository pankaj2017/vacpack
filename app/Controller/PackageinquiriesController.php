<?php
App::uses('AppController', 'Controller');

/**
 * Profiles Controller
 *
 * @property Profile $Profile
 * @property PaginatorComponent $Paginator
 */
class PackageinquiriesController extends AppController {
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public $helpers = array('Paginator');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public function create()
	{
		$layout = 'ajax';
        $this->autoRender = false;
      
      	$this->loadModel('Packageinquiry');
      	if ($this->Packageinquiry->save($this->request->data)) {

        	$result = ['success' => true, 'message' => "Inquiry has been submited"];
            echo json_encode($result);
            exit;
        }else{
        	// pr($this->Agencyinquiry->validationErrors);die;
			$this->set('errors',$this->Packaginquiry->validationErrors);die;
        }
		
	}
}
