<?php
App::uses('AppController', 'Controller');


class StatesController extends AppController {

	private function _details($id = null)
	{
		$fields=['State.id','State.name','State.slug','State.imagename','State.description','Country.name','State.country_id','Country.active'];
		$joins=[
		['table' => 'countries','alias' => 'Country','type' => 'left','conditions' => ['Country.id = State.country_id']]];
		$options =['joins'=>$joins,'fields'=>$fields,'recursive'=>-1,'conditions' =>['State.'.$this->State->primaryKey => $id]];
		$data = $this->State->find('first', $options);

		return $data ;
	}

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Flash');
	
	public function beforeFilter() {
		parent::beforeFilter();
		// For CakePHP 2.1 and up
		//$this->Auth->allow();
		// $this->Auth->authorize = array(
		// 	AuthComponent::ALL => array('actionPath' => 'controllers/', 'userModel' => 'Admin'),
		// 	'Actions',
		// 	'Controller'
		// );

		$this->set('masterclass','');
		$this->set('dashboardclass','');
		$this->set('aclclass','');
		$this->set('usersclass','');
		$this->set('groupsclass','active');
	}

	public function index($countryid=104,$admin=false) {

		$this->set('opendestination','has-class');
		$this->set('activestate','has-class');

		$this->State->recursive = 0;
		$this->Paginator->settings = array(
			'State' => array(
				'fields'=>array('State.id','State.name','State.image','State.slug','State.description'),
				'conditions'=>array('State.country_id'=>$countryid),
				'order'=>array('State.name'=>'asc'),
			)
		);
		$states = $this->Paginator->paginate();
		//pr($states);exit;
		$this->set('states', $states);
	}
	private function _ajaxCall() {
        if (isset($this->request->query['ajax'])) {
            if ($this->isajaxcallonly()) {
                $this->autoRender = false;
                $param = $this->request->query['ajax'];
                if($param == 'get-states-list') {
                    return $this->_getStatesList();
                }
                return;    
            }            
        }
    }
    private function _getStatesList() 
	{
		$data = $this->State->getListingData();
        echo json_encode($data);
        exit; 
	}
	public function admin_index($admin=true) {
		$this->_ajaxCall();	

		$this->set('opendestination','has-class');
		$this->set('activestate','has-class');

		// $this->State->recursive = 0;
		// $this->Paginator->settings = array(
		// 	'State' => array(
		// 		'order'=>array('State.name'=>'asc'),
		// 	)
		// );
		// $states = $this->Paginator->paginate();
		// $this->set('states', $states);
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	
	public function admin_view($id = null) {
		
		$this->set('opendestination','has-class');
		$this->set('activestate','has-class');
		
		$this->view($id);
	}

	public function admin_create() {
		$this->set('opendestination','has-class');
		$this->set('activestate','has-class');

		if (!empty($this->request->data['State'])) 
		{	
			$this->State->create();
			if($this->request->data['State']['slug'] == '') {
				$slug = Inflector::slug ($this->request->data['State']['name'],'-');
				$slug = strtolower($slug);
				$this->request->data['State']['slug'] = $slug;
			} else {
				$slug = Inflector::slug ($this->request->data['State']['slug'],'-');
				$slug = strtolower($slug);
				$this->request->data['State']['slug'] = $slug;
			}
			if($this->State->save($this->request->data)) 
			{
				$this->Session->setFlash(__('State created successfully.'), 'default',['class' => 'alert alert-success']);
				$this->redirect(['controller'=>'states', 'action'=>'index', 'admin'=>true]);
			}
			else 
			{
				$this->set('errors',$this->State->validationErrors);
			}
		}
		$this->loadModel('Country');
		$countrylists = $this->Country->find('list',['recursive'=>-1,'conditions'=>['Country.active'=>1]]);
		$this->set(compact('countrylists'));
		
	}

	public function admin_edit($id = null) {
		
		$this->set('opendestination','has-class');
		$this->set('activestate','has-class');
		
		if (!$this->State->exists($id)) 
		{
			throw new NotFoundException(__('Invalid State'));
		}
		if ($this->request->is(['post', 'put'])) 
		{
			if($this->request->data['State']['slug'] == '') {
				$slug = Inflector::slug ($this->request->data['State']['name'],'-');
				$slug = strtolower($slug);
				$this->request->data['State']['slug'] = $slug;
			} else {
				$slug = Inflector::slug ($this->request->data['State']['slug'],'-');
				$slug = strtolower($slug);
				$this->request->data['State']['slug'] = $slug;
			}
			if(empty($this->request->data['State']['image']['name']))
			{
				unset($this->request->data['State']['image']);
			}

			if ($this->State->save($this->request->data)) 
			{
				$this->Session->setFlash(__('State has been saved.'), 'default',['class' => 'alert alert-success']);
				return $this->redirect(['action' => 'index']);
			} 
			else 
			{
				$this->set('errors',$this->State->validationErrors);
			}
		} 
		else 
		{
			$this->request->data = $this->_details($id);
		}
		$this->loadModel('Country');
		$countrylists = $this->Country->find('list',['recursive'=>-1,'conditions'=>['Country.active'=>1]]);
		$this->set(compact('countrylists'));
	}
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Destination->id = $id;
		if (!$this->Destination->exists()) {
			throw new NotFoundException(__('Invalid Destination'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Destination->delete()) {
			$this->Flash->success(__('The Destination has been deleted.'));
		} else {
			$this->Flash->error(__('The Destination could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function view($slug = null)
	{
		$this->State->Behaviors->load('Containable');
		$states_data = $this->State->find('first',
			[
				'contain'=>[
					'Country',
					'Destination'=>[
						'fields'=>['Destination.id','Destination.name','Destination.slug','Destination.image','Destination.imagename','Destination.about'],
						'conditions'=>['Destination.active'=>1]

						],
					'Package' =>[
						'fields'=>['Package.id','Package.title','Package.description','Package.pkgimage','Package.numberofnights','Package.numberofdays','Package.travelagency_id','Package.discounttype','Package.price','Package.discount'],
						'conditions'=>['Package.active'=>1]
						]
					],
				'fields'=>['Country.id','Country.name','State.id','State.name','State.description','State.slug','State.image','State.description','Country.id','Country.name',],
				'conditions' => ['State.slug' => $slug]
			]
		);

		//pr($states_data);die;
		$this->set('states_data',$states_data);
	}
}
