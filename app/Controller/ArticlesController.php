<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Posts Controller
 *
 * @property Post $Post
 * @property PaginatorComponent $Paginator
 */
class ArticlesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('RequestHandler','Paginator',
		'Captcha.Captcha',
		'Feedback.Ratings' => array('on' => array('admin_view', 'view','index')),
		'MobileDetect.MobileDetect','Cookie'
	);
	public $helpers = array('Js' => array('Jquery'),'Paginator','Tree',
		'Tags.TagCloud',
		'SocialShare',
		'Captcha.Captcha',
		'Feedback.Ratings'
	);
	public $paginate = array('order'=>array('Article.created'=>'desc'));
	public $uses=array('Article','Tofriend','Fulltextpost');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
		$this->set('masterclass','');
	}
	
	public function beforeRender(){
		//$this->_persistValidation();
	}
	private function _createcategorytree($catid){
        $this->loadModel('Articlecategory');
        $category_tree = $this->Articlecategory->find('threaded', ['conditions' => ['Articlecategory.active' => 1]]);
        $this->set('categories', $category_tree);

        $category_list = $this->Articlecategory->find('all', ["recursive" => 1, "conditions" => ['Articlecategory.parent_id' => '']]);

        $this->set('categorylist', $category_list);
         $check = [];
        foreach ($category_list as $key => $value) {

            $check[$key] = array_column($value['ChildArticlecategory'], 'id');
        }
       
        $level_active = $this->_getKey($check, $catid);
        $level_active = $level_active + 1;

        $menuTreeCheck = $this->_treeFormCheck($category_tree, 0, 'Articlecategory'); //
        $this->set('menutreecheck', $menuTreeCheck);
        $this->set('level_active', $level_active);
    }
	private function _treeFormCheck(array &$elements, $parentId = 0, $elkey = null) {
	        $branch = array();

	        foreach ($elements as $key => $element) {
	            if ($element[$elkey]['parent_id'] == $parentId) {
	                $children = $this->_treeFormCheck($elements, $element[$elkey]['id'], $elkey);
	                if ($children) {
	                    $element['children'] = $children;
	                }
	                $branch[$element[$elkey]['id']] = $element;
	                unset($elements[$key]);
	            }
	        }
	        return $branch;
	    }
    // private function _getArticlesList() {
    //     $data = $this->Article->getListingData();
    //     echo json_encode($data);
    //     exit;
    // }
    private function _getKey($check, $id) {
        foreach ($check as $key => $val) {
            if (in_array($id, $val)) {
                return $key;
            }
        }
        return "not found";
    } 
	function _getRealIpAddr(){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])){
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	private function _confurl() { //this function is used in sendtofriend method
		$pageURL = 'http';
		if (isset($_SERVER["HTTPS"]) == "on") {$pageURL .= "s";}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
	
	public function sendtofriend(){		
			//pr($this->request->data);
			$this->autoRender = false; 
			$this->Tofriend->setCaptcha('captcha', $this->Captcha->getCode('Tofriend.captcha'));
			$this->Tofriend->set($this->request->data);
			
			if($this->Tofriend->validates()){
				if($this->Tofriend->save($this->request->data)){
					
					$domain = $_SERVER['SERVER_NAME'];				
					$adminmail = 'contact@chirayu.im';//$this->Sitesetting->field('value',array('Sitesetting.key'=>'ContactEmail'));
					$email = new CakeEmail('smtp');
					$email->to ($this->request->data['Tofriend']['recemail']);
					$email->subject('Interesting Post For You');
					$email->from ($this->request->data['Tofriend']['senderemail']);
					$email->emailFormat('html');				
					$email->template('default','interestingpost')->viewVars(
									array(
										'username' => $this->request->data['Tofriend']['receiver'],
										'sender' => $this->request->data['Tofriend']['senderemail'],
										'postlink' => $this->request->data['Tofriend']['link'],
										'message' => $this->request->data['Tofriend']['message']
										)
									);
					if($email->send()){
						$this->Tofriend->saveField('mailsent', 1);
						//$this->_send_custom_notification($email);
					}else{
						$this->Session->setFlash('Sorry! We are not able to forward your request this time.');
						$this->redirect(array('controller'=>'contacts','action'=>'index'));exit;
					}
					if($this->RequestHandler->isAjax()){
						$this->render('successtofriend','ajax');
					}
				}
			}else{
                //$this->Session->setFlash('Data Validation Failure', 'default', array('class' => 'cake-error'));
                //pr($this->Tofriend->validationErrors['captcha']);
            }
	}
	private function _send_custom_notification($objemail){
		$notification_team = $this->custom_notification_team;		
		$objemail->subject('Contact Message Received');
		$objemail->emailFormat('html');
		$objemail->template('default','contactmessagenotification')->viewVars(
							array(
								'username' => 'Site Admin',
								'message'=> $this->request->data['Contact']['message']
								)
							);
		foreach($notification_team as $notification){
			$objemail->to ($notification);
			$objemail->sender($this->request->data['Contact']['email_address'], $this->request->data['Contact']['firstname']);
			$objemail->from (array($this->request->data['Contact']['email_address']=> $this->request->data['Contact']['firstname']));
			$objemail->send();
		}
	}
	function validateform(){
		if($this->RequestHandler->isAjax()){//pr($this->params['data']['field']);exit;
			$this->request->data['Tofriend'][$this->params['data']['field']] = $this->params['data']['value'];
			$this->Tofriend->set($this->request->data);
			if($this->Tofriend->validates()){
				$this->autoRender = false;
			}else{
				$error = $this->Tofriend->validationErrors;
				//pr($error);exit;
				$this->set('error',(!empty($error[$this->params['data']['field']])?$error[$this->params['data']['field']]:''));
			}
		}
	
	}
	function captcha()	{
        $this->autoRender = false;
        $this->layout='ajax';
        if(!isset($this->Captcha))	{ //if you didn't load in the header
            $this->Captcha = $this->Components->load('Captcha'); //load it
        }
        $this->Captcha->create();
    }
/**
 * fullsearch method
 *
 * @return void
 */	
	public function searchfull($phrase=null) {
		if (!empty($this->request->data['Post']['fullsearch'])) {
			//pr($this->request->data['Post']['fullsearch']);exit;
			$phrase =  $this->request->data['Article']['fullsearch'];
			$postcondition =array("MATCH (Fulltextpost.title) AGAINST('{$phrase}' IN BOOLEAN MODE)","Fulltextpost.active=1");
			$this->Paginator->settings = array(
				"Fulltextpost"=>array(
					'fields'=>array('Fulltextpost.id','Fulltextpost.title','Fulltextpost.body'),
					'conditions' => array($postcondition),
					'limit' => 10,
					'order'=>array('Fulltextpost.modified'=> 'desc'),
				)
			);
			$posts = $this->Paginator->paginate('Fulltextpost');
			$this->set(array('posts'=>$posts,'_serialize'=>array('posts')));
			//pr($posts);exit;
		}else{
			$this->redirect($this->referer());
		}
	}
/**
 * index method
 *
 * @return void
 */
	public function index($slug=null,$id=null) {
		// pr($slug);die;
		// $postcondition = array();
		// $this->Article->unbindModel(array('hasMany'=>array('Comment','Articleview')));
		// $this->Article->Behaviors->load('Containable');
		// $this->Paginator->settings = array(
		// 	'Article'=>array(
		// 		'contain'=>array(
		// 			'Tag'=>array('id','name','keyname'),
		// 			'Admin'=>array('id','firstname'),
		// 			'Articlecoverimage'=>array('namesmall','namemedium'),
		// 			'Rating'=>array()
		// 		),
		// 		'conditions'=>array($postcondition),
		// 		'limit'=>10,
		// 		'order'=>array('Article.created' =>'desc')
		// 	)
		// );
		// $posts = $this->Paginator->paginate('Article');
		// //pr($posts);exit;
		// //$this->set(array('posts'=>$posts,'_serialize'=>array('posts')));
		// $this->set('posts',$posts);
		// /*fetch all nested categories list*/
		// App::import('Model','Articlecategory');
		// $postcategory = new Articlecategory();
		// $postcategory->recursive = 1;
		// $data = $postcategory->find('threaded');// Extra parameters added
		// $this->set('postcategories', $data);//it is handled in index.ctp
		// $this->set('tags',$this->Article->Tagged->find('cloud',array('limit'=>20)));
		/*Tag Cloud Ends*/
		//Get all article with categories.
		//
		//$this->loadModel('Articlecategory');
		//$category_list= $this->Articlecategory->find('all',["recursive"=> 1,"conditions" => ['Articlecategory.parent_id' => '']]);
		// pr($category_list);die;
			//$this->set('categorylist',$category_list);


		// $this->loadModel('Articlecategory');
		//$conditions = array('Articlecategory.active'=>1);
		// $cat = $this->Articlecategoryrticlecategory->find('all',array('conditions'=>$conditions));
		 //$this->Articlecategory->recursive = 2;		
		//$cat = $this->Articlecategory->find('all',array('conditions'=>$conditions,'order'=>['Articlecategory.modified'=>'ASC'],'contain' => ['Article'=>['order'=>['Article.modified'=>'DESC'],'limit'=>10],'ParentCategory','ChildCategory']));
		
		//$this->set('cat',$cat);

		// pr($d);die;
		$this->_createcategorytree($id);
        $this->set('id', $id);        
		$articlelist = $this->Article->find('all',['conditions'=>['Article.articlecategory_id' => $id, 'Article.active' => 1],'order' => array('Article.modified'=>'ASC')]);
        
  //       $this->paginate = array(
  //           'conditions' => array('Article.articlecategory_id' => $id, 'Article.status' => 1),
  //           'order' => array('Article.modified'=>'ASC'),
  //           'limit' => 5
  //       );                 
  //       $articlelist = $this->paginate('Article');   
  //       pr($articlelist);die;             
        $this->set('articlelist',$articlelist);

	}
	
	public function filter($category=null,$postcategoryid=null,$page=null) {
	
		if (empty($postcategoryid)) {
			throw new PostcategoryNotFoundException(__('Invalid post category'));
		}
		$postcondition = array();
		App::import('Controller','Articlecategories');
		$postcate = new ArticlecategoriesController();
		$postcats = $postcate->_getchildcategories($postcategoryid);
		//pr($postcats);exit;		
		if(!empty($postcats)){
			if(count($postcats)>1)
			$postconditions = array("Article.articlecategory_id IN "=>$postcats);
			else
			$postconditions = array("Article.articlecategory_id"=>$postcats);
		}
		$this->Article->unbindModel(array('hasMany'=>array('Comment','Articleview')));
		$this->Article->Behaviors->load('Containable');
		$this->Paginator->settings = array(
			'Article'=>array(
					'contain'=>array(
						'Tag'=>array('id','name','keyname'),
						'Admin'=>array('id','firstname'),
						'Articlecoverimage'=>array('namesmall','namemedium'),
						'Rating'=>array()
					),
			'conditions' => array($postconditions),
			'limit' => 10,
			'order'=>array('Article.created'=> 'desc'),
			)
		);
		$posts = $this->Paginator->paginate();
		//pr($posts);exit;
		$this->set('posts', $posts);
		
		/*fetch all nested categories list*/
		App::import('Model','Articlecategory');
		$postcategory = new Articlecategory();
		$postcategory->recursive = 1;
		$data = $postcategory->find('threaded');// Extra parameters added
		$this->set('postcategories', $data);//it is handled in index.ctp
		$this->set('tags',$this->Article->Tagged->find('cloud',array('limit'=>20)));
		
		/*Tag Cloud Ends*/
	}
	
	public function search($tag=null,$tagid=null){	
		//$posts = $this->Post->Tagged->find('tagged', array('by' => $tag, 'model' => 'Post','recursive'=>'1','fields'=>array('Tagged.foreign_key','Tag.name','Tag.keyname')));	
		
		$this->Paginator->settings['Tagged'] = array(
			'tagged',
			'model' =>'Article',			
			'fields'=>array('Admin.id','Admin.email_address','Admin.firstname','Tagged.foreign_key','Tag.name','Tag.id','Tag.keyname'),
			'by' => $tag,'recursive'=>'1',
			'joins'=>array(array('table' => 'users',
                                        'alias' => 'Admin',
                                        'type' => 'INNER',
                                        'conditions' => array('Admin.id = Article.admin_id'))),
		);
		$posts = $this->Paginator->paginate('Tagged');
		//pr($posts);exit;
		$this->set('posts', $posts);
	
		/*fetch all nested categories list*/
		App::import('Model','Articlecategory');
		$postcategory = new Articlecategory();
		$postcategory->recursive = 1;
		$data = $postcategory->find('threaded');// Extra parameters added
		$this->set('postcategories', $data);//it is handled in index.ctp
		/*Tag Cloud Starts*/
		$this->set('tags', $this->Article->Tagged->find('cloud', array('limit' => 20)));
		/*Tag Cloud Ends*/
	}
	private function _ajaxCall() {
        if (isset($this->request->query['ajax'])) {
		// pr($this->request->query);die;
            if ($this->isajaxcallonly()) {
                $this->autoRender = false;
                $param = $this->request->query['ajax'];
                if($param == 'get-articles-list') {
                    return $this->_getArticlesList();
                }
                return;    
            }            
        }
    }
    private function _getArticlesList() 
	{
		$data = $this->Article->getListingData();
        echo json_encode($data);
        exit; 
	}
	public function admin_index($postcategoryid=null) {
		$this->_ajaxCall();	

		// $postcondition = array();
		// $this->Article->unbindModel(array('hasMany'=>array('Comment','Articleview')));
		// $this->Article->Behaviors->load('Containable');
		// $this->Paginator->settings = array(
		// 	'Article'=>array(
		// 		'contain'=>array(
		// 			'Tag'=>array('id','name','keyname'),
		// 			'Admin'=>array('id','firstname'),
		// 			'Articlecoverimage'=>array('namesmall','namemedium'),
		// 			'Rating'=>array()
		// 		),
		// 		'conditions'=>array($postcondition),
		// 		'limit'=>10,
		// 		'order'=>array('Article.created' =>'desc')
		// 	)
		// );
		// $posts = $this->Paginator->paginate('Article');
		// $this->set('posts',$posts);

		// $this->index($postcategoryid);
		// $this->set('','');
		$this->set('openarticle','has-class');
		$this->set('activearticle','has-class');
	}
	
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($slug=null,$id = null) {
		// if(empty($id))
		// {
			
		// 	$this->Session->setFlash('Invalid Article id.', 'default', array('class' => 'login-error-message'));
		// 	$this->redirect(array('controller' => 'articlecategories','action'=>'index'));
		// 	exit;
		// } else {
		// 	$this->loadModel('Articlecategory');
		// 	$articlecategory = $this->Articlecategory->find('first',array('conditions'=>array('Articlecategory.id'=>$id,'Articlecategory.active'=>1)));
		// 	if(isset($articlecategory)  && !empty($articlecategory))
		// 	{

		// 	$this->set('cat',$articlecategory);

		// 	$this->loadModel('Article');
		// 	$category_tree = $this->Articlecategory->find('threaded',['conditions'=>['Articlecategory.active'=>1]]);
		// 	$this->set('categories', $category_tree);
			
		// 	$category_list= $this->Articlecategory->find('all',["recursive"=> 1,"conditions" => ['Articlecategory.parent_id' => '']]);
		// 	$this->set('categorylist',$category_list);
		// 	$this->set('id',$id);
			
		// 	$check = [];
		// 	foreach ($category_list as $key => $value) {

		// 		$check[$key] = array_column($value['ChildCategory'],'id');
		// 	}
		// 	function getKey($check, $id) {
		// 	    foreach($check as $key => $val){
		// 	        if(in_array($id, $val)){
		// 	            return $key;
		// 	        }
		// 	    }
		// 	    return "not found";
		// 	}

		// 	$level_active = getKey($check, $id);
		// 	$level_active = $level_active +1;

		// 	$this->set('level_active',$level_active);



		// 	$conditions=['articlecategory_id'=>$category_list[0]['Articlecategory']['id']];
		// 	$articlelist = $this->Article->find('all',["conditions"=>$conditions]);
	
		// 	 function treeFormCheck(array &$elements, $parentId = 0, $elkey = null) {
	 //            $branch = array();

	 //            foreach ($elements as $key=>$element) {
	 //                if ($element[$elkey]['parent_id'] == $parentId) {
	 //                    $children = treeFormCheck($elements, $element[$elkey]['id'], $elkey);
	 //                    if ($children) {
	 //                        $element['children'] = $children;
	 //                    }
	 //                    $branch[$element[$elkey]['id']] = $element;
	 //                    unset($elements[$key]);
	 //                }
	 //            }
	 //            return $branch;
	 //        }   
	 //        $menuTreeCheck = treeFormCheck($category_tree, 0, 'Articlecategory'); //
	    
	 //        $this->set('menutreecheck', $menuTreeCheck);


		// 	}else{
				
		// 		$this->Session->setFlash(__('Invalid Article id.'), 'default',['class' => 'alert alert-success']);
		// 		$this->redirect(array('controller' => 'articlecategories','action'=>'index'));
		// 		exit;
		// 	}
		// }
		if (!$this->Article->exists($id)) {
            throw new NotFoundException(__('Invalid Article'));
        }

        $article = $this->Article->find('first', array('conditions' => array('Article.id' => $id, 'Article.active' => 1)));
        // pr($article);die;
        if (isset($article) && !empty($article)) {
            $rest_article = $this->Article->find('all', array('fields' => array('Article.title', 'Article.slug', 'Article.id'), 'recursive' => 0, 'order' => array('Article.title' => 'ASC'),
                'conditions' => array('Article.id !=' => $id, 'Article.articlecategory_id' => $article['Article']['articlecategory_id'], 'Article.active' => 1)));
            $this->set('article', $article);
            $this->set('rest_article', $rest_article);
            $this->set('meta_title', $article['Article']['meta_title']);
            $this->set('meta_desc', $article['Article']['meta_desc']);
        } else {
            throw new NotFoundException(__('Invalid Article'));
        } 
	}
	
	public function admin_view($id = null){
		$fields = array("Article.id","Article.articlecategory_id","Article.admin_id","Article.title","Article.body","Article.modified","Admin.firstname","Admin.id","Comment.id");
		$options = array('conditions' => array('Article.' . $this->Article->primaryKey => $id));
		$post = $this->Article->find('first', $options);
	//	pr($post);exit;
		$this->set('post', $post);
		$this->set('article_id',$id);
		
		App::import('Model','Articlecategory');
		$postcategory = new Articlecategory();
		$treePath = $postcategory->getPath($post['Article']['articlecategory_id']);
		$this->set(compact('treePath'));
		
		/*this generate list of comment in the tree structure but need to set UI for this */
		$commentconditions = array("Comment.article_id"=>$post['Article']['id']);
		$comments = $this->Article->Comment->find('threaded',array('conditions' => $commentconditions));
		//pr($comments);exit;		
		$this->set(compact('comments'));
		
		$this->set('masterclass','');
	}

/**
 * add method
 *
 * @return void
 */
	public function create($admin=null) {
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Article']['admin_id'] = $this->Session->read("Auth.Admin.id");
			$this->Article->Articlecoverimage->set($this->request->data);
			if($this->Article->validates() && $this->Article->Articlecoverimage->validates()){
				$this->Article->create();	
				if($this->request->data['Article']['slug'] == '') {
					$slug = Inflector::slug ($this->request->data['Article']['title'],'-');
					$slug = strtolower($slug);
					$this->request->data['Article']['slug'] = $slug;
				} else {
					$slug = Inflector::slug ($this->request->data['Article']['slug'],'-');
					$slug = strtolower($slug);
					$this->request->data['Article']['slug'] = $slug;
				}			
				if ($this->Article->save($this->request->data)) {
					$post_id = $this->Article->getLastInsertId();
					$this->request->data['Articlecoverimage']['article_id'] = $post_id;
					if($this->Article->Articlecoverimage->save($this->request->data['Articlecoverimage'])){
						$this->Session->setFlash(__('The post has been saved.'));
						return $this->redirect(array('controller'=>'articles','action'=>'index'));						
					}else{
						$this->Session->setFlash(__('Featured image for post is not uploaded'));
						return $this->redirect(array('controller'=>'articles','action'=>'index'));
					}
					
				} else {
					$this->Session->setFlash(__('The post could not be saved. Please, try again'));
					return $this->redirect(array('controller'=>'articles','action'=>'index'));
				}
			}
		}
		/*fetch all nested categories list*/
		App::import('Model','Articlecategory');
		$postcategory = new Articlecategory();
		$postcategory->recursive = 1;
		$data = $postcategory->find('threaded');// Extra parameters added
		$this->set('postcategories', $data);//it is handled in index.ctp
		if($this->Session->check("Auth.Admin")){
			$user_id = $this->Session->read("Auth.Admin.id");
			$this->set('admin_id',$user_id);
		}else{
			$this->Session->setFlash(__('User identity is not recognised'));
			return $this->redirect($this->Auth->logoutRedirect);
		}
	}

	public function admin_create(){
		$this->set('openarticle','has-class');
		$this->set('activearticle','has-class'); 
		$this->create($admin=true);
		// App::import('Model','Articlecategory');
		// $treecategory = new Articlecategory();
		
		// $spacer="--";
		// $parent = $treecategory->generateTreeList('', '', '', $spacer);
		// $this->set(compact('parent'));

		$conditions=['active'=>'1'];
		$this->loadModel('Articlecategory');
		$spacer="-";
		$parent = $this->Articlecategory->generateTreeList($conditions, '', '', $spacer);
		$parent_disabled = [];
		foreach($parent as $key => $value) {
			if(!strstr($value,$spacer)) {
				$parent_disabled[] = $key;
			}
		}
		
		$this->set(compact('parent_disabled'));
		$this->set(compact('parent'));
		
	}
	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Article->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			//pr($this->request->data);exit;
			if($this->request->data['Article']['slug'] == '') {
				$slug = Inflector::slug ($this->request->data['Article']['title'],'-');
				$slug = strtolower($slug);
				$this->request->data['Article']['slug'] = $slug;
			} else {
				$slug = Inflector::slug ($this->request->data['Article']['slug'],'-');
				$slug = strtolower($slug);
				$this->request->data['Article']['slug'] = $slug;
			}
			if ($this->Article->save($this->request->data)) {
				if(!empty($this->request->data['Articlecoverimage']['image']['name'])){
					$this->Article->Articlecoverimage->delete($this->request->data['Articlecoverimage']['id']);
					$this->request->data['Articlecoverimage']['article_id'] = $id;
					//$this->Article->Articlecoverimage->set($this->request->data['Articlecoverimage']);
					$this->Article->Articlecoverimage->save($this->request->data['Articlecoverimage']);
				}
				// $this->Session->setFlash(__('The post has been saved.'));
				// $this->redirect($this->referer());
				$this->Session->setFlash(__('The post has been updated.', 'default', ['class' => 'alert alert-success']));
				return $this->redirect(array('controller'=>'articles','action'=>'index'));
			} else {
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		} else {
			
		}
			$options = array('conditions' => array('Article.' . $this->Article->primaryKey => $id));
			$this->request->data = $this->Article->find('first', $options);
			$this->set('featured_image',$this->request->data['Articlecoverimage']['namesmall']);
		$users = $this->Article->Admin->find('list');
		$this->set(compact('users'));
	}
	
	public function admin_edit($id = null){
		$this->set('openarticle','has-class');
		$this->set('activearticle','has-class');
		$this->edit($id);
		// App::import('Model','Articlecategory');
		// $postcategory = new Articlecategory();
		// $spacer="--";
		// $parent = $postcategory->generateTreeList('', '', '', $spacer);
		// $this->set(compact('parent'));

		$conditions=['active'=>'1'];
		$this->loadModel('Articlecategory');
		$spacer="-";
		$parent = $this->Articlecategory->generateTreeList($conditions, '', '', $spacer);
		$parent_disabled = [];
		foreach($parent as $key => $value) {
			if(!strstr($value,$spacer)) {
				$parent_disabled[] = $key;
			}
		}
		
		$this->set(compact('parent_disabled'));
		$this->set(compact('parent'));
}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Article->id = $id;
		if (!$this->Article->exists()) {
			throw new NotFoundException(__('Invalid post'));
		}
		//$this->request->allowMethod('post', 'admin_delete');
		if ($this->Article->delete()) {
			$this->Session->setFlash(__('Article has been deleted.'), 'default', ['class' => 'alert alert-success']);
		} else {
			$this->Session->setFlash(__('The post could not be deleted. Please, try again.'));
		}
		
		return $this->redirect(array('action' => 'index'));
	}
	public function detail($art ='')
	{
		$article = $this->Article->find('first',array('conditions'=>array('Article.slug'=>$art,'Article.active'=>1)));
		
		if(isset($article)  && !empty($article))
		{
			$rest_article = $this->Article->find('all',array('fields'=>array('Article.title','Article.slug'),'order'=>array('Article.title'=>'ASC'),
				'conditions'=>array('Article.slug !='=>$art, 'Article.articlecategory_id'=>$article['Article']['articlecategory_id'],'Article.active'=>1)));
		// pr($article); die;
			$this->set('articlecategories',$article);
			$this->set('rest_article',$rest_article);

		}
	}
}