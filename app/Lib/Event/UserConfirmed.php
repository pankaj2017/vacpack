<?php //this is listner
App::uses('CakeEventListener', 'Event');
App::uses('CakeEmail', 'Network/Email');
class UserConfirmed implements CakeEventListener {

    public function implementedEvents() {
        return array(
            'Model.User.userRegistered' => 'sendConfirmationEmail',
			//'Model.Seller.sellerRegistered' => 'sendConfirmationEmail',
			'Controller.User.makeActive' => 'activeuser',
			//'Controller.Seller.makeActive' => 'activeseller',
			'Controller.User.makeProfile' => 'createprofile',
			//'Controller.Seller.makeProfile' => 'createsellerprofile'
        );
    }

    public function sendConfirmationEmail(CakeEvent $Event) {

		$Email = new CakeEmail('smtp');
		//pr($Event->data);exit;
		$Email->from(array('no-reply@Vacdoo.com' => 'no reply'));
		$Email->emailFormat('html');
		$Email->to($Event->data['user']['email']);
		$Email->subject('Your registration information is received');
		$Email->template('userconfirmation','layoutwc');
		$Email->viewVars(array(
			'data' => $Event->data['user'],
			'link' => $Event->data['link']
		));
		if($Email->send()){
			return true;
		}else{
			throw new EmailConfirmationNotSent(__('Email varification not sent to provided email'));
			return false;
		}

	}
	
	public function activeuser(CakeEvent $Event) {
		App::import('Model','User');
		$user = new User();
		$user->id = $Event->data['user']['id'];
		$user->saveField('active','1');
	}
	
	public function createprofile(CakeEvent $Event) {
		App::import('Model','Profile');
		$profile = new Profile();
		$profile->saveField('user_id',$Event->data['user']['id']);
	}
	
	public function activeseller(CakeEvent $Event) {
		App::import('Model','Seller');
		$seller = new Seller();
		$seller->id = $Event->data['user']['id'];
		$seller->saveField('active','1');
	}
	
	public function createsellerprofile(CakeEvent $Event) {
		App::import('Model','Sellerprofile');
		$sellerprofile = new Sellerprofile();
		$sellerprofile->saveField('seller_id',$Event->data['user']['id']);
	}
}
?>