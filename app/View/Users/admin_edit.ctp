<?php echo $this->element("admin/admin_sidebar"); ?>
	<!--main content start-->
<div class="main-body">
	<div class="page-wrapper">
		<div class="page-header">
			<div class="page-header-title"><h4>User</h4></div>
		</div>
		<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<header class="card-header">
					Update User<div class="pull-right"><?php echo $this->Html->link("Back to list", array("controller"=>"users","action"=>"index","admin"=>true)); ?></div>
					</header>
					<div class="card-body">
						<div class="row">
							<div class="col-md-6">						
							
								<?php echo $this->Form->create('User', array("url"=>array("controller"=>"users","action"=>"edit","admin"=>true),"role"=>"form","class"=>""));?>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label for="firstname">First Name</label>
											<?php echo $this->Form->text('User.firstname',array("class"=>"form-control","placeholder"=>"First name"));echo $this->Form->hidden('User.id'); ?>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
										<label for="lastname">Last Name</label>
											<?php echo $this->Form->text('User.lastname',array("class"=>"form-control","placeholder"=>"Last name")); ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label for="email_address">Email Id</label>
											<?php echo $this->Form->text('User.email',array("class"=>"form-control","placeholder"=>"Email Id")); ?>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
										<label for="Phone">Phone</label>
											<?php echo $this->Form->text('User.phone',array("class"=>"form-control","placeholder"=>"Phone number")); ?>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label for="city">City</label>
											<?php echo $this->Form->text('User.city',array("class"=>"form-control","placeholder"=>"City name")); ?>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
										<label for="Zipcode">Zipcode</label>
											<?php echo $this->Form->text('User.zipcode',array("class"=>"form-control","placeholder"=>"Zipcode")); ?>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label for="group">Group</label>
											<?php echo $this->Form->select('User.group_id',$groups,array("class"=>"form-control","empty"=>"--Group--")); ?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="checkbox">
											<label>
											<?php echo $this->Form->checkbox('User.active',array()); ?>Make it active</label>
										</div>
									</div>									
								</div>
								<div class="row">
									<div class="col-md-6">
										<button type="submit" class="btn btn-success">Update</button>
									</div>
								</div>
								<?php echo $this->Form->end(); ?>
							</div><!--col-md-6-->
						
							<div class="col-md-6"> YET TO DECIDE FOR CONTENT</div>
						</div>
					</div>
                </div>	
		
			</div><!-- end of id welcome -->
		</div><!--/#content.span10-->
	</div><!--page-boddy-->
	</div>
</div>