<div class="body-content">
	<div class="container">
		<div class="terms-conditions-page">
			<div class="row">
				<div class="col-md-12 terms-conditions">
					<h2 class="heading-title">Registration information received</h2>
					<hr />
					<div class="col-md-6">
						<h3>Email confirmation</h3>
						<ul>
							<li>Thank you for providing your details. We have sent you the link to confirm the email id. It is mandetory to complete the registration. Please click on the link or copy paste into the new window.
							</li>
						</ul>
					</div>
				</div>
			</div><!-- /.row -->
		</div><!-- /.sigin-in-->
	</div>
</div>