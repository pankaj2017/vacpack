<?php echo $this->element('admin/admin_sidebar');?>
<!--main content start-->
<div class="main-body">
	<div class="page-wrapper">
		<div class="page-header">
			<div class="page-header-title"><h4>Users</h4></div>
		</div>
		<div><?php echo $this->Session->flash(); ?> </div>
	
		
		<div class="page-body">
			<div class="row">
				<div class="col-md-12 col-xl-12">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-md-10">
									<h5>Users on vacdo</h5>
								</div>
								<div class="col-md-2 text-right">
							<?php echo $this->Html->link("Add New",array('plugin'=>'','controller'=>'users','action'=>'create','admin'=>true),array('class'=>'btn btn-outline-info btn-sm'))?>
								</div>
							</div>
						</div>
			
						<div><?php echo $this->Session->flash(); ?> </div>
					
						<div class="card-body">
													
							<table class="table table-bordered table-striped table-condensed">
								<thead>
									<tr>
										<th width="10%">First Name</th>
										<th width="10%">Last Name</th>
										<th width="34%">Email</th>
										
										<th width="7%">Group</th>
										<th width="10%">Modified</th>
										<th width="14%" style="text-align:right">Action</th>									
									</tr>							
								</thead>
								<tbody>
									<?php 
									foreach($users as $user){	?>
									<tr>
										<td><?php echo $user['User']['firstname']?> </td>
										<td><?php echo $user['User']['lastname']?> </td>
										<td><?php echo $user['User']['email'];?> </td>
										
										<td><?php echo $user['Group']['name']; ?></td>
										<td><?php echo $this->Time->format('M j, Y',$user['User']['modified']); ?></td>
										<td style="text-align:right"><?php echo $this->Html->link('View',array('controller'=>'users','action'=>'view/'.$user['User']['id'],'admin'=>true),array('class'=>'badge badge-success')); ?> 
										<?php echo $this->Html->link('Edit',array('controller'=>'users','action'=>'edit/'.$user['User']['id'],'admin'=>true),array('class'=>'badge badge-info')); ?> 
										<?php echo $this->Html->link('Delete',array('controller'=>'users','action'=>'delete/'.$user['User']['id'],'admin'=>true),array('class'=>'badge badge-danger'),array('Are you sure you want to delete the record?')); ?></td>
									</tr>
									<?php } ?>					
								</tbody>
							</table>
						
							<p>
							<?php
							echo $this->Paginator->counter(array(
							'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
							));
							?>	</p>
							
							<ul class="pagination">
								<?php
									echo $this->Paginator->prev(__('Prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
									echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
									echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								?>
							</ul>
						</div><!-- card-body -->
					</div><!-- card -->
				</div><!--col-md-12-->
			</div><!--row-->
		</div><!--page-body-->
	</div><!--page-wrapper-->
</div>		