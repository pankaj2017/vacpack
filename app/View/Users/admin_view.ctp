<?php echo $this->element('admin/admin_sidebar');?>	
<div class="main-body">
	<div class="page-wrapper">
		<div class="page-header">
			<div class="page-header-title"><h4>User</h4></div>
		</div>
	
			<div class="page-body">
                <div class="row">
                    <div class="col-md-12 col-xl-12">
                        <div class="card">
                            <div class="card-header">
                            	<div class="row">
                            		<div class="col-md-10">
	                                	<h5>Account Details Of <?php echo $user['User']['firstname'].'&nbsp;&nbsp;'.$user['User']['lastname'];?></h5>
	                            	</div>
	                                <div class="col-md-2 text-right"><?php echo $this->Html->link("Back to list",array('plugin'=>'','controller'=>'users','action'=>'index','admin'=>true),array('class'=>'btn btn-outline-info btn-sm'))?>
	                                </div>
                            	</div>
                            </div>
                            
                           	<div class="card-body">
								<div class="row">						
									<div class="col-md-6">
										<p><label>Name </label><?php echo $user['User']['firstname'].'&nbsp;&nbsp;'.$user['User']['lastname'];?></p>
										
										<p><label>Group </label><?php echo $user['Group']['name']; ?></p>
										<p><label>Email </label><?php echo $user['User']['email'];?></p>
										<p><label>Registered on </label><?php echo $user['User']['created'];?></p>
										<p><label>Status </label><?php echo ($user['User']['active']=='1')? "Active":"Disable";?></p>
										<p><label>Last LoggedIn </label><?php echo $user['User']['lastlogin'];?></p>
									</div>
								</div>
							</div>
                        </div>
                        
                    </div>
                </div>
            </div>
    </div>
</div>