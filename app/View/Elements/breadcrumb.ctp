<?php
$_controller = $this->request->params['controller'];
$_action = $this->request->params['action'];
// print_r($_controller);
// print_r($_action);
// pr($cities_data);
// die;

switch ($_controller) {
	case 'articles':
		switch ($_action) {
			case 'index':
				$this->Html->addCrumb('Articles',array('controller'=>'articlecategories','action'=>'index'),array('title'=>'Vacpack Article Categories'));
				if(!empty($articlelist))
					$this->Html->addCrumb($articlelist[0]['Articlecategory']['name']);
				break;
			case 'view':
				$this->Html->addCrumb('Article',array('controller'=>'articles','action'=>'index'),array('title'=>'Article'));
				$this->Html->addCrumb($article['Articlecategory']['name'],array('controller'=>'articles','action'=>'index','slug'=>$article['Articlecategory']['slug'],'id'=>$article['Articlecategory']['id']),array('title'=>'Article Category'));
				$this->Html->addCrumb($article['Article']['title']);
				break;
			default:
				break;
		}
		break;
	case 'articlecategories':
		switch ($_action) {
			case 'index':
				$this->Html->addCrumb('Articles');
				break;
			default:
				break;
		}
		break;
	case 'countries':
		switch ($_action) {
			case 'getCountry':
				$this->Html->addCrumb('India');
				break;
			default:
				break;
		}
		break;
	case 'states':
		switch ($_action) {
			case 'citiesList':
				$this->Html->addCrumb($states_data['Country']['name'],array('controller'=>'countries','action'=>'getCountry'),array('title'=>'India'));
				
				$this->Html->addCrumb($states_data['State']['name']);
				break;
			default:
				break;
		}
		break;
	case 'destinations':
		switch ($_action) {
			case 'placesList':

				$this->Html->addCrumb($states_data['Country']['name'],array('controller'=>'countries','action'=>'getCountry'),array('title'=>'India'));
				
				$this->Html->addCrumb($states_data['State']['name']);
				$this->Html->addCrumb($cities_data['Destination']['name']);
				break;
			default:
				break;
		}
		break;
	default:
		# code...
	break;
}
?>