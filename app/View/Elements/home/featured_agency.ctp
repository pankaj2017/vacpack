<section class="section text-center pb-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title text-center">
                    <p>An Amaizing Travel Experience with</p>
                    <h2>Featured Travel <span>Agencies</span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid p-0" id="features">
        <div class="features-parallax" style="background-image: url(assets/images/banner_3.png)"></div>
        <div class="row no-gutters">
            <div class="offset-xl-6 col-xl-6 offset-lg-5 col-lg-7 col-10 offset-1 my-4 travel-list">
                <div class="row no-gutters">
                        
                <?php 
                    if(isset($tagency) && ! empty($tagency)) {
                    foreach ($tagency as $tagency_key => $tagency_value) { ?>

                    <div class="col-md-6 col-12">
                        <div class="travel-block border-left-0 border-bottom-0">
                            <div class="d-flex align-items-center">
                                <?php 
                                    if(!empty($tagency_value['Tagency']['imagesmall'])){
                                        echo $this->Html->image($tagency_value['Tagency']['imagesmall'],array('class'=>'agency-image','alt'=>$tagency_value['Tagency']['name'])); 
                                    }
                                    else{
                                        echo $this->Html->image("https://dummyimage.com/90x74/10a1a1/ff3300&text=Agency",array('class'=>'')); 
                                    }
                                ?>
                                <h4 class="agency-name">
                                     <?php  echo $this->Html->link($tagency_value['Tagency']['name'],array('plugin'=>'','controller'=>'tagencies','action'=>'view','admin'=>false,'id' => $tagency_value['Tagency']['id']),array("class"=>""));
                                    ?>
                                    <?php   ?></h4>
                            </div>
                            <ul class="agency-info">
                                <li>
                                    <?php
                                        $options =[];
                                        $options['model']='Tagency';
                                        $options['view'] = 'listing';
                                        echo $this->Ratings->display_for($tagency_value,$options);
                                    ?>
                                </li>
                                <li><a href="tel:9999999999"><img class="img-fluid" src="assets/svg/phone-fill.svg" alt="phone"><span><?php  echo $tagency_value['Tagency']['phone'] ?></span></a></li>
                                <li><a href="mailto:info@comanyname.com"><img class="img-fluid" src="assets/svg/mail.svg" alt="mail"><span><?php  echo $tagency_value['Tagency']['email_address'] ?></span></a></li>
                            </ul>
                           
                          
                            <a class="btn btn-round btn-orange" href="javascript:;">Get A Quote</a>
                        </div>
                    </div>

                <?php 
                    }
                    } 
                ?>
                </div>
            </div>
        </div>
    </div>
</section>