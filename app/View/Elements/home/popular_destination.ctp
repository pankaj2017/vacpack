<section class="section text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title text-center">
                    <p>India's Best Tourist Destinations</p>
                    <h2>Popular Destinations <span>Offered</span></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                
                    <?php /*coming from PagesController */
                    if(isset($popular_destinations['State'])) { ?> 
                    <div class="row">

                        <?php 
                        $j=1;$i=1;
                        $po=array_chunk($popular_destinations['State'],5);
                        foreach ($po as $pop){//echo ($i % 2);
                            foreach ($pop as $destination_k => $destination_v) { 
                            if(($i % 2)==1 & ($j % 5)==0) {echo "\n<div class=\"col-md-6\">";}
                            else if(($i % 2)==0 & ($j % 5)==1){echo "\n<div class=\"col-md-6\">";}
                            else {echo "\n<div class=\"col-md-3\">";}
                            ?>
                                <div class="single-block overlay">
                                    <?php 
                                        if(!empty($destination_v['imagebig'])){
                                            echo $this->Html->image($destination_v['imagebig'],array('class'=>'img-fluid w-100','alt'=>$destination_v['name'])); 
                                        }
                                        else{
                                            echo $this->Html->image("https://via.placeholder.com/345x345.png?text=",array('class'=>'w-100')); 
                                        }
                                    ?>
                                    <a href="" class="hover">
                                        <?php //echo $this->Html->link("",array('plugin'=>'','controller'=>'states','action'=>'citiesList','admin'=>false,'slug' => $destination_v['slug']),array("class"=>""));?>
                                        <!-- <p class="price">FROM <span>$250</span></p> -->
                                        <h4 class="name"><?= $destination_v['name'] ?></h4>
                                        <p class="location">Feeling Blessed</p>
                                    </a>
                                </div>
                            </div><!--col-md-6 conditional div -->
                            <?php 
                            $j++;
                            } 
                        $i++;
                        }?>
                    </div>
                    <?php } ?>
                
            </div>
        </div>
    </div>
</section>