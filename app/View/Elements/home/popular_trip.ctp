<?php if(isset($random_list_packages) && !empty($random_list_packages)) { ?>
    <?php echo $this->Html->css(['/assets/css/owl.carousel.min','/assets/css/owl.theme.default.min']);?>
<section class="section" id="popular_trips">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8">
                <div class="title">
                    <p>Popular Trips</p>
                    <h2>Worthy time spent <span class="d-block">around the Incredible India</span></h2>
                    <p class="text">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="trip-carousel owl-carousel owl-theme">
                    <?php foreach ($random_list_packages as $key => $value) { ?>
                    <div class="trip-block">
                        <div class="trip-image">
                            <?php if(!empty($value['Package']['pkgimage']) ) {  ?>
                            <?php echo $this->Html->link($this->Html->image($value['Package']['pkgimage'],array("class"=>"img-fluid", "alt"=>$value['Package']['title'])),array("controller"=>"packages","action"=>"view",'admin'=>false,'slug' => Inflector::slug($value['Package']['title']),'id' => $value['Package']['id']),array("escape"=>false)); ?>
                           
                            <?php }else{
                                echo $this->Html->image("https://via.placeholder.com/453x300.png?text=+Package",array('class'=>'card-img-top')); 
                            } ?>
                        </div>
                        <div class="trip-details">
                            <div class="trip-header">
                                <div class="card-title">
                                    <h4><?php echo $this->Html->link($value['Package']['title'],array("controller"=>"packages","action"=>"view",'admin'=>false,'slug' => Inflector::slug($value['Package']['title']),'id' => $value['Package']['id'])); ?>
                                
                                        Starting From ₹ <?php echo number_format($value['Package']['price']);?></h4>
                                </div>
                                
                            </div>
                            <div class="trip-content">
                                <?= $this->Text->truncate($value['Package']['description'],100); ?>
                            </div>
                            <div class="trip-footer justify-content-between">
                                <div class="left">
                                    <img class="img-fluid" src="assets/svg/clock.svg" alt="clock"><span><?php echo $value['Package']['numberofdays']." Days"." ".$value['Package']['numberofnights']." Nights" ?> </span>
                                </div>
                                <div class="right">
                                    <?php echo $this->Html->link("View Package",array('plugin'=>'','controller'=>'packages','action'=>'view','admin'=>false,'slug' => Inflector::slug($value['Package']['title']),'id' => $value['Package']['id']),array("class"=>"view-link"));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
 $(document).ready(function() {
        // parallax
        $(window).stellar({
            responsive: true,
            positionProperty: 'position',
            horizontalOffset: 0,
            verticalOffset: 0,
            horizontalScrolling: false
        });
      
        // carousel
        $('.trip-carousel').owlCarousel({
            lazyLoad: true,
            nav: true,
            navText: ['<img src="assets/svg/left_arrow.svg" alt="left_arrow" />', '<img src="assets/svg/right_arrow.svg" alt="right_arrow" />'],
            loop: true,
            dots: false,
            margin: 20,
            responsive: {
                0: {
                    items: 1,
                    margin: 15
                },
                768: {
                    items: 2
                },
                1200: {
                    items: 3
                }
            }
            //- autoplay: true
        });
      })
</script>
<?php 
echo $this->Html->script(['/assets/js/owl.carousel.min']);
} ?>