<div id="banner">
	<div class="parallax-window" id="test" data-stellar-background-ratio="0.5">
	    <div class="banner-text">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-12 text-center">
	                    <h1 class="banner-title">Recharge Your Wellness</h1>
	                    <p class="sub-title mb-5">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
	                </div>
	                <div class="col-md-12">
						<?php echo $this->Form->create('Searchform', array('type' => 'post','class'=>'search-panel'));?>
	                	
	                    <!-- <form class="search-panel"> -->
	                        <div class="row">
	                            <div class="col-lg-9 col-12">
	                                <div class="row">
	                                    <div class="col-lg-3 col-md-6 pr-0">
	                                        <div class="form-group form-control">
	                                            <img class="icon img-fluid fill-orange" src="assets/svg/pin-fill.svg" alt="destination">
	                                             <?php 
										           echo $this->Form->select('state',$state_list,['empty'=>'Destination','class'=>'']);
										            ?>
	                                        </div> 
	                                    </div>
	                                    <div class="col-lg-3 col-md-6 pr-0">
	                                        <div class="form-group form-control">
	                                            <img class="icon img-fluid" src="assets/svg/activity.svg" alt="activities">
	                                            <?php 
										           echo $this->Form->select('holidaythem',$holiday_thems,['empty'=>'Holiday them','class'=>'','required'=>true]);
										            ?>
	                                        </div>
	                                    </div>
	                                    <div class="col-lg-3 col-md-6 pr-0">
	                                        <div class="form-group form-control">
	                                            <img class="icon img-fluid" src="assets/svg/duration.svg" alt="duration">
	                                            <select name="days[]">
	                                                <option value="">Duration</option>
	                                                <option value="1-3">1 to 3 days</option>
	                                                <option value="4-6">4 to 6 days</option>
	                                                <option value="7-9">7 to 9 days</option>
	                                                <option value="10-12">10 to 12 days</option>
	                                                <option value="13-31">13 days or more</option>
	                                                <option value="0">Not decided</option>
	                                            </select>
	                                        </div>
	                                    </div>
	                                    <div class="col-lg-3 col-md-6 pr-0">
	                                        <div class="form-group form-control">
	                                            <img class="icon img-fluid" src="assets/svg/budget.svg" alt="budget">
	                                            <select name="price_ranges[]">
	                                                <option value="">Budget</option>
	                                                <option value="0-10000">Less Than 10,000</option>
	                                                <option value="10000-20000">10,000 - 20,000</option>
	                                                <option value="20000-40000">20,000 - 40,000</option>
	                                                <option value="40000-60000">40,000 - 60,000</option>
	                                                <option value="60000-80000">60,000 - 80,000</option>
	                                                <option value="80000-200000">Above 80,000</option>
	                                            </select>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col-lg-3 col-12">
	                                <div class="form-group">
	                                    <button class="btn btn-block btn-orange" type="submit">Search</button>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
    </div>
</div>