<div class="related-articles">
    <h2>Related Articles</h2>
    <ul>
        <?php 
        foreach ($rest_article as $key => $value) { ?>
            <?php echo "<li>".$this->Html->link($value['Article']['title'], array("controller"=>"articles","action"=>"detail",'art'=>$value['Article']['slug']),array("escape"=>false))."</li>"; ?>
        <?php } ?>
    </ul>
</div>