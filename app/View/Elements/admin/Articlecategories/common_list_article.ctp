<div class="row artical-box">
      <?php if(!empty($value['featuredimagesmall']) && file_exists(ROOT.'/app/webroot'.$value['featuredimagesmall'])){ ?>
    <div class="artical-box-img col-lg-3">

       <?php $img = $this->Html->image($value['featuredimagesmall'],array('class'=>'img-fluid','alt'=>$value['title']), array('escape'=>true)); 
        echo $this->Html->link($img, array("controller"=>"articles","action"=>"detail",'art'=>$value['slug']),array("escape"=>false,'title'=>$value['title']));
            ?>
    </div>
      <?php } ?> 
        <div class="artical-box-content col-lg-9">
            <h3><?php echo $this->Html->link( $value['title'], array("controller"=>"articles","action"=>"detail",'art'=>$value['slug']),array("escape"=>false,'title'=>$value['title'],'class'=>'advice_link')); ?>
            </h3>
            <p class="mb-0"><?php echo $this->Text->truncate(strip_tags($value['body'],'<p><b>'),120,array('ellipsis'=>'..'));?></p>


            <?php echo $this->Html->link("Read More <i class='fas fa-caret-right'></i>", array("controller"=>"articles","action"=>"detail",'art'=>$value['slug']),array("escape"=>false,'title'=>$value['title'],'class'=>"readmore")); ?>
        </div>
</div>