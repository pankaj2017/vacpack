<?php
$defaultOptions = [
	'link_class' => '',
	'counts' => isset($systemcounts) ? $systemcounts : [],
	'divider' => '<li class="nav-devider"></li>'
];

$menuItems = [
	[
		'name' => 'Dashboard',
		'label' => '<span class="hide-menu">Dashboard</span>',
		'icon' => '<i class="fa fa-book"></i>',
		'link_class' => '',
		'childs' => [
			[
				'name' => 'Default',
				'label' => '<div class="lbldivstyle">Default</div>',
				'url' => ['controller' => 'admins', 'action' => 'dashboard', 'admin' => true, 'plugin'=> false],
				'count_key' => 'articlecategory',
				'active' => 'activearticlecategory'
			],
			[
				'name' => 'Package types',
				'label' => '<div class="lbldivstyle">Package types</div>',
				'url' => ['controller' => 'packagetypes', 'action' => 'index', 'admin' => true, 'plugin'=> 'tagents'],
				'count_key' => 'article',
				'active' => 'activearticle'
			],
			[
				'name' => 'Holiday themes',
				'label' => '<div class="lbldivstyle">Package types</div>',
				'url' => ['controller' => 'holidaythemes', 'action' => 'index', 'admin' => true, 'plugin'=> false],
				'count_key' => 'article',
				'active' => 'activearticle'
			]
		]
	],
	[
		'name' => 'ACL',
		'label' => '<span class="hide-menu">ACL</span>',
		'url' => ['controller' => 'AclDevelopers', 'action' => 'index', 'admin' => true, 'plugin' => 'AclDeveloper'],
		'icon' => '<i class="ti-view-grid"></i>'
	],
	
	[
		'name' => 'Article Management',
		'label' => '<span class="hide-menu">Article Management</span>',
		'icon' => '<i class="fa fa-book"></i>',
		'link_class' => '',
		'childs' => [
			[
				'name' => 'Article Categories',
				'label' => '<div class="lbldivstyle">Article Categories</div>',
				'url' => ['controller' => 'articlecategories', 'action' => 'index', 'admin' => true, 'plugin'=> false],
				'count_key' => 'articlecategory',
				'active' => 'activearticlecategory'
			],
			[
				'name' => 'Articles',
				'label' => '<div class="lbldivstyle">Articles</div>',
				'url' => ['controller' => 'articles', 'action' => 'index', 'admin' => true, 'plugin'=> false],
				'count_key' => 'article',
				'active' => 'activearticle'
			]
		]
	],
	[
		'name' => 'Feedbacks',
		'label' => '<span class="hide-menu">Feedbacks</span>',
		'icon' => '<i class="fa fa-comment"></i>',
		'link_class' => '',
		'childs' => [
			[
				'name' => 'Comments',
				'label' => '<div class="lbldivstyle">Comments</div>',
				'url' => ['controller' => 'Comments', 'action' => 'index', 'admin' => true, 'plugin'=> 'Feedback'],
				'count_key' => 'commentcount',
				'active' => 'activecommentlist',
				'param' => ['whitelist' => [1,2,3]],
			],
			[
				'name' => 'Ratings',
				'label' => '<div class="lbldivstyle">Ratings</div>',
				'url' => ['controller' => 'Ratings', 'action' => 'index', 'admin' => true, 'plugin'=> 'Feedback'],
				'count_key' => 'ratingcount',
				'active' => 'activeratinglist',
				'param' => ['whitelist' => [1,2,3]],
			],
			[
				'name' => 'Rating Types',
				'label' => '<div class="lbldivstyle">Rating Types</div>',
				'url' => ['controller' => 'RatingTypes', 'action' => 'index', 'admin' => true, 'plugin'=> 'Feedback'],
				'count_key' => 'ratingtypes',
				'active' => 'activeratingtypelist',
				'param' => ['whitelist' => [1,2,3]],
			],
		]
	],
	[
		'name' => 'Travel Destinations',
		'label' => '<span class="hide-menu">Travel Destinations</span>',
		'icon' => '<i class="ti-layout-cta-btn-right"></i>',
		'link_class' => '',
		'childs' => [
			[
				'name' => 'Countries',
				'label' => '<div class="lbldivstyle">Countries</div>',
				'url' => ['controller' => 'countries', 'action' => 'index', 'admin' => true, 'plugin'=> false],
				'count_key' => '',
				'active' => ''
			],
			[
				'name' => 'States',
				'label' => '<div class="lbldivstyle">States</div>',
				'url' => ['controller' => 'states', 'action' => 'index', 'admin' => true, 'plugin'=> false],
				'count_key' => '',
				'active' => ''
			],
			[
				'name' => 'Destinations',
				'label' => '<div class="lbldivstyle">Destinations</div>',
				'url' => ['controller' => 'destinations', 'action' => 'index', 'admin' => true, 'plugin'=> false],
				'count_key' => '',
				'active' => ''
			],
			[
				'name' => 'Places',
				'label' => '<div class="lbldivstyle">Places</div>',
				'url' => ['controller' => 'places', 'action' => 'index', 'admin' => true, 'plugin'=> false],
				'count_key' => '',
				'active' => ''
			]
		]
	],
	[
		'name' => 'Travel Packages',
		'label' => '<span class="hide-menu">Travel Packages</span>',
		'url' => ['controller' => 'packags', 'action' => 'index', 'admin' => true, 'plugin' => 'tagents'],
		'icon' => '<i class="ti-view-grid"></i>'
	],
	[
		'name' => 'Travel agencies',
		'label' => '<span class="hide-menu">Travel agencies</span>',
		'url' => ['controller' => 'travelagencies', 'action' => 'index', 'admin' => true, 'plugin' => 'tagents'],
		'icon' => '<i class="ti-view-grid"></i>'
	],
	[
		'name' => 'Users',
		'label' => '<span class="hide-menu">Users</span>',
		'icon' => '<i class="ti-user"></i>',
		'link_class' => '',
		'childs' => [
			[
				'name' => 'Customers',
				'label' => '<div class="lbldivstyle">Customers</div>',
				'url' => ['controller' => 'users', 'action' => 'index', 'admin' => true, 'plugin'=> false],
				'count_key' => '',
				'active' => ''
			],
			[
				'name' => 'Travel Agents',
				'label' => '<div class="lbldivstyle">Travel Agents</div>',
				'url' => ['controller' => 'travelagents', 'action' => 'index', 'admin' => true, 'plugin'=> 'tagents'],
				'count_key' => '',
				'active' => ''
			],
			[
				'name' => 'Travelagent Groups',
				'label' => '<div class="lbldivstyle">Travelagent Groups</div>',
				'url' => ['controller' => 'travelagentgroups', 'action' => 'index', 'admin' => true, 'plugin'=> 'tagents'],
				'count_key' => '',
				'active' => ''
			],
			[
				'name' => 'Administrators',
				'label' => '<div class="lbldivstyle">Administrators</div>',
				'url' => ['controller' => 'admins', 'action' => 'index', 'admin' => true, 'plugin'=> false],
				'count_key' => '',
				'active' => ''
			],
			[
				'name' => 'Administrator groups',
				'label' => '<div class="lbldivstyle">Administrator groups</div>',
				'url' => ['controller' => 'admingroups', 'action' => 'index', 'admin' => true, 'plugin'=> false],
				'count_key' => '',
				'active' => ''
			]
		]
	],
	
	
];

$menuHelper = $this->Helpers->load('Menu');
?>
<div class="main-menu">
	
	<?php echo $menuHelper->create($menuItems, $defaultOptions); ?>
	
</div>