<?php 
if(!isset($activecountries) || empty($activecountries)){  $activecountries =''; }
if(!isset($activearticle) || empty($activearticle)){  $activearticle =''; }
if(!isset($activearticlecategory) || empty($activearticlecategory)){  $activearticlecategory =''; }
if(!isset($opencountry) || empty($opencountry)){  $opencountry =''; }
if(!isset($openuser) || empty($openuser)){  $openuser =''; }
if(!isset($activeuser) || empty($activeuser)){  $activeuser =''; }
if(!isset($activeadmingroup) || empty($activeadmingroup)){  $activeadmingroup =''; }
if(!isset($activeadmin) || empty($activeadmin)){  $activeadmin =''; }
if(!isset($activetravelagentgroup) || empty($activetravelagentgroup)){  $activetravelagentgroup =''; }
if(!isset($activetravelagent) || empty($activetravelagent)){  $activetravelagent =''; }
?>
<!-- Menu aside start -->
<div class="main-menu">
	
	<div class="main-menu-content">
		<ul class="main-navigation">
			
			<li class="nav-title" data-i18n="nav.category.navigation">
				<i class="ti-line-dashed"></i>
				<span>Navigation</span>
			</li>
			<li class="nav-item <?php echo $activecountries;?>">
				<a href="#!">
					<i class="fas fa-home"></i>
					<span>Dashboard</span>
				</a>
				<ul class="tree-1">
					<li class="">
						<?php echo $this->Html->link("Default",array('plugin'=>'','controller'=>'admins','action'=>'dashboard','admin'=>true)); ?>
					</li>
					<li><?php echo $this->Html->link("Package types",array('plugin'=>'tagents','controller'=>'packagetypes','action'=>'index','admin'=>true)); ?></li>
					<li><?php echo $this->Html->link("Holiday themes",array('plugin'=>'','controller'=>'holidaythemes','action'=>'index','admin'=>true)); ?></li>
				</ul>
			</li>
				
			<li class="nav-item single-item">
				<?php echo $this->Html->link("<i class='ti-view-grid'></i> Travel Packages",array('plugin'=>'tagents','controller'=>'packags','action'=>'index','admin'=>'true'),array('escape'=>false)); ?>				
					<label class="label label-danger menu-caption">100+</label>
			</li>
			<li class="nav-item single-item">
				<?php echo $this->Html->link("<i class='ti-view-grid'></i> Travel agencies",array('plugin'=>'tagents','controller'=>'travelagencies','action'=>'index','admin'=>true),array('escape'=>false)); ?>				
				<label class="label label-danger menu-caption">100+</label>
			</li>
			<li class="nav-item <?php if(isset($opendestination) && !empty($opendestination)){ echo $opendestination; }?>">
				<a href="#!">
					<i class="ti-layout-cta-btn-right"></i>
					<span >Travel Destinations</span>
					<!-- <label class="label label-danger menu-caption">100+</label> -->
				</a>				
				<ul class="tree-1">
					<li class="<?php echo !empty($activecountry) ?$activecountry :'';?>" ><?php echo $this->Html->link("Countries",array('plugin'=>'','controller'=>'countries','action'=>'index','admin'=>true)); ?></li>
					<li class="<?php echo !empty($activestate) ?$activestate :'';?>"><?php echo $this->Html->link("States",array('plugin'=>'','controller'=>'states','action'=>'index','admin'=>true)); ?></li>
					<li class="<?php echo !empty($activedestination) ?$activedestination :'';?>"><?php echo $this->Html->link("Destinations",array('plugin'=>'','controller'=>'destinations','action'=>'index','admin'=>true)); ?></li>
					<li class="<?php echo !empty($activeplace) ?$activeplace :'';?>"><?php echo $this->Html->link("Places",array('plugin'=>'','controller'=>'places','action'=>'index','admin'=>true)); ?></li>
				</ul>
			</li>
			<li class="nav-item <?php if(isset($openarticle) && !empty($openarticle)){ echo $openarticle; }?>">
				<a href="#!">
					<i class="ti-layout-cta-btn-right"></i>
					<span >Travel Articles</span>
					<label class="label label-danger menu-caption">100+</label>
				</a>				
				<ul class="tree-1">
					<li class="<?php echo $activearticlecategory;?>"><?php echo $this->Html->link("<i class='ti-view-grid'></i>Articles Categories",array('plugin'=>'','controller'=>'articlecategories','action'=>'index','admin'=>true),array('escape'=>false)); ?></li>
					<li class="<?php echo $activearticle;?>"><?php echo $this->Html->link("<i class='ti-view-grid'></i> Travel articles",array('plugin'=>'','controller'=>'articles','action'=>'index','admin'=>true),array('escape'=>false)); ?></li>
				</ul>
			</li>
			
			<li class="nav-title" data-i18n="nav.category.forms">
				<i class="ti-line-dashed"></i>
				<span>Forms</span>
			</li>
			<li class="nav-item <?php if(isset($openuser) && !empty($openuser)){ echo $openuser; }?>">
				<a href="#!">
					<i class="ti-user"></i>
					<span data-i18n="nav.user-profile.main">Users</span>
				</a>
				<ul class="tree-1">
					<li class="<?php echo $activeuser;?>"><?php echo $this->Html->link("Customers", array('plugin'=>'','controller'=>'users','action'=>'index','admin'=>true)); ?></li>
					<li class="<?= $activetravelagent?>"><?php echo $this->Html->link("Travel Agents", array('plugin'=>'tagents','controller'=>'travelagents','action'=>'index','admin'=>true)); ?></li>
					<li class="<?= $activetravelagentgroup ?>"><?php echo $this->Html->link("Travelagent Groups", array('plugin'=>'tagents','controller'=>'travelagentgroups','action'=>'index','admin'=>true)); ?></li>
					<li class="<?= $activeadmin?>"><?php echo $this->Html->link("Administrators", array('plugin'=>'','controller'=>'admins','action'=>'index','admin'=>true)); ?></li>
					<li class="<?= $activeadmingroup?>"><?php echo $this->Html->link("Administrator groups", array('plugin'=>'','controller'=>'admingroups','action'=>'index','admin'=>true)); ?></li>
				</ul>
			</li>
			<li class="nav-item single-item">
				<a href="form-picker.html">
					<i class="ti-pencil-alt"></i>
					<span data-i18n="nav.form-pickers.main"> Form Picker </span>
					<label class="label label-warning menu-caption">NEW</label>
				</a>
			</li>
			<li class="nav-item">
				<a href="#!">
					<i class="ti-layout-cta-btn-right"></i>
					<span data-i18n="nav.json-form.main">JSON Form</span>
					<label class="label label-danger menu-caption">HOT</label>
				</a>
				<ul class="tree-1">
					<li><a href="json-forms/simple-form.html" data-i18n="nav.json-form.simple-form">Simple Form</a></li>
					
					<li><a href="json-forms/localized-login.html" data-i18n="nav.json-form.localized-login">Localized Login</a></li>
				</ul>
			</li>
			
		</ul><!--main-nevigation -->
	</div><!--main-menu-content-->
</div>
<!-- Menu aside end -->