<div class="row">
	<div class="col-md-4 form-group">
	<label class="">Country <span class="text-danger">*</span></label>
		<?php echo $this->Form->select('State.country_id',$countrylists,array("class"=>"form-control","empty"=>"Select Country",)); 

      	echo $this->Form->error('State.country_id',['class'=>'error-message']);
		?>

	</div>
	<div class="col-md-8 form-group">
	<label class="">Name <span class="text-danger">*</span></label>
		<?php echo $this->Form->text('State.name',array("class"=>"form-control")); 
      	echo $this->Form->error('State.name',['class'=>'error-message']);

		?>
	</div>
</div>
<div class="row">
	<div class="col-md-4 form-group">
		<label for="slug">Slug</label>
			<?php echo $this->Form->text('State.slug',array("class"=>"form-control","placeholder"=>"Enter Slug")); ?>
		<span class="help-block"><small>Will be automatically generated from your title, if left empty</small></span>
	</div>
	<div class="col-md-4 form-group">
		<label>State Image<span class="">*</span></label>
		<?php echo $this->Form->input('State.image', array('type'=>'file',"class"=>"file", "data-max-file-count"=>"1", "label"=>false)); ?>
	</div>
	<div class="col-md-4 form-group">
		<label>State Image</label>
		<?php 
		if(!empty($this->request->data['State']['imagename'])){
			echo $this->Html->image($this->request->data['State']['imagename'],array('class'=>'img-fluid')); ?>
		<?php } ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12 form-group">
		<label class="">Description <span class="text-danger">*</span></label>
		<?php echo $this->Form->textarea('State.description', array("class"=>"form-control ckeditor")); 
      	echo $this->Form->error('State.description',['class'=>'error-message']);
		
		?>
	</div>
</div>


