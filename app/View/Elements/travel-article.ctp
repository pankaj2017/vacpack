<div class="container" style="padding: 30px">
	<div class="row">
		<div class="card-deck">
		<?php if(isset($random_list_articles) && !empty($random_list_articles)) {
			foreach ($random_list_articles as $article_key => $articlevalue) {  
		 	?>
		  <div class="card">
		    <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
		    <?php $img = $this->Html->image($articlevalue['Articlecoverimage']['namemedium'],array('class'=>'card-img-top','alt'=>$articlevalue['Article']['title']), array('escape'=>true)); 
		    	 echo $this->Html->link($img, array("controller"=>"articles","action"=>"view",'slug'=>$articlevalue['Article']['slug'],'id'=>$articlevalue['Article']['id']),array("escape"=>false,'title'=>$articlevalue['Article']['title']));
		    ?>
		    <div class="card-body">
		      <h5 class="card-title"><?php echo $articlevalue['Article']['title'];?></h5>
		      <p class="card-text"><?php 
		      		echo $this->Text->truncate(
					    $articlevalue['Article']['body'], 250,
					    array('ellipsis' => '...','exact' => false
					    )
					);?>
				</p>
		      <p class="card-text"><small class="text-muted">Last updated <?php echo $articlevalue['Article']['modified']?></small></p>
		    </div>
		  </div>
		<?php } ?>
		</div>
		<?php } ?>
		</div>
	</div>
</div>
