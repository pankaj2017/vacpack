<header class="">
	<?php echo $this->element('header-top-row'); ?>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
		
			<?php //echo $this->Html->link($this->Html->image("vacdo.png",array("alt"=>"logo")),array('plugin'=>'','controller'=>'/'),array('escape'=>false));?>
			
			<div class="header-bottom-area collapse navbar-collapse" id="navbarNavDropdown">
				<ul class="navbar-nav main-menu float-right">
					<li class="nav-item active">
					<a class="nav-link" href="#">Weekends <span class="sr-only">(current)</span></a>
					</li>
				  
					<li class="nav-item">
					<!-- <a class="nav-link" href="#"></a> -->
					<?php echo $this->Html->link('Destination Guides',array('plugin'=>'','controller'=>'countries','action'=>'getCountry','admin'=>false),array("class"=>"nav-link"));?>
					</li>
					</li>
					<li class="nav-item">
					<?php echo $this->Html->link('Travel Articles',array('plugin'=>'','controller'=>'articles','action'=>'index','admin'=>false),array("class"=>"nav-link"));?>
					</li>
					<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					  Holiday Themes
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
					<?php 
						foreach($appholidaythemes as $key=>$appholidaytheme){
					?>
					  <?php echo $this->Html->link($appholidaytheme,array('plugin'=>'','controller'=>'packages','action'=>'filter','slug' => $appholidaytheme),array("class"=>"dropdown-item"));?>
					  
					<?php }?>
					</div>
					</li>
				</ul>
			</div>

		</div><!-- /.container -->
	</nav>
</header>