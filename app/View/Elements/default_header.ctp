<header>
    <div class="top-bar">
        <div class="container">
            <div class="d-flex flex-row justify-content-between align-items-center">

                <?php echo $this->Html->link('Travel Agent? Join Us',array('plugin'=>'tagents','controller'=>'pages','action'=>'homepage','admin'=>false));?>
                <?php if (!$this->Session->check("Auth.User")){  ?>
                <ul class="social-icons">
                    <li>
                        <?php echo $this->Html->link('Travelers Sign In',array('plugin'=>'','controller'=>'users','action'=>'login','admin'=>false),array("class"=>""));?>
                    </li>|
                    <li>
                        <?php echo $this->Html->link('Sign Up',array('plugin'=>'','controller'=>'users','action'=>'register','admin'=>false),array("class"=>""));?>
                    </li>
                </ul>
                <?php }else{ ?>
                <div class="dropdown mt-2">
                    <a class="btn btn-success dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Welcom&nbsp;<?php echo ucfirst($this->Session->read("Auth.User.firstname"));?></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <?php echo $this->Html->link('My Profile',array('plugin'=>'','controller'=>'users','action'=>'login','admin'=>false),array("class"=>"dropdown-item"));?>
                        <?php echo $this->Html->link('My Reviews',array('plugin'=>'','controller'=>'reviews','action'=>'register','admin'=>false),array("class"=>"dropdown-item"));?>
                        <?php echo $this->Html->link('My Destinations',array('plugin'=>'','controller'=>'reviews','action'=>'register','admin'=>false),array("class"=>"dropdown-item"));?>
                        <div class="dropdown-divider"></div>
                        <?php echo $this->Html->link('Logout',array('plugin'=>'','controller'=>'users','action'=>'logout','admin'=>false),array("class"=>"dropdown-item"));?>
                    </div>
                </div>
                <?php } ?>


            </div>
        </div>
    </div>
    <div class="main-header mb-3">
        <div class="container">
            <div class="d-flex flex-row justify-content-between align-items-center">
                <div class="logo">
                    <?php $logo_img = $this->Html->image("/assets/images/logo.png",array('class'=>'','alt'=>"vacdoo.com"), array('escape'=>false)); 
                        echo $this->Html->link($logo_img, array("controller"=>"pages","action"=>"display"),array("escape"=>false,'title'=>"vacdoo.com"));
                    ?>
                </div>
                <ul class="info-list">
                    <li>
                        <div class="icon">
                            <?php echo $this->Html->image("/assets/svg/pin.svg", ['alt' => 'location','class'=>'img-fluid','escape' => false, 'title' => 'location']); ?>
                        </div>
                        <div class="icon-info">
                            <h4>Never ending vacation experience</h4>
                            <p>No middle layer, direct vacation agent</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <?php echo $this->Html->image("/assets/svg/phone.svg", ['alt' => 'contact','class'=>'img-fluid','escape' => false, 'title' => 'contact']); ?>
                        </div>
                        <div class="icon-info">
                            <h4>+91 99250 83003</h4>
                            <p>Toll Free</p>
                        </div>
                    </li>
                </ul>
                <a class="bars-icon" href="javascript:;"><span class="line"></span></a>
            </div>
            <ul class="navigation">
                <li class="nav-item"><a class="nav-item-link" href="javascript:void(0);">Weekends</a></li>
                <li class="nav-item">
                    <a class="nav-item-link" href="javascript:void(0);">
                        Destination Guides
                        <svg class="caret ml-2" id="Capa_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 213.333 213.333" style="enable-background:new 0 0 213.333 213.333;" xml:space="preserve" width="15px" height="15px">
                            <g>
                                <g>
                                    <g>
                                        <polygon class="active-path" points="0,53.333 106.667,160 213.333,53.333   " data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></polygon>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                    <div class="submenu">
                    <h4>Indian Destinations </h4>
                        <div class="container mw-100">
                            <div class="row">
                                <div class="col-md-9">
                                    

                                    <?php /*State date is coming from appcontroller _list_stats */
                                    if(isset($country_of_states['State']) && !empty($country_of_states['State'])) { 

                                   
                                    ?> 
                                    <ul class="menu-list">
                                        <?php 
                                        $i=0;
                                        foreach ($country_of_states['State'] as $state_k => $state_v) { 
 if(($i%4)==0){echo "\n<div class=\"row\">\n\n";}
                                        ?>
                                        <li class="col-md-3">
                                            <?php echo $this->Html->link($state_v['name'],array('plugin'=>'','controller'=>'states','action'=>'view','admin'=>false,'slug' => $state_v['slug']),array("class"=>""));?>
                                        </li>
                                        <?php 
                                        $i++;
                                        if(($i%4)==0){echo "\n</div>\n\n";}
                                        }?>
                                    </ul>
                                    <?php } ?>
                                </div>
                                <!-- <div class="col-md-4">
                                    <h4>International Destinations </h4>
                                    <ul class="menu-list">
                                        <li><a href="javascript:void(0);">Cambodia</a></li>
                                        <li><a href="javascript:void(0);">Thailand</a></li>
                                        <li><a href="javascript:void(0);">Bali</a></li>
                                        <li><a href="javascript:void(0);">Singapore</a></li>
                                        <li><a href="javascript:void(0);">Malaysia</a></li>
                                        <li><a href="javascript:void(0);">Mauritius</a></li>
                                        <li><a href="javascript:void(0);">Maldives</a></li>
                                        <li><a href="javascript:void(0);">Australia</a></li>
                                        <li><a href="javascript:void(0);">Seychelles</a></li>
                                        <li><a href="javascript:void(0);">Italy</a></li>
                                        <li><a href="javascript:void(0);">Dubai</a></li>
                                        <li><a href="javascript:void(0);">South Africa</a></li>
                                        <li><a href="javascript:void(0);">Europe</a></li>
                                    </ul>
                                </div> -->
                                <div class="col-md-3 text-center"><a href="javascript:void(0);">
                                    <?php echo $this->Html->image("/assets/images/menu-destination.jpg",array('class'=>'img-fluid','alt'=>"Destinations"), array('escape'=>false));?>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-item-link" href="javascript:void(0);">
                        Holiday themes
                        <svg class="caret ml-2" id="Capa_4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 213.333 213.333" style="enable-background:new 0 0 213.333 213.333;" xml:space="preserve" width="15px" height="15px">
                            <g>
                                <g>
                                    <g>
                                        <polygon class="active-path" points="0,53.333 106.667,160 213.333,53.333   " data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></polygon>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                    <div class="submenu">
                        <div class="container mw-100">
                            <ul class="menu-icon">
                                <?php foreach($appholidaythemes as $key =>$appholidaytheme) { 
                                    ?>
                                <li>
                                    <?php 
                                        if(!empty($appholidaytheme['Holidaytheme']['holidayimage'])){
                                            $img =  $this->Html->image($appholidaytheme['Holidaytheme']['holidayimage'],array('class'=>'img-fluid','alt'=>$appholidaytheme['Holidaytheme']['name']));
                                        }
                                        else{
                                            $img =  $this->Html->image("https://via.placeholder.com/200x200.png?text=+Holiday",array('class'=>'img-fluid')); 
                                        }

                                    // $img = $this->Html->image($appholidaytheme['Holidaytheme']['holidayimage'],array('class'=>'img-fluid','alt'=>""), array('escape'=>true)); 
                                    echo $this->Html->link($img.'<h5>'.$appholidaytheme['Holidaytheme']['name'].'</h5>', array("controller"=>"packages","action"=>"filter",'slug'=>$appholidaytheme['Holidaytheme']['name']),array("escape"=>false,'title'=>$appholidaytheme['Holidaytheme']['name']));
                                    ?>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <?php echo $this->Html->link('Travel Articles',array('plugin'=>'','controller'=>'articles','action'=>'index','admin'=>false),array("class"=>"nav-item-link"));?>
                </li>
            </ul>
        </div>
        <!-- Mobile navigation-->
        <div class="mobile-navigation">
            <ul class="sidebar-nav">
                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Weekends</a></li>
                <li class="menu-link">
                    <a class="menu-link-item menu" href="javascript:;">Destination Guides</a>
                    <ul class="mob-submenu">
                        <li class="menu-link">
                            <a class="menu-link-item menu" href="javascript:;">Indian Destinations</a>
                            <?php if(isset($country_of_states['State']) && !empty($country_of_states['State'])) { ?>
                            <ul class="mob-submenu">
                                <?php foreach ($country_of_states['State'] as $state_k => $state_v) { ?>
                                <li class="menu-link">
                                    <?= $this->Html->link($state_v['name'],array('plugin'=>'','controller'=>'states','action'=>'citiesList','admin'=>false,'slug' => $state_v['slug']),array("class"=>"menu-link-item"));?>
                                </li>
                                <?php }?>
                            </ul>
                            <?php }?>
                        </li>
                        <!-- <li class="menu-link">
                            <a class="menu-link-item menu" href="javascript:;">International Destinations</a>
                            <ul class="mob-submenu">
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Cambodia</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Thailand</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Bali</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Singapore</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Malaysia</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Mauritius</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Maldives</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Australia</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Seychelles</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Italy</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Dubai</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">South Africa</a></li>
                                <li class="menu-link"><a class="menu-link-item" href="javascript:;">Europe</a></li>
                            </ul>
                        </li> -->
                    </ul>
                </li>
                <li class="menu-link">
                    <a class="menu-link-item menu" href="javascript:;">Holiday themes</a>
                    <ul class="mob-submenu">
                        <?php foreach($appholidaythemes as $key=>$appholidaytheme) { ?>
                            <li class="menu-link">
                            <?php
                            echo $this->Html->link($appholidaytheme['Holidaytheme']['name'], array("controller"=>"packages","action"=>"filter",'slug'=>$appholidaytheme['Holidaytheme']['name']),array("escape"=>false,'title'=>$appholidaytheme['Holidaytheme']['name'],'class'=>'menu-link-item'));
                            ?>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</header>