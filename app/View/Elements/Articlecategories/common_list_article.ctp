<div class="row artical-box mb-3">
      <?php if(!empty($value['Articlecoverimage']['namemedium']) && file_exists(ROOT.'/app/webroot'.$value['Articlecoverimage']['namemedium'])){ ?>
    <div class="artical-box-img col-lg-3">

       <?php $img = $this->Html->image($value['Articlecoverimage']['namemedium'],array('class'=>'img-fluid','alt'=>$value['title']), array('escape'=>true)); 
        echo $this->Html->link($img, array("controller"=>"articles","action"=>"view",'slug'=>$value['slug'],'id'=>$value['id']),array("escape"=>false,'title'=>$value['title']));
            ?>
    </div>
      <?php } ?> 
        <div class="artical-box-content col-lg-9">
            <h5><?php echo $this->Html->link( $value['title'], array("controller"=>"articles","action"=>"view",'slug'=>$value['slug'],'id'=>$value['id']),array("escape"=>false,'title'=>$value['title'],'class'=>'advice_link')); ?>
            </h5>
            <p class="mb-0"><?php echo $this->Text->truncate(strip_tags($value['body'],'<p><b>'),120,array('ellipsis'=>'..'));?></p>


            <?php echo $this->Html->link("Read More <i class='fas fa-caret-right'></i>", array("controller"=>"articles","action"=>"view",'slug'=>$value['slug'],'id'=>$value['id']),array("escape"=>false,'title'=>$value['title'],'class'=>"readmore")); ?>
        </div>
</div>
