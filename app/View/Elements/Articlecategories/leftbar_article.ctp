<div class="">
	<div class="card">
		<div class="card-header">Related Articles</div>
		
			<ul class="list-group list-group-flush">
				<?php    	
				foreach ($rest_article as $key => $value) { ?>
					<?php echo "<li class='list-group-item'>".$this->Html->link($value['Article']['title'], array("controller"=>"articles","action"=>"view",'slug'=>$value['Article']['slug'],'id'=>$value['Article']['id']),array("escape"=>false))."</li>"; ?>
				<?php } ?>
			</ul>
		
</div>