 <?php
 if ($this->Session->check('Message.flash')):
 	if(isset($this->Session->read('Message.flash')[0]['params']['class']) && $this->Session->read('Message.flash')[0]['params']['class'] != '' )
 	{
 		$class=$this->Session->read('Message.flash')[0]['params']['class'];
 	}
 	else
 	{
 		$class=' alert-info ';
 	}
 	?>
 	<?php
 	echo $this->Session->flash();
 	?>
 	<?php
 endif; 
 if(isset($errors))
 {
 	if (is_array($errors) && !empty($errors))
 	{
 		foreach($errors as $error)
 		{
 			if(isset($error[0]) && !empty($error[0]))
 			{
 				//	echo '<div class="alert alert-danger display-hide">'.$error[0].'</div>';
 			}
 			if(isset($error['auth']) && !empty($error['auth']))
 			{
 				echo '<div class="alert alert-danger display-hide">'.$error['auth'].'</div>';
 			}
 		}
 	}
 	else
 	{
 		echo (isset($errors['name'])?('<div class="alert alert-danger display-hide">'.$errors['name'].'</div>'):'');

 	}
 }
 ?>
 <script>
 $("document").ready(function(){
 	setTimeout(function(){
 		$("div.alert").fadeOut('slow');
 	}, 30000 ); 
 });
 </script>