<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->Html->charset(); ?>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Vacdoo</title>

	<?php
	echo $this->Html->meta('favicon.png','favicon.png',array('type' => 'icon'));
	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->Html->css(['/assets/css/bootstrap.min','/assets/css/main']);
	
	echo $this->Html->script(['/assets/js/jquery.min','/assets/js/popper.min','/assets/js/jquery-migrate-3.0.0','/assets/js/jquery-ui.min','/assets/js/jquery.nice-select.min','/assets/js/jquery.stellar.min','/assets/js/bootstrap.min']);

	echo $this->fetch('script');
	?>
	<script type="text/javascript">
	var wroot = '<?php echo Router::url('/');?>';
	</script>		
</head>
<body class="">		
	<?php 
	echo $this->element('default_header');		
	echo $this->fetch('content');
	echo $this->element('default_footer');			
	echo $this->element('sql_dump');
	?>

	<?php echo $this->Html->script([]); ?>	
</body>
</html>