<?php

/**
 * CakePHP ACL Link Helper
 *
 * Based on Joel Stein AclLinkHelper
 * http://bakery.cakephp.org/articles/joel.stein/2010/06/26/acllinkhelper
 *
 * @author      Shahril Abdullah - shahonseven
 * @link        https://github.com/shahonseven/CakePHP-Acl-Link-Helper
 * @package     Helper
 * @license     MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('FormHelper', 'View/Helper');
App::uses('AclComponent', 'Controller/Component');

class AclLinkHelper extends FormHelper {

    public $helpers = array('Session');
    public $userModel = 'User';
    public $primaryKey = 'id';
    public $groupModel = 'Group';
    public $groupPrimaryKey = 'id';
    public $routePrefix = [];

    public function __construct(View $View, $settings = array()) {
        parent::__construct($View, $settings);

        if (is_array($settings) && isset($settings['userModel'])) {
            $this->userModel = $settings['userModel'];
        }

        if (is_array($settings) && isset($settings['primaryKey'])) {
            $this->primaryKey = $settings['primaryKey'];
        }

        if (is_array($settings) && isset($settings['groupModel'])) {
            $this->groupModel = $settings['groupModel'];
        }

        if (is_array($settings) && isset($settings['groupPrimaryKey'])) {
            $this->groupPrimaryKey = $settings['groupPrimaryKey'];
        }

        if (is_array($settings) && isset($settings['routePrefix'])) {
            $this->routePrefix = $settings['routePrefix'];
        }
    }

    protected function _aclCheck($url, $options = array(), $appendCurrent = true) {
        $params = $url;
        if ($appendCurrent) {
            $url = array_merge($this->request->params, $url);
        }        

        /*$prefix = '';
        if (isset($url['prefix'])) {
            $prefix = Inflector::camelize($url['prefix']) . '/';
        }

        $plugin = '';
        if (isset($url['plugin'])) {
            $plugin = Inflector::camelize($url['plugin']) . '/';
        }

        $controller = '';
        if (isset($url['controller'])) {
            $controller = Inflector::camelize($url['controller']) . '/';
        }

        $action = 'index';
        if (isset($url['action'])) {
            $action = $url['action'];
        }*/

        $prefix = 'controllers/';

        $plugin = '';
        if (isset($url['plugin']) && $url['plugin']) {
            $plugin = Inflector::camelize($url['plugin']) . '/';
        }

        $controller = '';
        if (isset($url['controller'])) {
            $controller = Inflector::camelize($url['controller']) . '/';
        }

        if (isset($url['action'])) {
            $action = $url['action'];

            $matchedKeys = array_values(array_intersect(array_keys($params), $this->routePrefix));
            if (!empty($this->routePrefix) && !empty($matchedKeys)) {
                $action = $matchedKeys[0]."_".$action;
            }

            /*if (isset($url['prefix'])) {
                $action = $url['prefix']."_".$action;    
            }*/
        }

        $collection = new ComponentCollection();
        $acl = new AclComponent($collection);
        $aro = [$this->userModel => $this->Session->read('Auth.'.$this->userModel)]; //[$this->userModel => AuthComponent::user()];
        //$aco = $prefix . $plugin . $controller . $action;
        $aco = $prefix . $plugin . $controller . $action;
        //echo $aco."==>";
        //var_dump($acl->check($aro, $aco) || $this->_isWhiteListed($aco, $aro, $options));
        return $acl->check($aro, $aco) || $this->_isWhiteListed($aco, $aro, $options);
    }

    public function link($title, $url = null, $options = array(), $confirmMessage = null) {
        if ($this->_aclCheck($url)) {
            return $this->Html->link($title, $url, $options, $confirmMessage);
        }
        return '';
    }

    public function postLink($title, $url = null, $options = array(), $confirmMessage = false) {
        if ($this->_aclCheck($url)) {
            return parent::postLink($title, $url, $options, $confirmMessage);
        }
        return '';
    }

    /*
     * check if you have access by array url
     */
    public function aclCheck($url, $options = array(), $appendCurrent = true) {
        return $this->_aclCheck($url, $options, $appendCurrent);
    }


    protected function _isWhiteListed($url, $aro, $options = array()) {
        $isWhiteListed = false;
        if (isset($this->_View->viewVars['whiteListPermissions'])) {
            $whiteListPermissions = $this->_View->viewVars['whiteListPermissions'];
            if (in_array($url, $whiteListPermissions)) {
                $isWhiteListed = true;
            }
        }
        if (!empty($options) && isset($options['whitelist'])) {
            $aroGroupId = $aro[$this->userModel][$this->groupModel][$this->groupPrimaryKey];
            if (in_array($aroGroupId, $options['whitelist'])) {
                $isWhiteListed = true;    
            }
        }
        return $isWhiteListed;
    }
}