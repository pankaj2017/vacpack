<?php

App::uses('AppHelper', 'View/Helper');

class MenuHelper extends AppHelper {
	public $helpers = array('Html', 'AclLink' => [
		'userModel' => 'Admin',
		'groupModel' => 'Admingroup',
		'routePrefix' => ['admin', 'report']
	]);

	public function create($items, $options = []) {
		$menuHtml = '';
		$divider = isset($options['divider']) ? $options['divider'] : '';
		$menuHtml .= '<div class="main-menu-content"><ul class="main-navigation">'.$divider;

		foreach ($items as $k => $item) {
			$param = isset($item['param']) ? $item['param'] : [];
			$before = $after = $href = $liClass = $singleitem ='';
			$linkClass = $this->_getLinkClass($item, $options);

			if (isset($item['divider'])) {
				$before = isset($item['divider']['before']) ? $item['divider']['before'] : '';
				$after = isset($item['divider']['after']) ? $item['divider']['after'] : '';
			}
			$icon = isset($item['icon']) ? $item['icon']." " : '';
			$label = $icon . $item['label'];
			
			if (isset($item['url']) &&
				$this->AclLink->aclCheck($item['url'], $param)) {
				$url = $this->Html->url($item['url']);
				$href = ' href="'.$url.'"';
				$show = true;
			} else {
				$show = $this->_checkAnyChildIsAllowed($item);
			}

			$child = $this->_getChilds($item, $options);
			if ($this->_checkAnyChildActive($item)) {
				$liClass = 'class="active"';
			}
			if(!isset($item['childs'])){
				$singleitem = 'single-item';
				// pr("code");
			}

			if ($show) {
				$menuHtml .= $before.'<li class="nav-item '.$singleitem.'" '.$liClass.'>';
				$menuHtml .= '<a '.$href.' '.$linkClass.'>'.$label.'</a>';
				$menuHtml .= $child.'</li>'.$after;	
			}			
		}

		$menuHtml .= '</ul></div>';

		return $menuHtml;
	}

	private function _getChilds($item, $options = []) {
		$childHtml = $linksHtml = '';		
		if (!empty($item['childs'])) {
			$childs = $item['childs'];						
			foreach ($childs as $key => $child) {
				$param = isset($child['param']) ? $child['param'] : [];
				$count = $this->_getCount($child, $options);
				$icon = isset($child['icon']) ? $child['icon'] : '';
				$label = $icon . $child['label'] . $count;				
				$linkClass = $this->_getLinkClass($child);

				if ($this->AclLink->aclCheck($child['url'], $param)) {					
					$url = $this->Html->url($child['url']);
					$linksHtml .= '<li class="has-class">';
					$linksHtml .= '<a href="'.$url.'" '.$linkClass.'>'.$label.'</a>';
					$linksHtml .= '</li>';	
				}				
			}

			//$areaExpanded = 'false';
			$ulClass = '';
			if ($this->_checkAnyChildActive($item)) {
				//$areaExpanded = 'true';
				$ulClass .= ' open';
			}

			$childHtml .= '<ul class=" tree-1 '.$ulClass.'">';
			$childHtml .= $linksHtml;
			$childHtml .= '</ul>';
		}

		return $childHtml;
	}

	private function _checkAnyChildActive($item) {
		$anyChildActive = false;
		if (!empty($item['childs'])) {
			$childs = $item['childs'];
			foreach ($childs as $key => $child) {
				$linkClass = $this->_getLinkClass($child, [], true);
				if (!$anyChildActive && in_array('active', $linkClass)) {
					$anyChildActive = true;	
				}
			}
		}
				// pr($anyChildActive);die;
		return $anyChildActive;
	}

	private function _checkAnyChildIsAllowed($item) {
		$anyChildAllowed = false;
		if (!empty($item['childs'])) {
			$childs = $item['childs'];
			foreach ($childs as $key => $child) {
				$param = isset($child['param']) ? $child['param'] : [];
				if (!$anyChildAllowed && $this->AclLink->aclCheck($child['url'], $param)) {
					$anyChildAllowed = true;	
				}
			}
		}
		return $anyChildAllowed;
	}

	private function _isCurrentUrl($url) {
		$request = Router::getRequest();
		$isCurrent = ($request->here == $url) ? true : false;
		return $isCurrent;
	}

	private function _getLinkClass($item, $options = [], $needArray = false) {
		$linkClasses = [];
		$linkClass = '';
		if (isset($options['link_class'])) {
			$linkClasses[] = $options['link_class'];
		}

		if (isset($item['link_class'])) {
			$linkClasses[] = $item['link_class'];	
		}

		if (isset($item['url']) 
			&& $this->_isCurrentUrl($this->Html->url($item['url']))) {
			$linkClasses[] = 'active';
			$linkClasses[] = 'current';
		}

		if (isset($item['active']) && isset($this->_View->viewVars[$item['active']])
			&& !in_array('active', $linkClasses)) {
			$linkClasses[] = 'active';
		}

		if ($needArray) {
			return $linkClasses;
		}

		if (!empty($linkClasses)) {
			$linkClass = 'class="'.implode(" ", $linkClasses).'"';
		}

		return $linkClass;
	}

	private function _getCount($item, $options = []) {
		$countHtml = '';

		if (!empty($options['counts']) && isset($item['count_key']) 
			&& isset($options['counts'][$item['count_key']])) {
			$count = $options['counts'][$item['count_key']];
			$countHtml .= '<span class="singlerowcount hide-menu label label-rouded label-warning pull-right"> '.$count.'</span>';
		}

		return $countHtml;
	}
}