<?php
App::uses('HtmlHelper', 'View/Helper');
class CdnHelper extends HtmlHelper {
    // Add your code to override the core HtmlHelper
    public function image($path, $options = array()) {
        //$path = 'http://cdn.localhost/'.$path;
		$path = 'https://d34pa849skkxsw.cloudfront.net/'.$path;
        return parent::image($path, $options);
    }
	public function css($path, $options = array()) {
        //$path = 'http://cdn.localhost/'.$path;
		$path = 'https://d34pa849skkxsw.cloudfront.net/'.$path;
        return parent::css($path, $options);
    }
	
	public function script($path, $options = array()) {
        //$path = 'http://cdn.localhost/'.$path;
		$path = 'https://d34pa849skkxsw.cloudfront.net/'.$path;
        return parent::script($path, $options);
    }
}
?>