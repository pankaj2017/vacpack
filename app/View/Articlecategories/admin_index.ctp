<?php echo $this->element("admin/admin_sidebar"); ?>	
<div class="main-body">
	<div class="page-wrapper">
		<div class="page-header">
			<div class="page-header-title"><h4>Travel article categories</h4></div>
		</div>
		<div class="page-body">
			<div class="row">
				<div class="col-md-12 col-xl-12">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-md-10">
									<h5>Latest article categories on vacdo</h5>
								</div>
								<div class="col-md-2 text-right">
							<?php echo $this->Html->link("Add New",array('plugin'=>'','controller'=>'articlecategories','action'=>'create','admin'=>true),array('class'=>'btn btn-outline-info btn-sm'))?>
								</div>
							</div>
						</div>
						<?php
                        if(isset($categories) && !empty($categories))
                        {

                        function treeForm(array &$elements, $parentId = 0, $elkey = null) {
                            $branch = array();

                            foreach ($elements as $key=>$element) {
                                if ($element[$elkey]['parent_id'] == $parentId) {
                                    $children = treeForm($elements, $element[$elkey]['id'], $elkey);
                                    if ($children) {
                                        $element['children'] = $children;
                                    }
                                    $branch[$element[$elkey]['id']] = $element;
                                    unset($elements[$key]);
                                }
                            }
                            return $branch;
                        }

                        //Build Menu       
                        function convertToMenu($arr, $elmkey) {
                            echo "<ul id='tree3'>";
                            foreach ($arr as $val) {
                                    if (!empty($val['children'])) {
                                            echo "<li>";
                                            echo "<a href='".'articlecategories/edit/'.$val[$elmkey]['id']."'>".$val[$elmkey]['name']."</a>";
                                            echo "&nbsp;&nbsp;";
                                            echo "<a href='".'articlecategories/delete/'.$val[$elmkey]['id']."'>"."<i class='fa fa-trash'></i>"."</a>";
                                            convertToMenu($val['children'], $elmkey);
                                            echo "</li>";
                                    } else {
                                            echo "<li>";
                                            echo "<a href='".'articlecategories/edit/'.$val[$elmkey]['id']."'>".$val[$elmkey]['name']."</a>";
                                            echo "&nbsp;&nbsp;";
                                            echo "<a href='".'articlecategories/delete/'.$val[$elmkey]['id']."'>"."<i class='fa fa-trash'></i>"."</a>";
                                            echo "</li>";
                                    }
                            }
                            echo "</ul>";
                        }


                            $menuTree = treeForm($categories, 0, 'Articlecategory'); // Preparing $categories.
                            convertToMenu($menuTree, 'Articlecategory');// changing to list, you can add <a> inside it.
                        }else{
                            echo "No Articles Category found, Please create a Articles category";
                        }
                    ?>
					</div><!-- card-->
				</div><!--col-md-12-->
			</div><!--row-->
		</div><!--page-body-->		
	</div><!--page-wrapper-->
</div><!--main-body-->

<script type="text/javascript">

  $(document).ready(function(){

  $.fn.extend({
    treed: function (o) {
      
      var openedClass = 'fa fa-chevron-down';
      var closedClass = 'fa fa-chevron-right';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch> i > a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});



$('#tree3').treed({openedClass:'fa fa-chevron-right', closedClass:'fa fa-chevron-down'});

  });
</script>