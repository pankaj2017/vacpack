<div class="container">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
		<li class="breadcrumb-item" style="">
		<?php
			echo $this->element('breadcrumb');
			echo $this->Html->getCrumbs('</li><li class="breadcrumb-item">',  array('text'=>'Vacdoo','title'=>'Vacpack')); 
		?>
		</li>
	  </ol>
	</nav>
</div>
		
<div class="faq-tabs section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <!-- Accordion -->
                <div class="accordion mb-30 faq-box" id="accordionExample">
                    <?php
                        $i=1; 
                        foreach ($categorylist as $key => $value) {
                            
                    ?>
                    <div class="card">
                        <div class="card-header">
                            
                            <a class="rounded-0 text-left w-100 text-decoration-none <?php if($i>1){echo 'collapsed';}?>" type="button" data-toggle="collapse" data-target="#collapseOne<?php echo $i;?>" aria-expanded="<?php echo ($i==1) ? 'true': 'false'; ?>" aria-controls="collapseOne<?php echo $i;?>"><?php echo $value['Articlecategory']['name'] ?><i class="fas fa-chevron-down float-right"></i> <i class="fas fa-chevron-up float-right"></i></a>
                          
                        </div>
                        <div id="collapseOne<?php echo $i;?>" class="collapse  <?php if($i==1){echo 'show';}?>" data-parent="#accordionExample">
                            <div class="card-body">
                                <ul>
                                    <?php 
                                    foreach ($value['ChildArticlecategory'] as $key_1 => $value_1) {
                                    ?>
                                    <li class="">
                                        <?php
                                        echo $this->Html->link($value_1['name'], array(
                                        'controller' => 'articles',
                                        'action' => 'index',
                                        'slug'=>$value_1['slug'],
                                        'id'=>$value_1['id'],
                                        ));
                                        ?>
                                    </li>
                                    <?php 
                                    }

                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php      
                    $i++;
                    }

                    ?>
                </div>
                <!-- Accordion end -->
            </div>
            <div class="col-lg-9">
                <div class="artical-area">

                    <?php
                    $flag = false;
                    foreach ($cat as $cat_key => $cat_value) { ?>
                        <?php if(isset($cat_value['Article']) && !empty($cat_value['Article'])){ ?>
                        <div class="card mb-3">
                            
                        <h3 class="card-header">
                            <?php echo  $cat_value['Articlecategory']['name']; ?>
                             <span class="float-right">
                             <?php echo $this->Html->link('View All', array("controller"=>"articles","action"=>"index",'slug'=> $cat_value['Articlecategory']['slug'],'id'=>$cat_value['Articlecategory']['id']),array("escape"=>false,'title'=>$cat_value['Articlecategory']['name'],'class'=>'badge badge-success')); ?>         
                             </span>
                        </h3>
							<div class="card-body">
                            <?php  foreach ($cat_value['Article'] as $key => $value) { 
                                 $flag = true;
                                echo  $this->element('Articlecategories/common_list_article', array('value'=>$value));
                                ?>
                                
                            <?php } ?>
							</div><!--card-body-->
                        </div>
                            <?php }?>
                    <?php } ?>

                    <?php 
                    if($flag == false) {
                        echo "No Articles Available";
                    }
                    ?>                

                </div>
                                
            </div>
        </div>
    </div>
</div> 