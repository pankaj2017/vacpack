<?php $this->assign('title', 'India'); ?>

<div class="jumbotron">
  <h1 class="text-center"><?php echo $countries_data['Country']['name']; ?> Tourism Guide</h1>
</div>

  <div class="container">
  	<div class="row">
		<div class="col-md-12 col-center m-auto">
			<h3><?php echo "About ".$countries_data['Country']['name']." Tourism"; ?>  </h3>
            <p><?php echo $countries_data['Country']['about'];?></p>
            <label> <strong><?php echo "Top States In ".$countries_data['Country']['name']; ?></label>

			<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
				<!-- Carousel indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
				</ol>   
				<!-- Wrapper for carousel items -->
				<div class="carousel-inner">
					<?php if(isset($countries_data['State'])){
		            	$states_array = array_chunk($countries_data['State'],3,false);
		            	if(isset($states_array)){ 
		            	foreach ($states_array as $state_key => $state_value) { ?>
						<div class="item carousel-item <?php echo $state_key == 0 ? 'active' : '';?>">
							<div class="row">
								<?php  foreach ($state_value as $key => $value) { ?>
								<div class="col-sm-4">
									<div class="thumb-wrapper">
										<div class="img-box">
											<?php 
												if(!empty($value['image'])){
													echo $this->Html->image($value['image'],array('class'=>'img-responsive img-fluid card-img')); 
												}
												else{
													echo $this->Html->image("https://via.placeholder.com/245x160.png?text=+State",array('class'=>'card-img-top')); 
												}
												 ?>
											
										</div>
										
										<h4><?php echo $this->Html->link($value['name'],array('plugin'=>'','controller'=>'states','action'=>'citiesList','admin'=>false,'slug' => $value['slug']),array("class"=>""));?></h4>
										<div class="thumb-content">
											<?php echo $this->Text->truncate($value['description'],100);?>
											
										</div>						
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
						<?php		
		            	}
		            	}
		            }

		            ?>
				</div>
				<!-- Carousel controls -->
				<a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
					<i class="fa fa-angle-left"></i>
				</a>
				<a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
					<i class="fa fa-angle-right"></i>
				</a>
			</div>
		</div>
	</div>
  
</div>