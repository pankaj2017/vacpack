<?php echo $this->element("admin/admin_sidebar"); ?>
<?php echo $this->Html->css('fileinput.min');echo $this->Html->script('fileinput.min'); ?>
<?php echo $this->Html->script('ckeditor/ckeditor'); ?>
<div class="main-body">
	<div class="page-wrapper">
		<div class="page-header">
			<div class="page-header-title"><h4>Travel Articles</h4></div>
		</div>
		<div class=""><?php echo $this->Session->flash(); ?> </div>
		<div class="page-body">
			<div class="row">
				<div class="col-md-12 col-xl-12">
					<section class="card">
						<header class="card-header">
						<div class="row">
								<div class="col-md-10">
									<h5 >Update this article</h5>
								</div>
								<div class="col-md-2 text-right">
							<?php echo $this->Html->link("Back to list",array('plugin'=>'','controller'=>'articles','action'=>'index','admin'=>true),array('class'=>'btn btn-outline-info btn-sm'))?>
								</div>
							</div>
						</header>					
						<div class="row">
							<div class="col-md-12">						
								<div class="card-body">
									<?php echo $this->Form->create('Article', array("url"=>array("controller"=>"articles",'action' => 'edit','admin'=>true),'type'=>'file'));?>
										<div class="row">
											<div class="col-md-4 form-group">
											<label class="">Category </label>
												<?php echo $this->Form->select('Article.articlecategory_id',$parent,array("class"=>"form-control","empty"=>"--Select Category--",'disabled' => $parent_disabled)); ?>
											</div>
											<div class="col-md-8 form-group">
											<label class="">Title </label>
												<?php echo $this->Form->text('Article.title',array("class"=>"form-control")); echo $this->Form->hidden('Article.id');?>
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
												<label for="slug">Slug</label>
													<?php echo $this->Form->text('Article.slug',array("class"=>"form-control","placeholder"=>"Enter Slug")); ?>
												<span class="help-block"><small>Will be automatically generated from your title, if left empty</small></span>
												</div>
											</div>									
											<div class="col-md-4 form-group">
												<label>Featured Image<span class="">*</span></label>
												<?php echo $this->Form->input('Articlecoverimage.image', array('type'=>'file',"class"=>"file", "data-max-file-count"=>"1", "label"=>false,'required'=>false)); ?>
											</div>
											<div class="col-md-4 form-group">
												<?php echo $this->Html->image($featured_image,array('class'=>'img-responsive'));echo $this->Form->hidden('Articlecoverimage.id',array());
												?>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
												<label for="slug">Meta Title</label>
													<?php echo $this->Form->text('Article.meta_title',array("class"=>"form-control","placeholder"=>"Enter Slug")); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
												<label for="slug">Meta description</label>
													<?php echo $this->Form->text('Article.meta_desc',array("class"=>"form-control","placeholder"=>"Enter Slug")); ?>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 form-group">
												<label class="">Body</label>
												<?php echo $this->Form->textarea('Article.body', array("class"=>"form-control ckeditor")); ?>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<label>
												<?php echo $this->Form->checkbox('Article.active',array()); ?>&nbsp;Make it active</label>
											</div>
											<div class="col-md-6">
												<label>
												<?php echo $this->Form->checkbox('Article.fullwidth',array()); ?>&nbsp;Full Width</label>
											</div>
										</div>
										<?php /*<div class="row">
											<div class="col-md-6 form-group">
												<label class="">Tags </label>
												<?php echo $this->Form->text('Article.tags', array("class"=>"form-control")); ?>
											</div>
											
											<div class="col-md-6 form-group">
												Tags are keywords relevent to the Article. These tags will be useful to find this Article to viewers. Select 2 to 4 key words which describs best to your Article and separate by comma.
											</div>
										</div>*/?>
										<div class="form-group">
											<input name="" type="submit" value="Update" class="btn btn-success" />
										</div>									
									<?php echo $this->Form->end(); ?>
								</div>
							</div>
						</div>
					</section>		
				</div><!-- end of id welcome -->
			</div><!--/#content.span10-->
		</div>
	</div>
</div>
<script type="text/javascript">
CKEDITOR.replace('ArticleBody', {
toolbar: [[ 'Source' ],[ 'Bold', 'Italic','Underline','Subscript','Superscript'],[ 'NumberedList','BulletedList' ],[ 'Link', 'Unlink', 'Anchor'],[ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],[ 'Source']],
width: '',
height: '300',
});</script>
<script>$("#PostcoverimageImage").fileinput({showUpload: false, maxFileCount: 10, mainClass: "input-group-lg"});</script>