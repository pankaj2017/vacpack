<?php $this->assign('title', 'Vacation Tips | Vacdo.in'); ?>
<?php
echo $this->Html->meta(
    'description',
    'Get our latest and most engaging product and company announcements, as well as top business insurance advice and trends that are actually worth to read.',
    array('inline' => false)
);
?>
<div class="container">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
		<li class="breadcrumb-item" style="">
		<?php
			echo $this->element('breadcrumb');
			echo $this->Html->getCrumbs('</li><li class="breadcrumb-item">',  array('text'=>'Vacdoo','title'=>'Vacpack')); 
		?>
		</li>
	  </ol>
	</nav>
</div>
<div class="faq-tabs section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <!-- Accordion -->
                <div class="accordion mb-30 faq-box" id="accordionExample">
                    <?php
                        $i=1; 
                        foreach ($categorylist as $key => $value) {
                    ?>
                    <div class="card">
                        <div class="card-header">
                           
                            <a class="rounded-0 text-left w-100 text-decoration-none <?php if($i>1){echo 'collapsed';}?>" type="button" data-toggle="collapse" data-target="#collapseOne<?php echo $i;?>" aria-expanded="<?php echo ($i==1) ? 'true': 'false'; ?>" aria-controls="collapseOne<?php echo $i;?>"><?php echo $value['Articlecategory']['name'] ?><i class="fas fa-chevron-down float-right"></i> <i class="fas fa-chevron-up float-right"></i></a>
                         
                        </div>
                        <div id="collapseOne<?php echo $i;?>" class="collapse  <?php if($i==1){echo 'show';}?>" data-parent="#accordionExample">
                            <div class="card-body">
                                <ul>
                                    <?php 
                                    foreach ($value['ChildArticlecategory'] as $key_1 => $value_1) {
                                    ?>
                                    <li class="">
                                        <?php
                                        echo $this->Html->link($value_1['name'], array(
                                        'controller' => 'articles',
                                        'action' => 'filter',
                                        'category' =>$value_1['slug'],
                                        'postcategoryid' =>$value_1['id'],
                                        ));
                                        ?>
                                    </li>
                                    <?php 
                                    }

                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php      
                    $i++;
                    }

                    ?>
                </div>
                <!-- Accordion end -->
            </div>
            <div class="col-lg-9 col-sm-6 mb-4">
                <div class="artical-area">
                    <div class="card mb-3">
                    <?php if(isset($posts) && !empty($posts)){ ?>
                        <div class="card-header">
							<h3><?php echo $articlecategoryname['Articlecategory']['name']; ?></h3>
						</div>
						<div class="card-body">
                        <?php  
                        foreach ($posts as $key => $value) {
                        ?>                           
                            
                            <?php echo  $this->element('Articles/common_list_article', array('value'=>$value)); ?>   
                            
                        <?php      
                        }
                        ?>

						<?php     
						} else { 

							echo "No Articles Available";
						}?>
						</div>
                    </div>                    
                <?php if(isset($articlelist) && !empty($articlelist)){ ?>
                <ul class="pagination">
                    <?php echo $this->Paginator->first(' First ', null, null, array('class' => 'disabled')); ?>
                    <?php echo $this->Paginator->prev('Previous ', null, null, array('class' => 'disabled')); ?>
                    <?php echo $this->Paginator->numbers(array('separator' => '')); ?>
                    <?php echo $this->Paginator->next(' Next ', null, null, array('class' => 'disabled')); ?>
                    <?php echo $this->Paginator->last(' Last ', null, null, array('class' => 'disabled')); ?>
                </ul> 
                <?php } ?>
                    
                </div>
            </div>
        </div>
    </div>
</div>