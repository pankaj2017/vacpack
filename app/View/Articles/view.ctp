<?php 
$is_fullwidth = isset($article['Article']['fullwidth']) && $article['Article']['fullwidth'] == 1 ? true : false;
$content_class = $is_fullwidth ? "col-lg-12" : "col-lg-9";   

?>
<?php $this->assign('title', 'Article'); ?>
<div class="container">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb">
		<li class="breadcrumb-item" style="">
		<?php
			echo $this->element('breadcrumb');
			echo $this->Html->getCrumbs('</li><li class="breadcrumb-item">',  array('text'=>'Vacdoo','title'=>'Vacpack')); 
		?>
		</li>
	  </ol>
	</nav>
</div>
<div class="artical-area artical-details-area">
  <div class="container">
    <div class="row">
        <div class="<?php echo $content_class; ?> article-content">
            <div class="card">
				<div class="card-header">
					<h1> <?php echo $article['Article']['title']; ?></h1>
				</div>
				
				<div class="card-img-top">
            <?php 
			if(!empty($article['Articlecoverimage']['name']) && file_exists(ROOT.'/app/webroot'.$article['Articlecoverimage']['name'])){ 
				echo $this->Html->image($article['Articlecoverimage']['name'],array('class'=>'img-fluid','alt'=>$article['Article']['title']), array('escape'=>true)); 
            }?>
				</div><!--card-img-top-->
				<div class="card-body text-justify">
            <?php echo $article['Article']['body']; ?>
				</div><!--card-body-->
			</div><!--card-->
        </div>
        <?php 

        if(!$is_fullwidth){
			if(isset($rest_article) && count($rest_article) > 0){ ?>
			<div class="col-lg-3">
				<?php echo $this->element('Articlecategories/leftbar_article'); ?>
			</div>
			<?php }
        }
        ?>
    </div>
  </div>
</div>