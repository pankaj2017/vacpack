<?php 
$is_fullwidth = isset($articlecategories['Article']['is_fullwidth']) && $articlecategories['Article']['is_fullwidth'] == 1 ? true : false;
$content_class = $is_fullwidth ? "col-lg-12" : "col-lg-9";   

?>
<?php $this->assign('title', 'Article'); ?>
<div class="container">
    <div class="page-title-main text-center">
        <!-- <h1 class="text-left customesize"><?php echo $articlecategories['Article']['title'];?></h1>
        <hr> -->
    </div>
</div>
<div class="artical-area artical-details-area">
  <div class="container">
    <div class="row">
    	<div class="<?php echo $content_class; ?> article-content">
            <h1 class="my-3"> <?php echo $articlecategories['Article']['title']; ?></h1>
          
            <?php if(!empty($articlecategories['Articlecoverimage']['namemedium']) && file_exists(ROOT.'/app/webroot'.$articlecategories['Articlecoverimage']['namemedium'])){ 
                 echo $this->Html->image($articlecategories['Articlecoverimage']['namemedium'],array('class'=>'img-fluid','alt'=>$articlecategories['Article']['title']), array('escape'=>true)); 
            }
            echo "<br><br>";
             ?>
            <?php echo $articlecategories['Article']['body']; ?>      
        </div>
        <?php 

        if(!$is_fullwidth){
        if(isset($rest_article) && count($rest_article) > 0){ ?>
        <div class="col-lg-3 mt-3">
            <?php echo $this->element('admin/Articlecategories/leftbar_article'); ?>
        </div>
        <?php } 
        }

        ?>
    </div>
    
  </div>
</div>