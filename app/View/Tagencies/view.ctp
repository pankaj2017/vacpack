<?php echo $this->Html->meta('canonical', Router::url('/', true).'Travelagency/'.Inflector::slug(strtolower($travelagency['Tagency']['name']),'-'),array('rel'=>'canonical', 'type'=>null, 'title'=>null, 'inline' => false)); ?>
<?php $this->assign('title',$travelagency['Tagency']['name']);?>
<section class="section-agency pb-10" id="agency-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-4 order-md-2 text-center">
                <?php if(!empty($travelagency['Tagency']['imagesmall'])){
                echo $this->Html->image($travelagency['Tagency']['imagesmall'], array('class'=>'img-responsive'));
                echo $this->Html->meta(array('property'=>'og:image','type'=>'meta', 'content'=>substr(Router::url('/', true), 0, -1).'travelagency/'.$travelagency['Tagency']['imagesmall'], 'rel' => null),NULL, array('inline'=>false));
                
                }else{
                echo $this->Html->image('company.png', array('class'=>'img-fluid'));
                }
                echo $this->Html->meta(array('property'=>'og:type','type'=>'meta', 'content'=>'website', 'rel' => null),NULL, array('inline'=>false));
                echo $this->Html->meta(array('property'=>'og:url','type'=>'meta', 'content'=>Router::url('/', true).$travelagency['Tagency']['name'], 'rel' => null),NULL, array('inline'=>false));
            ?> 
            </div>
            <div class="col-md-8 order-md-1">
                <h2 class="c-blue"><?php echo $travelagency['Tagency']['name'] ?></h2>
                <div class="text-justify"><?php echo $travelagency['Tagency']['description']; ?></div>
                
                <a class="btn vacdoo-bg-light-grey text-white mt-3" href="javascript:;">Learn More</a>
            </div>
        </div>
    </div>
</section>
<section class="section-agency mb-10">
    <div class="container">
        <div class="col-12">
            <h2 class="c-blue agency-title">The team</h2>
        </div>
        <?php if(isset($travelagency['Travelagent']) && !empty($travelagency['Travelagent'])) { ?>
        <div class="row">
            
             <?php foreach ($travelagency['Travelagent'] as $travelagency_k => $travelagency_v) { ?>
            <div class="col-md-6">
                <div class="team-block">
                    <div class="row">
                        <div class="col-md-5">
                            <?php 
                                if(!empty($travelagency_v['Travelagentprofile']['userimage'])){
                                    echo $this->Html->image($travelagency_v['Travelagentprofile']['userimage'],array('class'=>'img-fluid','alt'=>$travelagency_v['Travelagentprofile']['userimage'])); 
                                }
                                else{
                                    echo $this->Html->image("https://via.placeholder.com/300x300.png?text=Image",array('class'=>'img-fluid')); 
                                } 
                            ?>
                        </div>
                        <div class="col-md-7">
                            <h5 class="team-title"><?php echo $travelagency_v['firstname'].' '. $travelagency_v['lastname']?></h5>
                            <p class="teamprofile"><?php echo $travelagency_v['phone'];?></p>
                            <p class="teamprofile"><?php echo $travelagency_v['email_address'];?></p>
                            <p><?php //echo $this->Text->truncate($travelagency_v['Travelagentprofile']['aboutme'],90);?></p>
                            <ul class="social-icons blue-icon">
                                <li>
                                    <a href="javascript:void(0);">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <g>
                                                <path class="active-path" d="m297.277344 508.667969c-2.132813.347656-4.273438.667969-6.421875.960937 2.148437-.292968 4.289062-.613281 6.421875-.960937zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m302.398438 507.792969c-1.019532.1875-2.039063.359375-3.058594.535156 1.019531-.175781 2.039062-.347656 3.058594-.535156zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m285.136719 510.339844c-2.496094.28125-5.007813.53125-7.527344.742187 2.519531-.210937 5.03125-.460937 7.527344-.742187zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m290.054688 509.738281c-1.199219.160157-2.40625.308594-3.609376.449219 1.203126-.140625 2.410157-.289062 3.609376-.449219zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m309.367188 506.410156c-.898438.191406-1.800782.382813-2.703126.566406.902344-.183593 1.804688-.375 2.703126-.566406zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m326.664062 502.113281c-.726562.207031-1.453124.402344-2.179687.605469.726563-.203125 1.453125-.398438 2.179687-.605469zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m321.433594 503.542969c-.789063.207031-1.582032.417969-2.375.617187.792968-.199218 1.585937-.40625 2.375-.617187zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m314.589844 505.253906c-.835938.195313-1.679688.378906-2.523438.566406.84375-.1875 1.6875-.371093 2.523438-.566406zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m277.527344 511.089844c-1.347656.113281-2.695313.214844-4.046875.304687 1.351562-.089843 2.699219-.191406 4.046875-.304687zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m512 256c0-141.363281-114.636719-256-256-256s-256 114.636719-256 256 114.636719 256 256 256c1.503906 0 3-.03125 4.5-.058594v-199.285156h-55v-64.097656h55v-47.167969c0-54.703125 33.394531-84.476563 82.191406-84.476563 23.367188 0 43.453125 1.742188 49.308594 2.519532v57.171875h-33.648438c-26.546874 0-31.6875 12.617187-31.6875 31.128906v40.824219h63.476563l-8.273437 64.097656h-55.203126v189.453125c107.003907-30.675781 185.335938-129.257813 185.335938-246.109375zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m272.914062 511.429688c-2.664062.171874-5.339843.308593-8.023437.398437 2.683594-.089844 5.359375-.226563 8.023437-.398437zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m264.753906 511.835938c-1.414062.046874-2.832031.082031-4.25.105468 1.417969-.023437 2.835938-.058594 4.25-.105468zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                            </g>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <g>
                                                <path class="active-path" d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm116.886719 199.601562c.113281 2.519532.167969 5.050782.167969 7.59375 0 77.644532-59.101563 167.179688-167.183594 167.183594h.003906-.003906c-33.183594 0-64.0625-9.726562-90.066406-26.394531 4.597656.542969 9.277343.8125 14.015624.8125 27.53125 0 52.867188-9.390625 72.980469-25.152344-25.722656-.476562-47.410156-17.464843-54.894531-40.8125 3.582031.6875 7.265625 1.0625 11.042969 1.0625 5.363281 0 10.558593-.722656 15.496093-2.070312-26.886718-5.382813-47.140624-29.144531-47.140624-57.597657 0-.265624 0-.503906.007812-.75 7.917969 4.402344 16.972656 7.050782 26.613281 7.347657-15.777343-10.527344-26.148437-28.523438-26.148437-48.910157 0-10.765624 2.910156-20.851562 7.957031-29.535156 28.976563 35.554688 72.28125 58.9375 121.117187 61.394532-1.007812-4.304688-1.527343-8.789063-1.527343-13.398438 0-32.4375 26.316406-58.753906 58.765625-58.753906 16.902344 0 32.167968 7.144531 42.890625 18.566406 13.386719-2.640625 25.957031-7.53125 37.3125-14.261719-4.394531 13.714844-13.707031 25.222657-25.839844 32.5 11.886719-1.421875 23.214844-4.574219 33.742187-9.253906-7.863281 11.785156-17.835937 22.136719-29.308593 30.429687zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                            </g>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <g>
                                                <path class="active-path" d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm-74.390625 387h-62.347656v-187.574219h62.347656zm-31.171875-213.1875h-.40625c-20.921875 0-34.453125-14.402344-34.453125-32.402344 0-18.40625 13.945313-32.410156 35.273437-32.410156 21.328126 0 34.453126 14.003906 34.859376 32.410156 0 18-13.53125 32.402344-35.273438 32.402344zm255.984375 213.1875h-62.339844v-100.347656c0-25.21875-9.027343-42.417969-31.585937-42.417969-17.222656 0-27.480469 11.601563-31.988282 22.800781-1.648437 4.007813-2.050781 9.609375-2.050781 15.214844v104.75h-62.34375s.816407-169.976562 0-187.574219h62.34375v26.558594c8.285157-12.78125 23.109375-30.960937 56.1875-30.960937 41.019531 0 71.777344 26.808593 71.777344 84.421874zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                            </g>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <svg xmlns="http://www.w3.org/2000/svg" height="512px" viewBox="0 0 512 512" width="512px">
                                            <g>
                                                <path class="active-path" d="m305 256c0 27.0625-21.9375 49-49 49s-49-21.9375-49-49 21.9375-49 49-49 49 21.9375 49 49zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m370.59375 169.304688c-2.355469-6.382813-6.113281-12.160157-10.996094-16.902344-4.742187-4.882813-10.515625-8.640625-16.902344-10.996094-5.179687-2.011719-12.960937-4.40625-27.292968-5.058594-15.503906-.707031-20.152344-.859375-59.402344-.859375-39.253906 0-43.902344.148438-59.402344.855469-14.332031.65625-22.117187 3.050781-27.292968 5.0625-6.386719 2.355469-12.164063 6.113281-16.902344 10.996094-4.882813 4.742187-8.640625 10.515625-11 16.902344-2.011719 5.179687-4.40625 12.964843-5.058594 27.296874-.707031 15.5-.859375 20.148438-.859375 59.402344 0 39.25.152344 43.898438.859375 59.402344.652344 14.332031 3.046875 22.113281 5.058594 27.292969 2.359375 6.386719 6.113281 12.160156 10.996094 16.902343 4.742187 4.882813 10.515624 8.640626 16.902343 10.996094 5.179688 2.015625 12.964844 4.410156 27.296875 5.0625 15.5.707032 20.144532.855469 59.398438.855469 39.257812 0 43.90625-.148437 59.402344-.855469 14.332031-.652344 22.117187-3.046875 27.296874-5.0625 12.820313-4.945312 22.953126-15.078125 27.898438-27.898437 2.011719-5.179688 4.40625-12.960938 5.0625-27.292969.707031-15.503906.855469-20.152344.855469-59.402344 0-39.253906-.148438-43.902344-.855469-59.402344-.652344-14.332031-3.046875-22.117187-5.0625-27.296874zm-114.59375 162.179687c-41.691406 0-75.488281-33.792969-75.488281-75.484375s33.796875-75.484375 75.488281-75.484375c41.6875 0 75.484375 33.792969 75.484375 75.484375s-33.796875 75.484375-75.484375 75.484375zm78.46875-136.3125c-9.742188 0-17.640625-7.898437-17.640625-17.640625s7.898437-17.640625 17.640625-17.640625 17.640625 7.898437 17.640625 17.640625c-.003906 9.742188-7.898437 17.640625-17.640625 17.640625zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                                <path class="active-path" d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm146.113281 316.605469c-.710937 15.648437-3.199219 26.332031-6.832031 35.683593-7.636719 19.746094-23.246094 35.355469-42.992188 42.992188-9.347656 3.632812-20.035156 6.117188-35.679687 6.832031-15.675781.714844-20.683594.886719-60.605469.886719-39.925781 0-44.929687-.171875-60.609375-.886719-15.644531-.714843-26.332031-3.199219-35.679687-6.832031-9.8125-3.691406-18.695313-9.476562-26.039063-16.957031-7.476562-7.339844-13.261719-16.226563-16.953125-26.035157-3.632812-9.347656-6.121094-20.035156-6.832031-35.679687-.722656-15.679687-.890625-20.6875-.890625-60.609375s.167969-44.929688.886719-60.605469c.710937-15.648437 3.195312-26.332031 6.828125-35.683593 3.691406-9.808594 9.480468-18.695313 16.960937-26.035157 7.339844-7.480469 16.226563-13.265625 26.035157-16.957031 9.351562-3.632812 20.035156-6.117188 35.683593-6.832031 15.675781-.714844 20.683594-.886719 60.605469-.886719s44.929688.171875 60.605469.890625c15.648437.710937 26.332031 3.195313 35.683593 6.824219 9.808594 3.691406 18.695313 9.480468 26.039063 16.960937 7.476563 7.34375 13.265625 16.226563 16.953125 26.035157 3.636719 9.351562 6.121094 20.035156 6.835938 35.683593.714843 15.675781.882812 20.683594.882812 60.605469s-.167969 44.929688-.886719 60.605469zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                                            </g>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
           
        </div><!--row-->
    <?php } ?>
    </div><!--container -->
</section>
<section class="section-agency bg-light pb-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="c-blue agency-title">Packages by agency</h2>
            </div>
        </div>
        <?php if(isset($travelagency['Packag']) && !empty($travelagency['Packag'])) { ?>

        <div class="row">
            <?php foreach ($travelagency['Packag'] as $packag_k => $packag_v) { ?>
                <div class="col-md-4">
                    <div class="packages-block">
                        <div class="package-image">
                            <?php if(!empty($packag_v['pkgimage']) && file_exists(ROOT.'/app/webroot'.$packag_v['pkgimage'])) {  ?>
                            <?php echo $this->Html->image($packag_v['pkgimage'],array("class"=>"img-fluid","alt"=>$packag_v['title'])); ?>
                           
                            <?php }else{
                                echo $this->Html->image("https://via.placeholder.com/453x300.png?text=+Package",array('class'=>'card-img-top')); 
                            } ?>
                        </div>
                        <div class="package-detail">
                            <div class="package-header">
                                <div class="">
                                    <h5><?php echo $packag_v['title'] ?></h5>
                                </div>
                                <div class=""><span class="c-blue"><?php echo $packag_v['numberofdays']." Days - ".$packag_v['numberofnights']." Nights" ?></span></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="package-body">
                                <p><?= $this->Text->truncate($packag_v['description'],100); ?></p>
                            </div>
                        </div>
                        <div class="package-footer">
                        <?php echo $this->Html->link("EXPLORE MORE INFO",array('plugin'=>'','controller'=>'packages','action'=>'view','admin'=>false,'slug' => Inflector::slug($packag_v['title']),'id' => $packag_v['id']),array("class"=>"btn btn-blue"));
                            ?>      
                        </div> 
                    </div>
                </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
</section>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<?php 
echo $this->fetch('script_execute');?>

<div class="container">
    <h4 class="mb-20 text-color">Rating</h4>
    <div class="row row-cols-lg-3 row-cols-md-3 row-cols-sm-2 row-cols-1">
    <?php
        $options['model']='Tagency';
        if($this->Session->read('Auth.User.id')) {
            $options['showForm'] = true;
            echo $this->Ratings->display_multipleratings($travelagency,$options);
        } else {
            // pr("Ss");die;
            $options['showForm'] = false;       
            /*Only Show Ratings */
            echo $this->Ratings->getrating($travelagency,$options);
        }       
    ?>
    </div>
    <h4 class="mb-20 text-color">Overall rating</h4>
    <div class="row col-md-4">
        <?php echo $this->Ratings->display_for($travelagency);?>
    </div>
</div>
<style type="text/css">
.get-in-touch-mainbox {
    border-radius: 0px;
}
.get-in-touch-box-left {
    /*background-color: #7da7a5;*/
}
.get-in-touch-box-right {
    /*background-color: #7da7a5;*/
}
.get-in-touch-img img {
    margin-left: -15px;
}
.contact-info h3 {
    font-weight: 700;
}
.contact-info p {
    text-decoration: none;
    /*color: rgba(255,255,255,0.6);*/
}
.contact-info p a {
    text-decoration: none;
    /*color: rgba(255,255,255,0.6);*/
}
.contact-info p a:hover {
    /*color: #fff;*/
}
@media (max-width: 991px) {
    .contact-info {
        padding-right: 15px;
    }
}
@media (max-width: 575px) {
    .get-in-touch-img {
        display: none;
    }
    .contact-info, .contact-form {
        padding: 15px !important;
    }
}
</style>
<section class="">
    <div class="container">
        <div class="row">
            <div class="col-12 mt-3">
                <h2 class="c-blue agency-title">Get in touch</h2>
            </div>
        </div>
        <div class="get-in-touch-block shadow p-3 mb-5 bg-white">
            <div class="row row-cols-lg-2 row-cols-md-1 row-cols-sm-1 row-cols-1 no-gutters get-in-touch-mainbox overflow-hidden">
                <div class="col">
                    <div class="row h-100 get-in-touch-box-left">
                        <div class="col-lg-6 col-md-6 col-sm-6 overflow-hidden">
                        	 <div class="get-in-touch-img">
                             <?php echo $this->Html->image("/assets/svg/email.svg", ['alt' => 'contact_icon','class'=>'img-fluid','escape' => false, 'title' => 'contact']); ?>
                        	</div>
                        </div><!--md-sm-overflowhidden-->
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="contact-info pt-5">
                                <h5 class="text-info mb-5"><?php echo $travelagency['Tagency']['name'];?></h5>
                                <p class="mb-2"><a href=""><?php echo $travelagency['Tagency']['email_address'];?></a></p>
                                <p class="mb-2"><a href=""><?php echo $travelagency['Tagency']['phone'];?></a></p>
                                <p class="mb-2">5367 Conroy Road, Suite 300 Orlando, Florida 32811, USA</p>
                            </div>
                        </div>
                    </div><!--getintouchleft-->
                </div><!--col-->

                <div class="col">
                    <div class="p-5 h-100 get-in-touch-box-right">
                         <span id="Message"></span>
                        <?php  
                        echo $this->Form->create('Agencyinquiry', ['id' => 'agencyformsave', 'url' => ['controller' => 'agencyinquiries', 'action' => 'create'], 'type' => 'file']);?>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <?php 
                                    echo $this->Form->text('Agencyinquiry.fname',['class'=>'form-control','Placeholder'=>'First Name','required'=>true,'pattern'=>"^[a-zA-Z0-9]{2,50}$",'min'=>"2" ,'max'=>"50", 'maxlength'=>"50",'title'=>"Please enter First Name with Alpha Numeric with at least 2 and up to 50 characters only." ,'aria-required'=>true]);
                                    echo $this->Form->hidden('Agencyinquiry.travelagency_id',array('value'=>$agencyid));
                                    echo $this->Form->error('Agencyinquiry.fname',['class'=>'error']);
                                ?>
                            </div>
                            <div class="col-md-6 form-group">
                                <?php 
                                    echo $this->Form->text('Agencyinquiry.lname',['class'=>'form-control','Placeholder'=>'Last Name','required'=>true,'pattern'=>"^[a-zA-Z0-9]{2,50}$",'min'=>"2" ,'max'=>"50", 'maxlength'=>"50",'title'=>"Please enter Last Name with Alpha Numeric with at least 2 and up to 50 characters only." ,'aria-required'=>true]);
                                    echo $this->Form->error('Agencyinquiry.lname',['class'=>'error']);
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <?php 
                                    echo $this->Form->text('Agencyinquiry.email',['class'=>'form-control','Placeholder'=>'Email Address','required'=>'required','min'=>"3" ,'max'=>"50", 'maxlength'=>"50",'type'=>'email','label'=>'Email <span class="requiresign">*</span>']);
                                    echo $this->Form->error('Agencyinquiry.email',['class'=>'error']);
                                ?>
                            </div>
                            <div class="col-md-6 form-group">
                                <?php 
                                    echo $this->Form->text('Agencyinquiry.phone',['class'=>'form-control','Placeholder'=>'Phone Number','required'=>true,'min'=>"8",'max'=>"15",'maxlength'=>15,'title'=>"Please provide valid phone number" ,'aria-required'=>true]);
                                    echo $this->Form->error('Agencyinquiry.phone',['class'=>'error']);
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col form-group">
                                <?php 
                                    echo $this->Form->textarea('Agencyinquiry.message',['Placeholder'=>'Message','class'=>'form-control','rows'=>6,'required'=>false]);
                                    echo $this->Form->error('Agencyinquiry.message',array('class'=>'error'));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button class="btn btn-blue btn-block" type="submit">SEND MESSAGE</button>
                            </div>
                        </div>
                        <?php echo $this->Form->end();?>
                   
                    </div><!--getintouchright-->
                </div><!--col-->
                
            </div><!--get in tourch main box-->
        </div><!--get in touch block-->
    </div><!-- container -->
</section>
<script type="text/javascript">

    $(document).on('submit', '#agencyformsave', function(e) {

        e.preventDefault();

        var formdata = $('#agencyformsave').serializeArray();
        $.ajax({
            type: "post",
            url: wroot + "agencyinquiries/create",
            cache: false,
            dataType: 'text',
            data: formdata,
            success: function(results) {

                result = JSON.parse(results);
        
                if(result.success==true)
                {
                    $('#Message').append('<div class="alert alert-success" role="alert">'+result.message+'</div>')
                    $('#agencyformsave')[0].reset();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                   alert('Error'); 
            }
        });
        return false;
    });
</script>