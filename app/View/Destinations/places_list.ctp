<?php $this->assign('title', 'India'); ?>
<div class="jumbotron">
  	<h1 class="text-center"><?php echo $cities_data['Destination']['name']; ?> Tourism Guide</h1>
</div>
<div class="container">
	<div class="row">
        <div class="col-md-12">
        	<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
				  <a class="nav-link active" data-toggle="tab" href="#home">Overview</a>
				</li>
				 <li class="nav-item">
				  <a class="nav-link" data-toggle="tab" href="#besttime">Best Time</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" data-toggle="tab" href="#howtoreach">How To Reach</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" data-toggle="tab" href="#localfood">Local Food</a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div id="home" class="container tab-pane active"><br>
					<h3><?php echo "About ".$cities_data['Destination']['name']." Tourism"; ?>  </h3>
					<p><?php echo $cities_data['Destination']['about'];?></p>
					
				</div>
				<div id="besttime" class="container tab-pane fade"><br>
					<p><?php echo $cities_data['Destination']['besttime'];?></p>
				  
				</div>
				<div id="howtoreach" class="container tab-pane fade"><br>
					<p><?php echo $cities_data['Destination']['howtoreach'];?></p>
				  
				</div>
				<div id="localfood" class="container tab-pane fade"><br>
					<p><?php echo $cities_data['Destination']['localfood'];?></p>
				  
				</div>
			</div>

            <label> <strong><?php echo "Other Destinations ".$states_data['State']['name']; ?></strong></label>
	        <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
				<!-- Carousel indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
				</ol>   
				<!-- Wrapper for carousel items -->
				<div class="carousel-inner">
					<?php if(isset($states_data['Destination'])){
		            	$states_array = array_chunk($states_data['Destination'],3,false);
		            	if(isset($states_array)){ 
		            	foreach ($states_array as $city_key => $city_value) { ?>
						<div class="item carousel-item <?php echo $city_key == 0 ? 'active' : '';?>">
							<div class="row">
								<?php  foreach ($city_value as $key => $value) { ?>
								<div class="col-sm-4">
									<div class="thumb-wrapper">
										<div class="img-box">
											<?php 
												if(!empty($value['imagename'])){
													echo $this->Html->image($value['imagename'],array('class'=>'img-responsive img-fluid card-img')); 
												}
												else{
													echo $this->Html->image("https://via.placeholder.com/245x160.png?text=+city",array('class'=>'card-img-top')); 
												}
												?>
											
										</div>
										
										<h4>
											
											<?php echo $this->Html->link($value['name'],array('plugin'=>'','controller'=>'destinations','action'=>'placesList','admin'=>false,'slug1' => $states_data['State']['slug'],'slug2' => $value['slug']),array("class"=>"nav-link"));?>
										</h4>
										<div class="thumb-content">
											<?php echo $this->Text->truncate($value['about'],100);?>
											
										</div>						
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
						<?php		
		            	}
		            	}
		            }

		            ?>
				</div>
				<!-- Carousel controls -->
				<a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
					<i class="fa fa-angle-left"></i>
				</a>
				<a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
					<i class="fa fa-angle-right"></i>
				</a>
			</div>

			<label> <strong><?php echo "Places To Visit In ".$cities_data['Destination']['name']; ?></strong></label>

			<?php if(isset($cities_data['Place']) && !empty($cities_data['Place'])){ ?>
	        <div id="placeCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
				<!-- Carousel indicators -->
				<ol class="carousel-indicators">
					<li data-target="#placeCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#placeCarousel" data-slide-to="1"></li>
					<li data-target="#placeCarousel" data-slide-to="2"></li>
				</ol>   
				<!-- Wrapper for carousel items -->
				<div class="carousel-inner">
					<?php
		            	$cities_array = array_chunk($cities_data['Place'],3,false);
		            	if(isset($cities_array)){ 
		            	foreach ($cities_array as $place_key => $place_value) { ?>
						<div class="item carousel-item <?php echo $place_key == 0 ? 'active' : '';?>">
							<div class="row">
								<?php  foreach ($place_value as $key => $value) { ?>
								<div class="col-sm-4">
									<div class="thumb-wrapper">
										<div class="img-box">
											<?php 
												if(!empty($value['image'])){
													echo $this->Html->image($value['image'],array('class'=>'img-responsive img-fluid card-img')); 
												}
												else{
													echo $this->Html->image("https://via.placeholder.com/245x160.png?text=+city",array('class'=>'card-img-top')); 
												}
												 ?>
											
										</div>
										
										<h4>
											<?php echo $this->Html->link($value['name'],array('plugin'=>'','controller'=>'destinations','action'=>'placesList','admin'=>false,'slug1' => $cities_data['State']['name'],'slug2' => $value['name']),array("class"=>"nav-link"));?>
										</h4>
										<div class="thumb-content">
											<?php echo $this->Text->truncate($value['about'],180,
							    				array('ellipsis' => '...', 'exact' => false));?>
										</div>						
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
						<?php		
		            	}
		            	}
		            

		            ?>
				</div>
				<!-- Carousel controls -->
				<a class="carousel-control left carousel-control-prev" href="#placeCarousel" data-slide="prev">
					<i class="fa fa-angle-left"></i>
				</a>
				<a class="carousel-control right carousel-control-next" href="#placeCarousel" data-slide="next">
					<i class="fa fa-angle-right"></i>
				</a>
			</div> 
			<?php } ?>  

			<label> <strong><?php echo "Popular Packages";?></strong></label>
			<div class="row">
				<?php if(isset($package_data)){
					foreach ($package_data as $package_key => $package_value) { 
		            		?>
		            	
						<div class="col-sm-4">
				            <div class="card mb-3">
				                <?php 
									if(!empty($package_value['Package']['pkgimage'])){
										echo $this->Html->image($package_value['Package']['pkgimage'],array('class'=>'card-img-top card-img')); 
									}
									else{
										echo $this->Html->image("https://via.placeholder.com/245x160.png?text=+City",array('class'=>'card-img-top')); 
									}
									 ?>
				                <div class="card-body">
				                    <h4 class="card-title">
				                    	<?php echo $this->Html->link($package_value['Package']['title'],array('plugin'=>'','controller'=>'packages','action'=>'view','admin'=>false,'slug' => Inflector::slug($package_value['Package']['title']),'id' => $package_value['Package']['id']),array("class"=>""));?>
				                    	</h4>
				                    <p class="card-text">
				                    	<?php echo $package_value['Package']['numberofdays']." Days". " & ".$package_value['Package']['numberofnights']." Nights" ?> </p>
									<p><?php echo "Starting From: ₹".$package_value['Package']['price']; ?></p>
										<?php echo $this->Text->truncate($package_value['Package']['description'],100);?>
				                    </p>
				                    
				                </div>
				            </div>
						</div>
			    	<?php }
				}?>
			  	
			</div>
		</div>
    </div>
</div>