<?php echo $this->element("admin/admin_sidebar"); ?>
<?php echo $this->Html->css('fileinput.min');echo $this->Html->script('fileinput.min'); ?>
<?php echo $this->Html->script('ckeditor/ckeditor'); ?>
<div class="main-body">
	<div class="page-wrapper">
		<div class="page-header">
			<div class="page-header-title"><h4>Destinations</h4></div>
		</div>
		
		<div class="page-body">
			<div class="row">
				<div class="col-md-12 col-xl-12">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-md-10">
									<h5 >Latest destinations on vacdo</h5>
								</div>
								<div class="col-md-2 text-right">
							<?php echo $this->Html->link("Back to list",array('plugin'=>'','controller'=>'destinations','action'=>'index','admin'=>true),array('class'=>'btn btn-outline-info btn-sm'))?>
								</div>
							</div>
						</div>
						<div class="card-body">
							
							<?php echo $this->Form->create('Destination', array("url"=>array("controller"=>"destinations",'action' => 'edit','admin'=>true),'type'=>'file'));
							echo $this->Form->hidden('Destination.id'); ?>
								<div class="row">
									<div class="col-md-6 form-group">
									<label class="">Country </label>
										<?php echo $this->Form->select('Destination.country_id',$countries,array("class"=>"form-control","empty"=>"Select Country",'onchange'=>'getstates(this.value);')); ?>
									</div>
									<div class="col-md-6 form-group">
									<label class="">State </label>
									<?php 
								      if(empty($states)){$states=array();}
								      echo $this->Form->select('Destination.state_id',$states,['empty'=>'Select State','class'=>'form-control','required'=>true,'id'=>'stateid','required'=>true]);
								      echo $this->Form->error('Destination.state_id',['class'=>'error_msg']);
								      ?> 
								    </div>
								</div>
								<div class="row">
									<div class="col-md-6 form-group">
										<label class="">Dastination name </label>
											<?php echo $this->Form->text('Destination.name',array("class"=>"form-control")); ?>
									</div>
									<div class="col-md-6 form-group">
										<label for="slug">Slug</label>
											<?php echo $this->Form->text('Destination.slug',array("class"=>"form-control","placeholder"=>"Enter Slug")); ?>
										<span class="help-block"><small>Will be automatically generated from your title, if left empty</small></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4 form-group">
										<label>Destination Image<span class="">*</span></label>
										<?php 
										echo $this->Form->input('Destination.image', array('type'=>'file',"class"=>"file", "data-max-file-count"=>"1", "label"=>false)); ?>
									</div>
									<div class="col-md-4 form-group">
										<label>Destination Image<span class="">*</span></label>
										<?php 
										if(!empty($this->request->data['Destination']['imagename'])){
											echo $this->Html->image($this->request->data['Destination']['imagename'],array('class'=>'img-fluid')); ?>
										<?php } ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 form-group">
										<label class="">About destination</label>
										<?php echo $this->Form->textarea('Destination.about', array("class"=>"form-control ckeditor")); ?>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-6 form-group">
										<label class="">Local food to get here</label>
										<?php echo $this->Form->textarea('Destination.localfood',array("class"=>"form-control")); ?>
									</div>
									<div class="col-md-6 form-group">
										<label class="">How to reach to this destination</label>
										<?php echo $this->Form->textarea('Destination.howtoreach',array("class"=>"form-control")); ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 form-group">
										<label class="">Best time to visit</label>
										<?php echo $this->Form->textarea('Destination.besttime',array("class"=>"form-control")); ?>
									</div>
									<div class="col-md-6 form-group">
										<label class="">Things to do at this destination</label>
										<?php echo $this->Form->textarea('Destination.thingstodo',array("class"=>"form-control")); ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 form-group">
									<label class="">
										<?php echo $this->Form->checkbox('Destination.active',array()); ?>&nbsp;Make it active</label>
									</div>
								</div>
								<div class="form-group">
									<input name="" type="submit" value="Create" class="btn btn-success" />
								</div>									
							<?php echo $this->Form->end(); ?>
									
						</div><!--card-block-->
					</div><!--card-->
				</div><!--col-md-12 -->
			</div><!--row-->
		</div><!--page-body-->		
	</div><!--page-wrapper-->
</div><!--main-body-->
<script type="text/javascript">
CKEDITOR.replace( 'DestinationAbout', {
toolbar: [[ 'Source' ],[ 'Bold', 'Italic','Underline','Subscript','Superscript'],[ 'NumberedList','BulletedList' ],[ 'Link', 'Unlink', 'Anchor'],[ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ]],
width: '',
height: '300',
});</script>
<script>$("#DestinationImage").fileinput({showUpload: false, maxFileCount: 10, mainClass: "input-group-lg"});</script>
<script type="text/javascript">
	
function getstates(country_id)
    {
     $.ajax({
      type: "post",  
      url: wroot+"Destinations/getstates/",
      data: {country_id: country_id},
      dataType : 'json',
      beforeSend : function(){
        $("select#stateid").html("<option value=''>Loading...</option>");
      },   
      success: function(results) {
        $("#stateid").empty();
        var options='<option value="0">Select State</option>';
        $.each(results,
          function(key,value) {
            options += '<option value="' + key + '">' + value + '</option>';
          });
        $(options).appendTo('#stateid');
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
   }
</script>
