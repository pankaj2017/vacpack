<?php echo $this->element('admin/admin_sidebar');?>

<?php echo $this->Html->script(['jquery.dataTables.min','admin/date.format','admin/bootstrap-datepicker.min']);
echo $this->Html->css('admin/bootstrap-datepicker.min');
?>
<!--main content start-->
<div class="main-body">
	<div class="page-wrapper">
		<div class="page-header">
			<div class="page-header-title"><h4>Destination</h4></div>
		</div>
		<div class="page-body">
			<div class="row">
				<div class="col-md-12 col-xl-12">
				<div class=""><?php echo $this->Session->flash(); ?> </div>
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-md-10">
									<h5>Latest Destination on vacdo</h5>
								</div>
								<div class="col-md-2 text-right">
							<?php echo $this->Html->link("Add New",array('plugin'=>'','controller'=>'destinations','action'=>'create','admin'=>true),array('class'=>'btn btn-outline-info btn-sm'))?>
								</div>
							</div>
						</div>
					
						<div class="card-body">
							<div class="table-content">
            	 <?php echo $this->element('/admin/Destinations/search-form');?>
            			<div class="form-body ajax-dt">
									<table id="article-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
					                    <thead>
					                      <tr>                
					                        <th>Image</th>
					                        <th>Destination Name</th>
					                        <th>State Name</th>
                                  <!-- <th>Status</th> -->
					                        <th>Actions</th>
					                      </tr>
					                    </thead>
					                    <tbody>
					                    </tbody>
					                </table>
					            </div>
                  </div>
						</div><!--/card-body-->

					</div><!-- card-->
				</div><!--col-md-12-->
			</div><!--row-->
		</div><!--page-body-->		
	</div><!--page-wrapper-->
</div><!--main-body-->

<?php echo $this->Html->script(['admin/select2.full.min']); ?>
<?php echo $this->Html->script(['http://code.jquery.com/jquery-migrate-1.2.1.min.js']); ?>
<?php echo $this->Html->css(['admin/select2']); ?>
<script>
   var articleTable;

    $('#article-table').on('processing.dt', function (e, settings, processing) {
      if (processing) {      
        $('.dataTables_processing').parent().addClass('dataTables_processing_wrapper');
      } else {
        $('.dataTables_processing').parent().removeClass('dataTables_processing_wrapper');
      }
    });
	$(document).ready(function() {  
        $(".select2").select2({
        width: '100%'
        });

        var articleUrl = "?ajax=get-destinations-list";
     
        articleTable = $('#article-table').DataTable({
            "dom": '<"pull-right"l><r><"table-responsive"t>ip',
            "iDisplayLength": 25,
            "ajax": {
              "url": articleUrl,
              "dataType": "json",
              "data": function ( data ) {
                data.name = $('input[name="data[search-form][name]"]').val();
                data.sname = $('input[name="data[search-form][sname]"]').val();
                // data.iso_2 = $('input[name="data[search-form][iso_2]"]').val();
                // data.status = $('select[name="data[search-form][status]"]').val();
              }
            },
            "columns": [
              {"data": "Destination.image"},
              {"data": "Destination.name"},
              {"data": "State.name"},
              // {"data": "State.active", "bSortable": false},
              {"data": "Destination.actions", "bSortable": false},
            ],
            //"bDestroy": true,
            "aaSorting": [],
            "pagingType": "full_numbers",
            "serverSide": true,
            "processing": true,
            "serverMethod": "post",
            "fnDrawCallback": function(oSettings) {
              if (oSettings.fnRecordsDisplay() > oSettings._iDisplayLength) {
                $(oSettings.nTableWrapper).find('.dataTables_paginate').show();
              } else {
                $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
              }
            }
          }); 

        $('form[name="search-form"]').on("submit", function(e) {
            e.preventDefault();
            articleTable.ajax.reload();
        });
        $('#reset-search').on("click", function(e) {
            e.preventDefault();
            $('select.select2').val(null).trigger('change');
            $('form[name="search-form"]')[0].reset();
            articleTable.ajax.reload();
        });
        $('#date-range').datepicker({
            format: date_format,
            toggleActive: true,
            autoclose: true,
        });

        $('body').tooltip({
            selector: '[data-toggle="tooltip"]'
        });  
    });
function deleteme(id) 
{
  $( "#ArticleId").val(id);
  $( "#deletemodal").modal();
}
</script>
<div id="deletemodal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirm Delete</h4>
        <button type="button pull-right" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" >
        <?php
        echo $this->Form->create('Article',['url'=>['controller'=>'articles','action'=>'delete','admin'=>true]]);
        ?>
        <label>Reason to Delete</label><span class="text-danger">*</span>
        <?php
        echo $this->Form->text('Article.reasontodelete',['class'=>'form-control','required'=>true,'maxlength'=>'100']);
        echo $this->Form->input('Article.id',['type'=>'hidden','class'=>'form-control']);
        echo $this->Form->input('Article.deletedby',['type'=>'hidden','class'=>'form-control','value'=>$this->Session->read('Auth.Admin.id')]);
        echo $this->Form->input('Article.deletedon',['type'=>'hidden','class'=>'form-control','value'=>date("Y-m-d H:i:s")]);
        ?>
        <div class="modal-footer">
          <?php echo $this->Form->submit('Delete',["class"=>"btn btn-success"]);?>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        <?php echo $this->Form->end();?>
      </div>
    </div>
  </div>
</div>

</script>
