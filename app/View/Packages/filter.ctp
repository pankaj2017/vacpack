<?php 

$selectedprice = [];
$selecteddays = [];
if (isset($query['price_ranges']) && !empty($query['price_ranges'])) {
	$selectedprice = $query['price_ranges'];
}
if (isset($query['days']) && !empty($query['days'])) {
	$selecteddays = $query['days'];
}
?>

<div class="container">
    <div class="row">
        <div class="col-lg-3">
        	<div class="card">
			<?php echo $this->Form->create('', array('type' => 'post'));?>

				<div class="px-2 py-2">
					<div class="card-title vacdoo-bg-light-grey px-2 py-2"> Budget Per Person ( in INR )</div>
					
						<div class="px-3">
						<?php 
			            $prices = array('0-10000' => 'Less Than 10,000', '10000-20000' => '10,000-20,000', '20000-40000' => '20,000-40,000','40000-60000'=>'40,000-60,000','80000-200000'=>'Above 80,000');
			            echo $this->Form->input('price_ranges', array('options' => $prices, 'label' => false, 'div' => false, 'type' => 'select', 'multiple' => 'checkbox', 'class' => "",'selected' => $selectedprice));
			            ?>
						</div>
					
					
				
				
					<div class="card-title vacdoo-bg-light-grey px-2 py-2">Duration ( in Days ) </div>
						<div class="px-3">
					
				 	<?php $days = array('1-3' => '1 to 3', '4-6' => '4 to 6', '7-9' => '7 to 9','10-12'=>'10 to 12','13-31'=>'13 or more');
			            echo $this->Form->input('days', array('options' => $days, 'label' => false, 'div' => false, 'type' => 'select', 'multiple' => 'checkbox', 'class' => "",'selected' => $selecteddays)); ?>
					</div>
				</div>
				<div class="px-2 py-2">
					<input class="btn btn-primary" type="submit" value="Submit">
					<input class="btn btn-primary" type="reset" value="Reset">
				</div>
			</form> 
				
			</div><!-- card- -->
        </div>

        <div class="col-lg-9">
        	<div class="row">
			<?php if(isset($package_data)){
	            	foreach ($package_data as $package_key => $package_value) { ?>
	            	
				<div class="col-sm-4 col-md-4">
		            <div class="card mb-3">
		                
		                <?php 
							if(!empty($package_value['Package']['pkgimage'])){
								echo $this->Html->image($package_value['Package']['pkgimage'],array('class'=>'card-img-top card-img')); 
							}
							else{
								echo $this->Html->image("https://via.placeholder.com/245x160.png?text=+Package",array('class'=>'card-img-top')); 
							}
							 ?>
		                <div class="px-2 py-2">
		                    <h5 class="card-title">
	                    	 <?php echo $this->Html->link($package_value['Package']['title'],array('plugin'=>'','controller'=>'packages','action'=>'view','admin'=>false,'slug' => Inflector::slug($package_value['Package']['title']),'id' => $package_value['Package']['id']),array("class"=>""));?> 
	                    		
	                    	</h5>
		                    <p class="card-text">
		                    	<?php echo $package_value['Package']['numberofdays']." Days". " & ".$package_value['Package']['numberofnights']." Nights" ?> </p>
							<p><?php echo "Starting From: ₹".$package_value['Package']['price']; ?></p>
								<?php echo $this->Text->truncate($package_value['Package']['description'],100);?>
		                    </p>
		                </div>
		            </div>
		        </div>
		    <?php }}?>
			</div>
        </div>
    </div>
</div>

<!-- Shows the page numbers -->
<!-- <div class="paging">
<?php
echo $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled'));
echo $this->Paginator->numbers(array('separator' => ''));
echo $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled'));
?>
</div> -->
