
<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <H5><?php echo $package['Package']['title'];?><small>&nbsp;&nbsp;By&nbsp;&nbsp;<?php echo $this->Html->link($package['Travelagency']['name'],array("plugin"=>"","controller"=>"tagencies","action"=>"view","admin"=>false,$package['Travelagency']['id']));?></small></H5>
                </div>
                
                <div class="card-body">
                   
                    <div class="row">
                        <div class="col-md-4"><p><?php echo $this->Html->image($package['Package']['pkgimage'],array("class"=>"img-fluid"));?></div>
                        <div class="col-md-8">
                            <p class="text-justify"><?php echo $package['Package']['description'];?></p>
                        </div>
                        <div class="col-md-4">
                            <?php echo $this->Ratings->display_for($package);?>
                        </div>
                    </div>
                    <?php 
                    $originalprice = $package['Package']['price'];
                    if ($package['Package']['discount'] > 0){
                        $class='del';
                            
                        if($package['Package']['discounttype'] == 'PC'){
                            $packageprice = $package['Package']['price'] - floor($package['Package']['price']/$package['Package']['discount']);
                        }else{
                            $packageprice = $package['Package']['price'] - $package['Package']['discount'];
                        }
                    }else{
                        $packageprice = $package['Package']['price'];
                        $class='';
                    } ?>
                    <div class="row mb-4 mt-3 px-1 py-3 bg-secondary" style="color:#fff">
                        <div class="col-md-3">
                            <p class="<?php echo $class?>">
                                <h6>Price&nbsp;<br>
                                <?php echo $originalprice;?> </h6>
                            </p>
                        </div>

                        <?php if ($class=='del'){?>

                         <div class="col-md-3">
                            <p class="<?php echo $class?>">
                                <h6>discount&nbsp;<?php echo $package['Package']['discounttype']." ".$package['Package']['discount']?></h6>
                            </p>
                        </div>
                        
                        <div class="col-md-3">
                            <p>
                               <h6>Now &nbsp;<br><?php echo number_format((float)$packageprice, 2, '.', '');?></h6>
                            </p>
                        </div>
                        <?php  } ?>
                        <div class="col-md-3">
                            <p>
                               <h6><?php echo $package['Package']['numberofnights'];?>&nbsp;Nights&nbsp;&nbsp;<?php echo $package['Package']['numberofdays'];?>&nbsp;Days</h6>
                            </p>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-6"><H5 class="mb-2">Included in this package</h5>
                            <?php echo $package['Package']['inclusions'];?></div>
                        <div class="col-md-6"><h5 class="mb-2">Not Included in this package</h5>
                            <?php echo $package['Package']['exclusions'];?></div>
                    </div>
                    <div class="timeline-centered mt-3">
                        <?php foreach ($package['Itinerary'] as $key => $value) {?>
                        <article class="timeline-entry">
                            <div class="timeline-entry-inner">
                                <div class="timeline-icon bg-success">
                                    <i class="entypo-feather"></i>
                                </div>
                                <div class="timeline-label">
                                    <h2>Day <?php echo $value['daynumber']." ".$value['titleofday']?></h2>
                                    <div class="row">
                                    
                                    
                                        <div class="col-md-8 text-justify"><?php echo $value['abouttheday'];?></div>
                                        <div class="col-md-4">
                                        <?php if(!empty($value['imageofday'])){
                                            echo $this->Html->image($value['imageofday'],array('class'=>'img-fluid img-rounded full-width')); 
                                        } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <?php }?>
                    </div><!--timeline-centered-->

                </div><!--card-body-->
            </div><!--card -->
        </div><!-- col-lg-12-->
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <H5>Inquiry</H5>
                </div>
                
                <div class="card-body">
                    <!-- <div class="col-md-6"> -->
                    <span id="Message"></span>
                     <?php  
                        echo $this->Form->create('Packageinquiry', ['id' => 'packageformsave', 'url' => ['controller' => 'packageinquiries', 'action' => 'create'], 'type' => 'file']);
                     ?>   
                        <div class="form-group">
                            <?php 
                                echo $this->Form->text('Packageinquiry.fname',['class'=>'form-control','Placeholder'=>'First Name','required'=>true,'pattern'=>"^[a-zA-Z0-9]{2,50}$",'min'=>"2" ,'max'=>"50", 'maxlength'=>"50",'title'=>"Please enter First Name with Alpha Numeric with at least 2 and up to 50 characters only." ,'aria-required'=>true]);
                                echo $this->Form->hidden('Packageinquiry.packag_id',['value'=>$packagid]);
                                echo $this->Form->error('Packageinquiry.fname',['class'=>'error']);
                            ?>
                        </div>
                        <div class="form-group">
                            <?php 
                                echo $this->Form->text('Packageinquiry.lname',['class'=>'form-control','Placeholder'=>'Last Name','required'=>true,'pattern'=>"^[a-zA-Z0-9]{2,50}$",'min'=>"2" ,'max'=>"50", 'maxlength'=>"50",'title'=>"Please enter Last Name with Alpha Numeric with at least 2 and up to 50 characters only." ,'aria-required'=>true]);
                                echo $this->Form->error('Packageinquiry.lname',['class'=>'error']);
                            ?>
                        </div>
                        <div class="form-group">
                            <?php 
                                echo $this->Form->text('Packageinquiry.email',['class'=>'form-control','Placeholder'=>'Email Address','required'=>'required','min'=>"3" ,'max'=>"50", 'maxlength'=>"50",'type'=>'email','label'=>'Email <span class="requiresign">*</span>']);
                                echo $this->Form->error('Packageinquiry.email',['class'=>'error']);
                            ?>
                        </div>
                        <div class="form-group">
                            <?php 
                                echo $this->Form->text('Packageinquiry.phone',['class'=>'form-control','Placeholder'=>'Phone Number','required'=>true,'min'=>"8",'max'=>"15",'maxlength'=>15,'title'=>"Please provide valid phone number" ,'aria-required'=>true]);
                                echo $this->Form->error('Packageinquiry.phone',['class'=>'error']);
                            ?>
                        </div>
                        <div class="form-row">
                            <div class="col form-group">
                                <?php 
                                    echo $this->Form->textarea('Packageinquiry.message',['Placeholder'=>'Message','class'=>'form-control','rows'=>6,'required'=>false]);
                                    echo $this->Form->error('Packageinquiry.message',array('class'=>'error'));
                                ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <button class="btn btn-blue btn-block" type="submit">SEND MESSAGE</button>
                            </div>
                        </div>
                    <?php         
                        echo $this->Form->end();
                    ?>
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </div><!--row-->
</div>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>

<?php 
echo $this->fetch('script_execute');?>

<div class="container">
    <h4 class="mb-20 text-color">Rating</h4>
    <div class="row row-cols-lg-3 row-cols-md-3 row-cols-sm-2 row-cols-1">
    <?php
        $options['model']='Package';
        if($this->Session->read('Auth.User.id')) {
            $options['showForm'] = true;
            echo $this->Ratings->display_multipleratings($package,$options);
        } else {
            // pr("Ss");die;
            $options['showForm'] = false;       
            /*Only Show Ratings */
            echo $this->Ratings->getrating($package,$options);
        }       
    ?>
    </div>
</div>
<script type="text/javascript">

    $(document).on('submit', '#packageformsave', function(e) {

        e.preventDefault();

        var formdata = $('#packageformsave').serializeArray();
        $.ajax({
            type: "post",
            url: wroot + "packageinquiries/create",
            cache: false,
            dataType: 'text',
            data: formdata,
            success: function(results) {

                result = JSON.parse(results);
        
                if(result.success==true)
                {
                    $('#Message').append('<div class="alert alert-success">'+result.message+'</div>')
                    $('#packageformsave')[0].reset();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                   alert('Error'); 
            }
        });
        return false;
    });
</script>