<?php echo $this->element("admin/admin_sidebar"); ?>
<?php echo $this->Html->css('fileinput.min');echo $this->Html->script('fileinput.min'); ?>
<?php echo $this->Html->script('ckeditor/ckeditor'); ?>
<div class="main-body">
	<div class="page-wrapper">
		
		<div class="page-header">
			<div class="page-header-title"><h4>Places</h4></div>
		</div>
		
		<div class="page-body">
			<div class="row">
				<div class="col-md-12 col-xl-12">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-md-10">
									<h5 >Latest Places on vacdo</h5>
								</div>
								<div class="col-md-2 text-right">
							<?php echo $this->Html->link("Back to list",array('plugin'=>'','controller'=>'places','action'=>'index','admin'=>true),array('class'=>'btn btn-outline-info btn-sm'))?>
								</div>
							</div>
						</div>
						<div class="card-body">
							
							<?php echo $this->Form->create('Place', array("url"=>array("controller"=>"places",'action' => 'create','admin'=>true),'type'=>'file'));?>
								<div class="row">
									<div class="col-md-4 form-group">
									<label class="">Country </label>
										<?php echo $this->Form->select('Place.country_id',$countries,array("class"=>"form-control","empty"=>"Select Country",'onchange'=>'getstates(this.value);')); ?>
									</div>

									<div class="col-md-4 form-group">
									<?php if(empty($states)){$states=array();} ?>
									<label class="">State </label>
										<?php echo $this->Form->select('Place.state_id',$states,array("class"=>"form-control","empty"=>"Select State",'id'=>'stateid','onchange'=>'getdestination(this.value);')); ?>
									</div>

									<div class="col-md-4 form-group">
									<?php if(empty($destinations)){$destinations=array();} ?>
									<label class="">Destination </label>
										<?php echo $this->Form->select('Place.destination_id',$destinations,array("class"=>"form-control","empty"=>"Select Destination",'id' =>'destinationid')); ?>
									</div>

								</div>
								<div class="row">
									<div class="col-md-8 form-group">
									<label class="">Place name </label>
										<?php echo $this->Form->text('Place.name',array("class"=>"form-control")); ?>
									</div>
									<div class="col-md-12 form-group">
										<label>Place Image<span class="">*</span></label>
										<?php echo $this->Form->input('Place.image', array('type'=>'file',"class"=>"file", "data-max-file-count"=>"1", "label"=>false)); ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 form-group">
										<label class="">About destination</label>
										<?php echo $this->Form->textarea('Place.about', array("class"=>"form-control ckeditor")); ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 form-group">
									<label class="">
										<?php echo $this->Form->checkbox('Place.active',array()); ?>&nbsp;Make it active</label>
									</div>
								</div>	
								
								<?php  /*<div class="row">
									<div class="col-md-6 form-group">
										<label class="">Tags </label>
										<?php echo $this->Form->text('Destination.tags',array("class"=>"form-control")); ?>
									</div>
									
									<div class="col-md-6 form-group">
										Tags are keywords relevent to the post. These tags will be useful to find this post to viewers. Select 2 to 4 key words which describs best to your post and separate by comma.
									</div>*/?>
								</div>
								<div class="form-group">
									<input name="" type="submit" value="Create" class="btn btn-success" />
								</div>									
							<?php echo $this->Form->end(); ?>
									
						</div><!--card-block-->
					</div><!--card-->
				</div><!--col-md-12 -->
			</div><!--row-->
		</div><!--page-body-->		
	</div><!--page-wrapper-->
</div><!--main-body-->
<script type="text/javascript">
CKEDITOR.replace( 'PlaceAbout', {
toolbar: [[ 'Source' ],[ 'Bold', 'Italic','Underline','Subscript','Superscript'],[ 'NumberedList','BulletedList' ],[ 'Link', 'Unlink', 'Anchor'],[ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ]],
width: '',
height: '300',
});</script>
<script>$("#DestinationImage").fileinput({showUpload: false, maxFileCount: 10, mainClass: "input-group-lg"});</script>
<script type="text/javascript">
	
function getstates(country_id)
    {
     $.ajax({
      type: "post",  
      url: wroot+"Places/getstates/",
      data: {country_id: country_id},
      dataType : 'json',
      beforeSend : function(){
        $("select#stateid").html("<option value=''>Loading...</option>");
      },   
      success: function(results) {
        $("#stateid").empty();
        var options='<option value="0">Select State</option>';
        $.each(results,
          function(key,value) {
            options += '<option value="' + key + '">' + value + '</option>';
          });
        $(options).appendTo('#stateid');
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
   }

   function getdestination(state_id)
    {
     $.ajax({
      type: "post",  
      url: wroot+"Places/getdestination/",
      data: {state_id: state_id},
      dataType : 'json',
      beforeSend : function(){
        $("select#destinationid").html("<option value=''>Loading...</option>");
      },   
      success: function(results) {
        $("#destinationid").empty();
        var options='<option value="0">Select State</option>';
        $.each(results,
          function(key,value) {
            options += '<option value="' + key + '">' + value + '</option>';
          });
        $(options).appendTo('#destinationid');
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
   }
</script>