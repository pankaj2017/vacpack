<div class="body-content py-3">
	<div class="container">
		<div class="card">
			<div class="row">
				<div class="col-md-12">
					<h2 class="card-header">Terms of use</h2>
					<div class="card-body text-justify">
						<p>Welcome to vacdoo.com ("Website") and the ‘vacdoo' android and iOS app (collectively, “App”). The domain name of the Website and the App is owned by CBbytes (“we/ us/ our”), a company registered under the provisions of the Companies Act, 1956.
The customer’s (“you/ your/ traveler”) use of the Website, App, services and tools are governed by the following terms and conditions ("Terms of Use") including the applicable policies which are incorporated herein by way of reference and constitutes a valid agreement between you and us. If you transact on the Website or App, you shall be subject to the policies that are applicable, as displayed on the Website and App for such transaction. Your use of this Website and App constitutes your binding obligation and agreement to all such terms, conditions, and notices which are subject to amendment without any notice by CBbytes. Any usage of the Website or App is subject to the following Terms of Use:</p>
						<h4>Limitation on content usage:</h4>
						
						<p>Your use of any information or materials on this Website and App is entirely at your own risk, for which CBbytes shall not undertake any liability. It shall be your own responsibility to ensure that any products, services or information available through this Website or App, meet your specific requirements.
You are restricted from modifying, copying, distributing, transmitting, displaying, performing, reproducing, publishing, licensing, creating derivative works from, transferring, or selling any information, software, products or services obtained from this Website or App. Reproduction of content from the Website and App is prohibited save and except in accordance with the copyright notice, which forms part of these Terms of Use.
Unauthorized use of the Website or App may give rise to a claim for damages and/or be a criminal offence. You shall solely bear the consequences arising out of the breach of any of the terms given above. Further CBbytes reserves exclusive rights to alter, modify, and delete any of the contents without giving prior notice to any person.</p>
						<h4>Third party websites:</h4>
						<p>The Website and App contain materials which is either owned by, or licensed, to CBbytes. This material includes, but is not limited to, the design, layout, look, appearance and graphics of the content displayed on the Website and App. From time to time, the Website and App may also include links to other websites. These links are provided for your convenience for providing further useful information. They do not signify that we endorse the website(s). We, or our contractors, agents, owners, and employees are not responsible for the content or policies of such other websites to which our Website and App may link or use offers from. Please take time to learn about them as well.</p>
						<h4>Limitation of liability for fault not attributable to CBbytes:</h4>
						<p>You agree and acknowledge that CBbytes will have the right of indemnification for any loss that may be caused to CBbytes for any misuse of its Website or App, by you and CBbytes will have all rights to approach the appropriate court of law for enforcement of such rights.</p>
						<p>CBbytes and/or its parents, subsidiaries, affiliates, officers, directors, employees, agents or suppliers and their respective successors, heirs and permitted assigns will not be liable for any loss that you may suffer due to rejection of your visa, hotel cancellation policies and/or any other policy of third parties service providers who are involved in the product that you purchase from CBbytes.
It shall be your obligation to ensure that your passport is valid and eligible for undertaking international travel. CBbytes and/or its parents, subsidiaries, affiliates, officers, directors, employees, agents or suppliers and their respective successors, heirs and permitted assigns will not be liable for passport being declared ineligible for entry/exit during or prior to undertaking the international travel. Given below, are certain essential do’s and don’ts with respect to your passport:</p>
						<ul>
							<li>Validity of passports should be at least 6 months from date of return;</li>
							<li>Hand written passports will not be considered valid;</li>
							<li>Passports must have barcode;</li>
							<li>Passports should not be mutilated or tampered; and</li>
							<li>Name on passport should match with name on other documents provided for visa. </li>
						</ul>
						<h4>Force Majeure:</h4>
						<p>Neither CBbytes nor any of its parents, subsidiaries, affiliates, officers, directors, employees, agents or suppliers and their respective successors, heirs and permitted assigns shall be responsible for, or be deemed to be in default on account of, any failure to perform or due to delay in performance of any of its obligations hereunder, in whole or in part, if such performance is rendered impracticable by the occurrence of acts of war, whether declared or undeclared, sabotage, embargo, acts of terrorism, riot or other civil commotion, failure in transportation, act of any government or any court or administrative agency thereof, acts of God, fire, explosion, flood, earthquake, strike, act of government or other catastrophes, epidemics or quarantine restrictions, or other cause(s) beyond the reasonable control of CBbytes (“Force Majeure”). Both you and CBbytes shall use their best efforts to avoid, overcome and offset the effects of any cause or potential cause of an event of Force Majeure. Upon cessation of the cause of the Force Majeure, the Terms of Use given hereunder, shall again become fully operative. However, a Force Majeure event will not relieve you or CBbytes of the obligations accrued prior to the occurrence of the Force Majeure.</p>
						<h4>Transaction privities:</h4>
						<p>The Website and App are platforms that are offered to you for meeting and interacting with concerned agents and other service providers for organizing your travel. CBbytes is not, and cannot be a party to, or, control in any manner any transaction between such travel agents and other service providers. Hence, please note that:</p>
						<ul>
							<li>All commercial/contractual terms are offered by and agreed to between the travelers and travel agents alone. The commercial/contractual terms include without limitation terms relating to price, payment methods, payment terms, date, period and mode of delivery, warranties related to products and services and after sales services related to products and services. CBbytes does not have any control or does not determine or advise or in any way involve itself in the offering or acceptance of such commercial/contractual terms between the travelers and travel agents.</li>
							<li>You acknowledge that through the Website and App, CBbytes merely provides a platform which enables you to have access to various travel services offered by the travel agents. It is agreed that the contract for sale of any of the products or services shall be a strictly bipartite contract between you and the travel agent and CBbytes shall not be a privy to such contract.</li>
							<li>CBbytes, and/or its parents, subsidiaries, affiliates, officers, directors, employees, agents or suppliers and their respective successors, heirs and permitted assigns, shall not be or deemed to be responsible or be liable for any direct, indirect, punitive, incidental, special, consequential damages or any damages whatsoever including, but not limited to, damages for loss of use, data or profits, arising out of or in any way connected with the use or performance of this Website and App, or for any lack or deficiency of services provided by any person (including any airline, travel agent / tour operator, hotel, facility or similar agency), whom you shall engage or hire or appoint, pursuant to or resulting from, the content available on the Website and App.</li>
							<li>CBbytes does not make any representation or warranty as to the item-specifics (such as legal title, creditworthiness, identity, etc.) of any of its travel agents or other service providers. You are advised to independently verify the bona fides of any particular travel agent that you choose to deal with on the Website or App, and use your best judgment in that behalf.</li>
							<li>CBbytes shall not be liable for delays or inabilities in performance or non-performance in whole or in part of its obligations due to any causes that are not due to its acts or omissions and are beyond its reasonable control, such as Force majeure events or other similar causes, problems at airlines, rails, buses, hotels or transporters end. In such event, the affected traveler will be promptly given notice as the situation permits.</li>
						</ul>
						<h4>Privacy:</h4>
						<p>Your privacy is extremely important to us. Upon acceptance of these Terms of Use, you confirm that you have read, understood, and unequivocally accepted our <?php echo $this->Html->link("Privacy Policy",array("controler"=>"pages","action"=>"privacypolicies"));?>.</p>
						<h4>Ensuring Quality:</h4>
						<p>CBbytes shall ensure prompt redressed of any deficiency in service registered by a traveler. It may further, as per its discretion, provide credit vouchers of the travel agent and/or Holiday Triangle covering full or partial amount of the payment received from the traveler.</p>
						<h4>Contact Us:</h4>
						<p>Please send any questions or comments (including all inquiries related to copyright infringement) regarding this Website to contact@vacdoo.com.</p>
						<h4>Disclaimer:</h4>
						<p>The materials on vacdoo.com website are provided "as is". vacdoo.com makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, vacdoo.com does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.</p>
						<h4>Links:</h4>
						<p>vacdoo.com has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Holidify.com of the site. Use of any such linked web site is at the user's own risk.</p>
						<h4>Governing Law:</h4>
						<p>Any claim relating to vacdoo.com website shall be governed by the laws of the Republic of India without regard to its conflict of law provisions.</p>
						<h4>Grievance officer:</h4>
						<p>	Mr. FirstName LastName</p>
						<p>	CBbytes</p>
						<p>	362-Earth EON,</p>
						<p>	Sama Savli Road,</p>
						<p>	Vadodara-390008, Gujarat, India<p>
						<p>	<strong>Contact:</strong> 0123456789</p>
						<p>	<strong>E-mail:</strong> contact@vacdoo.com</p>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container -->
</div><!-- /.body-content -->