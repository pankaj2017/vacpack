<div class="body-content py-3">
	<div class="container">
		<div class="card">
			<div class="row">
				<div class="col-md-12">
					<h2 class="card-header">Privacy policies</h2>
					<div class="card-body text-justify">
						<p>CBbytes (“we/ us/ our”) values the trust which a customer (“you/ your”) places in us and our website, vacdoo.com (“Website”). Hence, we insist upon the highest standard of practices to ensure security of transactions and utmost privacy of customer information. We respect your need to understand how information furnished by you is being collected, used, disclosed, transferred and stored by us. In this regard, please read this privacy policy (“Policy”) to get familiarized about our information gathering and dissemination practice.</p>
						
						<p>We assure you of our firm commitment to your privacy and the protection of your priceless information. The Policy contains information about the Website and the Vacdo android and iOS app (such applications are collectively referred as the “App”). In order to endow you with the uninterrupted use of our services, we may collect and, in some circumstances, disclose information about you.</p>
						
						<p>If you do not agree to the terms in this Policy, we kindly ask you to leave the site.
If you have any questions or concerns, regarding this privacy policy, you should contact our Customer Support Desk at support@vacdoo.com.</p>
						
						<p>As you may know, a new European privacy regulation called General Data Protection Regulation ("GDPR") has come into effect on May 25, 2018. GDPR is designed to unify the current data protection privacy laws throughout the EU, and to enhance the rights of European residents to protect their personal information. The GDPR also applies to any business that offers its products or services to European residents.</p>
						<p>We collect and use your personal information when you avail our services. For the purpose of the General Data Protection Regulation (“GDPR”), we are the ‘data controller’. Our data protection officer can be contacted at dpo@vacdoo.com.
By accessing our Websites and utilizing our services, you acknowledge that you have read and understood this Privacy Policy and the information collection and handling practices outlined in it.</p>
						
						<h4>Overview</h4>
						<p>This Privacy Policy describes in detail our policy and practices regarding our collection, use and disclosure of your personal data.
We understand that providing information online involves a great deal of trust on your part. We take this trust very seriously, and make it a high priority to ensure the security and confidentiality of the personal data you provide us when you visit our Website or use our services.</p>
						<ol>
							<h5><li>Collection of your personal data</li></h5>
							<p>Personal data means any information about an individual from which that person can be identified, addresses, date of birth, password and payment information. It does not include data where the identity has been removed or otherwise anonymized (anonymous data).</p>
							<p>We collect your personally identifiable information including but not limited to your (i) name; (ii) e-mail address; (iii) contact number; (iv) residential address; (v) credit/debit/bank account details; (vi) internet protocol (“IP”) address, in the following circumstances:</p>
								<ul>
									<li>When you make a reservation, or opt for a package from our Website or App or through the aid of our customer service team;</li>
									<li>When you register with us, make a requisition for information pertaining to holiday packages, subscribe to our newsletters, enter competitions, share with us your experience or provide any other details in relation to a holiday package availed by you, send us queries or register for promotions;</li>
									<li>When you set up a free account on our Website or App or participate in surveys or provide us your feedback; and</li>
									<li>When you engage with us in any online or offline event or portray your interest on any page hosted by us on a third-party platform or location or voluntarily provide your details for our promotions.</li>
									<li>While you can browse certain sections of our Website or App, prior to registering as a member, certain activities (such as placing an order) require mandatory registration1. We may use your contact information for sending you (i) offers based on your previous orders and your interests, (ii) payment reminder notices, (iii) travel vouchers and (iv) updates on the travel sector through our newsletters. In the event, you do not wish to receive such information, you may unsubscribe through the facility in the email message you receive.</li>
								</ul>
							</ul>
							<h5><li>How we use personal data</li></h5>
							<p>We use personal information to provide the services that you request. To the extent we use your information including personal information to market our services to you, we will provide you the ability to opt-out of such uses.
We may use your personal information for the following purposes, including but not limited to: (i) resolve disputes; (ii) troubleshoot problems; (iii) promote a safe service; (iv) collect fees owed to us; (v) cater to consumer interests while providing our services, (vi) inform you about online and offline offers, products, services, and updates; (vii) customize your experience; (viii) detect and protect us against error, fraud and other criminal activity; (ix) enforce our terms and conditions; (x) conduct research and analysis for improvising customer experience; (xi) to conduct periodical audit; (xii) contact you to send important information or notices etc. and as otherwise described to you at the time of collection.</p>
							<p><strong>Marketing Promotions:</strong></p>
							<p>Marketing promotions, research and programs help us to identify your preferences, develop programs and improve user experience. CBbytes frequently sponsors promotions to give its Users the opportunity to win great travel and travel related prizes. Personal Information collected by us for such activities may include contact information and survey questions. We use such Personal Information to notify contest winners and survey information to develop promotions and product improvements.</p>
							<h5><li>Use of Demographic and Profile Data</li></h5>
							<p>In our efforts to continually improve our product and service offerings, we may, from time to time, collect and analyses demographic and profile data about your activity on our Website and App. We identify and use your IP address to help diagnose problems with our server, and to administer our Website and App. Your IP address is also used to help identify you and to gather broad demographic information. We may occasionally ask you to complete optional online surveys, which may require you to provide your contact information and demographic information (like zip code, age, or income level). We use this data to tailor your experience at our Website and App, providing you with content that we think you might be interested in and to display the relevant content according to your preferences.</p>
							<h5><li>With whom we share your Personal Information</li></h5>
							<p>We may share the personal information, obtained from you, with our other corporate entities and affiliates to:</p>
							<ul>
								<li>help detect and prevent identity theft</li>
								<li>fraud and other potentially illegal acts;</li>
								<li>co-relate related or multiple accounts to prevent abuse of our services;</li>
								<li>to facilitate joint or co-branded services that you request where such services are provided by more than one corporate entity.
Those entities and affiliates may not market to you as a result of such sharing of personal information by us, unless you explicitly opt-in. We may disclose personal information in the following circumstances:</li>
									<ul>
										<li>if required to do so by law or by law enforcement offices for investigating any civil or criminal offences; or</li>
										<li>if such disclosure is reasonably necessary to respond to subpoenas, court orders, or other legal process.</li>
										<li>to legal, financial or other professional advisors of CBbytes, or</li>
										<li>to enforce our terms of the policy;</li>
										<li>to respond to claims that an advertisement, posting or other content violates the rights of a third party; or</li>
										<li>to protect the rights, property or personal safety of the users of our Website and App, or the general public.</li>
										
									</ul>
				
							</ul>
							<p>We may share our data, including personally identifiable information about you, with our parent and/or subsidiaries or business partners that are committed to serving your online needs and related services, throughout the world. Such data will be shared for the sole purpose of enhancing your browsing experience. To the extent that these entities have access to your information, they will treat it as protectively as they treat information they obtain from their other members. You should expect that we would share some or all of your information in order to continue to provide the service and the entity with which such information is being shared, will be required to follow this Policy with respect to your personal information.</p>
							<h5><li>How long we keep your personal data</li></h5>
							<p>We will only retain your personal data for as long as necessary to fulfil the purposes we collected it for, including for the purposes of satisfying any legal, accounting or statutory reporting requirements.</p>
							<p>To determine the appropriate retention period for personal data, we consider the amount, nature and sensitivity of the personal data, the potential risk of harm from unauthorized use or disclosure or your personal data, the purposes for which we process your personal data and whether we can achieve those purposes through other means and the applicable legal requirements.</p>
							<p>We may retain certain information subsequent to the closing of your account, for example if it is necessary to fulfil our legal obligations or to exercise, to maintain security, to prevent fraud and abuse and to defend or enforce our rights.</p>
							<h5><li>Cookies</li></h5>
							<p>We use our as well as third parties data collection devices such as cookies on certain pages of our Website and App. Cookies are small files stored on your hard drive that assist us in providing services customized to your requirements, interests and preferences. We also offer certain features which are only available through the use of a cookie. Cookies may be used whether you choose to register with us or not. Third party vendors such as www.google.com (Google) may use cookies to serve advertisements based on your visits to this website. You may visit the website of the third party and choose to opt out of the use of cookies for interest-based advertising, if the third party offers such an option. We place both permanent and temporary cookies in your computer's hard drive. However, such cookies do not contain any of your personally identifiable information.</p>
							<h5><li>How we protect your personal data</li></h5>
							<p>CBbytes takes the protection of your Customer Data seriously but, unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your Customer Data, we cannot guarantee the security of your Customer Data transmitted through any channel, such as the Website; any transmission is at your own risk.</p>
							<h5><li>Notification of Modifications and Changes to the Policy</li></h5>
							<p>We reserve the right to change the terms of the Policy from time to time, as seem fit, without any intimation to you, and your continued use of the Website or App will signify your acceptance of any amendment to these terms. You are therefore advised to re-read the terms of the Policy on a regular basis. Should it be that you do not accept any of the modifications or amendments to the terms of the Policy, you may terminate your use of our services immediately.</p>
							<h5><li>Sharing of travelers’ photos and videos</li></h5>
							<p>By voluntarily submitting your travel photos or videos to Vacdoo.com after availing our services, either on our Website or App or through email or any other medium of communication to us, you grant to CBbytes, a non-exclusive, perpetual, worldwide, royalty-free, transferable, and sub-licensable, license to edit, telecast, re-run, reproduce, use, syndicate, print and distribute the materials you submit, or any portion thereof, in or in connection with CBbytes and any of its affiliates’ websites, programming, platforms and/or App (the manner, mode and extent of which are subject to change without notice to you), in any manner, and in any medium or forum. You represent and warrant to CBbytes that you have the full legal right, power and authority to grant the aforementioned license to CBbytes. You further agree that your submission(s), and the exercise of the rights granted herein, pursuant to the aforesaid terms, do not and will not violate any applicable laws or regulations, or cause a breach of any agreement(s) entered by you with any third parties, and that none of the submission(s) will infringe any copyright, invade any right of privacy, right of publicity, contain any libelous or slanderous material, or infringe or violate any other intellectual property right or other right (including common law rights) of any other person or entity.</p>
							<h5><li>Security Precautions</li></h5>
							<p>Our Website and App have stringent security measures in place to protect the loss, misuse, and alteration of the information under our control. Whenever you change or access your account information, we offer the use of a secure server. Once your information is in our possession, we adhere to strict security guidelines, protecting it against unauthorized access. As soon as the you declare the intent to avail any of the services offered by us on the Website or App, the Website or App, as the case may be, shall transfer the control to a specified and authentic payment gateway [like PayU, Citrus, EBS, PayTm ] that takes your credit card or debit card or other banking information and processes the payment transaction.</p>
							<h5><li>Choice to Opt-Out</li></h5>
							<p>If you so desire, you may opt-out of receiving non-essential (promotional, marketing- related) communications from us on behalf of our partners, and from us, in general, after setting up an account. You may also contact us, at support@vacdoo.com, to unsubscribe to our newsletters, publications or any other material.</p>
							<p>Further, please note that all information, including personal information provided by you is voluntary in nature. You may, at any time, withdraw your consent, in accordance with this Policy. However, in the event you withdraw consent to provide information, including personal information; you agree that you will no longer be permitted to use our services.</p>
							<p>You can also access, modify, correct and eliminate information provided by you which has been collected by us. We will not retain any personal information for a longer period than is required for the purposes for which the personal information may lawfully be used or is otherwise required under any other law for the time being in force. In the event you are not agreeable to the terms and conditions under this Policy and you are presently availing our services, we request you to kindly withdraw your consent or modify your information, by writing to us, at contact@vacdoo.com, and uninstall our App through which you may be availing our services.</p>
							<h5><li>Advertisement on website</li></h5>
							<p>We may use third-party advertising websites to serve advertisements when you visit our Website and App. Such websites may use information including your personal information in order to provide advertisements regarding goods and services that may be of interest to you. We cannot confirm or guarantee or take responsibility for the privacy practices or policies of such third-party advertising websites. We disclaim any liability towards any losses due to privacy infringement or other causes that you may suffer pursuant to access of such third-party advertising websites. We encourage you to read the privacy policies of the relevant third-party advertising websites prior to accessing them.</p>
							<h5><li>Transfer of information overseas</li></h5>
							<p>CBbytes’s Corporate Office is based in Vadodara, Gujarat, India. It is the place from where it carries its operations. Accordingly, Customer Data will be accessible to CBbytes’s offices and appointed agents based out of India. This means that Customer Data will be transferred to, and stored at, a destination outside of your country and outside the European Economic Area ("EEA"). If you are an EU resident, and we transfer Customer Data outside the EEA, this is done on the basis that it is necessary in discharge of the services rendered to you by CBbytes or its agents.</p>
							<h5><li>EU data subject rights</li></h5>
							<p>If you are a resident in the EU, you may have certain rights in relation to the Customer Data we hold about you, which we detail below. Some of these only apply in certain circumstances where CBbytes is holding authority of the data and legally/contractually allowed as per the local laws as set out in more detail below. We also set out how to exercise those rights.</p>
							<ul>
								<li>Right to information - including contact details to the DPO, the purposes for processing Personal Information and the rights of the individual.</li>
								<li>Right to access the Personal Information that are processed</li>
								<li>Right to erasure (” Right to be forgotten”)</li>
								<li>Right to rectification</li>
								<li>Right to restriction of processing</li>
								<li>Right to data portability of the Personal Information supplied to CBbytes by the EU resident</li>
								<li>Right to object (opt-out) to processing (unless CBbytes otherwise has compelling legitimate grounds)</li>
							</ul>
							<p>EU residents can exercise these rights by raising a request directly at contact@vacdoo.com.</p>
							<h5><li>Confidentiality</li></h5>
							<p>You further acknowledge that we may provide you certain information (apart from information available on the Website and App, which is under public domain) which is designated confidential by us. You agree that you shall not disclose such confidential information without our prior written consent. Further, your information shall be regarded as confidential and therefore, will not be divulged to any third party, unless if legally required to do so to the appropriate authorities or otherwise permitted in terms of this Policy.</p>
							<h5><li>Disclaimer</li></h5>
							<p>As a user of the Website and App, you understand and agree that you assume all responsibility and risk for your use of the Website and App, the internet generally, and the documents you post or access and for your conduct on and off the Website and App. Your use of site is subjected to our Policy. If you do not agree to this, we kindly request you not to use this website.</p>
						</ol>
						
					</div><!--card-body text-justify-->
				</div><!--col-md-12-->
			</div>
		</div>
	</div><!-- /.container -->
</div><!-- /.body-content -->