<div class="body-content py-3">
	<div class="container">
		<div class="card">
			<div class="row">
				<div class="col-md-12">
					<h2 class="card-header">About vacdoo.com</h2>
					<div class="card-body text-justify">
						<div class="row mb-4">
							<div class="col-md-3">
							<?php echo $this->Html->image("technology-focused.jpg",array("class"=>"img-fluid float-right"));?>
							</div>
							<div class="col-md-9">
							<p class="mb-2">vacdoo.com is an innovative and internet-based martke place for vacation planners to find great places and local travel agencies to get direct leads. At vacdoo.com we help vacationers to explore right destinations as per their preferences.
							</p><p class="mb-2">For vacationers it should not become a hassel to find appropriate stays and timely booking, And thus we encourage travel genencies to define best packages for everyone's need.</p>
							</div>
						</div>
						<div><p class="mb-2"><strong>Replace Robotic with the Relation</strong></p>
							<p>Today most of the big brands are communicating with customers by the automatic answering machines or softwares. Communication with customers through interactive voice respons (IVR) is also the popular way big travel brands uses.</p>
							<p>We believe vacationers should be responded by human being when it comes to auspicious occasion like vacation. And that is what we directly introduce vacationers to the travel agencies to have satisfactory one to one communication and build a life long relations.</p>
						</div>
						<div class="row mb-4 mt-4">
							<div class="col-md-9">
							<p class="mb-2"><strong>Our mission</strong> is to become best destination guide to right from backpackers to honeymoon couples and from adventure enthusiast to bounded families.</p>
							<p class="mb-2">Filter your vacation packages with destinations, holiday themes and select the best to make the vacation life long memory. You can take advantage of our travel articles on veriety of topics too. </p>
							
							</div>
							<div class="col-md-3">
							<?php echo $this->Html->image("clear-vision-statement.jpg",array("class"=>"img-fluid float-right"));?>
							</div>
						</div><!--row-->
						<div class="row mb-4">
							<div class="col-md-3">
								<?php echo $this->Html->image("clear-vision-statement.jpg", array("class"=>"img-fluid float-right"));?>
							</div>
							<div class="col-md-9">
							<p class="mb-2"><strong>We have clear vision</strong> to provide as much as possible information about incredible indian and overseas destinations. We wish travel agencies to create great reputation for their businesses by creating their agency profiles, staff expertise and getting direct travel leads, so no other outside obstracle involved to their business. </p>
							</div>
						</div><!--row-->
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container -->
</div><!-- /.body-content -->