<?php $this->assign('title', 'India'); ?>
<?php echo $this->Html->css(['/assets/css/owl.carousel.min','/assets/css/owl.theme.default.min'],null,array('inline'=>false));?>
<!-- <div class="jumbotron">
	<h1 class="text-center"><?php echo $states_data['State']['name']; ?> Tourism Guide</h1>
</div> -->
<div class="title-bar" id="banner" style="background-image: url(../assets/images/page-banner.jpg)">
  <h2><?php echo $states_data['State']['name']; ?> Tourism Guide</h2>
  <div class="breadcrumblist">
    <div class="container">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Library</a></li>
        <li class="breadcrumb-item active" aria-current="page">Kerala</li>
      </ol>
    </div>
  </div>
</div>

<div class="container mb-3">
    <div class="row">
        <div class="col-md-12">
		  <!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
			    <li class="nav-item">
			    	<a class="nav-link active" data-toggle="tab" href="#home">Overview</a>
			    </li>
		  	</ul>
		  
		  <!-- Tab panes -->
		  	<div class="tab-content">
			    <div id="home" class="tab-pane active"><br>
			      	<div class="col-md-12 text-justify m-auto">
						<?php echo $states_data['State']['description']; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div class="col-md-12"><h3>Destinations in <?php echo $states_data['State']['name']; ?></h3></div>
	</div>
	<div class="row" id="states_view">
		<div class="col-md-12">
			<div id="" class="trip-carousel owl-carousel owl-theme">
				
				<?php if(isset($states_data['Destination'])){
				$states_array = array_chunk($states_data['Destination'],3,false);
				if(isset($states_array)){ 
					foreach ($states_array as $city_key => $city_value) { ?>
					
						
						<?php  foreach ($city_value as $key => $value) { ?>
							
								<div class="card trip-block">
									<div class="trip-image">
										<?php 
											if(!empty($value['image'])){
												echo $this->Html->image($value['image'],array('class'=>'img-fluid card-img')); 
											}
											else{
												echo $this->Html->image("https://via.placeholder.com/245x160.png?text=+city",array('class'=>'card-img-top')); 
											}
											 ?>
									</div>
									
									<h4>
										<?php echo $this->Html->link($value['name'],array('plugin'=>'','controller'=>'destinations','action'=>'placesList','admin'=>false,'slug1' => $states_data['State']['slug'],'slug2' => $value['slug']),array("class"=>"nav-link"));?>
									</h4>
									<div class="card-body">
										<?php echo $this->Text->truncate($value['about'],100);?>
									</div>						
								</div>
							
								<?php 
								} ?>
						
						<?php		
						}
					}
				}?>
			</div>
		</div>
	</div>
</div>
<script>
 $(document).ready(function() {
      	// parallax
      	$(window).stellar({
      		responsive: true,
      		positionProperty: 'position',
      		horizontalOffset: 0,
      		verticalOffset: 0,
      		horizontalScrolling: false
      	});
      
      	// carousel
      	$('.trip-carousel').owlCarousel({
      		lazyLoad: true,
      		nav: true,
      		navText: ['<img src="../assets/svg/left_arrow.svg" alt="left_arrow" />', '<img src="../assets/svg/right_arrow.svg" alt="right_arrow" />'],
      		loop: true,
      		dots: false,
      		margin: 20,
      		responsive: {
      			0: {
      				items: 1,
      				margin: 15
      			},
      			768: {
      				items: 2
      			},
      			1200: {
      				items: 3
      			}
      		}
      		//- autoplay: true
      	});
      })
</script>
<?php echo $this->Html->script(['/assets/js/owl.carousel.min'],array('inline'=>false)); ?>
<section class="discover-tour-packages py-5">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <div class="title">
          <h2>Discover Tour Packages <span>in <?php echo $states_data['State']['name']; ?></span></h2>
        </div>
      </div>
    </div>
    <div class="row">
      <?php if(isset($states_data)){ //pr($states_data['Package']);
            foreach ($states_data['Package'] as $package_key => $package_value) { ?>
      <div class="col-12 col-md-6">
        <div class="packages-block">
          <div class="package-image">
            <?php 
            if(!empty($package_value['pkgimage'])){
              echo $this->Html->image($package_value['pkgimage'],array('class'=>'img-fluid')); 
            }
            else{
              echo $this->Html->image("https://via.placeholder.com/245x160.png?text=+State",array('class'=>'card-img-top')); 
            }
             ?>
            <!-- <img class="img-fluid" src="assets/images/discover-tour-1.png" alt="discover-tour-1" title="discover-tour-1"> -->
          </div>
          <div class="package-detail">
            <h3><?php echo $this->Html->link($package_value['title'],array('plugin'=>'','controller'=>'packages','action'=>'view','admin'=>false,'slug' => Inflector::slug($package_value['title']),'id' => $package_value['id']),array("class"=>""));?></h3>
            <p><?php echo "Duration :" .$package_value['numberofdays']." Days". " & ".$package_value['numberofnights']." Nights" ?></p>
            <p><?php echo "Starting From: ₹".$package_value['price']; ?></p>
            <div class="package-arrow"><a class="btn btn-orange" href="#"> 
                <object class="right-arrow" type="image/svg+xml" data="assets/svg/long-arrow-right.svg"></object></a></div>
          </div>
        </div>
      </div>
      <?php }
    }?>
    </div>
  </div>
</section>	
	    
<div class="container mt-3">
	<div class="row">
		<div class="col-md-12"><h3>Popular pacakges in <?php echo $states_data['State']['name']; ?></h3></div>
	</div>
	<div class="row">
		
		<?php if(isset($states_data)){ //pr($states_data['Package']);
            foreach ($states_data['Package'] as $package_key => $package_value) { ?>
            	
			<div class="col-md-4 trip-block">
	            <div class="card">
	                <div class="trip-image">
	                <?php 
						if(!empty($package_value['pkgimage'])){
							echo $this->Html->image($package_value['pkgimage'],array('class'=>'card-img-top card-img')); 
						}
						else{
							echo $this->Html->image("https://via.placeholder.com/245x160.png?text=+State",array('class'=>'card-img-top')); 
						}
						 ?>
					</div>
	                <div class="card-body">
	                    <h5 class="card-title">
                    	 <?php echo $this->Html->link($package_value['title'],array('plugin'=>'','controller'=>'packages','action'=>'view','admin'=>false,'slug' => Inflector::slug($package_value['title']),'id' => $package_value['id']),array("class"=>""));?> 
                    		
                    	</h5>
	                    <p class="card-text">
	                    	<?php echo $package_value['numberofdays']." Days". " & ".$package_value['numberofnights']." Nights" ?> </p>
						<p><?php echo "Starting From: ₹".$package_value['price']; ?></p>
						<?php echo $this->Text->truncate($package_value['description'],100);?>
	                    </p>
	                    
	                </div>
	            </div>
	        </div>
	    	<?php }
		}?>
	  	
	</div>
</div>