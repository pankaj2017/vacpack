<?php echo $this->element("admin/admin_sidebar"); ?>	
<!--main content start-->
<div class="main-body">
	<div class="page-wrapper">
		<div class="page-header">
			<div class="page-header-title"><h4>States</h4></div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class=""><?php echo $this->Session->flash(); ?> </div>
				
				<ul class="pagination">
				<?php
					echo $this->Paginator->prev(__('Prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
					echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
					echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
				?>
				</ul>
				<div class="card">
					<div class="card-header"><h6>List of States<?php echo $this->Html->link("Add New", array("plugin"=>"","controller"=>"states","action"=>"create","admin"=>true),array("class"=>"btn btn-outline-info float-right btn-sm")); ?></h6></div>
					<div class="card-body">

							
							<table class="table table-bordered table-striped table-condensed">
								<thead>
								<tr>
									<th width="20%">Image</th>
									<th width="20%">State Name</th>
									<th width="8%">Country Name</th>
									<th width="5%">Active</th>									
									<th width="15%">Action</th>								  
								</tr>
								</thead>							
								<tbody>
									<?php
										foreach($states as $state){	?>
										<tr>
										<td class="col-md-2" style="width: 7%">
											<?php 
											if(isset($state['State']['imagename']) && !empty($state['State']['imagename'])) {

											echo $this->Html->image($state['State']['imagename'],array("class"=>"img-fluid","alt"=>"", 'width'=>'100px')); 
											}
											?>
										</td>
										
										<td><?php echo h($state['State']['name']); ?>&nbsp;</td>
										<td><?php echo h($state['Country']['name']); ?>&nbsp;</td>
										<td><?php echo ($state['Country']['active']?"Yes":"No"); ?>&nbsp;</td>
										
										<td class="actions">
											<?php echo $this->Html->link(__('View'), array('action' => 'view', $state['State']['id']),array('class'=>'badge badge-success')); ?>
											<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $state['State']['id']),array('class'=>'badge badge-info')); ?>
											<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $state['State']['id']), array('class'=>'badge badge-danger'), __('Are you sure you want to delete # %s?', $state['State']['id'])); ?>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>

						<p>
						<?php
						echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
						));
						?></p>
						<ul class="pagination">
							<?php
								echo $this->Paginator->prev(__('Prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							?>
						</ul>
					</div><!--/#content-->
				</div><!-- panel -->
			</div><!--/col-lg-12-->
		</div>
	</div><!--page-wrapper-->
</div><!--main-body-->