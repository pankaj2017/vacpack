<?php echo $this->element("admin/admin_sidebar"); ?>
<?php echo $this->Html->script('ckeditor/ckeditor'); ?>
<style type="text/css">
	
.error-message
{
	    color: #dc3545!important;
}
</style>
<div class="main-body">
	<div class="page-wrapper">
		
		<div class="page-header">
			<div class="page-header-title"><h4>States</h4></div>
		</div>
		
		<div class="page-body">
			<div class="row">
				<div class="col-md-12 col-xl-12">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-md-10">
									<h5>Edit States</h5>
								</div>
								<div class="col-md-2 text-right">
							<?php echo $this->Html->link("Back to list",array('plugin'=>'','controller'=>'states','action'=>'index','admin'=>true),array('class'=>'btn btn-outline-info btn-sm'))?>
								</div>
							</div>
						</div>
						<div class="card-body">
						<?php echo $this->Form->create('State', array("url"=>array("controller"=>"states",'action' => 'edit','admin'=>true),'type'=>'file'));
						 echo $this->Form->hidden('State.id'); ?>
       						<?php echo  $this->element('admin/States/common_admin_form');?>
       						<div class="form-group">
								<input name="" type="submit" value="Create" class="btn btn-success" />
							</div>	
						<?php echo $this->Form->end(); ?>
						</div><!--card-block-->
					</div><!--card-->
				</div><!--col-md-12 -->
			</div><!--row-->
		</div><!--page-body-->

	</div><!--page-wrapper-->
</div><!--main-body-->
<script type="text/javascript">
CKEDITOR.replace('StateDescription', {
toolbar: [[ 'Source' ],[ 'Bold', 'Italic','Underline','Subscript','Superscript'],[ 'NumberedList','BulletedList' ],[ 'Link', 'Unlink', 'Anchor'],[ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ]],
width: '',
height: '300',
});</script>