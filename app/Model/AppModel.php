<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
    public $dtFilterDataFunction = 'parseData';
    
	
	protected function _confurl() { //this function is used in sendtofriend method
		$pageURL = 'http';
		if (isset($_SERVER["HTTPS"]) == "on") {$pageURL .= "s";}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];//.$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"];//.$_SERVER["REQUEST_URI"];
		}
		
		return $pageURL;
	}
	
	protected function _aeskey($key){
		$new_key = str_repeat(chr(0), 16);
		for($i=0,$len=strlen($key);$i<$len;$i++){
			$new_key[$i%16] = $new_key[$i%16] ^ $key[$i];
		}
		return $new_key;
	}
	protected function _aes_encrypt($val){
		$key = $this->_aeskey('15061974');
		$pad_value = 16-(strlen($val) % 16);
		$val = str_pad($val, (16*(floor(strlen($val) / 16)+1)), chr($pad_value));
		return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $val, MCRYPT_MODE_ECB, mcrypt_create_iv( mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND));
	}
	protected function _aes_decrypt($val){	
		$key = $this->_aeskey('15061974');
		$val = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $val, MCRYPT_MODE_ECB, mcrypt_create_iv( mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND));
		return rtrim($val, "\0..\16");		
	}	
	protected function _base64url_encode($data) {
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}
	protected function _base64url_decode($data) {
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}

	/** ==========> Functions for DataTable starts <========== **/
    /**
     * Searching / Filtering
     * 
     * Construct the conditions for WHERE clause
     *
     * @param array $request
     * @return array
     */
    protected function _dtConditions($request) {        
        $searchFields = method_exists($this, 'getSearchFields') ? $this->getSearchFields() : [];
        $where = $having = [];

        /*if ( isset($request['search']) && trim($request['search']['value']) != '' ) {
            $str = $request['search']['value'];

            $conditions = $conditions + [
                'OR' => [
                    'Jinquiry.reference_number like' => "$str%",
                    'Jinquiry.insuredname like' => "%$str%",
                    'Bvertical.name like' => "%$str%"
                ]
            ];
        }*/

        if(!empty($searchFields)) {
            foreach ($searchFields as $key => $searchField) {
                $column = $searchField['db'];
                $condition = [];
                if(in_array($key, array_keys($request)) && (!empty($request[$key]) 
                    || (isset($searchField['type']) && $searchField['type'] == 'int' 
                        && $request[$key]!= '' && $request[$key] >= 0))) {                
                    $value = $request[$key];
                    if(isset($searchField['type']) && $searchField['type'] == 'int') {
                        $condition = [$column => $value];
                    } else {
                        if(is_array($value)) {
                            $condition = [$column .' REGEXP' => implode('|', $request[$key])];
                        } else {
                            $value = trim($value);
                            $condition = [$column .' like' => "%$value%"];
                        }
                        
                    }               
                } else if(isset($searchField['type']) && $searchField['type'] == 'date') {
                    if(isset($searchField['range'])) {
                        $range = $searchField['range'];
                        $from = $key."_".$range[0];
                        $to = $key."_".$range[1];                   
                        if(isset($request[$from]) && !empty($request[$from])) {
                            $condition[$column . " >="] = $request[$from].' 00:00:00';
                        }
                        if(isset($request[$to]) && !empty($request[$to])) {
                            $condition[$column . " <="] = $request[$to].' 23:59:59';
                        }
                    }
                }
                if (isset($searchField['having']) && $searchField['having']) {
                    $having += $condition;    
                } else {
                    $where += $condition;
                }
                
            }
        }

        return compact('where', 'having');
    }
    
    public function isAuthorized($user) {
        return true;
    }

    /**
     * Ordering
     * 
     * Construct the ORDER BY clause
     *
     * @param array $request
     * @return array
     */
    protected function _dtOrder($request, &$options) {
        $columns = $request['columns'];
        $primaryKey = $this->alias.".".$this->primaryKey;

        if (isset($request['order'])) {
            $orderColumnIndex = $request['order'][0]['column'];
            $sortColumn = $columns[$orderColumnIndex]['data'];
            $sortOrder = $request['order'][0]['dir'];

            $order = [$sortColumn.' '.$sortOrder];
        } else if (isset($options['order'])) {
            $order = $options['order'];
            unset($options['order']);
        } else {
            $sortColumn = $primaryKey;
            $sortOrder = 'DESC';

            $order = [$sortColumn.' '.$sortOrder];
        }

        return $order;
    }

    /**
     * Paging
     * 
     * Construct the LIMIT clause
     *
     * @param array $request
     * @return array 
     */
    protected function _dtLimit($request) {        
        $limitOffset = [];
        if(isset($request['start']) && isset($request['length'])) {
            $limitOffset = [
                'limit' => $request['length'],
                'offset' => $request['start']
            ];
        }
        return $limitOffset;
    }

    /**
     * Get data for Datatable 
     *
     * @param array $options | ['group' => 'table.id']
     * @return array
     */
    protected function _dtData($options = []) {
        $request = Router::getRequest()->data;
        $draw = $request['draw'];
        $primaryKey = $this->alias.".".$this->primaryKey;

        $defaultConditions = [];
        if (isset($options['conditions'])) {
            $defaultConditions = $options['conditions'];
            unset($options['conditions']);
        }
        
        $fields = $this->getColumns();
        $joins = method_exists($this, 'getTableJoins') ? $this->getTableJoins() : [];
        $whereHavingCondition = $this->_dtConditions($request);
        $conditions = $defaultConditions + $whereHavingCondition['where'];
        $having = $whereHavingCondition['having'];
        $order = $this->_dtOrder($request, $options);
        $limit = $this->_dtLimit($request);

        $data = $this->find('all', [
            'fields' => array_column($fields, 'db'),
            'joins' => $joins,
            'conditions' => $conditions,
            'having' => $having,
            'order' => $order
        ] + $limit + $options);

        $recordsFiltered = $this->find('count', [
            'joins' => $joins,
            'conditions' => $conditions,
            'having' => $having,
            'order' => $order
        ] + $options, [$primaryKey]);

        $recordsTotal = $this->find('count', [
            'joins' => $joins,
            'order' => $order,
            'conditions' => $defaultConditions
        ] + $options, [$primaryKey]);

        $data = method_exists($this, $this->dtFilterDataFunction) ? $this->{$this->dtFilterDataFunction}($data) : $data;

        return [
            "draw" => $draw,
            "recordsTotal" => intval($recordsTotal),
            "recordsFiltered" => intval($recordsFiltered),
            "data" => $data
        ];
    }
    /** ==========> Functions for DataTable ends <========== **/
}
