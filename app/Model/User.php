<?php
App::uses('AppModel', 'Model');
App::uses('CakeEvent', 'Event');//using for the event-driven programming
/**
 * User Model
 *
 * @property Group $Group
 * @property Order $Order
 * @property Profile $Profile
 * @property Store $Store
 */
class User extends AppModel {

	public $actsAs = array(
					'Acl' => array('type' => 'requester', 'enabled' => false),
					'Captcha' => array(
						'field' => array('security_code'),
						'error' => 'Incorrect captcha code value'
					),
				);

	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
		}
        return true;
    }
	
	public function afterSave($created, $options = array()) {
		//$created returns true if it inserts new row
		if($created){
			$link = $this->_confurl();
			$encryptconfirm = $this->_base64url_encode($this->_aes_encrypt($this->data['User']['confirmkey']));
			$eventregister = new CakeEvent('Model.User.userRegistered', $this, array(
				'user' => $this->data['User'],
				'link'=> $link.'/vacdo/users/register/'.$encryptconfirm
				)
			);
			$this->getEventManager()->dispatch($eventregister);
		}
	}
	
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'group_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'firstname' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'notEmpty' => array(
				'rule' => array('notBlank'),
				'message' => 'Provide an email address'
			),
			'email' => array(
				'rule' => array('email'),
				'message' => 'Email Id is not correct',
				//'allowEmpty' => false,
				'required' => false,
				'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'unique' => array(
				'rule' => array('isUnique'),
				'message' => 'Email Id is already registered',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'phone' => array(
			'notBlank'=>array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			
			'phone'=>array(
				'rule' => array('minLength',10),
				'message' => 'Start with area code to phone',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
		'password'=>array(
			'notBlank'=>array(
				'rule' => array('notBlank'),
				'message' => 'Password can not leave blank',
				//'required'=>true,
				//'allowEmpty'=>false,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'minLength'=>array(
				'rule'=>array('minLength',6),
				//'required'=>true,
				//'allowEmpty'=>false,
				'message'=>'Please enter min 6 character Password',
				//'on' => 'create'
				),
			'identical'=>array(
				'rule' => array('identicalFieldValues', 'confirmpassword' ),
				'message' => 'Passwords does not match',
				//'on' => 'create'
				)
		),
		'confrimpassword'=>array(
			'notBlank'=>array(
				'rule' => array('notBlank'),
				'message' => 'Your custom message here',
				//'required'=>true,
				//'allowEmpty'=>false,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'minLength'=>array(
				'rule'=>array('minLength',6),
				//'required'=>true,
				//'allowEmpty'=>false,
				'message'=>'Please retype your Password',
				//'on' => 'create'
			),
			'identical'=>array(
				'rule' => array('identicalFieldValues', 'password'),
				'message' => 'Passwords does not match',
				//'on' => 'create'
			)
		),

	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public $hasAndBelongsToMany = array( /*used for wishlist*/
		/*'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)*/
	);
	public $hasOne = array(
		'Profile' => array(
			'className' => 'Profile',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		/*'Article' => array(
			'className' => 'Article',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		
		
		'Deliveryaddress' => array(
			'className' => 'Deliveryaddress',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)*/
	);
	
	public function beforeValidate($options = array()) {
		parent::beforeValidate($options);
	}
	
	function identicalFieldValues( $field=array(), $compare_field=null ){
		foreach( $field as $key => $value ){
		$v1 = $value;
		$v2 = $this->data[$this->name][ $compare_field ];

			if($v1 !== $v2) { 
				return FALSE;
			} else {
				continue;
			}
		}
        	return TRUE;
	}
	 public function bindNode($user) {
        
    }


	public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
    }
	/** ==========> Functions for DataTable starts <========== **/
    /**
     * Define columns/fileds you need to select
     * db: fields in database, dt: for datatables; not using dt for now
     *
     * @return array
     */
    public function getColumns() { 
        return [
            ['db' => 'User.id', 'dt' => 'id'], 
            ['db' => 'User.firstname', 'dt' => 'firstname'],
            ['db' => 'User.lastname', 'dt' => 'lastname'],
            ['db' => 'User.email', 'dt' => 'email'],
            ['db' => 'User.phone', 'dt' => 'phone'],
            ['db' => 'User.active', 'dt' => 'active'],
            // ['db' => 'State.*', 'dt' => 'State'],
        ];
    }
    /**
     * Define search fields
     *
     * @return array
     */
    public function getSearchFields() {
        return [
            'firstname' => ['db' =>'User.firstname'],
            'lastname' => ['db' => 'User.lastname'],
            // 'iso_2' => ['db' => 'State.iso_2'],
            // 'iso_3' => ['db' => 'Region.iso_2'],
            // 'status' => ['db' => 'Country.active', 'type' => 'int'],
        ];
    }
    /**
     * Get listing data
     *
     * @return array
     */
    public function getListingData() {
        $this->recursive = 1;
        
        $result = $this->_dtData();
        // pr($result);die;
        return $result;
    }
    /**
     * Parse data; if you want to modify/alter data then define this function    * 
     *
     * @param array $data
     *
     * @return array                                  
     */
    public function parseData($data) {
        // pr($data);die;
        foreach ($data as $key => &$val) {
            $user = 'User';
            // $state = 'State';
            $id = $val[$user]['id'];
            $viewUrl = Router::url(["controller" => "users", "action" => "view", "admin"=>true])."/".$id;

            $val[$user]['firstname'] = !empty($val[$user]['firstname']) ? 
                '<a href="'.$viewUrl.'">'.$val[$user]['firstname'].'</a>' : '-';      
            $val[$user]['lastname'] = !empty($val[$user]['lastname']) ? $val[$user]['lastname'] : '-';        
            $val[$user]['email'] = !empty($val[$user]['email']) ? $val[$user]['email'] : '-';        
            $val[$user]['phone'] = !empty($val[$user]['phone']) ? $val[$user]['phone'] : '-';        
            $val[$user]['actions'] = $this->_actionButtons($val);
            $val[$user]['active'] = $this->_prepareStatusData($val);
        }
        return $data;
    }
    /**
     * Prepare status
     *
     * @param array $data
     *
     * @return string
     */
    private function _prepareStatusData($data) {
        $user = 'User';
        $id = $data[$user]['id'];
        $status = $data[$user]['active'];
        $statusName = self::getStatuses($status);
        $statusHtml = $lableClass = $onClick = $style = $other = '';

        if($status == 2) {
            $status_to_update = 1;
            $lableClass = 'label-danger';
            $url = Router::url(array('controller' => 'users','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            // $style = 'pointer-events: none;';
        } else {
           $status_to_update = 0;
            if($status == 0) {
                $status_to_update = 1;
            }

            $url = Router::url(array('controller' => 'users','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            if ($status == 1) {
                $lableClass = 'label-success';
            }else if($status == 0) {
                $lableClass = 'label-warning';

            }
            
        }

        $statusHtml = '<a data-toggle="tooltip" data-placement="left" 
            data-original-title="Update status" class="label '.$lableClass.'" style="color:#f0f0f0;'.$style.'" '.$onClick.'>'.$statusName.'</a>'.$other;

        return $statusHtml;
    }

    /**
     * Prepare different actions
     *
     * @param array $data
     *
     * @return string
     */
    private function _actionButtons($data) {
        // pr($data);die;
        $user = 'User';
        $id = $data[$user]['id'];
        // $status = $data[$state]['active'];
        $status = 1;
        // print_r($a);
        if ($status !== 2) {
            // pr('dd');die;
            $editUrl = Router::url(["controller" => "users", "action" => "edit", "admin"=>true])."/".$id;
            $deleteUrl = Router::url(["controller" => "users", "action" => "delete", "admin"=>true])."/".$id;
            $onClick = 'onclick="deleteme('.$id.');"';
        
            $actions = ' <a href="'.$editUrl.'" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                    <i class="far fa-edit text-info"></i>
                </a>';

            $actions .= ' <a '.$onClick.' class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="far fa-trash-alt text-danger"></i>
                </a>';
                       
        } else {
            // pr('');die;
            $actions = '-';
        }       

        return $actions;
    }

    public static function getStatuses($key = NULL, $filter = []) {
        $statuses = [
            0 => "Inactive",
            1 => "Active",
            2 => "Deleted",
            3 => "Pending",
            4 => "In Progress",
            5 => "Completed",
            6 => "Close"
        ];

        if (!is_null($key)) {
            return isset($statuses[$key]) ? $statuses[$key] : '-';
        }

        if (!empty($filter)) {
            return array_filter($statuses, function ($k) use ($filter) {
                return in_array($k, $filter);
            }, ARRAY_FILTER_USE_KEY);
        }

        return $statuses;
    }
}
