<?php
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
/**
 * Admin Model
 *
 * @property Group $Group
 */
class Admin extends AppModel {
	public $actsAs = array('Acl' => array('type' => 'requester','enabled'=>false));
	
	public function bindNode($user){
		return array('model'=>'Admingroup','foreign_key'=>$user['Admin']['admingroup_id']);
	}

	public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['Admin']['admingroup_id'])) {
            $groupId = $this->data['Admin']['admingroup_id'];
        } else {
            $groupId = $this->field('admingroup_id');
        }
        if (!$groupId) {
            return null;
        }
        return array('Admingroup' => array('id' => $groupId));
    }

	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$this->data['Admin']['password'] = AuthComponent::password($this->data['Admin']['password']);
		}
        return true;
    }
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'admingroup_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'firstname' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email_address' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'phone' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'password' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Admingroup' => array(
			'className' => 'Admingroup',
			'foreignKey' => 'admingroup_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public $hasOne = array(
		'Adminprofile' => array(
			'className' => 'Adminprofile',
			'foreignKey' => 'admin_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	/** ==========> Functions for DataTable starts <========== **/
    /**
     * Define columns/fileds you need to select
     * db: fields in database, dt: for datatables; not using dt for now
     *
     * @return array
     */
    public function getColumns() { 
        return [
            ['db' => 'Admin.id', 'dt' => 'id'], 
            ['db' => 'Admin.admingroup_id', 'dt' => 'admingroup_id'],
            ['db' => 'Admin.firstname', 'dt' => 'firstname'],
            ['db' => 'Admin.lastname', 'dt' => 'lastname'],
            ['db' => 'Admin.email_address', 'dt' => 'email_address'],
            ['db' => 'Admingroup.name', 'dt' => 'name'],
        ];
    }
    /**
     * Define search fields
     *
     * @return array
     */
    public function getSearchFields() {
        return [
            'firstname' => ['db' =>'Admin.firstname'],
            'lastname' => ['db' =>'Admin.lastname'],
            'email_address' => ['db' => 'Admin.email_address'],
            // 'iso_2' => ['db' => 'State.iso_2'],
            // 'iso_3' => ['db' => 'Region.iso_2'],
            // 'status' => ['db' => 'Country.active', 'type' => 'int'],
        ];
    }
    /**
     * Get listing data
     *
     * @return array
     */
    public function getListingData() {
        $this->recursive = 	1;
        
        $result = $this->_dtData();
        // pr($result);die;
        return $result;
    }
    /**
     * Parse data; if you want to modify/alter data then define this function    * 
     *
     * @param array $data
     *
     * @return array                                  
     */
    public function parseData($data) {
        // pr($data);die;
        foreach ($data as $key => &$val) {
            $admin = 'Admin';
            $admingroup = 'Admingroup';
            $id = $val[$admin]['id'];
            $viewUrl = Router::url(["controller" => "admins", "action" => "view", "admin"=>true])."/".$id;

            $val[$admin]['firstname'] = !empty($val[$admin]['firstname']) ? 
                '<a href="'.$viewUrl.'">'.$val[$admin]['firstname'].'</a>' : '-';      
            // $val[$admin]['description'] = !empty($val[$admin]['description']) ? $val[$admin]['description'] : '-';       
            $val[$admin]['actions'] = $this->_actionButtons($val);
            // $val[$admin]['active'] = $this->_prepareStatusData($val);
        }
        return $data;
    }
    /**
     * Prepare status
     *
     * @param array $data
     *
     * @return string
     */
    private function _prepareStatusData($data) {
        $admin = 'Admin';
        $id = $data[$admin]['id'];
        $status = $data[$admin]['active'];
        $statusName = self::getStatuses($status);
        $statusHtml = $lableClass = $onClick = $style = $other = '';

        if($status == 2) {
            $status_to_update = 1;
            $lableClass = 'label-danger';
            $url = Router::url(array('controller' => 'admingroups','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            // $style = 'pointer-events: none;';
        } else {
           $status_to_update = 0;
            if($status == 0) {
                $status_to_update = 1;
            }

            $url = Router::url(array('controller' => 'admingroups','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            if ($status == 1) {
                $lableClass = 'label-success';
            }else if($status == 0) {
                $lableClass = 'label-warning';

            }
            
        }

        $statusHtml = '<a data-toggle="tooltip" data-placement="left" 
            data-original-title="Update status" class="label '.$lableClass.'" style="color:#f0f0f0;'.$style.'" '.$onClick.'>'.$statusName.'</a>'.$other;

        return $statusHtml;
    }

    /**
     * Prepare different actions
     *
     * @param array $data
     *
     * @return string
     */
    private function _actionButtons($data) {
        // pr($data);die;
        $admin = 'Admin';
        $id = $data[$admin]['id'];
        // $status = $data[$state]['active'];
        $status = 1;
        // print_r($a);
        if ($status !== 2) {
            // pr('dd');die;
            $editUrl = Router::url(["controller" => "admins", "action" => "edit", "admin"=>true])."/".$id;
            $deleteUrl = Router::url(["controller" => "admins", "action" => "delete", "admin"=>true])."/".$id;
            $onClick = 'onclick="deleteme('.$id.');"';
        
            $actions = ' <a href="'.$editUrl.'" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                    <i class="far fa-edit text-info"></i>
                </a>';

            $actions .= ' <a '.$onClick.' class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="far fa-trash-alt text-danger"></i>
                </a>';
                       
        } else {
            // pr('');die;
            $actions = '-';
        }       

        return $actions;
    }

    public static function getStatuses($key = NULL, $filter = []) {
        $statuses = [
            0 => "Inactive",
            1 => "Active",
            2 => "Deleted",
            3 => "Pending",
            4 => "In Progress",
            5 => "Completed",
            6 => "Close"
        ];

        if (!is_null($key)) {
            return isset($statuses[$key]) ? $statuses[$key] : '-';
        }

        if (!empty($filter)) {
            return array_filter($statuses, function ($k) use ($filter) {
                return in_array($k, $filter);
            }, ARRAY_FILTER_USE_KEY);
        }

        return $statuses;
    }
}
