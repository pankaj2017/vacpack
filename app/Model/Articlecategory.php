<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Category $ParentCategory
 * @property Category $ChildArticlecategory
 * @property Product $Product
 */
class Articlecategory extends AppModel {

/**
 * Behaviors
 *
 * @var array
 */
	// public $actsAs = array(
	// 	'Tree',
	// );
	public $actsAs = array('Tree','Containable');


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	// public $belongsTo = array(
	// 	'ParentCategory' => array(
	// 		'className' => 'Articlecategory',
	// 		'foreignKey' => 'parent_id',
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => ''
	// 	)
	// );
	public $belongsTo = array(
		'ParentArticlecategory' => array(
			'className' => 'Articlecategory',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	var $validate = array(
		'name' => array(
			array(
				'allowEmpty'=>false,
				'rule' => array('notBlank'),
				'message'=>'Article Category is mandatory'
			),array(
				'rule' => array('custom',"/^[a-z0-9 '\-&]{3,}$/i"),//^[a-z0-9 -\.,:\/]{3,}$/i
				'message'=>'Please enter valid Article Category'
			),/*array(			   
			    'rule' => array('trimvalidate','name'),
			    'message' => 'Too many white spaces'
			),*/
			
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	// public $hasMany = array(
	// 	'ChildCategory' => array(
	// 		'className' => 'Articlecategory',
	// 		'foreignKey' => 'parent_id',
	// 		'dependent' => false,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'exclusive' => '',
	// 		'finderQuery' => '',
	// 		'counterQuery' => ''
	// 	),
	// 	'Article' => array(
	// 		'className' => 'Article',
	// 		'foreignKey' => 'articlecategory_id',
	// 		'dependent' => false,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'exclusive' => '',
	// 		'finderQuery' => '',
	// 		'counterQuery' => ''
	// 	)
	// );
var $hasMany = array(
		'ChildArticlecategory' => array(
			'className' => 'Articlecategory',
			'foreignKey' => 'parent_id',
			'dependent' => false,
			'conditions' => 'active = 1',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Article' => array(
			'className' => 'Article',
			'foreignKey' => 'articlecategory_id',
			'dependent' => false,
			'conditions' => 'active = 1',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
		);

}
