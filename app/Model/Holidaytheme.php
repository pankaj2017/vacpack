<?php
App::uses('AppModel', 'Model');
App::uses('AttachmentBehavior', 'Uploader451.Model/Behavior');
/**
 * Aagentgroup Model
 *
 * @property Aagent $Aagent
 */
class Holidaytheme extends AppModel {

public $uses = array(
        'Containable'
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'active' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Packag' => array(
			'className' => 'Tagents.Packag',
			'joinTable' => 'holidaythemes_packags',
			'foreignKey' => 'holidaytheme_id',
			'associationForeignKey' => 'packag_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
    public $actsAs = array(
        'Uploader451.Attachment' => array(
            'image' => array(
                'nameCallback' => 'formatName',
                'append' => '',
                'prepend' => '',
                'tempDir' => TMP,
                'uploadDir' => 'files/uploads/holidaytheme/',
                'finalPath' => '/files/uploads/holidaytheme/',
                'dbColumn' => 'holidayimage',
                'metaColumns' => array(
                    'ext' => 'extension',
                    'type' => 'mimetype',
                    'size' => 'filesize'
                ),
                'defaultPath' => '',
                'overwrite' => true,
                'stopSave' => true,
                'allowEmpty' => true,
                'transforms' => array(
                ),
            )
        ),
        'Uploader451.FileValidation' => array(
            'image' => array(
                'extension' => array('gif', 'jpg', 'png', 'jpeg'),
                'filesize' => 5242880 //5 Mb default
            )
        )
    );
    public function formatName($name, $file) {
        return sprintf('%s-%s', $name, $file->size());
    }
    public function transformNameCallback($name, $file) {
        return $this->getUploadedFile()->name();
    }

	/** ==========> Functions for DataTable starts <========== **/
    /**
     * Define columns/fileds you need to select
     * db: fields in database, dt: for datatables; not using dt for now
     *
     * @return array
     */
    public function getColumns() { 
        return [
            ['db' => 'Holidaytheme.id', 'dt' => 'id'], 
            ['db' => 'Holidaytheme.name', 'dt' => 'name'],
            ['db' => 'Holidaytheme.description', 'dt' => 'description'],
            ['db' => 'Holidaytheme.active', 'dt' => 'active'],
        ];
    }
    /**
     * Define search fields
     *
     * @return array
     */
    public function getSearchFields() {
        return [
            'name' => ['db' =>'Holidaytheme.name'],
            'description' => ['db' =>'Holidaytheme.description'],
            // 'iso_2' => ['db' => 'State.iso_2'],
            // 'iso_3' => ['db' => 'Region.iso_2'],
            // 'status' => ['db' => 'Country.active', 'type' => 'int'],
        ];
    }
    /**
     * Get listing data
     *
     * @return array
     */
    public function getListingData() {
        $this->recursive = 	1;
        
        $result = $this->_dtData();
        // pr($result);die;
        return $result;
    }
    /**
     * Parse data; if you want to modify/alter data then define this function    * 
     *
     * @param array $data
     *
     * @return array                                  
     */
    public function parseData($data) {
        // pr($data);die;
        foreach ($data as $key => &$val) {
            $holiday = 'Holidaytheme';
            $id = $val[$holiday]['id'];
            $viewUrl = Router::url(["controller" => "holidaythemes", "action" => "view", "holiday"=>true])."/".$id;

            $val[$holiday]['firstname'] = !empty($val[$holiday]['firstname']) ? 
                '<a href="'.$viewUrl.'">'.$val[$holiday]['firstname'].'</a>' : '-';      
            // $val[$holiday]['description'] = !empty($val[$holiday]['description']) ? $val[$holiday]['description'] : '-';       
            $val[$holiday]['actions'] = $this->_actionButtons($val);
            $val[$holiday]['active'] = $this->_prepareStatusData($val);
        }
        return $data;
    }
    /**
     * Prepare status
     *
     * @param array $data
     *
     * @return string
     */
    private function _prepareStatusData($data) {
        $holiday = 'Holidaytheme';
        $id = $data[$holiday]['id'];
        $status = $data[$holiday]['active'];
        $statusName = self::getStatuses($status);
        $statusHtml = $lableClass = $onClick = $style = $other = '';

        if($status == 2) {
            $status_to_update = 1;
            $lableClass = 'label-danger';
            $url = Router::url(array('controller' => 'admingroups','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            // $style = 'pointer-events: none;';
        } else {
           $status_to_update = 0;
            if($status == 0) {
                $status_to_update = 1;
            }

            $url = Router::url(array('controller' => 'admingroups','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            if ($status == 1) {
                $lableClass = 'label-success';
            }else if($status == 0) {
                $lableClass = 'label-warning';

            }
            
        }

        $statusHtml = '<a data-toggle="tooltip" data-placement="left" 
            data-original-title="Update status" class="label '.$lableClass.'" style="color:#f0f0f0;'.$style.'" '.$onClick.'>'.$statusName.'</a>'.$other;

        return $statusHtml;
    }

    /**
     * Prepare different actions
     *
     * @param array $data
     *
     * @return string
     */
    private function _actionButtons($data) {
        // pr($data);die;
        $holiday = 'Holidaytheme';
        $id = $data[$holiday]['id'];
        // $status = $data[$state]['active'];
        $status = 1;
        // print_r($a);
        if ($status !== 2) {
            // pr('dd');die;
            $editUrl = Router::url(["controller" => "holidaythemes", "action" => "edit", "admin"=>true])."/".$id;
            $deleteUrl = Router::url(["controller" => "holidaythemes", "action" => "delete", "admin"=>true])."/".$id;
            $onClick = 'onclick="deleteme('.$id.');"';
        
            $actions = ' <a href="'.$editUrl.'" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                    <i class="far fa-edit text-info"></i>
                </a>';

            $actions .= ' <a '.$onClick.' class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="far fa-trash-alt text-danger"></i>
                </a>';
                       
        } else {
            // pr('');die;
            $actions = '-';
        }       

        return $actions;
    }

    public static function getStatuses($key = NULL, $filter = []) {
        $statuses = [
            0 => "Inactive",
            1 => "Active",
            2 => "Deleted",
            3 => "Pending",
            4 => "In Progress",
            5 => "Completed",
            6 => "Close"
        ];

        if (!is_null($key)) {
            return isset($statuses[$key]) ? $statuses[$key] : '-';
        }

        if (!empty($filter)) {
            return array_filter($statuses, function ($k) use ($filter) {
                return in_array($k, $filter);
            }, ARRAY_FILTER_USE_KEY);
        }

        return $statuses;
    }

}
