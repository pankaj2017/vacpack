<?php
App::uses('AppModel', 'Model');
/**
 * Agent Model
 *
 * @property Group $Group
 * @property Post $Post
 */
class Tagency extends AppModel {
	public $useTable='travelagencies';

	public $actsAs = array(/*'Feedback.Commentable',*/ 'Feedback.Rated');
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Packag' => array(
			'className' => 'Tagents.Packag',
			'foreignKey' => 'travelagency_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Travelagent' => array(
			'className' => 'Tagents.Travelagent',
			'foreignKey' => 'travelagency_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Agencyinquiry' => array(
			'className' => 'Agencyinquiry',
			'foreignKey' => 'travelagency_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),	
	);
	/**
 * hasOne associations
 *
 * @var array
 */
	//public $hasOne = array();
	
	
	public function phone($check) {
		if(is_array($check)) {$value = array_shift($check);} else { $value = $check; }
		if(strlen($value) == 0) {return true;}
		return preg_match('/^[0-9-+()# ]{6,23}+$/', $value);
	}
}