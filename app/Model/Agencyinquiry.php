<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 * @property User $User
 */
class Agencyinquiry extends AppModel {
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'fname' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'lname' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public $belongsTo = array(
		'Travelagency' => array(
			'className' => 'Tagents.Travelagency',
			'foreignKey' => 'travelagency_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tagency' => array(
			'className' => 'Tagency',
			'foreignKey' => 'travelagency_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}