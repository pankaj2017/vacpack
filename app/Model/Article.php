<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 * @property User $User
 */
class Article extends AppModel {
	public $actsAs = array('Tags.Taggable','Feedback.Rated');
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'admin_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'title' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Admin' => array(
			'className' => 'Admin',
			'foreignKey' => 'admin_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		/*'Destination' => array(
			'className' => 'Destination',
			'foreignKey' => 'destination_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)*/
		'Articlecategory' => array(
			'className' => 'Articlecategory',
			'foreignKey' => 'articlecategory_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)

	);
	/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Comment' => array(
			'className' => 'Comment',
			'foreignKey' => 'article_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => '',
			
		),
		'Articleview' => array(
			'className' => 'Articleview',
			'foreignKey' => 'article_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => '',
		),
	/*	'Tofriend' => array(
			'className' => 'Tofriend',
			'foreignKey' => 'post_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => '',
			
		)*/
	);
	public $hasOne = array(
		'Articlecoverimage' => array(
			'className' => 'Articlecoverimage',
			'foreignKey' => 'article_id',
			'dependent' => false
			)
		);
	public function getRandomListArticle() {
		
		$articles = $this->find('all',['recursive'=>0,'order' =>['Article.modified'=>'DESC'],'conditions'=>['Article.active'=>1],'limit'=>3]);

		return $articles;
    }
     /** ==========> Functions for DataTable starts <========== **/
    /**
     * Define columns/fileds you need to select
     * db: fields in database, dt: for datatables; not using dt for now
     *
     * @return array
     */
    public function getColumns() { 
        return [
            ['db' => 'Article.id', 'dt' => 'id'], 
            ['db' => 'Article.title', 'dt' => 'title'],
            ['db' => 'Article.active', 'dt' => 'status'],
            ['db' => 'Articlecategory.name', 'dt' => 'name'],
        ];  
    }
    /**
     * Define search fields
     *
     * @return array
     */
    public function getSearchFields() {
        return [
            'title' => ['db' =>'Article.title'],
            'articlecategory_id' => ['db' => 'Articlecategory.id'],
            'regions' => ['db' => 'Region.id', 'type' => 'int'],
            'status' => ['db' => 'Article.active', 'type' => 'int'],
        ];
    }
    /**
     * Get listing data
     *
     * @return array
     */
    public function getListingData() {
        $this->recursive = 1;
        
        $result = $this->_dtData();
        // pr($result);die;
        return $result;
    }
    /**
     * Parse data; if you want to modify/alter data then define this function    * 
     *
     * @param array $data
     *
     * @return array                                  
     */
    public function parseData($data) {
    	// pr("ss");die;
        foreach ($data as $key => &$val) {
            $ar = 'Article';
            $arc = 'Articlecategory';
            $id = $val[$ar]['id'];
            $viewUrl = Router::url(["controller" => "articles", "action" => "view", "admin"=>true])."/".$id;

            $val[$ar]['title'] = !empty($val[$ar]['title']) ? 
                '<a href="'.$viewUrl.'">'.$val[$ar]['title'].'</a>' : '-';
            $val[$arc]['name'] = !empty($val[$arc]['name']) ? $val[$arc]['name'] : '-';        
            $val[$ar]['actions'] = $this->_actionButtons($val);
            $val[$ar]['active'] = $this->_prepareStatusData($val);
        }
        return $data;
    }
    

    /**
     * Prepare status
     *
     * @param array $data
     *
     * @return string
     */
    private function _prepareStatusData($data) {
        $ar = 'Article';
        $id = $data[$ar]['id'];
        $status = $data[$ar]['active'];
        $statusName = self::getStatuses($status);
        $statusHtml = $lableClass = $onClick = $style = $other = '';

        if($status == 2) {
            $status_to_update = 1;
            $lableClass = 'label-danger';
            $url = Router::url(array('controller' => 'articles','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            // $style = 'pointer-events: none;';
        } else {
           $status_to_update = 0;
            if($status == 0) {
                $status_to_update = 1;
            }

            $url = Router::url(array('controller' => 'articles','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            if ($status == 1) {
                $lableClass = 'label-success';
            }else if($status == 0) {
                $lableClass = 'label-warning';

            }
            
        }

        $statusHtml = '<a data-toggle="tooltip" data-placement="left" 
            data-original-title="Update status" class="label '.$lableClass.'" style="color:#f0f0f0;'.$style.'" '.$onClick.'>'.$statusName.'</a>'.$other;

        return $statusHtml;
    }

    /**
     * Prepare different actions
     *
     * @param array $data
     *
     * @return string
     */
    private function _actionButtons($data) {
        // pr($data);die;
        $ar = 'Article';
        $id = $data[$ar]['id'];
        $status = $data[$ar]['active'];

        if ($status != 2) {
            $editUrl = Router::url(["controller" => "articles", "action" => "edit", "admin"=>true])."/".$id;
            $deleteUrl = Router::url(["controller" => "articles", "action" => "delete", "admin"=>true])."/".$id;
            $onClick = 'onclick="deleteme('.$id.');"';
        
            $actions = ' <a href="'.$editUrl.'" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                    <i class="far fa-edit text-info"></i>
                </a>';

            $actions .= ' <a href="'.$deleteUrl.'" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="far fa-trash-alt text-danger"></i>
                </a>';
                       
        } else {
            $actions = '-';
        }       

        return $actions;
    }

    public static function getStatuses($key = NULL, $filter = []) {
        $statuses = [
            0 => "Inactive",
            1 => "Active",
            2 => "Deleted",
            3 => "Pending",
            4 => "In Progress",
            5 => "Completed",
            6 => "Close"
        ];

        if (!is_null($key)) {
            return isset($statuses[$key]) ? $statuses[$key] : '-';
        }

        if (!empty($filter)) {
            return array_filter($statuses, function ($k) use ($filter) {
                return in_array($k, $filter);
            }, ARRAY_FILTER_USE_KEY);
        }

        return $statuses;
    }

}