<?php
App::uses('AppModel', 'Model');
App::uses('AttachmentBehavior', 'Uploader451.Model/Behavior');


class State extends AppModel {

    public $actsAs = array(
        'Uploader451.Attachment' => array(
            'image' => array(
                'nameCallback' => 'formatName',
                'append' => '',
                'prepend' => '',
                'tempDir' => TMP,
                'uploadDir' => 'files/uploads/states/',
                'finalPath' => '/files/uploads/states/',
                'dbColumn' => 'imagename', //this will store the default image path and name
                'metaColumns' => array(
                    'ext' => 'extension',
                    'type' => 'mimetype',
                    'size' => 'filesize'
                ),
                'defaultPath' => '',
                'overwrite' => true,
                'stopSave' => true,
                'allowEmpty' => true,
                'transforms' => array(
                   /* 'imagesmall' => array(
                    'method' => 'resize', // or crop / AttachmentBehavior::CROP
                    'append' => '-small',
                    'overwrite' => true,
                    'self' => false,
                    'width' => 175,
                    'height' => 108,
                    'aspect' =>true,
                    'mode'=>'width',
                    'quality' => 70,
                    'dbColumn'=>'namesmall', //this will store transformed path and name
                    'nameCallback' => 'transformNameCallback',
                    ),*/
                    'imagemedium' => array(
                    'method' => 'resize', // or crop / AttachmentBehavior::CROP
                    'append' => '-medium',
                    'overwrite' => true,
                    'self' => false,
                    'width' => 256,
                    'height' => 171,
                    'aspect' =>false,
                    //'mode'=>'width',
                    'quality' => 70,
                    'dbColumn'=>'image', //this will store transformed path and name
                    'nameCallback' => 'transformNameCallback',
                    ),
                    'imagebig' => array(
                    'method' => 'resize',
                    'append' => '-big',
                    'width' => 500,
                    'height' => 350,
                    'overwrite' =>true,
                    'self' => false,
                    'mode'=>'width',
                    'aspect' => true,
                    'dbColumn'=>'imagebig',
                    'quality' => 70,
                    'mode'=>'height',
                    'nameCallback' => 'transformNameCallback',
                    )
                ),
            )
        ),
        'Uploader451.FileValidation' => array(
            'image' => array(
                'extension' => array('gif', 'jpg', 'png', 'jpeg'),
                'filesize' => 5242880 //5 Mb default
            )
        )
    );


	var $validate =[
    	'name' =>[
    	'unique' => ['rule' => 'isUnique',[ 'name'],'message' => 'State already exists.'],
    	'notBlank' => ['rule' => ['notBlank'],'message' => 'State name is required.']
    	],
    	'State_id' =>[
    	'notBlank' => ['rule' => ['notBlank'],'message' => ' Country is required.']
    	],
    	'description' =>[
    	'notBlank' => ['rule' => ['notBlank'],'message' => ' Description is required.']
    	]
    ];
   
    public $belongsTo =['Country' => ['className' => 'Country','foreignKey' => 'country_id']];

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
      
        'Destination' => array(
            'className' => 'Destination',
            'foreignKey' => 'state_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Package' => array(
            'className' => 'Package',
            'foreignKey' => 'state_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /** ==========> Functions for DataTable starts <========== **/
    /**
     * Define columns/fileds you need to select
     * db: fields in database, dt: for datatables; not using dt for now
     *
     * @return array
     */
    public function getColumns() { 
        return [
            ['db' => 'State.id', 'dt' => 'id'], 
            ['db' => 'State.name', 'dt' => 'name'],
            ['db' => 'State.image', 'dt' => 'image'],
            ['db' => 'State.country_id', 'dt' => 'country_id'],
            ['db' => 'Country.*', 'dt' => 'Country'],
        ];
    }
    /**
     * Define search fields
     *
     * @return array
     */
    public function getSearchFields() {
        return [
            'name' => ['db' =>'State.name'],
            'iso_2' => ['db' => 'State.iso_2'],
            'iso_3' => ['db' => 'Region.iso_2'],
            // 'status' => ['db' => 'Country.active', 'type' => 'int'],
        ];
    }
    /**
     * Get listing data
     *
     * @return array
     */
    public function getListingData() {
        $this->recursive = 1;
        
        $result = $this->_dtData(array( 'order' => 'State.name'));
        // pr($result);die;
        return $result;
    }
    /**
     * Parse data; if you want to modify/alter data then define this function    * 
     *
     * @param array $data
     *
     * @return array                                  
     */
    public function parseData($data) {
        // pr($data);die;
        foreach ($data as $key => &$val) {
            $state = 'State';
            $coun = 'Country';
            $id = $val[$state]['id'];
            $viewUrl = Router::url(["controller" => "states", "action" => "view", "admin"=>true])."/".$id;

            $val[$state]['name'] = !empty($val[$state]['name']) ? 
                '<a href="'.$viewUrl.'">'.$val[$state]['name'].'</a>' : '-';
            //$val[$state]['image'] = !empty($val[$state]['image']) ? $val[$state]['image'] : '-';        
             $val[$state]['image'] =$this->_setImageData($val);       
            $val[$coun]['name'] = !empty($val[$coun]['name']) ? $val[$coun]['name'] : '-';        
            $val[$state]['actions'] = $this->_actionButtons($val);
            //$val[$state]['active'] = $this->_prepareStatusData($val);
        }
        return $data;
    }
    private function _setImageData($data) {
        $state ='State';
        // pr($data);die;
        $id = $data[$state]['id'];
        if(isset($data['State']['image']) && !empty($data['State']['image'])) {
        $image = $data[$state]['image'];
$imgsrc = '<img src="'.Router::Url('/',true).$image .'" class="img-fluid" alt="" width="100">';
            return  $imgsrc; 
            //return $this->Html->image($data['State']['image'],array("class"=>"img-fluid","alt"=>"", 'width'=>'100px')); 
        }else{
            return  "-";
        }
    }

    /**
     * Prepare status
     *
     * @param array $data
     *
     * @return string
     */
    private function _prepareStatusData($data) {
        $state = 'State';
        $id = $data[$state]['id'];
        $status = $data[$state]['active'];
        $statusName = self::getStatuses($status);
        $statusHtml = $lableClass = $onClick = $style = $other = '';

        if($status == 2) {
            $status_to_update = 1;
            $lableClass = 'label-danger';
            $url = Router::url(array('controller' => 'countries','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            // $style = 'pointer-events: none;';
        } else {
           $status_to_update = 0;
            if($status == 0) {
                $status_to_update = 1;
            }

            $url = Router::url(array('controller' => 'countries','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            if ($status == 1) {
                $lableClass = 'label-success';
            }else if($status == 0) {
                $lableClass = 'label-warning';

            }
            
        }

        $statusHtml = '<a data-toggle="tooltip" data-placement="left" 
            data-original-title="Update status" class="label '.$lableClass.'" style="color:#f0f0f0;'.$style.'" '.$onClick.'>'.$statusName.'</a>'.$other;

        return $statusHtml;
    }

    /**
     * Prepare different actions
     *
     * @param array $data
     *
     * @return string
     */
    private function _actionButtons($data) {
        // pr($data);die;
        $state = 'State';
        $id = $data[$state]['id'];
        // $status = $data[$state]['active'];
        $status = 1;
        // print_r($a);
        if ($status !== 2) {
            // pr('dd');die;
            $editUrl = Router::url(["controller" => "states", "action" => "edit", "admin"=>true])."/".$id;
            $deleteUrl = Router::url(["controller" => "states", "action" => "delete", "admin"=>true])."/".$id;
            $onClick = 'onclick="deleteme('.$id.');"';
        
            $actions = ' <a href="'.$editUrl.'" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                    <i class="far fa-edit text-info"></i>
                </a>';

            $actions .= ' <a '.$onClick.' class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="far fa-trash-alt text-danger"></i>
                </a>';
                       
        } else {
            // pr('');die;
            $actions = '-';
        }       

        return $actions;
    }

    public static function getStatuses($key = NULL, $filter = []) {
        $statuses = [
            0 => "Inactive",
            1 => "Active",
            2 => "Deleted",
            3 => "Pending",
            4 => "In Progress",
            5 => "Completed",
            6 => "Close"
        ];

        if (!is_null($key)) {
            return isset($statuses[$key]) ? $statuses[$key] : '-';
        }

        if (!empty($filter)) {
            return array_filter($statuses, function ($k) use ($filter) {
                return in_array($k, $filter);
            }, ARRAY_FILTER_USE_KEY);
        }

        return $statuses;
    }
    
}