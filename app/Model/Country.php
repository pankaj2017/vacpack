<?php
App::uses('AppModel', 'Model');
/**
 * Country Model
 *
 * @property Zone $Zone
 * @property Buynsale $Buynsale
 * @property Eventsnshow $Eventsnshow
 */
class Country extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array();

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array();

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
      
        'State' => array(
            'className' => 'State',
            'foreignKey' => 'country_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
	function getshortname($countryid=null){
		if(!$countryid){
			
		}else{
			
			$short_name = $this->find('first',array('fields'=>array('iso_2'),'conditions'=>array('Country.id'=>$countryid)));
		}
		return $short_name;
	}
    /** ==========> Functions for DataTable starts <========== **/
    /**
     * Define columns/fileds you need to select
     * db: fields in database, dt: for datatables; not using dt for now
     *
     * @return array
     */
    public function getColumns() { 
        return [
            ['db' => 'Country.id', 'dt' => 'id'], 
            ['db' => 'Country.name', 'dt' => 'name'],
            ['db' => 'Country.iso_2', 'dt' => 'iso_2'],
            ['db' => 'Country.iso_3', 'dt' => 'iso_3'],
            ['db' => 'Country.phonecode', 'dt' => 'phonecode'],
            ['db' => 'Country.isonumeric', 'dt' => 'isonumeric'],
            ['db' => 'Country.lat', 'dt' => 'lat'],
            ['db' => 'Country.long', 'dt' => 'long'],
            ['db' => 'Country.active', 'dt' => 'Country'],
        ];
    }
    /**
     * Define search fields
     *
     * @return array
     */
    public function getSearchFields() {
        return [
            'name' => ['db' =>'Country.name'],
            'iso_2' => ['db' => 'Country.iso_2'],
            'iso_3' => ['db' => 'Region.iso_2'],
            'status' => ['db' => 'Country.active', 'type' => 'int'],
        ];
    }
    /**
     * Get listing data
     *
     * @return array
     */
    public function getListingData() {
        $this->recursive = 1;
        
        $result = $this->_dtData();
        // pr($result);die;
        return $result;
    }
    /**
     * Parse data; if you want to modify/alter data then define this function    * 
     *
     * @param array $data
     *
     * @return array                                  
     */
    public function parseData($data) {
        // pr($data);die;
        foreach ($data as $key => &$val) {
            $coun = 'Country';
            $id = $val[$coun]['id'];
            $viewUrl = Router::url(["controller" => "countries", "action" => "view", "admin"=>true])."/".$id;

            $val[$coun]['name'] = !empty($val[$coun]['name']) ? 
                '<a href="'.$viewUrl.'">'.$val[$coun]['name'].'</a>' : '-';
            $val[$coun]['iso_2'] = !empty($val[$coun]['iso_2']) ? $val[$coun]['iso_2'] : '-';        
            $val[$coun]['iso_3'] = !empty($val[$coun]['iso_3']) ? $val[$coun]['iso_3'] : '-';        
            $val[$coun]['actions'] = $this->_actionButtons($val);
            $val[$coun]['active'] = $this->_prepareStatusData($val);
        }
        return $data;
    }
    

    /**
     * Prepare status
     *
     * @param array $data
     *
     * @return string
     */
    private function _prepareStatusData($data) {
        $coun = 'Country';
        $id = $data[$coun]['id'];
        $status = $data[$coun]['active'];
        $statusName = self::getStatuses($status);
        $statusHtml = $lableClass = $onClick = $style = $other = '';

        if($status == 2) {
            $status_to_update = 1;
            $lableClass = 'label-danger';
            $url = Router::url(array('controller' => 'countries','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            // $style = 'pointer-events: none;';
        } else {
           $status_to_update = 0;
            if($status == 0) {
                $status_to_update = 1;
            }

            $url = Router::url(array('controller' => 'countries','action' => 'updatestatus','admin' => true))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            if ($status == 1) {
                $lableClass = 'label-success';
            }else if($status == 0) {
                $lableClass = 'label-warning';

            }
            
        }

        $statusHtml = '<a data-toggle="tooltip" data-placement="left" 
            data-original-title="Update status" class="label '.$lableClass.'" style="color:#f0f0f0;'.$style.'" '.$onClick.'>'.$statusName.'</a>'.$other;

        return $statusHtml;
    }

    /**
     * Prepare different actions
     *
     * @param array $data
     *
     * @return string
     */
    private function _actionButtons($data) {
        // pr($data);die;
        $coun = 'Country';
        $id = $data[$coun]['id'];
        $status = $data[$coun]['active'];
        // print_r($a);
        if ($status !== 2) {
            // pr('dd');die;
            $editUrl = Router::url(["controller" => "countries", "action" => "edit", "admin"=>true])."/".$id;
            $deleteUrl = Router::url(["controller" => "countries", "action" => "delete", "admin"=>true])."/".$id;
            $onClick = 'onclick="deleteme('.$id.');"';
        
            $actions = ' <a href="'.$editUrl.'" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                    <i class="far fa-edit text-info"></i>
                </a>';

            $actions .= ' <a '.$onClick.' class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="far fa-trash-alt text-danger"></i>
                </a>';
                       
        } else {
            // pr('');die;
            $actions = '-';
        }       

        return $actions;
    }

    public static function getStatuses($key = NULL, $filter = []) {
        $statuses = [
            0 => "Inactive",
            1 => "Active",
            2 => "Deleted",
            3 => "Pending",
            4 => "In Progress",
            5 => "Completed",
            6 => "Close"
        ];

        if (!is_null($key)) {
            return isset($statuses[$key]) ? $statuses[$key] : '-';
        }

        if (!empty($filter)) {
            return array_filter($statuses, function ($k) use ($filter) {
                return in_array($k, $filter);
            }, ARRAY_FILTER_USE_KEY);
        }

        return $statuses;
    }
}