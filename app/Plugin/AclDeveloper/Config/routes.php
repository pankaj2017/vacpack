<?php

Router::connect("/admin/acl-developers/:action/*", [
	'plugin' => 'AclDeveloper',
    'controller' => 'AclDevelopers',
    'admin' => true
]);

Router::connect("/admin/acl-developers/*", [
	'plugin' => 'AclDeveloper',
    'controller' => 'AclDevelopers',
    'action'=>'index',
    'admin' => true
]);