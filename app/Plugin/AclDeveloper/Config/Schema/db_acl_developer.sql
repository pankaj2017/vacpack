CREATE TABLE `aco_actions` (
  `id` int(10) NOT NULL,
  `aco_controller_id` int(10) DEFAULT NULL,
  `aco_id` int(10) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL
);

CREATE TABLE `aco_controllers` (
  `id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `plugin_name` varchar(255) DEFAULT NULL,
  `plugin` varchar(255) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL
);

ALTER TABLE `aco_actions` ADD PRIMARY KEY (`id`);
ALTER TABLE `aco_controllers` ADD PRIMARY KEY (`id`), ADD KEY `aco_id` (`aco_id`);

ALTER TABLE `aco_actions` MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
ALTER TABLE `aco_controllers` MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;