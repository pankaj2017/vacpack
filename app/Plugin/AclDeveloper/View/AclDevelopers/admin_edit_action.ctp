<?php $this->assign('title', 'ACL: Manage Action'); ?>
<div class="page-wrapper">
    <div class="row page-titles ">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">
                ACL: Manage Action
            </h3>
        </div>        
    </div>
    <div class="container-fluid">    
        <?php echo $this->element('common_flash_message'); ?> 
        <div class="row">
        	<div class="col-lg-12">
	            <div class="card">
	            	<div class="card-body">
	            		<div class="form-body">
	            			<h3 class="card-title">
	            				Edit Action
	            				<?php echo $this->Html->link('Back to Action List', [
		            				'controller' => 'AclDevelopers', 
		            				'action' => 'list_action', 
		            				'plugin' => 'AclDeveloper',
		            				$this->request->data['AcoController']['id']
		            			], ['class' => 'pull-right btn btn-info btn-sm']); ?>
	            			</h3>
	            			<hr>

	            			<?php echo $this->Form->create('AcoAction'); ?>
	            			<label>Controller: <?php echo $this->request->data['AcoController']['name']; ?></label>
	            			<label>Alias: <?php echo $this->request->data['AcoController']['alias']; ?></label>
							<?php								
								if (!empty($this->request->data['AcoController']['plugin_name'])) { ?>
									<div class="input text">
										<label>Plugin: <?php echo $this->request->data['AcoController']['plugin_name']; ?></label>				
									</div>
							<?php
								}
								echo $this->Form->input('id');
								echo $this->Form->hidden('aco_controller_id');
								echo $this->Form->input('name');
								echo $this->Form->input('status');
							?>							
							<?php echo $this->Form->end(__('Submit')); ?>
	            			
	            		</div>
	            	</div>
	            </div>
	        </div>
        </div>
    