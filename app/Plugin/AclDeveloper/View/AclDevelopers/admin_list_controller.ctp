<?php echo $this->Html->css("AclDeveloper.acl-developer"); ?>
<?php $this->assign('title', 'ACL: Controller List'); ?>
<div class="page-wrapper">
    <div class="row page-titles ">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">
                ACL: Controller List
            </h3>
        </div>        
    </div>
    <div class="container-fluid">    
        <?php echo $this->element('common_flash_message'); ?> 
        <div class="row">
        	<div class="col-lg-12">
	            <div class="card">
	            	<div class="card-body">
	            		<div class="form-body">
	            			<?php echo $this->Html->link('Sync Controller', [
	            				'controller' => 'AclDevelopers', 
	            				'action' => 'index', 
	            				'plugin' => 'AclDeveloper',
	            				'?' => ['sync' => 'Controller']
	            			], ['class' => 'btn btn-info btn-sm']); ?>
	            			
	            			<?php echo $this->Html->link('Sync Action', [
	            				'controller' => 'AclDevelopers', 
	            				'action' => 'index', 
	            				'plugin' => 'AclDeveloper',
	            				'?' => ['sync' => 'Action']
	            			], ['class' => 'btn btn-info btn-sm']); ?>

	            			<?php echo $this->Html->link('Back to Groups', [
	            				'controller' => 'AclDevelopers', 
	            				'action' => 'index', 
	            				'plugin' => 'AclDeveloper'
	            			], ['class' => 'btn btn-info btn-sm pull-right']); ?>

	            			<table class="table-striped table-bordered m-t-10" width="100%">
								<thead>
									<tr>
										<th><?php echo $this->Paginator->sort('id'); ?></th>
										<th><?php echo $this->Paginator->sort('alias'); ?></th>
										<th><?php echo $this->Paginator->sort('name'); ?></th>
										<th><?php echo $this->Paginator->sort('plugin_name'); ?></th>
										<th><?php echo h('Status'); ?></th>
										<th class="actions"><?php echo __('Actions'); ?></th>
									</tr>
								</thead>
								<tbody>		
								<?php foreach ($controllers as $controller): ?>
									<tr>
										<td><?php echo h($controller['AcoController']['id']); ?>&nbsp;</td>
										<td><?php echo h($controller['AcoController']['alias']); ?>&nbsp;</td>
										<td><?php echo h($controller['AcoController']['name']); ?>&nbsp;</td>
										<td><?php echo h($controller['AcoController']['plugin_name']); ?>&nbsp;</td>
										<td><?php echo h($controller['AcoController']['status_name']); ?>&nbsp;</td>
										<td class="actions">
											<?php echo $this->Html->link('Edit', [
												'action' => 'edit_controller', 
												$controller['AcoController']['id']
											], ['class' => 'btn btn-primary btn-sm']); ?>
											<?php echo $this->Html->link('Action List', [
												'action' => 'list_action', 
												$controller['AcoController']['id']
											], ['class' => 'btn btn-dark btn-sm']); ?>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
							<p>
								<?php
								echo $this->Paginator->counter(array(
									'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
								));
								?>	
							</p>
							<div class="paging">
								<?php
									echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
									echo $this->Paginator->numbers(array('separator' => ''));
									echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
								?>
							</div>

	            		</div>
	            	</div>
	            </div>
	        </div>
        </div>