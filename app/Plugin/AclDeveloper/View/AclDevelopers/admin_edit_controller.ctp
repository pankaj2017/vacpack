<?php $this->assign('title', 'ACL: Manage Controller'); ?>
<div class="page-wrapper">
    <div class="row page-titles ">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">
                ACL: Manage Controller
            </h3>
        </div>        
    </div>
    <div class="container-fluid">    
        <?php echo $this->element('common_flash_message'); ?> 
        <div class="row">
        	<div class="col-lg-12">
	            <div class="card">
	            	<div class="card-body">
	            		<div class="form-body">
	            			<h3 class="card-title">
	            				Edit Controller
	            				<?php echo $this->Html->link('Back to Controller', [
		            				'controller' => 'AclDevelopers', 
		            				'action' => 'list_controller', 
		            				'plugin' => 'AclDeveloper'
		            			], ['class' => 'pull-right btn btn-info btn-sm']); ?>
	            			</h3>
	            			<hr>

	            			<?php echo $this->Form->create('AcoController'); ?>
							<?php
								if (!empty($this->request->data['AcoController']['plugin_name'])) { ?>
									<div class="input text">
										<label>Plugin: <?php echo $this->request->data['AcoController']['plugin_name']; ?></label>				
									</div>
							<?php
								}
								echo $this->Form->input('id');
								echo $this->Form->input('name');
								echo $this->Form->input('status');
							?>							
							<?php echo $this->Form->end(__('Submit')); ?>
	            			
	            		</div>
	            	</div>
	            </div>
	        </div>
        </div>
    