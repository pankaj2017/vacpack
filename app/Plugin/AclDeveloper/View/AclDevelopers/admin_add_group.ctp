<?php $this->assign('title', 'ACL: Manage Group'); ?>
<div class="page-wrapper">
    <div class="row page-titles ">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">
                ACL: Manage Group
            </h3>
        </div>        
    </div>
    <div class="container-fluid">    
        <?php echo $this->element('common_flash_message'); ?> 
        <div class="row">
        	<div class="col-lg-12">
	            <div class="card">
	            	<div class="card-body">
	            		<div class="form-body">
	            			<h3 class="card-title">
	            				Add Group
	            				<?php echo $this->Html->link('Back', [
		            				'controller' => 'AclDevelopers', 
		            				'action' => 'index', 
		            				'plugin' => 'AclDeveloper'
		            			], ['class' => 'pull-right btn btn-info btn-sm']); ?>
	            			</h3>
	            			<hr>

	            			<?php echo $this->Form->create('Admingroup'); ?>
	            			<?php
	            			echo $this->Form->input('name');
	            			echo $this->Form->textarea('description');
	            			?>
							<?php echo $this->Form->end(__('Submit')); ?>
	            			
	            		</div>
	            	</div>
	            </div>
	        </div>
        </div>
    