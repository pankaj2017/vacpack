<?php echo $this->Html->css("AclDeveloper.acl-developer"); ?>
<?php $this->assign('title', 'ACL: Manage Permission'); ?>
<div class="page-wrapper">
    <div class="row page-titles ">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">
                ACL: Manage Permission
            </h3>
        </div>        
    </div>
    <div class="container-fluid">    
        <?php echo $this->element('common_flash_message'); ?> 
        <div class="row">
        	<div class="col-lg-12">
	            <div class="card">
	            	<div class="card-body">
	            		<div class="form-body">
	            			<h3 class="card-title">
	            				Edit Permission
	            				<?php echo $this->Html->link('Back', [
		            				'controller' => 'AclDevelopers', 
		            				'action' => 'index', 
		            				'plugin' => 'AclDeveloper'
		            			], ['class' => 'pull-right btn btn-info btn-sm']); ?>
	            			</h3>
	            			<hr>
							
							<h3><?php echo __('Group') . ": " . $group['Admingroup']['name']; ?></h3>
							<?php echo $this->Form->create('AroAco'); ?>
							<?php echo $this->Form->input('AroAco.aro', [
								'type' => 'hidden',
								'value' => $aro['Aro']['id']
							]); ?>
							<?php								
								$i = 0;
								$j = 0;
								foreach ($acoList as $key => $aco) {
									$controllerName = ' <span class="small-title">( ' . $aco['alias'] . ' )</span>';
									$pluginName = '';
									if (!empty($aco['plugin_name'])) {
										$pluginName = ' <span class="small-title">( Plugin: '.$aco['plugin_name'].' )</span>';
									}
									if (!empty($aco['childs'])) {
										$childOption = [];
										$allChecked = true;
										foreach ($aco['childs'] as $k => $child) {
											$childOption[$child['aco_id']] = $child['name'];
											if($allChecked && !in_array($child['aco_id'], $aroPermission)) {
												$allChecked = false;
											}
										}
							?>
									<div class="controller" id="controller_<?php echo $aco['aco_id']; ?>">
										<h3>
											<?php echo $aco['name'] . $controllerName . $pluginName; ?>
											<span class="check-all">
												<?php echo $this->Form->input('AroAco.controller.'.$i, array(
													'label' => 'Check All', 
													'class' => 'check-all',
													'type' => 'checkbox',
													'div' => false,
													'value' => $aco['aco_id'],
													'checked' => $allChecked
												)); ?>
											</span>					
										</h3>
										<div class="action-list">
										<?php
											foreach ($aco['childs'] as $k => $child) {
												$checked = in_array($child['aco_id'], $aroPermission) ? true : false;
												echo $this->Form->input('AroAco.aco_id.'.$j, array(
													'label' => '<span title="'.$child['alias'].'">'.$child['name'].'</span>',
													'type' => 'checkbox',							
													//'class' => 'action',
													'div' => ['class' => 'action'],
													'value' => $child['aco_id'],
													'required' => false,
													'checked' => $checked
												));
												$j++;
											}
										?>
										</div>
									</div>
							<?php
									$i++;
									}			
								}
							?>
							<?php echo $this->Form->end(__('Submit')); ?>           			
	            			
	            		</div>
	            	</div>
	            </div>
	        </div>
        </div>
<script type="text/javascript">
$(document).on('click', '.check-all', function(e) {
	var id = $(this).val();
	if ($(this).is(':checked')) {
		$('#controller_'+id+' .action-list').find('input[type="checkbox"]').each(function(e) {
			$(this).prop('checked', true);
		});
	} else {
		$('#controller_'+id+' .action-list').find('input[type="checkbox"]').each(function(e) {
			$(this).prop('checked', false);
		});
	}
});
</script>