<?php echo $this->Html->css("AclDeveloper.acl-developer"); ?>
<?php $this->assign('title', 'ACL: Group List'); ?>
<div class="page-wrapper">
    <div class="row page-titles ">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">
                ACL: Group List
            </h3>
        </div>        
    </div>
    <div class="container-fluid">    
        <?php echo $this->element('common_flash_message'); ?> 
        <div class="row">
        	<div class="col-lg-12">
	            <div class="card">
	            	<div class="card-body">
	            		<div class="form-body">
	            			<h3 class="card-title">Manage Group Permission</h3>
	            			<hr>
	            			
	            			<?php echo $this->Html->link('Controller List', [
	            				'controller' => 'AclDevelopers', 
	            				'action' => 'list_controller', 
	            				'plugin' => 'AclDeveloper'
	            			], ['class' => 'btn btn-info btn-sm']); ?>

	            			<?php echo $this->Html->link('Add Group', [
	            				'controller' => 'AclDevelopers', 
	            				'action' => 'add_group', 
	            				'plugin' => 'AclDeveloper'
	            			], ['class' => 'btn btn-info btn-sm']); ?>

	            			<table class="table-striped table-bordered m-t-10" width="100%">
								<thead>
									<tr>
										<th><?php echo $this->Paginator->sort('id'); ?></th>
										<th><?php echo $this->Paginator->sort('name'); ?></th>
										<th class="actions"><?php echo __('Actions'); ?></th>
									</tr>
								</thead>
								<tbody>		
								<?php foreach ($groups as $group): ?>
									<tr>
										<td><?php echo h($group['Admingroup']['id']); ?></td>
										<td><?php echo h($group['Admingroup']['name']); ?></td>
										<td class="actions">
											<?php echo $this->Html->link('Permission', [
												'action' => 'permission', 
												$group['Admingroup']['id']
											], ['class' => 'btn btn-primary btn-sm']); ?>
											<?php echo $this->Html->link('Edit', [
												'action' => 'edit_group', 
												$group['Admingroup']['id']
											], ['class' => 'btn btn-dark btn-sm']); ?>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
							<p>
								<?php
								echo $this->Paginator->counter(array(
									'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
								));
								?>	
							</p>
							<div class="paging">
								<?php
									echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
									echo $this->Paginator->numbers(array('separator' => ''));
									echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
								?>
							</div>
	            		</div>
	            	</div>
	            </div>
	        </div>
        </div>
    
