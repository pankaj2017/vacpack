<?php echo $this->Html->css("AclDeveloper.acl-developer"); ?>
<?php $this->assign('title', 'ACL: Action List'); ?>
<div class="page-wrapper">
    <div class="row page-titles ">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">
                ACL: Action List
            </h3>
        </div>        
    </div>
    <div class="container-fluid">    
        <?php echo $this->element('common_flash_message'); ?> 
        <div class="row">
        	<div class="col-lg-12">
	            <div class="card">
	            	<div class="card-body">
	            		<div class="form-body">	            			
	            			<h3 class="card-title">
	            				Controller: <?php echo $controller['AcoController']['name']; ?>
	            				<?php if ($controller['AcoController']['plugin_name'] != '') { ?>
	            					( Plugin: <?php echo $controller['AcoController']['plugin_name']; ?> )
	            				<?php } ?>
	            				<?php echo $this->Html->link('Back to Controller', [
		            				'controller' => 'AclDevelopers', 
		            				'action' => 'list_controller', 
		            				'plugin' => 'AclDeveloper',
		            				$controller['AcoController']['id']
		            			], ['class' => 'pull-right btn btn-info btn-sm']); ?>		
            				</h3>
	            			<hr>

	            			<?php echo $this->Html->link('Groups', [
	            				'controller' => 'AclDevelopers', 
	            				'action' => 'index', 
	            				'plugin' => 'AclDeveloper'
	            			], ['class' => 'btn btn-info btn-sm']); ?>

	            			<?php echo $this->Html->link('Controller List', [
	            				'controller' => 'AclDevelopers', 
	            				'action' => 'list_controller', 
	            				'plugin' => 'AclDeveloper'
	            			], ['class' => 'btn btn-info btn-sm']); ?>	            			
	            			<table class="table-striped table-bordered m-t-10" width="100%">
								<thead>
									<tr>
										<th><?php echo $this->Paginator->sort('id'); ?></th>
										<th><?php echo $this->Paginator->sort('alias'); ?></th>
										<th><?php echo $this->Paginator->sort('name'); ?></th>
										<th><?php echo h('Status'); ?></th>
										<th class="actions"><?php echo __('Actions'); ?></th>
									</tr>
								</thead>
								<tbody>		
								<?php foreach ($actions as $action): ?>
									<tr>
										<td><?php echo h($action['AcoAction']['id']); ?></td>
										<td><?php echo h($action['AcoAction']['alias']); ?></td>
										<td><?php echo h($action['AcoAction']['name']); ?></td>
										<td><?php echo h($action['AcoAction']['status_name']); ?></td>
										<td class="actions">
											<?php echo $this->Html->link('Edit', [
												'action' => 'edit_action', 
												$action['AcoAction']['id']
											], ['class' => 'btn btn-primary btn-sm']); ?>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
							<p>
								<?php
								echo $this->Paginator->counter(array(
									'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
								));
								?>	
							</p>
							<div class="paging">
								<?php
									echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
									echo $this->Paginator->numbers(array('separator' => ''));
									echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
								?>
							</div>
	            		</div>
	            	</div>
	            </div>
	        </div>
        </div>