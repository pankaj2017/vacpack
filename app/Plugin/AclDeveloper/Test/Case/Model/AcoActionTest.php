<?php
App::uses('AcoAction', 'AclDeveloper.Model');

/**
 * AcoAction Test Case
 */
class AcoActionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.acl_developer.aco_action',
		'plugin.acl_developer.aco_controller',
		'plugin.acl_developer.aco',
		'plugin.acl_developer.aro',
		'plugin.acl_developer.permission'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AcoAction = ClassRegistry::init('AclDeveloper.AcoAction');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AcoAction);

		parent::tearDown();
	}

}
