<?php
App::uses('AcoController', 'AclDeveloper.Model');

/**
 * AcoController Test Case
 */
class AcoControllerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.acl_developer.aco_controller',
		'plugin.acl_developer.aco',
		'plugin.acl_developer.aro',
		'plugin.acl_developer.permission',
		'plugin.acl_developer.aco_action'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AcoController = ClassRegistry::init('AclDeveloper.AcoController');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AcoController);

		parent::tearDown();
	}

}
