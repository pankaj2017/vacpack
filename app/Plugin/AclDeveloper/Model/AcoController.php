<?php
App::uses('AclDeveloperAppModel', 'AclDeveloper.Model');
/**
 * AcoController Model
 *
 * @property Aco $Aco
 * @property AcoAction $AcoAction
 */
class AcoController extends AclDeveloperAppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Aco' => array(
			'className' => 'Aco',
			'foreignKey' => 'aco_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'AcoAction' => array(
			'className' => 'AcoAction',
			'foreignKey' => 'aco_controller_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function getList($conditions = []) {
		$this->recursive = 0;
		$controllers = $this->find('all', [
			'conditions' => ['AcoController.status' => 1],
			'fields' => ['AcoController.*']
		]);
		
		$controllers = Hash::extract($controllers, '{n}.AcoController');		

		foreach ($controllers as $key => &$controller) {
			$aco_controller_id = $controller['id'];
			$actions = $this->AcoAction->find('all', [
				'conditions' => [
					'AcoAction.aco_controller_id' => $aco_controller_id,
					'AcoAction.status' => 1
				] + $conditions,
				'order' => ['AcoAction.id ASC'],
				'fields' => ['AcoAction.*']
			]);

			$actions = Hash::extract($actions, '{n}.AcoAction');
			$controller['childs'] = $actions;
		}

		return $controllers;
	}
}
