<?php
App::uses('AclDeveloperAppModel', 'AclDeveloper.Model');
/**
 * AcoAction Model
 *
 * @property AcoController $AcoController
 * @property Aco $Aco
 */
class AcoAction extends AclDeveloperAppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'AcoController' => array(
			'className' => 'AcoController',
			'foreignKey' => 'aco_controller_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Aco' => array(
			'className' => 'Aco',
			'foreignKey' => 'aco_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function getWhiteListPermissions($controller = null) {
		$params = Router::getParams();
		$plugin = $params['plugin'];
		$conditions = ['AcoAction.status' => 0];
		$fields = [];
		if ($controller) {
			$conditions += ['AcoController.name' => $controller];

			if ($plugin != '') {
				$conditions += ['AcoController.plugin_name' => $plugin];        
			}
			$fields = ['AcoAction.id', 'AcoAction.alias'];
		}

		$acoAction = $this->find('all', [
			'conditions' => $conditions,
			'fields' => $fields
		]);

		if (is_null($controller)) {
			return $this->_getUrls($acoAction);
		}		
		
		$actions = Hash::extract($acoAction, '{n}.AcoAction');

		$whiteListPermission = !empty($actions) ? array_column($actions, 'alias') : [];
		
		return $whiteListPermission;
	}

	private function _getUrls($acoAction) {
		$result = [];
		if (!empty($acoAction)) {
			foreach ($acoAction as $key => $action) {
				$url = 'controllers/';
				if ($action['AcoController']['plugin_name'] != '') {
					$url .= $action['AcoController']['plugin_name'] .'/';
				}
				$url .= $action['AcoController']['alias'] .'/';
				$url .= $action['AcoAction']['alias'];
				
				$result[] = $url;
			}
		}
		return $result;
	}
}
