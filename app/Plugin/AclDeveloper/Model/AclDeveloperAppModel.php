<?php

App::uses('AppModel', 'Model');

class AclDeveloperAppModel extends AppModel {
	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	public static function getStatus($key = null) {
		$statuses = [
			self::STATUS_ACTIVE => "Active",
			self::STATUS_INACTIVE => "Inactive"
		];

		if (!is_null($key) && isset($statuses[$key])) {
			return $statuses[$key];
		}
		return $statuses;
	}

	public function afterFind($results, $primary = false) {
		parent::afterFind($results, $primary);		
		foreach ($results as $key => &$result) {
			if(isset($result[$this->name]['status'])) {
				$result[$this->name]['status_name'] = self::getStatus($result[$this->name]['status']);	
			}            
        }

        return $results;
	}
}