<?php
App::uses('AppModel', 'Model');

class AroAco extends AclDeveloperAppModel {
    public $useTable = 'aros_acos';

    public $belongsTo = array(
        'Aco' => array(
            'className' => 'Aco',
            'foreignKey' => 'aco_id',
        ),
        'Aro' => array(
            'className' => 'Aro',
            'foreignKey' => 'aro_id',
        ),
    );
}