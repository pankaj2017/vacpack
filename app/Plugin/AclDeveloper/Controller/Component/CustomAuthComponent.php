<?php
App::import('Component','Auth');

class CustomAuthComponent extends AuthComponent {
	protected function _unauthorized(Controller $controller) {
		if ($this->unauthorizedRedirect === false) {
			throw new ForbiddenException($this->authError);
		}
		
		if ($this->unauthorizedRedirect === true) {
			$default = '/';
			if (!empty($this->loginRedirect)) {
				$default = $this->loginRedirect;
			}
			$url = $controller->referer($default, true);
		} else {
			$url = $this->unauthorizedRedirect;
		}

		if ($controller->request->is('ajax')) {
			$controller->response->statusCode(403);
			$controller->response->send();
			$this->_stop();
		} else {
			$this->flash($this->authError);
			$controller->redirect($url);	
		}		
		return false;
	}
}