<?php

App::uses('AclDeveloperAppController', 'AclDeveloper.Controller');

/**
 * AclDeveloper Controller
 *
 */
class AclDevelopersController extends AclDeveloperAppController {

	public $uses = [
		'Admingroup', 
		'Aro', 
		'Aco', 
		'AclDeveloper.AroAco', 
		'AclDeveloper.AcoController', 
		'AclDeveloper.AcoAction'
	];

	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function admin_index() {
		if(isset($this->request->query['sync'])) {
			$syncParam = $this->request->query['sync'];
			if(in_array($syncParam, ['Controller', 'Action'])) {
				$this->{"_acoSync".$syncParam}();
			} else if($syncParam == 'aco') {

			}
		}

		$this->Admingroup->recursive = 0;
		$groups = $this->Paginator->paginate($this->Admingroup);

		$this->set('groups', $this->Paginator->paginate($this->Admingroup));
	}

	public function admin_add_group() {
		if ($this->request->is('post')) {
			$this->Admingroup->create();
			if ($this->Admingroup->save($this->request->data)) {
				$this->Flash->success(__('The group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The group could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit_group($id = null) {
		if (!$this->Admingroup->exists($id)) {
			throw new NotFoundException(__('Invalid group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Admingroup->save($this->request->data)) {
				$this->Flash->success(__('The group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The group could not be saved. Please, try again.'));
			}
		}

		$options = array('conditions' => array('Admingroup.' . $this->Admingroup->primaryKey => $id));
		$this->request->data = $this->Admingroup->find('first', $options);
	}

	public function admin_list_controller() {
		$this->AcoController->recursive = 0;

		$this->Paginator->settings = array(
		    'limit' => 25
		);
		$this->set('controllers', $this->Paginator->paginate($this->AcoController));
	}

	public function admin_edit_controller($id = null) {
		if (!$this->AcoController->exists($id)) {
			throw new NotFoundException(__('Invalid Controller'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->AcoController->save($this->request->data)) {
				$this->Flash->success(__('The controller has been saved.'));
				return $this->redirect(array('action' => 'list_controller'));
			} else {
				$this->Flash->error(__('The controller could not be saved. Please, try again.'));
			}
		}

		$options = array('conditions' => array('AcoController.' . $this->AcoController->primaryKey => $id));
		$this->request->data = $this->AcoController->find('first', $options);		
		$statuses = AcoController::getStatus();
		$this->set(compact('statuses'));
	}

	public function admin_list_action($id = null) {
		if (!$this->AcoController->exists($id)) {
			throw new NotFoundException(__('Invalid Controller'));
		}

		$this->AcoAction->recursive = 0;
		$this->Paginator->settings = array(
			'conditions' => [
				'AcoAction.aco_controller_id' => $id,
				//'AcoAction.alias like ' => 'admin_%'
			],
		    'limit' => 25,
		);
		$actions = $this->Paginator->paginate($this->AcoAction);

		$options = array('conditions' => array('AcoController.' . $this->AcoController->primaryKey => $id));
		$controller = $this->AcoController->find('first', $options);
		$this->set('controller', $controller);
		$this->set('actions', $actions);
	}

	public function admin_edit_action($id = null) {
		if (!$this->AcoAction->exists($id)) {
			throw new NotFoundException(__('Invalid action'));
		}

		if ($this->request->is(array('post', 'put'))) {			
			if ($this->AcoAction->save($this->request->data)) {
				$this->Flash->success(__('The action has been saved.'));
				return $this->redirect(array('action' => 'list_action', $this->request->data['AcoAction']['aco_controller_id']));
			} else {
				$this->Flash->error(__('The action could not be saved. Please, try again.'));
			}
		}

		$options = array('conditions' => array('AcoAction.' . $this->AcoAction->primaryKey => $id));
		$this->request->data = $this->AcoAction->find('first', $options);		
		$statuses = AcoAction::getStatus();
		$this->set(compact('statuses'));
	}

	public function admin_permission($id = null) {
		if (!$this->Admingroup->exists($id)) {
			throw new NotFoundException(__('Invalid group'));
		}

		$options = array('conditions' => array('Admingroup.' . $this->Admingroup->primaryKey => $id));
		$group = $this->Admingroup->find('first', $options);

		$aro = $this->Aro->find('first', array(
            'conditions' => array(
                'Aro.model' => 'Admingroup',
                'Aro.foreign_key' => $id,
            )
        ));

        $acoList = $this->AcoController->getList([
        	'or' => [
        		['AcoAction.alias like' => 'admin_%'],
        		['AcoAction.alias like' => 'report_%']
        	]
        ]);

        $aroPermission = $this->AroAco->find('list', array(
			'conditions'=> array(
				'AroAco.aro_id' => $aro['Aro']['id'],
			), 
			'fields' => array(
				'AroAco.aco_id'
			)
		));

		if ($this->request->is('post')) {
			$data = $this->_prepareData($this->request->data);			

			$aroId = $this->request->data['AroAco']['aro'];
			$acoIds = array_filter($this->request->data['AroAco']['aco_id']);
			
			$aroAcoIds = !empty(array_column($data, 'id')) ? ['AroAco.id NOT IN' => array_column($data, 'id')] : [];			
			$this->AroAco->deleteAll([
				'AroAco.aro_id' => $aroId				
			] + $aroAcoIds);
			
            if ($this->AroAco->saveAll($data)) {
            	$this->Flash->success(__('Permission has been saved.'));
            	return $this->redirect(array('action' => 'index'));
            } else {
            	$this->Flash->error(__('Permission could not be saved. Please, try again.'));
            }
		}
		$this->set(compact('group', 'aro', 'acoList', 'aroPermission'));		
	}

	private function _prepareData($data) {
		$aroId = $this->request->data['AroAco']['aro'];
		$acoIds = array_filter($this->request->data['AroAco']['aco_id']);
		$result = [];
		if (!empty($acoIds)) {
			foreach ($acoIds as $key => $val) {
				$permissions = $this->AroAco->find('first', array(
					'conditions'=>array(
						'AroAco.aco_id' => $val,
						'AroAco.aro_id' => $aroId,
					)
				));
				$param = [];
				if (!empty($permissions)) {
					$param['id'] = $permissions['AroAco']['id'];
				}

				$result[] = $param + [
					'aro_id' => $aroId,
					'aco_id' => $val,
					'_create' => 1,
					'_read' => 1,
					'_update' => 1,
					'_delete' => 1,
				];
			}
		}
		return $result;
	}

	private function _acoSyncController() {
		$acoList = $this->_getAcoList();
		ksort($acoList);

		$acoController = [];
		$i = 0;
		foreach (array_values($acoList) as $key => $aco) {
			$controller = $this->AcoController->find('first', array(
				'conditions'=>array(
					'AcoController.aco_id' => $aco['id'],
					'AcoController.name' => $aco['name'],
				)
			));

			if(empty($controller)) {
				$acoController[$i] = [
					'aco_id' => $aco['id'],
					'alias' => $aco['name'],
					'name' => $aco['name'],
					'plugin_name' => !empty($aco['plugin']) ? $aco['plugin']['name'] : null,
					'plugin' => !empty($aco['plugin']) ? json_encode($aco['plugin']) : null,
					'status' => 1
				];	
				$i++;
			}
			
		}
		
		if(empty($acoController)) {
			$this->Flash->success(__('Controller already synced.')); 
		} else if ($this->AcoController->saveAll($acoController)) {
			$this->Flash->success(__('Controller synced.'));        	
		} else {
			$this->Flash->error(__('Controller could not be Synced. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	private function _acoSyncAction() {
		$acoList = $this->_getAcoList();
		ksort($acoList);

		$actions = [];
		$i = 0;
		foreach ($acoList as $key => $aco) {
			if (!empty($aco['childs'])) {
				ksort($aco['childs']);
				$controller = $this->AcoController->find('first', array(
					'conditions'=>array(
						'AcoController.aco_id' => $aco['id']
					)
				));	
				$controllerId = !empty($controller) ? $controller['AcoController']['id'] : null;

				foreach ($aco['childs'] as $cKey => $child) {
					$acoAction = $this->AcoAction->find('first', array(
						'conditions'=>array(
							'AcoAction.aco_controller_id' => $controllerId,
							'AcoAction.aco_id' => $child['id'],
						)
					));

					if (empty($acoAction)) {
						$actions[$i] = [
							'aco_controller_id' => $controllerId,
							'aco_id' => $child['id'],
							'name' => $child['name'],
							'alias' => $child['alias'],
							'status' => 1
						];
						$i++;						
					}
				}
			}
		}
		
		if(empty($actions)) {
			$this->Flash->success(__('Actions already synced.')); 
		} else if ($this->AcoAction->saveAll($actions)) {
			$this->Flash->success(__('Actions synced.'));        	
		} else {
			$this->Flash->error(__('Actions could not be Synced. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	private function _getAcoList($divisor = '->') {
        $groupID = 1;

        $this->paginate = array(
            'order' => array('parent_id', 'id'),
        );

        $controllersActions = $this->Aco->find('threaded', array(
            'order'=>array(
                'Aco.model'=>'asc'
            )
        ));
        
        foreach ($controllersActions as $ca) {
        	$actions = array();
            $ca['children'] = Hash::sort($ca['children'], '{n}.Aco.alias', 'asc');
            foreach ($ca['children'] as $firstLevel) {
            	$plugin = $childs = [];

            	$firstLvlAlias = $firstLevel['Aco']['alias'];
            	$firstLvlId = $firstLevel['Aco']['id'];

            	$controllerName = $controllerAlias = $firstLvlAlias;
            	$controllerId = $firstLvlId;

            	if (!empty($firstLevel['children'])) {
            		$firstLevel['children'] = Hash::sort($firstLevel['children'], '{n}.Aco.alias', 'asc');
            		
            		foreach ($firstLevel['children'] as $secondLevel) {
            			$secondLvlAlias = $secondLevel['Aco']['alias'];
            			$secondLvlId = $secondLevel['Aco']['id'];
            			$isPlugin = (!empty($secondLevel['children']) 
            				|| $secondLevel['Aco']['parent_id'] == 1) ? true : false;

            			if ($isPlugin) {
            				$childs = [];
            				$controllerName = $secondLvlAlias;
            				//$controllerAlias .= $divisor.$secondLvlAlias;
            				$controllerId = $secondLvlId;           				

            				$plugin['name'] = $firstLvlAlias;
							$plugin['id'] = $firstLvlId;

							foreach ($secondLevel['children'] as $thirdLevel) {
								$thirdLvlId = $thirdLevel['Aco']['id'];
								$thirdLvlAlias = $thirdLevel['Aco']['alias'];

								$childs[$thirdLvlId] = [
	            					'name' => $thirdLvlAlias,
	            					'alias' => $thirdLvlAlias,
	            					'id' => $thirdLvlId
	            				];
							}

							if (!empty($childs)) {
								$actions[$controllerId]['name'] = $controllerName;
				            	$actions[$controllerId]['alias'] = $controllerAlias.$divisor.$secondLvlAlias;
				            	$actions[$controllerId]['id'] = $controllerId;
				            	$actions[$controllerId]['plugin'] = $plugin;
				            	$actions[$controllerId]['childs'] = $childs;							
							}

            			} else {
            				$childs[$secondLvlId] = [
            					'name' => $secondLvlAlias,
            					'alias' => $secondLvlAlias,
            					'id' => $secondLvlId
            				];
            			}
            		}

            	}

            	if (!empty($childs)) {
	            	$actions[$controllerId]['name'] = $controllerName;
	            	$actions[$controllerId]['alias'] = $controllerAlias;
	            	$actions[$controllerId]['id'] = $controllerId;
	            	$actions[$controllerId]['plugin'] = $plugin;
	            	$actions[$controllerId]['childs'] = $childs;
	            }
            }
        }

        return $actions;
    }
}