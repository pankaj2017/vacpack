<?php

App::uses('AppController', 'Controller');

class AclDeveloperAppController extends AppController {
	public function beforeFilter(){        
        $this->Auth->allow();
        parent::beforeFilter();
    }
}
