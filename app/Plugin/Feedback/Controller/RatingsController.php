<?php

App::uses('FeedbackAppController', 'Feedback.Controller');

class RatingsController extends FeedbackAppController
{
	public $components = array('Feedback.Ratings');

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
       
		if ($this->request->params['action'] == 'add')
		{
			$this->viewClass = 'Json';
			$this->response->type('json');
		}
	}

	public function admin_index() {
		$this->_ajaxCall();
	}
	private function _ajaxCall() {
        if (isset($this->request->query['ajax'])) {
            if ($this->isajaxcallonly()) {
                $this->autoRender = false;
                $param = $this->request->query['ajax'];
             
                if($param == 'get-ratings-list') {
                    return $this->_getRatingsList();
                }
                return;    
            }            
        }
    }

    private function _getRatingsList() {
        $data = $this->Rating->getListingData();
        echo json_encode($data);
        exit;
	}
	
	public function add($foreign_model = null, $foreign_id = null,$ratingtypeid =null){
		
		if (empty($foreign_model) || empty($foreign_id) || !$this->request->is('post') || !$this->request->is('ajax')){
			return $this->redirect('/');
		}

		list($model, $modelClass) = $this->parseModel($foreign_model);

		App::uses($model, 'Model');
		$this->loadModel($model);

		if (!($this->{$modelClass} instanceof $modelClass)){
			return $this->redirect('/');
		}

		$row_exists = $this->{$modelClass}->hasAny(
			array($this->{$modelClass}->primaryKey => $foreign_id)
		);

		if (!$row_exists){
			return $this->redirect('/');
		}

		if (!isset($this->request->data['Rating']['foreign_model']) || !isset($this->request->data['Rating']['foreign_id']) || $this->request->data['Rating']['foreign_model'] != $modelClass || $this->request->data['Rating']['foreign_id'] != $foreign_id){
			return $this->redirect('/');
		}

		$this->request->data['Rating']['foreign_model'] = $this->{$modelClass}->name;
		$this->request->data['Rating']['foreign_id'] = $foreign_id;
		$this->request->data['Rating']['authorip'] = $this->request->clientIp();
		$this->request->data['Rating']['ratingtype_id'] = $ratingtypeid;

	/*	$existingrating=$this->Rating->find('all',['conditions'=>['Rating.foreign_id'=>$foreign_id ,'Rating.foreign_model'=>$this->{$modelClass}->name,'Rating.authorip' =>$this->request->clientIp()]]);

		if(!empty($existingrating))
		{
			$output = array
				(
					'success' => false,
					'message' => __('You cannot vote more than once!'),
					'data' => array(),
				);

			$this->set('output', $output);
			return;
		}*/
		$cookie = $this->Ratings->readCookie($this->request->data['Rating']['foreign_model']."_".$ratingtypeid."_".$foreign_id."_".$this->Session->read('Auth.User.id'));

		if (isset($cookie) && ($cookie != '') && ((int)$cookie > 0)){//$cookie[$this->request->data['Rating']['foreign_model']."_".$ratingtypeid."_".$foreign_id."_".$this->Session->read('Auth.User.id')]
			$output = array(
					'success' => false,
					'message' => __('You cannot vote more than once!'),
					'data' => array(),
				);

			$this->set('output', $output);
			return false;
			exit;
		}
		$this->Rating->create();

		if (!$this->Rating->save($this->request->data)){
			/*$this->Rating->last_queries();die;*/
			$this->log(serialize($this->Rating->validationErrors));

			$output = array
				(
					'success' => false,
					'message' => __('There was an error while saving your vote'),
					'data' => array(),
				);
			$this->set('output', $output);
			return false;
			exit;
		}

		//$cookie[$foreign_id] = $this->request->data['Rating']['rating'];
		$this->Ratings->writeCookie($this->request->data['Rating']['foreign_model']."_".$ratingtypeid."_".$foreign_id."_".$this->Session->read('Auth.User.id'),$this->request->data['Rating']['rating']);

		$updated = $this->{$modelClass}->read(null, $foreign_id);

		$output = array(
			'success' => true,
			'message' => __('Thanks for voting!'),
			'data' => $updated['RatingSummary'],
		);
		$this->set('output', $output);
	}
	/**
	 * Takes input in form of a model name, with or without the plugin:
	 *
	 *  'Post'
	 *  'Blog.Post'
	 *
	 * Returns an array of model path and model class:
	 *
	 *  array('Post', 'Post')
	 *  array('Blog.Post', 'Post')
	 *
	 * @param string $model
	 * @return array An array of model path and model class.
	 */
	private function parseModel($model)
	{
		if (strpos($model, '.') === false)
		{
			return array($model, $model);
		}

		list($model, $modelClass) = pluginSplit($model, true);
		$model .= $modelClass;

		return array($model, $modelClass);
	}
}
