<?php 

App::uses('FeedbackAppController','Feedback.Controller');

class RatingTypesController extends FeedbackAppController
{
	public $uses = ['Feedback.RatingType'];

	public function beforeFilter() {        
        $this->Auth->allow();
        parent::beforeFilter();
    }

	public function admin_index() {
		$this->_ajaxCall();
		$this->loadModel('Feedback.RatingType'); 

		$status = RatingType::getStatuses(null, [0,1]);
		$this->set(compact('status'));
	}

	private function _details($id = null)
	{
		$options =['recursive'=>-1,'conditions' =>['RatingType.' . $this->RatingType->primaryKey => $id]];
		$data = $this->RatingType->find('first', $options);
		return $data ;
	}

	private function _ajaxCall() {
        if (isset($this->request->query['ajax'])) {
            if ($this->isajaxcallonly()) {
                $this->autoRender = false;
                $param = $this->request->query['ajax'];
             
                if($param == 'get-ratingtypes-list') {
                    return $this->_getRatingTypeList();
                }
                return;    
            }            
        }
    }

    private function _getRatingTypeList() {
        $data = $this->RatingType->getListingData();
        echo json_encode($data);
        exit;
	}

	public function admin_add() {
		$this->set('activeratingtypelist','active');

		if (!empty($this->request->data['RatingType'])) 
		{	
			// pr($this->request->data);die;
			$this->RatingType->create();
			if($this->RatingType->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Rating type created successfully.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect(['controller'=>'RatingTypes', 'action'=>'index', 'admin'=>true]);
			}
			else 
			{
				$this->set('errors',$this->RatingType->validationErrors);
			}
		}
	}
	public function admin_edit($id=null)
	{	
		$this->set('activeratingtypelist','active');
		if (!$this->RatingType->exists($id)) 
		{
			throw new NotFoundException(__('Invalid Rating type '));
		}
		if ($this->request->is(array('post', 'put'))) 
		{
			if ($this->RatingType->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Rating type has been saved.'), 'default',['class' => 'alert alert-success']);
				return $this->redirect(['action' => 'index']);
			} 
			else 
			{
				$this->set('errors',$this->RatingType->validationErrors);
			}
		} 
		else 
		{
			$this->request->data = $this->_details($id);
		}
	}

	public function admin_updatestatus($id = null, $status = null){
		if(!$this->Session->read('Auth.Admin.id')){
			$this->Session->setFlash(__('Please login to access this location.'), 'default', ['class' => 'alert alert-danger']);
            $this->redirect(['controller' => 'admins', 'action' => 'login','plugin'=>false]);
		}
		$this->RatingType->id = $id;
		if($this->RatingType->saveField('status',$status)){
			$this->Session->setFlash(__('RatingType Type status has been updated.'), 'default',['class' => 'alert alert-success']);
			return $this->redirect(['action' => 'index','admin'=>true,'plugin'=>'feedback']);
		}else{
			$this->Session->setFlash(__('RatingType Type status has not been saved. Please try again.'), 'default',['class' => 'alert alert-danger']);
			return $this->redirect(['action' => 'index','admin'=>true,'plugin'=>'feedback']);	
		}	
	}
}