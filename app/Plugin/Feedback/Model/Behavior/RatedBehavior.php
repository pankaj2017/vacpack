<?php
/**
	CakePHP Feedback Plugin

	Copyright (C) 2012-3827 dr. Hannibal Lecter / lecterror
	<http://lecterror.com/>

	Multi-licensed under:
		MPL <http://www.mozilla.org/MPL/MPL-1.1.html>
		LGPL <http://www.gnu.org/licenses/lgpl.html>
		GPL <http://www.gnu.org/licenses/gpl.html>
*/

App::uses('ModelBehavior', 'Model');

class RatedBehavior extends ModelBehavior
{
	public function setup(Model $Model, $config = array())
	{
		$Model->bindModel
			(
				array
				(
					'hasMany' => array
					(
						'Rating' => array
						(
							'className'		=> 'Feedback.Rating',
							'conditions'	=> sprintf('Rating.foreign_model = \'%s\'', $Model->name),
							'foreignKey'	=> 'foreign_id',
						)
					)
        		),
				false
			);
	}

	public function cleanup(Model $Model)
	{
		$Model->unbindModel(array('hasMany' => array('Rating')), false);
	}

	// @TODO: implement bayesian average as a behaviour and use that
	// @TODO: this doesn't seem to be called when query is not primary...
	// why the arse do we have the $primary param then..?
	/*
	function afterFind(Model $Model, $results, $primary = false)
	{
		if ($primary)
		{
			foreach ($results as &$result)
			{
				$data = Hash::extract($result, 'Rating.{n}.rating');

				if (!empty($data))
				{
					$total = array_sum($data);
					$votes = count($data);
					$rating = round($total / $votes, 2);

					$result['RatingSummary'] =
						array
						(
							'total_votes' => $votes,
							'total_rating' => $rating
						);
				}
			}
		}

		return $results;
	}
	*/
	function afterFind(Model $Model, $results, $primary = false)
	{
		if ($primary)
		{
			foreach ($results as &$result)
			{
				$data = Hash::extract($result, 'Rating.{n}.rating');

				if (!empty($data))
				{
					$total = array_sum($data);
					$votes = count($data);
					$rating = round($total / $votes, 2);
 
					$result['RatingSummary'] =
						array
						(
							'total_votes' => $votes,
							'total_rating' => $rating
						);
				}
				// set rating type and rating summary array with multiplerating pannel
				
				$ratingid_array=ClassRegistry::init('Feedback.RatingType')->find('list',['conditions'=>['RatingType.foreign_model'=>$Model->name,
		            	'RatingType.status'=>1]]);
				$result['Ratingtypes']=$ratingid_array;

				$ratingArray = Hash::extract($result, 'Rating');
				if(!empty($ratingArray)){
					$ratingsummary=array('total_votes'=>0,'total_rating'=>0);

					$ratingid_array = array_unique(Hash::extract($result, 'Rating.{n}.ratingtype_id'));
					$id = current(array_unique(Hash::extract($result, 'Rating.{n}.foreign_id')));
					$modelName = current(array_unique(Hash::extract($result, 'Rating.{n}.foreign_model')));  

					foreach($ratingid_array  as $ratingid)
		            {
		                $ratingsummary[$modelName.'_'.$ratingid.'_'.$id]= ClassRegistry::init('Rating')->find("all", array(
		                    "fields"     => array("AVG(Rating.rating) AS AverageRating"),
		                    "conditions" => ['Rating.foreign_id'=>$id ,'Rating.foreign_model'=>$modelName,'Rating.ratingtype_id'=>$ratingid]
		                    ))[0][0]['AverageRating'];
		            }
		            $result['ratingsummary']=$ratingsummary; 
				}
			}
		}
		return $results;
	}
}
