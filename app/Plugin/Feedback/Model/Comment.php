<?php

App::uses('FeedbackAppModel', 'Feedback.Model');

class Comment extends FeedbackAppModel
{

	public function afterSave($created, $options = array())
	{
		if ($created) {
			$systemcount['variable']='commentcount';
			$this->updatesystemcount($systemcount);			
		}
	}

/*	public $validate = array
		(
			'foreign_model' => array
			(
				'notBlank' => array
				(
					'rule' => array('notBlank'),
					'allowEmpty' => false,
					'required' => true,
				),
			),
			'foreign_id' => array
			(
				'notBlank' => array
				(
					'rule' => array('notBlank'),
					'allowEmpty' => false,
					'required' => true,
				),
			),
			'authorname' => array
			(
				'notBlank' => array
				(
					'rule' => array('notBlank'),
					'message' => 'Name cannot be empty',
					'allowEmpty' => false,
					
				),
			),
			'authoremail' => array
			(
				'notBlank' => array
				(
					'rule' => array('notBlank'),
					'message' => 'E-mail address cannot be empty',
					'allowEmpty' => false,
					
				),
				'email' => array
				(
					'rule' => array('email'),
					'message' => 'E-mail address is not valid',
				)
			),
			'authorip' => array
			(
				'ip' => array
				(
					'rule' => array('ip'),
					'message' => 'Invalid IP address',
					'allowEmpty' => true,
				)
			),
			'content' => array
			(
				'notBlank' => array
				(
					'rule' => array('notBlank'),
					'message' => 'Comment cannot be empty',
					'allowEmpty' => false,
					
				),
			),
		);

	*/
	public $belongsTo = array
		(
			'User' => array
			(
				'className' => 'User',
				'foreignKey' => 'user_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			)
		);

	public $actsAs = array
		(
			/*'Feedback.Polymorphic' => array
			(
				'modelField'	=> 'foreign_model',
				'foreignKey'	=> 'foreign_id'
			)
			'Feedback.Honeypot' => array
			(
				'field' => 'hairy_pot',
				'message' => 'E-mail address is not valid',
				'errorField' => 'authoremail',
			),*/
		);

	public $order = 'Comment.created ASC';
	/** ==========> Functions for DataTable starts <========== **/
    /**
     * Define columns/fileds you need to select
     * db: fields in database, dt: for datatables; not using dt for now
     *
     * @return array
     */
    public function getColumns() { 
        return [
            ['db' => 'Comment.id', 'dt' => 'id'], 
            ['db' => 'Comment.user_id', 'dt' => 'user_id'], 
            ['db' => 'User.firstname', 'dt' => 'firstname'], 
            ['db' => 'User.lastname', 'dt' => 'lastname'], 
            ['db' => 'Comment.foreign_model', 'dt' => 'foreign_model'],
            ['db' => 'Comment.content', 'dt' => 'content'],
            ['db' => 'Comment.created', 'dt' => 'created'],
        ];  
    }
    /**
     * Define search fields
     *
     * @return array
     */
    public function getSearchFields() {
        return [
            'module' => ['db' => 'Comment.foreign_model'],
            'content' => ['db' => 'Comment.content'],
            'created' => ['db' => 'Comment.created'],
        ];
    }
    /**
     * Get listing data
     *
     * @return array
     */
    public function getListingData() {
        // $this->recursive = -1;
       	$options = [
             'order' => ['Comment.created' => 'ASC']
        ];
        $result = $this->_dtData($options);
        return $result;
    }
    /**
     * Parse data; if you want to modify/alter data then define this function    * 
     *
     * @param array $data
     *
     * @return array                                  
     */
    public function parseData($data) {
        foreach ($data as $key => &$val) {
            $c = 'Comment';
            $u = 'User';
            $id = $val[$c]['id'];
    
            $val[$c]['foreign_model'] = !empty($val[$c]['foreign_model']) ? $val[$c]['foreign_model'] : '-';     
            $val[$u]['username'] = !empty($val[$u]['firstname']) ? $val[$u]['firstname']. ' '.$val[$u]['lastname'] : '-'; 
            $val[$c]['content'] = !empty($val[$c]['content']) ? substr_replace($val[$c]['content'], "...", 40) : '-';     
            $val[$c]['created'] = !empty($val[$c]['created']) ? $val[$c]['created'] : '-';     	
        }
        return $data;
    }
 
    public static function getStatuses($key = NULL, $filter = []) {
        $statuses = [
            0 => "Inactive",
            1 => "Active",
            2 => "Deleted",
            3 => "Pending",
            4 => "In Progress",
            5 => "Completed",
            6 => "Close"
        ];

        if (!is_null($key)) {
            return isset($statuses[$key]) ? $statuses[$key] : '-';
        }

        if (!empty($filter)) {
            return array_filter($statuses, function ($k) use ($filter) {
                return in_array($k, $filter);
            }, ARRAY_FILTER_USE_KEY);
        }

        return $statuses;
    }
}
?>