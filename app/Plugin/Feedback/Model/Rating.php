<?php
/**
	CakePHP Feedback Plugin

	Copyright (C) 2012-3827 dr. Hannibal Lecter / lecterror
	<http://lecterror.com/>

	Multi-licensed under:
		MPL <http://www.mozilla.org/MPL/MPL-1.1.html>
		LGPL <http://www.gnu.org/licenses/lgpl.html>
		GPL <http://www.gnu.org/licenses/gpl.html>
*/

App::uses('FeedbackAppModel', 'Feedback.Model');
App::uses('Hash', 'Utility');

class Rating extends FeedbackAppModel
{
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array
		(
			'foreign_model' => array
			(
				'notBlank' => array
				(
					'rule' => array('notBlank'),
					//'message' => 'Your custom message here',
					'allowEmpty' => false,
					'required' => false,
					//'last' => false, // Stop validation after this rule
					//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
			),
			'foreign_id' => array
			(
				'notBlank' => array
				(
					'rule' => array('notBlank'),
					//'message' => 'Your custom message here',
					'allowEmpty' => false,
					'required' => false,
					//'last' => false, // Stop validation after this rule
					//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
			),
			'rating' => array
			(
				'notBlank' => array
				(
					'rule' => array('notBlank'),
					//'message' => 'Your custom message here',
					'allowEmpty' => false,
					'required' => true,
					//'last' => false, // Stop validation after this rule
					//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
				'decimal' => array
				(
					'rule' => array('decimal'),
				),
				'minimum' => array
				(
					'rule' => array('comparison', '>=', 0.5),
				),
				'maximum' => array
				(
					'rule' => array('comparison', '<=', 5),
				),
			),
		);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	public $actsAs = array
		(
			/*'Feedback.Polymorphic' => array
			(
				'modelField'	=> 'foreign_model',
				'foreignKey'	=> 'foreign_id'
			),*/
		);
	public $belongsTo =['RatingType' => ['className' => 'Feedback.RatingType','foreignKey' => 'ratingtype_id']];	

	public function afterSave($created, $options = array())
	{
		// if ($created) {
		// 	$systemcount['variable']='ratingcount';
		// 	$this->updatesystemcount($systemcount);			
		// }
	}
	/** ==========> Functions for DataTable starts <========== **/
    /**
     * Define columns/fileds you need to select
     * db: fields in database, dt: for datatables; not using dt for now
     *
     * @return array
     */
    public function getColumns() { 
        return [
            ['db' => 'Rating.id', 'dt' => 'id'], 
            ['db' => 'Rating.foreign_model', 'dt' => 'foreign_model'],
            ['db' => 'Rating.ratingtype_id', 'dt' => 'ratingtype_id'],
            ['db' => 'Rating.authorip', 'dt' => 'authorip'],
            ['db' => 'RatingType.name', 'dt' => 'name'],
            ['db' => 'Rating.rating', 'dt' => 'rating'],
        ];  
    }
    /**
     * Define search fields
     *
     * @return array
     */
    public function getSearchFields() {
        return [
            'foreign_model' => ['db' => 'Rating.foreign_model'],
            'authorip' => ['db' => 'Rating.authorip'],
        ];
    }
    /**
     * Get listing data
     *
     * @return array
     */
    public function getListingData() {
        // $this->recursive = -1;
       	$options = [
             'order' => ['Rating.created' => 'ASC']
        ];
        $result = $this->_dtData($options);
        return $result;
    }
    /**
     * Parse data; if you want to modify/alter data then define this function    * 
     *
     * @param array $data
     *
     * @return array                                  
     */
    public function parseData($data) {
        foreach ($data as $key => &$val) {
            $r = 'Rating';
            $rt = 'RatingType';
            $id = $val[$r]['id'];
    
            $val[$r]['foreign_model'] = !empty($val[$r]['foreign_model']) ? $val[$r]['foreign_model'] : '-'; 
            $val[$rt]['name'] = !empty($val[$rt]['name']) ? $val[$rt]['name'] : '-';     
            $val[$r]['authorip'] = !empty($val[$r]['authorip']) ? $val[$r]['authorip'] : '-';
            $val[$r]['rating'] = $this->_prepareRate($val);
        }
        return $data;
    }
    /**
     * Prepare status
     *
     * @param array $data
     *
     * @return string
     */
    private function _prepareRate($data) {
        $r = 'Rating';
		$rate =[]; 
		for($i=0 ;$i<$data[$r]['rating'];$i++){
		$rate[] ='<span class="fa fa-star text-warning" ></span>'; 
		}  
		for($j=0 ;$j<(5-$data[$r]['rating']);$j++){
		$rate[] ='<span class="fa fa-star-o text-warning" ></span>'; 
		} 
		$rate = implode($rate, '');
        return $rate;
    }

    public static function getStatuses($key = NULL, $filter = []) {
        $statuses = [
            0 => "Inactive",
            1 => "Active",
            2 => "Deleted",
            3 => "Pending",
            4 => "In Progress",
            5 => "Completed",
            6 => "Close"
        ];

        if (!is_null($key)) {
            return isset($statuses[$key]) ? $statuses[$key] : '-';
        }

        if (!empty($filter)) {
            return array_filter($statuses, function ($k) use ($filter) {
                return in_array($k, $filter);
            }, ARRAY_FILTER_USE_KEY);
        }

        return $statuses;
    }	
}
