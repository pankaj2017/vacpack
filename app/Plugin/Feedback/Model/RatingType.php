<?php 
App::uses('FeedbackAppModel', 'Feedback.Model');


class RatingType extends FeedbackAppModel {
    // var $name = 'Ratingtype';
    // var $override = false;

    public $hasMany = [
        'Rating' =>['className' => 'Rating',
        'foreignKey' => 'ratingtype_id',
        'dependent' => false,
        'conditions' => '',
        'fields' => '',
        'order' => '',
        'limit' => '',
        'offset' => '',
        'exclusive' => '',
        'finderQuery' => '',
        'counterQuery' => '']
    ];
    
    var $validate = [   
        'name'=>[   
            ['rule' => ['notBlank'],'message'=>'Rating Types Name is mandatory'],
            ['rule' => ['isUnique'], 'message' => 'Rating Types Name already exists']
        ],
        'foreign_model' => [
                ['allowEmpty' => false, 'rule' =>['notBlank'],'message'=>'Please select module']
        ],
        'status' => [
            ['rule' => ['numeric'],'message' => 'Please enter valid status.']
        ]   
    ];

    function beforeValidate($options = array()) { 
        parent::beforeValidate($options);  
        foreach ($this->data as $table_key => $table_value) { 
            foreach ($table_value as $key => $value) {
                if(!is_array($value)) 
                    $this->data[$table_key][$key] = trim($value); 
            }
        }
    }

    public function afterSave($created, $options = array())
    {
        // if ($created) 
        // {
        //     $systemcount['variable']='ratingtypes';
        //     $this->updatesystemcount($systemcount);
        // }
    }


	/** ==========> Functions for DataTable starts <========== **/
    /**
     * Define columns/fileds you need to select
     * db: fields in database, dt: for datatables; not using dt for now
     *
     * @return array
     */
    public function getColumns() { 
        return [
            ['db' => 'RatingType.id', 'dt' => 'id'], 
            ['db' => 'RatingType.name', 'dt' => 'name'],
            ['db' => 'RatingType.foreign_model', 'dt' => 'foreign_model'],
            ['db' => 'RatingType.status', 'dt' => 'status'],
        ];  
    }
    /**
     * Define search fields
     *
     * @return array
     */
    public function getSearchFields() {
        return [
            'name' => ['db' => 'RatingType.name'],
            'foreign_model' => ['db' => 'RatingType.foreign_model'],
            'status' => ['db' => 'RatingType.status', 'type' => 'int'],
        ];
    }
    /**
     * Get listing data
     *
     * @return array
     */
    public function getListingData() {
        $this->recursive = -1;
       	$options = [
             'order' => ['RatingType.name' => 'ASC']
        ];
        $result = $this->_dtData($options);
        return $result;
    }
    /**
     * Parse data; if you want to modify/alter data then define this function    * 
     *
     * @param array $data
     *
     * @return array                                  
     */
    public function parseData($data) {
        foreach ($data as $key => &$val) {
            $rt = 'RatingType';
            $id = $val[$rt]['id'];
    
            $val[$rt]['name'] = !empty($val[$rt]['name']) ? $val[$rt]['name'] : '-';     
            $val[$rt]['foreign_model'] = !empty($val[$rt]['foreign_model']) ? $val[$rt]['foreign_model'] : '-';     
            $val[$rt]['actions'] = $this->_actionButtons($val);
            $val[$rt]['status'] = $this->_prepareStatusData($val);
        }
        return $data;
    }
     


    /**
     * Prepare status
     *
     * @param array $data
     *
     * @return string
     */
    private function _prepareStatusData($data) {
        $rt = 'RatingType';
        $id = $data[$rt]['id'];
        $status = $data[$rt]['status'];
        $statusName = self::getStatuses($status);
        $statusHtml = $lableClass = $onClick = $style = $other = '';

        if($status == 2) {
            $status_to_update = 1;
            $lableClass = 'label-danger';
            $url = Router::url(array('controller' => 'ratingtypes','action' => 'updatestatus','admin' => true,'plugin'=>'feedback'))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            // $style = 'pointer-events: none;';
        } else {
           $status_to_update = 0;
            if($status == 0) {
                $status_to_update = 1;
            }

            $url = Router::url(array('controller' => 'ratingtypes','action' => 'updatestatus','admin' => true,'plugin'=>'feedback'))."/".$id."/".$status_to_update;
            $onClick = 'onclick= "return confirm(\'Are you sure to change the status?\');" href="'.$url.'"';

            if ($status == 1) {
                $lableClass = 'label-success';
            }else if($status == 0) {
                $lableClass = 'label-warning';

            }
            
        }

        $statusHtml = '<a data-toggle="tooltip" data-placement="left" 
            data-original-title="Update status" class="label '.$lableClass.'" style="color:#f0f0f0;'.$style.'" '.$onClick.'>'.$statusName.'</a>'.$other;

        return $statusHtml;
    }

    /**
     * Prepare different actions
     *
     * @param array $data
     *
     * @return string
     */
    private function _actionButtons($data) {
        $rt = 'RatingType';
        $id = $data[$rt]['id'];
        $status = $data[$rt]['status'];

        if ($status != 2) {
            $editUrl = Router::url(["controller" => "ratingtypes", "action" => "edit", "admin"=>true,'plugin'=>'feedback'])."/".$id;
            
            $actions = ' <a href="'.$editUrl.'" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                    <i class="far fa-edit text-info"></i>
                </a>';
                       
        } else {
            $actions = '-';
        }       

        return $actions;
    }

    public static function getStatuses($key = NULL, $filter = []) {
        $statuses = [
            0 => "Inactive",
            1 => "Active",
            2 => "Deleted",
            3 => "Pending",
            4 => "In Progress",
            5 => "Completed",
            6 => "Close"
        ];

        if (!is_null($key)) {
            return isset($statuses[$key]) ? $statuses[$key] : '-';
        }

        if (!empty($filter)) {
            return array_filter($statuses, function ($k) use ($filter) {
                return in_array($k, $filter);
            }, ARRAY_FILTER_USE_KEY);
        }

        return $statuses;
    }
}
?>