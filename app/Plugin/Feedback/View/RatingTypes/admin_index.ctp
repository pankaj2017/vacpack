<?php echo $this->element('admin/admin_sidebar');?>

<?php 
    echo $this->Html->script(['jquery.dataTables.min','admin/date.format','admin/bootstrap-datepicker.min']);
    echo $this->Html->css('admin/bootstrap-datepicker.min');
?>
<!--main content start-->
<div class="main-body">
    <div class="page-wrapper">
        <div class="page-header">
            <div class="page-header-title"><h4>Rating Types</h4></div>
        </div>
        <div class="page-body">
            <div class="row">
                <div class="col-md-12 col-xl-12">
                    <div class=""><?php echo $this->Session->flash(); ?> </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h5>Latest Rating Types on vacdo</h5>
                                </div>
                                <div class="col-md-2 text-right">
                                    <?php echo $this->Html->link("Add New", array("controller"=>"RatingTypes","action"=>"add","admin"=>true,'plugin'=>'Feedback'),array("class"=>"btn btn-sm btn-outline-info float-right")); 
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-content">
                                <?php echo  $this->element('RatingTypes/search-form');?>
                                <div class="form-body ajax-dt">
                                    <table id="ratingtypes-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                
                                                <th>Rating Type</th>
                                                <th>Module</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--/card-body-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script(['admin/select2.full.min']); ?>
<?php echo $this->Html->css(['admin/select2']); ?>
<script>
    var folderTable;

    $('#ratingtypes-table').on('processing.dt', function (e, settings, processing) {
        if (processing) {      
            $('.dataTables_processing').parent().addClass('dataTables_processing_wrapper');
        } else {
            $('.dataTables_processing').parent().removeClass('dataTables_processing_wrapper');
        }
    });

    $(document).ready(function() {  
        $(".select2").select2({
        width: '100%'
        });

        var folderUrl = "?ajax=get-ratingtypes-list";
     
        folderTable = $('#ratingtypes-table').DataTable({
            "dom": '<"pull-right"l><r><"table-responsive"t>ip',
            "iDisplayLength": 25,
            "ajax": {
                "url": folderUrl,
                "dataType": "json",
                "data": function ( data ) {
                data.name = $('input[name="data[search-form][name]"]').val();
                data.status = $('select[name="data[search-form][status]"]').val();
                data.foreign_model = $('input[name="data[search-form][module]"]').val();
               
                }
            },
            "columns": [
                {"data": "RatingType.name"},
                {"data": "RatingType.foreign_model"},
                {"data": "RatingType.status", "bSortable": false},
                {"data": "RatingType.actions", "bSortable": false},
            ],
            //"bDestroy": true,
            "aaSorting": [],
            "pagingType": "full_numbers",
            "serverSide": true,
            "processing": true,
            "serverMethod": "post",
            "fnDrawCallback": function(oSettings) {
                if (oSettings.fnRecordsDisplay() > oSettings._iDisplayLength) {
                    $(oSettings.nTableWrapper).find('.dataTables_paginate').show();
                } else {
                    $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
                }
            }
        }); 

        $('form[name="search-form"]').on("submit", function(e) {
            e.preventDefault();
            folderTable.ajax.reload();
        });
        $('#reset-search').on("click", function(e) {
            e.preventDefault();
            $('select.select2').val(null).trigger('change');
            $('form[name="search-form"]')[0].reset();
            folderTable.ajax.reload();
        });
        // $('#date-range').datepicker({
        //     format: date_format,
        //     toggleActive: true,
        //     autoclose: true,
        // });

        $('body').tooltip({
            selector: '[data-toggle="tooltip"]'
        });  
    });
</script>