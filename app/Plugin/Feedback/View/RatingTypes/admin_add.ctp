<?php echo $this->element("admin/admin_sidebar"); ?>
<?php $this->assign('title', 'Add Rating Type'); ?>
<div class="main-body">
    <div class="page-wrapper">        
        <div class="page-header">
            <div class="page-header-title"><h4>Add Rating Type</h4></div>
        </div>
        <div class="page-body">
            <div class="row">
                <div class="col-md-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h5 >Latest Rating Type on vacdo</h5>
                                </div>
                                <div class="col-md-2 text-right">
                            <?php echo $this->Html->link("Back to list",array('plugin'=>'','controller'=>'RatingTypes','action'=>'index','admin'=>true,'plugin'=>'Feedback'),array('class'=>'btn btn-outline-info btn-sm'))?>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <?php echo $this->Form->create('RatingType',['url'=>['controller'=>'RatingTypes','action'=>'add','admin'=>true,'plugin'=>'Feedback']]);?>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label class="control-label font-medium">Rating Type</label><span class="text-danger">*</span>
                                        <?php 
                                            echo $this->Form->text('RatingType.name',['class'=>'form-control','minlength'=>3,'maxlength'=>50]);
                                            echo $this->Form->error('RatingType.name',['class'=>'error_msg']);
                                        ?>   
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label class="control-label font-medium">Module </label><span class="text-danger">*</span>
                                        <?php 
                                        $modeloptions = Configure::read('rattablemodules');
                                        echo $this->Form->select('RatingType.foreign_model', $modeloptions,['class'=>'form-control','empty'=>"Select Module",'required'=>true]);
                                        echo $this->Form->error('RatingType.foreign_model',['class'=>'error_msg']);
                                        ?>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <?php  
                                        echo $this->Form->checkbox('RatingType.status',['class'=>'filled-in']); 
                                        ?> 
                                        <label for="RatingTypeStatus" class="font-medium">Active</label>
                                        <?php echo $this->Form->error('RatingType.status',['class'=>'error_msg']); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input name="" type="submit" value="Create" class="btn btn-success" />
                                </div>                      
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>