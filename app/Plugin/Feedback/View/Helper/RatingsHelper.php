<?php
/**
	CakePHP Feedback Plugin

	Copyright (C) 2012-3827 dr. Hannibal Lecter / lecterror
	<http://lecterror.com/>

	Multi-licensed under:
		MPL <http://www.mozilla.org/MPL/MPL-1.1.html>
		LGPL <http://www.gnu.org/licenses/lgpl.html>
		GPL <http://www.gnu.org/licenses/gpl.html>
*/

		App::uses('AppHelper', 'View/Helper');
		App::uses('Sanitize', 'Utility');

/**
 * @property HtmlHelper $Html
 * @property SessionHelper $Session
 */
class RatingsHelper extends AppHelper
{
	public $helpers = array('Html', 'Session');

	private $models = array();

	function __construct(View $view, $settings = array())
	{
		parent::__construct($view, $settings);
		$this->settings = $settings;
	}

	public function beforeRender($viewFile)
	{
		parent::beforeRender($viewFile);
		$this->models = $this->Session->read('Feedback.RatingModels');

		$options = array('inline' => false);		

		$this->Html->script('Feedback.rateit-1.0.4/jquery.rateit.min', $options);
		$this->Html->css('Feedback./js/rateit-1.0.4/rateit', null, $options);
		$this->Html->css('Feedback./js/rateit-1.0.4/bigstars', null, $options);
	}

	
	function display_for(array $data, array $options = array())
	{
		
		$output = '';
		$params = $this->prepareParams($data, $options);
		$output .= $this->_View->element
		(
			'Feedback.rating',
			$params
		);
		return $output;
	}

	protected function prepareParams($data, $options)
	{
		$model = null;
		$modelClass = null;
		$user_rates = 0;

		extract($options);

		if (empty($modelClass))
		{
			$modelClass = $model;
		}

		if (empty($model) && empty($modelClass))
		{
			$modelClass = key($this->models);
			$model = current($this->models);
		}

		$random_id = $this->getRandomId();
		$element_id = 'rating-'.$random_id;
		$message_id = 'message-'.$random_id;
		$values_id = 'values-'.$random_id;
		$id = $data[$modelClass]['id'];
		$submit_url = $this->Html->url
		(
			array
			(
				'plugin' => 'feedback',
				'controller' => 'ratings',
				'action' => 'add',
				$model,
				$id,
			)
		);

		$value = 0;
		$votes = 0;
		//modified by milind on 13042019 for restrict ratings add functionality for overall ratings
		if(isset($options['view']) && ($options['view'] == 'listing'))
			$readonly = 'true';			
		else if(count($options) > 0)
			$readonly = 'false';
		else
			$readonly = 'true';
		//end of modified by milind
		/*
		$ratingsummary=array();
		foreach($data['Ratingtypes']  as $key2=>$ratingtype)
			{
				$ratingsummary['Company_'.$key2.'_'.$id]= $this->Rating->find("all", array(
					"fields"     => array("AVG(Rating.rating) AS AverageRating"),
					"conditions" => ['Rating.foreign_id'=>$id ,'Rating.foreign_model'=>'Company','Rating.ratingtype_id'=>$key2]
					))[0][0]['AverageRating'];
			}
		*/
		if (isset($data['RatingSummary']))
		{
			$value = floatval($data['RatingSummary']['total_rating']);
			$votes = floatval($data['RatingSummary']['total_votes']);
		}

		$user_votes = $this->Session->read('Feedback.'.$modelClass.'Ratings');
		$user_vote = 0;
		
		if (isset($user_votes[$id]))
		{
			//$readonly = 'true';
			$user_vote = $user_votes[$id];
		}
		$usertotalvote=1;
		if(isset($options['view']) && ($options['view'] == 'listing')){
			$listing = 'true';
		}else{
			$listing = 'false';
		}
		return compact
		(
			'data',
			'model',
			'modelClass',
			'submit_url',
			'element_id',
			'message_id',
			'values_id',
			'readonly',
			'value',
			'votes',
			'user_vote',
			'usertotalvote',
			'listing'
			//'user_rates'
		);
	}

	/**
	 * Extracted for easier testing.
	 *
	 * @return integer A random id.
	 */
	protected function getRandomId()
	{
		return intval(mt_rand());
	}

	/**
	 * This method is to be used only for test purposes.
	 *
	 * @param array $models Models to be used for ratings.
	 */
	public function setModels($models)
	{
		$this->models = $models;
	}


	function display_multipleratings(array $data, array $options = array())
	{
		$output = '';
		
		App::import('Model','RatingType');
		$this->RatingType = new RatingType();
		
		$ratingtypes=$this->RatingType->find('list',['conditions'=>['RatingType.foreign_model'=>$options['model'],'RatingType.status'=>1]]);
		
		if(is_array($ratingtypes) && (count($ratingtypes) > 0)){
			foreach($ratingtypes as $key=>$type){
				$options['ratingtype']=$type;
				$options['ratingtypeid']=$key;
				//$output .= "<div class='col-lg-3'><strong style='display: inline;!important'>".$type."</strong>";
				$params = $this->prepareParams_multipleratings($data, $options);				
				$output .= $this->_View->element
				(
					'Feedback.rating',
					$params
				);
			}
		}	
		return $output;
	}

	protected function _getrating($data, $options)
	{	
		$model = null;
		$modelClass = null;

		extract($options);

		if (empty($modelClass))
		{
			$modelClass = $model;
		}

		if (empty($model) && empty($modelClass))
		{
			$modelClass = key($this->models);
			$model = current($this->models);
		}

		$id = $data[$modelClass]['id'];
		if(isset($options['ratingtypeid']))
		{
			$ratingtypeid = $options['ratingtypeid'];
		}else
		{
			$ratingtypeid =0;/*General rating */
		}
				
		if (isset($data['ratingsummary']))
		{
			if(isset($data['ratingsummary'][$model."_".$ratingtypeid."_".$id]))
			{
				$rating = floatval($data['ratingsummary'][$model."_".$ratingtypeid."_".$id]);
			}
			else
			{
				$rating = 0;
			}
		}

		if (isset($data['RatingSummary']))
		{
			$rating =$data['RatingSummary']['total_rating'];
		}
		else
		{
			$rating = 0;
		}

		return compact('rating');
	}
	function getrating(array $data, array $options = array())
	{
		//pr($data);die;
		$output = '';
		App::import('Model','RatingType');
		$this->RatingType = new RatingType();
		
		$ratingtypes=$this->RatingType->find('list',['conditions'=>['RatingType.foreign_model'=>$options['model'],'RatingType.status'=>1]]);
		
		if(is_array($ratingtypes) && (count($ratingtypes) > 0)){
			foreach($ratingtypes as $key=>$type){
				$opts['ratingtype']=$type;
				$opts['ratingtypeid']=$key;
				
				$params = $this->_getrating($data, $options);
				$output .= $this->_View->element
				(
					'Feedback.getrating',
					['rating'=>$params,'opts'=>$opts]
				);
			}
		}
		return $output;
	}



	protected function prepareParams_multipleratings($data, $options)
	{
		$random_id = $this->getRandomId();
		$message_id = 'message-'.$random_id;
		$values_id = 'values-'.$random_id;
		$user_rates = 0;
		//$user_rates = $this->Ratings->readCookie($model."_".$ratingtypeid."_".$id);

		$model = null;
		$modelClass = null;

		extract($options);

		if (empty($modelClass))
		{
			$modelClass = $model;
		}

		if (empty($model) && empty($modelClass))
		{
			$modelClass = key($this->models);
			$model = current($this->models);
		}

		//pr($data['Rating']);
		/*if(isset($data['Rating']) )
		{		
			foreach($data['Rating'] as $rating)
			{
				$dbdata[ $rating['Rating']['foreign_model']."_".$rating['Rating']['ratingtype_id']."_".$rating['Rating']['foreign_id']] =$rating['Rating']['rating'];
			}
		}*/
		$id = $data[$modelClass]['id'];
		$ratingtypeid = $options['ratingtypeid'];
		$ratingtype = $options['ratingtype'];
		$submit_url = $this->Html->url
		(
			[
			'plugin' => 'feedback',
			'controller' => 'ratings',
			'action' => 'add',
			$model,
			$id,
			$ratingtypeid
			]
			);

		$element_id = $model."_".$ratingtypeid."_".$id;
		
		if(isset($dbdata[$model."_".$ratingtypeid."_".$id]) && $dbdata[$model."_".$ratingtypeid."_".$id] > 0)
		{
			$user_rates = $dbdata[$model."_".$ratingtypeid."_".$id];	
			$readonly = 'true';			
		}
		else
		{
			$user_rates = 0;
			$readonly = 'false';
		}
		
		$value = 0;

		if (isset($data['ratingsummary']))
		{
			if(isset($data['ratingsummary'][$model."_".$ratingtypeid."_".$id]))
			{
				$value = floatval($data['ratingsummary'][$model."_".$ratingtypeid."_".$id]);
			}
			else
			{
				$value = 0;
			}
		}
		
		$user_vote = 0;
		$votes =0;
		$usertotalvote=0;

		return compact
		(
			'data',
			'model',
			'modelClass',
			'submit_url',
			'element_id',
			'message_id',
			'values_id',
			'readonly',
			'value',
			'votes',
			'user_rates',
			'ratingtypeid',
			'ratingtype',
			'usertotalvote'
			);
	}
}
