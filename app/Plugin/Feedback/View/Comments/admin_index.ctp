<?php $this->assign('title', 'Comments'); ?>
<?php echo $this->Html->script(['jquery.dataTables.min']);?>
<div class="page-wrapper">
  <div class="row page-titles ">
    <div class="col-md-10 align-self-center">
      <h3 class="text-themecolor">Comments    
      </h3>
    </div>
  </div>
  <div class="container-fluid">
    <?php echo  $this->element('common_flash_message');?>
     <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-default">
                <div class="card-body">
          		<?php echo  $this->element('Comments/search-form');?>
	            <div class="form-body ajax-dt">
	                <table id="comments-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
	                    <thead>
	                        <tr>
		                        <th>Module</th>
                            <th>User</th>
				                    <th>Content</th>
				                    <th>Create</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    </tbody>
	                </table>
	            </div>
          		</div> 
        	</div> 
      	</div>
    </div>
<?php echo $this->Html->script(['admin/select2.full.min']); ?>
<?php echo $this->Html->css(['admin/select2']); ?>
<script>
    var folderTable;

    $('#comments-table').on('processing.dt', function (e, settings, processing) {
      if (processing) {      
        $('.dataTables_processing').parent().addClass('dataTables_processing_wrapper');
      } else {
        $('.dataTables_processing').parent().removeClass('dataTables_processing_wrapper');
      }
    });

    $(document).ready(function() {  
        $(".select2").select2({
        width: '100%'
        });

        var folderUrl = "?ajax=get-comments-list";
     
        folderTable = $('#comments-table').DataTable({
            "dom": '<"pull-right"l><r><"table-responsive"t>ip',
            "iDisplayLength": 25,
            "ajax": {
              "url": folderUrl,
              "dataType": "json",
              "data": function ( data ) {
                data.module = $('input[name="data[search-form][module]"]').val();
                data.content = $('input[name="data[search-form][content]"]').val();
              }
            },
            "columns": [
              {"data": "Comment.foreign_model"},
              {"data": "User.username"},
              {"data": "Comment.content"},
              {"data": "Comment.created"},
            ],
            //"bDestroy": true,
            "aaSorting": [],
            "pagingType": "full_numbers",
            "serverSide": true,
            "processing": true,
            "serverMethod": "post",
            "fnDrawCallback": function(oSettings) {
              if (oSettings.fnRecordsDisplay() > oSettings._iDisplayLength) {
                $(oSettings.nTableWrapper).find('.dataTables_paginate').show();
              } else {
                $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
              }
            }
          }); 

        $('form[name="search-form"]').on("submit", function(e) {
            e.preventDefault();
            folderTable.ajax.reload();
        });
        $('#reset-search').on("click", function(e) {
            e.preventDefault();
            $('select.select2').val(null).trigger('change');
            $('form[name="search-form"]')[0].reset();
            folderTable.ajax.reload();
        });
        $('#date-range').datepicker({
            format: date_format,
            toggleActive: true,
            autoclose: true,
        });

        $('body').tooltip({
            selector: '[data-toggle="tooltip"]'
        });  
    });
</script>