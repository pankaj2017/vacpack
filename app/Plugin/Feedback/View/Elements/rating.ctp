<?php
if($readonly != 'true'){
?>	
<?php
}
if(isset($ratingtype)){
?>	

<?php /* <div class="col">
      	<div class="rating-box card card-body mb-10">
        <h5 class="mb-6"><?php echo $ratingtype; ?></h5> -->
    	
 
	<strong style='display: inline;!important'><?php echo $ratingtype; ?></strong> */ ?>
<?php
}
?>	
	<!-- <div class="clear"></div> -->
	<?php if($readonly != 'true') { ?>
	<div class="rating col" style="display: inline;!important">
		<div class="rating-box card card-body mb-10">
	<?php } ?>
		<?php if(isset($ratingtype)){ ?><h5 class="mb-6"><?php echo $ratingtype; ?></h5> <?php } ?>
		<div class="rateit bigstars" id="<?php  echo $element_id; ?>" style="display: inline;!important"></div>
		<div> 
			<span class="rating-values" id="<?php echo $values_id; ?>" style="display: inline;!important">
				<span class="rating-message" id="<?php echo $message_id; ?>"  > </span>
				<?php if($usertotalvote){ ?> <!-- Show Rating and vote on overall --> 
				<span class="rating-values" id="<?php echo $values_id; ?>">
					<?php
					if($listing == 'false'){
						echo __('Rating:');
					?>
					<span class="rating"><?php echo sprintf('%.02f', $value); ?></span>
					<?php echo __('Votes:'); ?>
					<span class="votes"><?php echo sprintf('%d', $votes); ?></span>
					<?php
					}
					?>
				</span>
				<?php } ?>
			</span>
		</div>
	<?php if($readonly!= 'true') { ?>
		</div>
	</div>
	<?php } ?>
<?php if($readonly != 'true'){ ?>	
<?php /* </div>
   </div> 
 </div>
</div> 
</div> */?>
<?php
}
?>

<?php
$user_vote_message = '';
$submitting_message = __('Sending vote...');
$error_message = __('There was an error while saving your vote');

$script = <<<END
$('#{$element_id}').rateit({
	starwidth: 16,
	starheight: 16,
	resetable: false,
	readonly: {$readonly},
});

$('#{$element_id}').rateit('value', {$value});
$('#{$element_id}').rateit('ispreset', true);

if ('{$readonly}' == 'true'  )
{
	$('#{$message_id}').text('{$user_vote_message}');;
}

$('#{$element_id}').bind('rated', function() {
	var elem = $(this);
	var message = $('#{$message_id}');
	var rating = elem.rateit('value');
	message.text('{$submitting_message}');

	$.ajax({
		type: 'POST',
		url: '{$submit_url}',
		data: {
			Rating: {
				foreign_model: '{$modelClass}',
				foreign_id: '{$data[$modelClass]['id']}',
				rating: rating
			}

		},
		success: function(response)
		{
			if (response.success)
			{
				elem.rateit('readonly', true);

				if (typeof response.data.total_rating != 'undefined')
				{
					$('#{$values_id} span.rating').text(response.data.total_rating.toFixed(2));
					$('#{$values_id} span.votes').text(response.data.total_votes);
				}
			}

			message.text(response.message);
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
			message.text('{$error_message}');
		}
	});
});
END;
?>
<script><?php echo $script; ?></script>