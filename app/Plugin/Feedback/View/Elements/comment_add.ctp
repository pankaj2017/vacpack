<!-- this page is lopading in  View\Companies\companiesview.ctp and productsview and Agenciesview
 -->
<!-- <div class="container"> -->
	<div class="row">
	    <div class="col-lg-12 text-right">  
			<a href="#collapseExample" class="write-a-review-btn" data-toggle="collapse">Write A Review</a>
		</div>
	</div>
	<hr>
	<div  class="collapse" id="collapseExample">
  	<h4 class="mb-20 text-color">Write A Review</h4>
	<?php 
	echo $this->Form->create('Feedback.Comment',['url' => ['plugin' => 'feedback','controller' => 'comments','action' => 'add',$foreign_model,$foreign_id]]);	 
    ?>
	    <div class="form-row">
	      	<div class="col">
		        <div class="form-group">
		        <?php
				$name = '';
				if(!isset($this->request->data['Comment']['authorname'])){
					if($this->Session->read('Auth.User.id')){
						$name = $this->Session->read('Auth.User.firstname').' '.$this->Session->read('Auth.User.lastname');
					}else{
						$name = '';
					}
				}else{
					$name = $this->request->data['Comment']['authorname'];
				}
				echo $this->Form->input('Comment.authorname',array('label' => '','class'=>'form-control','Placeholder'=>' Name','required'=>true,'pattern'=>"^[a-zA-Z0-9]+ ?([a-zA-Z0-9]+$){1}$",'min'=>"3" ,'max'=>"50",'title'=>"Please enter  Name with Alpha Numeric with at least 3 and up to 50 characters only." ,'aria-required'=>true,'type'=>'text','value'=>$name));
				?>
				<div id="CommentAuthornameerror" style="color:red"></div>
		        </div>
	      	</div>
	      	<div class="col">
		        <div class="form-group">
		        <?php
				$email = '';
				if(!isset($this->request->data['Comment']['authoremail'])){
					if($this->Session->read('Auth.User.emailaddress')){
						$email = $this->Session->read('Auth.User.emailaddress');
					}else{
						$email = '';
					}
				}else{
					$email = $this->request->data['Comment']['authoremail'];
				}
				echo $this->Form->input('Comment.authoremail',array('label' =>'','class'=>'form-control','Placeholder'=>' Email','required'=>true,'type'=>'email','value'=>$email));
				?>
				<div id="CommentAuthoremailerror" style="color:red"></div>
		        </div>
	      	</div>
	    </div>
	    <div class="form-group">
			<?php 
			echo $this->Form->input('Comment.content',array('label' => '','class'=>'form-control','Placeholder'=>' Review','required'=>true,'type'=>'textarea'));
			echo $this->Form->input('Comment.foreign_model', array('type' => 'hidden', 'value' => $foreign_model));
			echo $this->Form->input('Comment.foreign_id', array('type' => 'hidden', 'value' => $foreign_id));
			?>
			<div id="CommentContenterror" style="color:red"></div>
		</div>
	  
	    <?php 
		echo $this->Form->button('Submit',['type'=>'submit',"class"=>"write-a-review-btn border-0",'id'=>"commentsubmit"]);
		?>
	 
	<?php echo $this->Form->end(); ?> 
	</div>	
<!-- </div> -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#commentsubmit').click(function(event){
			var authorname = $('#CommentAuthorname').val();
			var authoremail = $('#CommentAuthoremail').val();
			var authorreview = $('#CommentContent').val();

			if($('#CommentAuthorname').val()== ''){
				$('#CommentAuthornameerror').html('Please enter name');
				event.preventDefault();
			}
			if($('#CommentAuthoremail').val()==''){
				$('#CommentAuthoremailerror').html('Please enter email');
				event.preventDefault();
			}
			if($('#CommentContent').val()==''){
				$('#CommentContenterror').html('Please enter review');
				event.preventDefault();
			}
		});
	});
</script>