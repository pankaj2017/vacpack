<div class="accordion" id="search-accordion" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="heading-search">      
            <a data-toggle="collapse" data-parent="#search-accordion" class="collapsed"
                href="#collapse-search" aria-expanded="true" aria-controls="collapse-search">
                <h5 class="mt-1 mb-0">
                    <i class="fa fa-filter"></i>
                    Filters
                    <i class="fa fa-chevron-down rotate-icon pull-right"></i>
                </h5>
            </a>
        </div>

        <div id="collapse-search" class="collapse" role="tabpanel" 
            aria-labelledby="heading-search" data-parent="#search-accordion">
            <div class="card-body">
                <?php
                    echo $this->Form->create('search-form', ['url'=> '#', 'name' => 'search-form', 'id' => 'inquiry-search-form', 'class' => 'search-form']);
                ?>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Search by Name</label>
                            <?php echo $this->Form->text('name', [
                                'class'=>'form-control form-control-sm',
                                'data-role' => "tagsinput"
                            ]); ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Search by Module</label>
                            <?php echo $this->Form->text('module', [
                                'class'=>'form-control form-control-sm',
                                'data-role' => "tagsinput"
                            ]); ?>
                        </div>
                    </div> 
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Search by Status</label>
                            <?php
                            echo $this->Form->select('status', $status, [
                                'class' => 'form-control form-control-sm select2',
                                'empty' => 'Select Status',
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" style="display: none;">
                        <div class="form-group">
                            <label>Search by Created Date</label>
                            <div class="input-daterange input-group" id="date-range">
                                <?php echo $this->Form->text('created_from', [
                                    'class'=>'form-control form-control-sm text-left'
                                ]); ?>
                                <div class="input-group-append">
                                    <span class="input-group-text bg-info b-0 text-white p-t-0 p-b-0">TO</span>
                                </div>
                                <?php echo $this->Form->text('created_to', [
                                    'class'=>'form-control form-control-sm text-left'
                                ]); ?>
                            </div>
                            <small>[<?php //echo Tools::JS_DATE_FORMAT; ?>]</small>
                        </div>        
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">                        
                        <div class="form-actions text-center">
                            <button type="submit" class="btn btn-sm btn-info">
                                <i class="fa fa-search"></i> Search
                            </button>
                            <button type="button" class="btn btn-sm btn-danger" id="reset-search">
                                <i class="fa fa-times"></i> Clear
                            </button>
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
    </div>
</div>