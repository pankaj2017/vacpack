<?php /*
<div class="sidebar-widget recent-comments">
<div class="sidebar-title">
<h2>Recent Comments</h2>
</div>
<div class="comments-area  comment-block">
<?php 
foreach ($comments as $key=>$comment)
{ 
	if (is_numeric($key))
	{
?>
<div class="comment-box inner">
<div class="comment content">
    <div class="image">
		<?php echo $this->Html->image("default/resource/blankuser.jpg", ["alt" => ""]); ?>
    </div>
    <div class="comment-inner ">
    	<h3><a href="#"><?php echo  Sanitize::html($comment['Comment']['authorname']);?></a></h3>
    	<div class="clearfix"></div>
        <div class="comment-info  date"><?php 
				$timestamp = $this->Comments->Time->timeAgoInWords($comment['Comment']['created'], array('format' => 'Y/m/d H:i'));
				$permalink = $this->Html->link($timestamp, '#comment-'.$comment['Comment']['id'], array('title' => __('Permalink')));
				echo "&nbsp;&nbsp;&nbsp;&nbsp;".$permalink;

				?></div>
        
        <div class="text"><?php echo nl2br(Sanitize::html($comment['Comment']['content'])); ?></div>
    </div>
</div>
</div>
<?php 
	}
}
?>    
</div>
</div> */
?>
<!-- <div class="container"> -->
    <h4 class="mb-20 text-color">Read Comments</h4>
    <?php 
    foreach ($comments as $key=>$comment){ 
        if (is_numeric($key)){
    ?>
    <div class="read-comment-box card card-body mb-20">
        <div class="media"><?php echo $this->Html->image("default/resource/blankuser.jpg", ["alt" => "",'class'=>"img-fluid"]); ?>
            <div class="media-body">
                <h5 class="mt-0"><?php echo  Sanitize::html($comment['Comment']['authorname']);?></h5>
                <p class="mb-0 text-muted"><small><?php 
                $timestamp = $this->Comments->Time->timeAgoInWords($comment['Comment']['created'], array('format' => 'M d, Y H:i'));
                echo $timestamp; 
                ?>
                </small>
                </p>
                <?php echo nl2br(Sanitize::html($comment['Comment']['content'])); ?>
            </div>
        </div>
    </div>
    <?php 
        }
    }
    ?>
<!-- </div> -->