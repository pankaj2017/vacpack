<?php

Router::connect("/admin/ratingtypes", [
	'plugin' => 'Feedback',
    'controller' => 'RatingTypes',
    'action'=>'index',
    'admin' => true
]);
Router::connect('/admin/ratingtypes/add', array(
	'plugin'=>'Feedback',
	'controller'=>'RatingTypes',
	'action'=>'add',
	'admin'=>true
));
Router::connect('/admin/ratingtypes/edit/:id',array('plugin'=>'Feedback','controller'=>'RatingTypes','action'=>'edit','admin'=>true),array('pass'=>array('id')));
Router::connect('/admin/ratingtypes/updatestatus/:id/:status',array('plugin'=>'Feedback','controller'=>'RatingTypes','action'=>'updatestatus','admin'=>true),array('pass'=>array('id','status')));

Router::connect("/admin/comments", [
	'plugin' => 'Feedback',
    'controller' => 'Comments',
    'action'=>'index',
    'admin' => true
]);

Router::connect("/admin/ratings", [
	'plugin' => 'Feedback',
    'controller' => 'Ratings',
    'action'=>'index',
    'admin' => true
]);