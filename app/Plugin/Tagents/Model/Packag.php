<?php
App::uses('TagentsAppModel', 'Tagents.Model');
App::uses('AttachmentBehavior', 'Uploader.Model/Behavior');

/**
 * Profile Model
 *
 * @property User $User
 */
class Packag extends TagentsAppModel {
	 public $uses = array(
        'Containable'
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		// 'travelagency_id' => array(
		// 	'numeric' => array(
		// 		'rule' => array('numeric'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Travelagency' => array(
			'className' => 'Tagents.Travelagency',
			'foreignKey' => 'travelagency_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'State' => array(
			'className' => 'State',
			'foreignKey' => 'state_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	// public $hasOne = array(
	// 	'Itinerary' => array(
	// 		'className' => 'Tagents.Itinerary',
	// 		'foreignKey' => 'packag_id',
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => ''
	// 	)
	// );
	 public $hasMany = array(
        'Itinerary' => array(
            'className' => 'Tagents.Itinerary',
            'foreignKey' => 'packag_id'
        ),
        'Packageinquiry' => array(
            'className' => 'Packageinquiry',
            'foreignKey' => 'packag_id'
        ),
    );
	public $hasAndBelongsToMany  = array(
		'Packagetype' => array(
			'className' => 'Tagents.Packagetype',
			'joinTable' => 'packagetypes_packags',
			'foreignKey' => 'packag_id',
			'associationForeignKey' => 'packagetype_id',
		),
		'Holidaytheme' => array(
			'className' => 'Holidaytheme',
			'joinTable' => 'holidaythemes_packags',
			'foreignKey' => 'packag_id',
			'associationForeignKey' => 'holidaytheme_id',
		)/*,
		'Destination' => array(
            'className' => 'Destination',
            'joinTable' => 'packags_destinations',
            'foreignKey' => 'packag_id',
            'associationForeignKey' => 'destination_id'
        ),*/
	);
	public $actsAs = array(
		'Uploader451.Attachment' => array(
			'pkgimage' => array(
				'nameCallback' => 'formatName',
				'append' => '',
				'prepend' => '',
				'tempDir' => TMP,
				'uploadDir' => 'files/uploads/packageimage/',
				'finalPath' => '/files/uploads/packageimage/',
				'dbColumn' => 'name',
				'metaColumns' => array(
					'ext' => 'extension',
					'type' => 'mimetype',
					'size' => 'filesize'
				),
				'defaultPath' => '',
				'overwrite' => true,
				'stopSave' => true,
				'allowEmpty' => true,
				'transforms' => array(
					'name' => array(
					'method' => 'resize', // or crop / AttachmentBehavior::CROP
					'append' => '-medium',
					'overwrite' => true,
					'self' => false,
					'width' => 390,
					'height' => 240,
					'aspect' =>true,
					'mode'=>'width',
					'quality' => 70,
					'dbColumn'=>'pkgimage',
					'nameCallback' => 'transformNameCallback',
				)
					// ),
					// 'imagesmall' => array(
					// 'method' => 'resize', // or crop / AttachmentBehavior::CROP
					// 'append' => '-small',
					// 'overwrite' => true,
					// 'self' => false,
					// 'width' => 175,
					// 'height' => 108,
					// 'aspect' =>true,
					// 'mode'=>'width',
					// 'quality' => 70,
					// 'dbColumn'=>'pkgimage',
					// 'nameCallback' => 'transformNameCallback',
					// ),
					// 'imagesedium' => array(
					// 'method' => 'resize',
					// 'append' => '-big',
					// 'width' => 650,
					// 'height' => 400,
					// 'overwrite' =>true,
					// 'self' => false,
					// 'mode'=>'width',
					// 'aspect' => true,
					// 'dbColumn'=>'pkgimage',
					// 'quality' => 70,
					// 'mode'=>'height',
					// 'nameCallback' => 'transformNameCallback',
					// )
				),
			)
		),
		'Uploader451.FileValidation' => array(
			'image' => array(
				'extension' => array('gif', 'jpg', 'png', 'jpeg'),
				'filesize' => 5242880 //5 Mb default
			)
		)
	);
	public function formatName($name, $file) {
		return sprintf('%s-%s', $name, $file->size());
	}
	public function transformNameCallback($name, $file) {
		return $this->getUploadedFile()->name();
	}
}