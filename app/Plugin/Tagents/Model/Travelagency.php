<?php
App::uses('AttachmentBehavior', 'Uploader.Model/Behavior');
App::uses('TagentsAppModel', 'Tagents.Model');
//App::uses('AuthComponent', 'Controller/Component');
/**
 * Agent Model
 *
 * @property Group $Group
 * @property Post $Post
 */
class Travelagency extends TagentsAppModel {
	/*public $actsAs = array(
		'Tagents.Captcha' => array(
			'field' => array('security_code'),						
			'error' => 'Incorrect captcha code value'
		)
	);*/

	public $location;
    
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Agency name is mandatory',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			array(
				'rule' => '/^[a-z0-9 ]{3,}$/i',
				'message' => 'Only letters and digits'
			)
		),
		'city' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'City is mandatory',
				'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),		
		
		'country_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Country is not being specified',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public $actsAs = array(
		'Uploader.Attachment' => array(
			'image' => array(
				/*'nameCallback' => 'formatName',*/
				'append' => '',
				'prepend' => '',
				'tempDir' => TMP,
				'uploadDir' => 'files/uploads/agencyimage/', //either write an absolute path
				'finalPath' => '/files/uploads/agencyimage/',
				'dbColumn' => 'imagesmall',
				'metaColumns' => array(
					'ext' => 'extension',
					'type' => 'mimetype',
					'size' => 'filesize'
				),
				'defaultPath' => '',
				'overwrite' => false,
				'stopSave' => true,
				'allowEmpty' => true,
				'transforms' => array(
					'imageSmall' => array(
					'method' => 'crop', // or crop / AttachmentBehavior::CROP
					'append' => '-small',
					'overwrite' => true,
					'self' => false,
					'width' => 350,
					'height' => 250,
					'dbColumn'=>'imagesmall',
					'quality'=>70,
					'nameCallback' => 'transformNameCallback',
					)/*,
					'imageMedium' => array(
					'method' => 'resize',
					'append' => '-medium',
					'width' => 600,
					'height' => 500,
					'aspect' => false,
					'dbColumn'=>'namemedium'
					)*/
				),
				/*'transport' => array(
					'class' => AttachmentBehavior::S3,
					'accessKey' => '08DFA9E7XFP15T0S55R2',
					'secretKey' => 'o+2qK8Jnw22IS5yvLzYNXyCBwav1rn6LqCkNJ6v5',
					'bucket' => 'cbcdnbucket',
					'region' => Aws\Common\Enum\Region::US_WEST_2,
					'folder' => 'announce/users/',
					'acl'=> 'public-read',
				),
				'transportDir' => 'announce/users/',
				'returnUrl'=> true,*/
			)
		)
	);
	public function formatName($name, $file) {
		return sprintf('%s-%s', $name, $file->size());
	}
	public function transformNameCallback($name, $file) {
		return $this->getUploadedFile()->name();
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	//public $belongsTo = array();

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Packag' => array(
			'className' => 'Packag',
			'foreignKey' => 'travelagency_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Travelagent' => array(
			'className' => 'Travelagent',
			'foreignKey' => 'travelagency_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Agencyinquiry' => array(
			'className' => 'Agencyinquiry',
			'foreignKey' => 'travelagency_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);
	/**
 * hasOne associations
 *
 * @var array
 */
	//public $hasOne = array();
	
	
	public function phone($check) {
		if(is_array($check)) {$value = array_shift($check);} else { $value = $check; }
		if(strlen($value) == 0) {return true;}
		return preg_match('/^[0-9-+()# ]{6,23}+$/', $value);
	}
}