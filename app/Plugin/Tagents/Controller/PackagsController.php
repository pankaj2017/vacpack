<?php
App::uses('TagentsAppController', 'Tagents.Controller');

/**
 * Profiles Controller
 *
 * @property Profile $Profile
 * @property PaginatorComponent $Paginator
 */
class PackagsController extends TagentsAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public $helpers = array('Paginator');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
	}

/**
 * index method
 * Lists only packages updated by Travel agency, thats its own packages
 * @return void
 */
	public function index($packtype=null,$admin=false) {
		
		$postconditions = array();
		// pr($this->Session->read('Auth.Travelagent.firstname'));die;
		if (!$admin){
			if($this->Session->check('Auth.Travelagent')){
				$postconditions[] = array('Packag.travelagency_id'=>$this->Session->read('Auth.Travelagent.travelagency_id'));
			}else{
				throw new NotFoundException(__('You need to login'));

			}
		}
		
		//$this->Package->recursive = 0;
		if($packtype){
			$postconditions[] = array("Packagetype.id"=>$packtype);
		}
		$this->Packag->unbindModel( 
			array(
        		'hasMany' => array(
            		'Itinerary'
        		)
    		)
		);
		$this->Packag->bindModel( 
			array(
        		'hasAndBelongsToMany' => array(
            		'Holidaytheme'=>array('fields'=>array('Holidaytheme.id','Holidaytheme.name')),
            		'Packagetype'=>array('fields'=>array('Packagetype.id','Packagetype.name'))
        		)
    		)
		);
		
		$this->Paginator->settings=array(
		 		'fields'=>array('Packag.title','Packag.id','Packag.numberofdays','Packag.numberofdays','Packag.numberofnights','Packag.modified','Packag.price','Packag.active','Travelagency.id','Travelagency.name','Travelagency.phone'),
		 		'conditions'=>$postconditions,
		 		'limit' => 10
		 );
		$packags=$this->Paginator->paginate();
		 //pr($packags);
		$this->set('packages',$packags);
	}

	function admin_index($packagetype=null,$admin=true){
		$this->index($packagetype,$admin);
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Packag->exists($id)) {
			throw new NotFoundException(__('Invalid profile'));
		}
		$conditions = array('Packag.' . $this->Packag->primaryKey => $id);
		//$profile = $this->Package->find('first', $options);
		
		$fields = array("Packag.*","Travelagent.id","Travelagent.firstname","Travelagent.");
		$this->Packag->Behaviors->load('Containable');
		$package=$this->Packag->find('first',
			array(
				'contain'=>array(
					'Travelagency'=>array(
						'Travelagent'=>array(
							'fields'=>array('Travelagent.firstname','Travelagent.lastname','Travelagent.email_address','Travelagent.phone')
						),
						'fields'=>array('Travelagency.name','Travelagency.phone','Travelagency.description','Travelagency.modified','Travelagency.created','Travelagency.active')
					)
				),
				'fields'=>array('Packag.title','Packag.id','Packag.inclusions','Packag.description','Packag.numberofdays','Packag.numberofnights','Packag.active','Packag.modified','Packag.price','Packag.travelagency_id'),
				'conditions'=>$conditions,
				'limit' => 10,
				'order'=>array('')
			)
		);
		//pr($package);exit;
		$this->set('package', $package);
	}

	public function admin_view($id = null) {
		$this->view($id);
	}
/**
 * add method
 *
 * @return void
 */
	public function create($admin=false) {
		if ($this->request->is(array('put','post'))) {
			if(!$this->Session->check("Auth.Travelagent")){
				$this->Flash->failure(__('The profile could not be saved. Please, try again.'));
				$this->redirect($this->Auth->logoutRedirect);
			}
			//pr($this->Session->read("Auth.Travelagent.travelagency_id"));die;
			$this->request->data['Packag']['travelagency_id'] = $this->Session->read("Auth.Travelagent.travelagency_id");
			// pr($this->request->data);exit;
			$this->Packag->create();
			if ($this->Packag->save($this->request->data)) {
				$this->Flash->success(__('The travle package has been saved.'));
				return $this->redirect(array('plugin'=>'tagents','controller'=>'packags','action'=>'index','admin'=>false));
			} else {
				$this->Flash->failure(__('The package could not be saved. Please, try again.'));
			}
		}
		$this->loadModel('State');
		$statelists = $this->State->find('list',['recursive'=>-1,'conditions'=>['State.country_id'=>104]]);
		$this->set(compact('statelists'));
		/*list of Package types and Holiday themes are being populated from the Appcontroller*/
	}

	function admin_create($admin=true){
		$this->create($admin);
	}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null,$admin=false) {
		if (!$this->Packag->exists($id)) {
			throw new NotFoundException(__('Invalid package'));
		}
		if(!$this->Session->check("Auth.Travelagent")){
				$this->Flash->failure(__('The profile could not be saved. Please, try again.'));
				//$this->redirect($this->Auth->logoutRedirect);
		}else{
		//pr($this->Session->read("Auth.Travelagent.Travelagency.id"));die;
		//$this->request->data['Packag']['travelagency_id'] = $this->Session->read("Auth.Travelagent.Travelagency.id");
		}
		if ($this->request->is(array('post', 'put'))) {
			if(!array_key_exists('id',$this->request->data['Packag'])){
				
			}
			if($this->request->data['Packag']['pkgimage']['name'] == '') {
				$this->request->data['Packag']['pkgimage'] = null;
				unset($this->request->data['Packag']['pkgimage']);
			}
			// pr($this->request->data);die;
			if ($this->Packag->saveAll($this->request->data)) {
				$this->Flash->success(__('Package has been saved.'));

				return $this->redirect(array('plugin'=>'tagents','controller'=>'packags','action'=>'index','admin'=>$admin));
			} else {
				// pr($this->Packag->validationErrors);die;
				$this->Flash->failure(__('The package could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Packag.' . $this->Packag->primaryKey => $id));
			$this->request->data = $this->Packag->find('first', $options);
			//pr($this->request->data);
		}
		$packagetypes = $this->Packag->Packagetype->find('list');		
		$this->set(compact('packagetypes'));
		$holidaythemes = $this->Packag->Holidaytheme->find('list');
		$this->set(compact('holidaythemes'));

		$this->loadModel('State');
		$statelists = $this->State->find('list',['recursive'=>-1,'conditions'=>['State.country_id'=>104]]);
		$this->set(compact('statelists'));
	}

	function admin_edit($id = null,$admin=true){
		$this->edit($id, $admin);
	}
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Package->id = $id;
		if (!$this->Package->exists()) {
			throw new NotFoundException(__('Invalid Package'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Package->delete()) {
			$this->Session->setFlash(__('The Package has been deleted.'));
		} else {
			$this->Session->setFlash(__('The Package could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	
}