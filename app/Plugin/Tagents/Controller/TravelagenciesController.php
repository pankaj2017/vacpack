<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('TagentsAppController', 'Tagents.Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class TravelagenciesController extends TagentsAppController {

	public $components = array('Paginator','Flash','RequestHandler');
/**
 * This controller does not use a model
 *
 * @var array
 */
	
	public function beforeFilter() { 
		parent::beforeFilter();
		//$this->Auth->allow();
		$this->set('masterclass','active');
		$this->set('dashboardclass','');
		$this->set('aclclass','');
		$this->set('usersclass','');
	}
/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function home() {
		
	}
	
	public function agencylist() {
		
		$this->index();
	}
	
	public function index() {
		
		$conditions = array();//',(Agency.name LIKE \'%'.$this->request->data['query'].'%\' or Agency.name LIKE \''.$this->request->data['query'].'%\')');
		$this->Paginator->settings = array(
			'Travelagency'=>array(
				'contain'=>array(
					'Travelagent'=>array('fields'=>array('id','firstname','lastname'),
						'Agentprofile'=>array('fields'=>array('id','imagesmall')),
					),
					//'Bscoverimage'=>array('imagesmall','imagebig'),
					//'Bscategory'=>array('id','name'),
					//'Rating'=>array()
				),
			'fields'=>array('Travelagency.id','Travelagency.name','Travelagency.city','Travelagency.phone','Travelagency.modified'),
			'conditions' => array($conditions),
			'limit' => 20,
			'order'=>array('Travelagency.active','Travelagency.name'=>'asc'),
			)
		);
		$travelagencies = $this->Paginator->paginate();
		//pr($travelagencies);exit;
		$this->set('travelagencies', $travelagencies);
		//echo json_encode($travelagencies);
		//exit;
	}
	
	public function admin_index(){
		$this->index();
	}
	
	public function create($admin=false) {
		if ($this->request->is(array('post','put'))) {
			$this->Travelagency->create();
			if ($this->Travelagency->save($this->request->data)) {
				$this->Flash->success(__('The Travelagency has been saved'),array('params'=>array('name'=>$this->request->data['Travelagency']['name'])));
				return $this->redirect(array('plugin'=>'tagents','controller'=>'travelagencies','action'=>'index','admin'=>$admin));
			} else {
				$this->Session->setFlash(__('The Travelagency could not be saved. Please, try again.'));
			}
		}
		$agencies = $this->Travelagency->find('list');
		$this->set(compact('agencies'));
	}
	
	public function admin_create($admin=true) {
		$this->create($admin);
	}
	
	public function edit($id = null,$admin=false) {
		// pr($id);pr($admin);die;
		if (!$this->Travelagency->exists($id)) {
			throw new NotFoundException(__('Invalid Travelagency'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Travelagency']['id'] = $id;
			// pr($id);
			// pr($this->request->data);die;
			if ($this->Travelagency->save($this->request->data)) {
				$this->Flash->success(__('The Travelagency has been saved'),
						array('params'=>array(
							'name'=>$this->request->data['Travelagency']['name']
							)
						)
					);
				return $this->redirect(array('plugin'=>'tagents','controller'=>'travelagents','action'=>'dashboard','admin'=>$admin));
			} else {
				$this->Flash->error(__('The Travelagency could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Travelagency.' . $this->Travelagency->primaryKey=>$id));
			$this->request->data = $this->Travelagency->find('first', $options);

			$travelagency_id = $this->Session->read('Auth.Travelagent.travelagency_id');
			$conditions = array("Travelagency.id" => $travelagency_id);
			$profile = $this->Travelagency->find('first',array('conditions'=>$conditions,'recursive'=>1));
			$profile['Travelagent'] = $profile['Travelagent'][0];

			$conditions = array("Travelagentprofile.travelagent_id" => $this->Session->read('Auth.Travelagent.id'));
			$this->loadModel('Travelagentprofile');
			$profile_data = $this->Travelagentprofile->find('first',['conditions' =>$conditions]);
			$profile['Travelagentprofile'] = $profile_data['Travelagentprofile'];
			$this->set('currentuser', $profile);
		
		}
	}
	
	public function admin_edit($id = null,$admin=true) {
		$this->edit($id,$admin);
	}
	
	public function view($id = null,$admin=false) {
		if (!$this->Travelagency->exists($id)) {
			throw new NotFoundException(__('Invalid Aagency'));
		}
		$conditions = array('Travelagency.' . $this->Travelagency->primaryKey => $id);

		$fields = array("Travelagency.id","Travelagency.name","Travelagency.description","Travelagency.city","Travelagency.region","Travelagency.zipcode","Travelagency.phone","Travelagency.imagesmall","Travelagency.email_address",
		);
		$this->Travelagency->Behaviors->load('Containable');
		$agency = $this->Travelagency->find('first',array(
			"contain"=>array(
				"Packag"=>array(
					"fields"=>array("Packag.id","Packag.title","Packag.description","Packag.price","Packag.numberofnights","Packag.numberofdays","Packag.modified"),
					"conditions"=>array("Packag.active"=>1),
					"Packageinquiry"=>array("fields"=>array("Packageinquiry.id","Packageinquiry.fname","Packageinquiry.lname","Packageinquiry.email","Packageinquiry.phone","Packageinquiry.created",)),
				),
				"Travelagent"=>array(
					"fields"=>array("Travelagent.firstname","Travelagent.lastname","Travelagent.phone","Travelagent.email_address")
				),
				"Agencyinquiry"=>array(
					"fields"=>array("Agencyinquiry.id","Agencyinquiry.travelagency_id","Agencyinquiry.fname","Agencyinquiry.lname","Agencyinquiry.email","Agencyinquiry.phone","Agencyinquiry.message","Agencyinquiry.created")

					),
				
			),
			"fields"=>$fields,
			"conditions"=>$conditions)
		);
		//pr($agency);exit;
		$this->set('agency', $agency);
	}
	
	public function admin_view($id = null,$admin=true) {
		$this->view($id,$admin);
	}

	public function uploadimage() {
		$this->layout='ajax';
		if ($this->request->is(array('post','put'))) {
		//pr($this->request->data);exit;
			if ($this->Travelagency->save($this->request->data,true)) {
				//$this->redirect(array('action' => 'uploadimage'));
			}
		}
		$fields = array("Travelagency.id","Travelagency.imagesmall");
		$conditions = array("Travelagency.id"=>$this->Session->read("Auth.Travelagent.travelagency_id"));
		$agency = $this->Travelagency->find('first',array('conditions'=>$conditions,'fields'=>$fields));
		//pr($user);exit;
		if(!empty($user)){
			$this->set('imagesmall',$user['Travelagency']['imagesmall']);
		}
	}
	function _profile($userid=null){
		$conditions = array("Travelagent.id"=>$userid);
		$fields = array("Travelagent.id","Travelagent.firstname","Travelagent.lastname","Travelagent.phone","Travelagent.email_address","Travelagentprofile.id","Travelagentprofile.travelagent_id","Travelagentprofile.userimage","Travelagentprofile.messanger","Travelagentprofile.msgtype","Travelagentprofile.aboutme","Travelagentprofile.quotes","Travelagency.name");
		return $currentAgent = $this->Travelagent->find('first',
			array(
				'contain'=>array(
					'Travelagentprofile',
					'Travelagency',
				),
				'conditions'=>$conditions,
				'fields'=>$fields,
			)
		);
	}
}