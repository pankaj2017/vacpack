<?php
App::uses('TagentsAppController', 'Tagents.Controller');

/**
 * Profiles Controller
 *
 * @property Profile $Profile
 * @property PaginatorComponent $Paginator
 */
class ItinerariesController extends TagentsAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public $helpers = array('Paginator');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
	}

/**
 * index method
 *
 * @return void
 */
	public function index($packtype=null,$admin=false) {

		$postconditions = array();
		//$this->Package->recursive = 0;
		
		$postconditions = array("Package.id"=>$packtype);
		
		$this->Itinerary->Behaviors->load('Containable');
		$this->Paginator->settings=array(
				
				'fields'=>array('Packag.title','Packag.id','Packag.numberofdays','Packag.numberofdays','Packag.numberofnights','Packag.modified','Packag.price','Packag.active'),
				'limit' => 10
			
		);
		$packags=$this->Paginator->paginate();
		//pr($packags);
		$this->set('packages',$packags);
	}

	function admin_index($packagetype=null,$admin=true){
		$this->index($packagetype,$admin);
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Itinerary->exists($id)) {
			throw new NotFoundException(__('Invalid profile'));
		}
		$conditions = array('Itinerary.' . $this->Itinerary->primaryKey => $id);
		//$profile = $this->Package->find('first', $options);
		
		$fields = array("Itinerary.*");
		$this->Itinerary->Behaviors->load('Containable');
		$package=$this->Itinerary->find('first',
			array(
				'fields'=>array('Packag.title','Packag.id','Packag.inclusions','Packag.description','Packag.numberofdays','Packag.numberofnights','Packag.active','Packag.modified','Packag.price','Packag.travelagency_id'),
				'conditions'=>$conditions,
				'limit' => 10,
				'order'=>array('')
			)
		);
		//pr($package);exit;
		$this->set('package', $package);
	}

	public function admin_view($id = null) {
		$this->view($id);
	}
/**
 * add method
 *
 * @return void
 */
	public function create($packageid=null,$admin=null) {
		if ($this->request->is(array('put','post'))) {
			// pr($this->request->data);die;
			if(!$this->Session->check("Auth.Travelagent")){
				$this->Flash->failure(__('The profile could not be saved. Please, try again.'));
				$this->redirect($this->Auth->logoutRedirect);
			}
			// $packag_id = $this->request->data['Itinerary']['packag_id'];
			// $abouttheday=json_encode($this->request->data['Itinerary']);
			// unset($this->request->data['Itinerary']);
			// $this->request->data['Itinerary']['abouttheday']= $abouttheday;
			// $this->request->data['Itinerary']['packag_id']= $packag_id;
			// pr($this->request->data);exit;
			foreach ($this->request->data['Itinerary'] as $key => $value) {
				if($value['imageofday']['name'] == '') {
					$value['imageofday'] = null;
					unset($value['imageofday']);
				}
				$value['Itinerary'] = $value;
				if(isset($value['Itinerary']['id'])) {

				$this->Itinerary->save($value);
				}else{
				$this->Itinerary->create();
				$this->Itinerary->save($value);
				}
			}
			
				$this->Flash->success(__('The travle Itinerary has been saved.',
						array(
							'key'=>'need-to-define',
							'params'=>array('class'=>'bg-danger-custom')
						)
					)
				);
				return $this->redirect(array('plugin'=>'tagents','controller'=>'packags','action'=>'index','admin'=>false));
			// $this->Itinerary->create();
			// if ($this->Itinerary->saveAll($this->request->data['Itinerary'])) {
			// } else {
			// 	// pr($this->Itineraryy->validationErrors);exit;
			// 	$this->Flash->failure(__('The Itinerary could not be saved. Please, try again.'));
			// }
		}
		if ($packageid){
			
			// $this->Itinerary->Behaviors->load('Containable');
			// $fields = array('Packag.title','Packag.id','Packag.inclusions','Packag.description','Packag.numberofdays','Packag.numberofnights','Packag.active','Packag.modified','Packag.price','Packag.travelagency_id');
			// $conditions = array('Itinerary.' . $this->Itinerary->primaryKey => $packageid);
			$itinerary = $this->Itinerary->find('all',array(
				'conditions'=>array('Itinerary.packag_id' => $packageid),'recursive' =>-1
				)
			);
			// pr($itinerary);die;
			if(!empty($itinerary)){
				$this->loadModel('Packag');
				$this->Packag->id = $packageid;
				// $numberofdays = $this->Packag->field('numberofdays');
				$pkgdata = $this->Packag->find('first',['conditions'=>['Packag.id' => $packageid],'recursive' => -1]);
				$itinerary1 = [];
				foreach ($itinerary as $key => $value) {
					$itinerary1['Itinerary'][$key] = $value['Itinerary'];
				}
				$this->set(compact('pkgdata'));
				$this->set(compact('itinerary1'));
				$this->set(compact('packageid'));
			}else{
				$this->loadModel('Packag');
				$this->Packag->id = $packageid;
				$pkgdata = $this->Packag->find('first',['conditions'=>['Packag.id' => $packageid],'recursive' => -1]);
				// pr($numberofdays);die;
				$this->set(compact('pkgdata'));
				$this->set(compact('packageid'));

				// App::import('Model','Packag');
				// $Packag = new Packag();
				// $Packag->recursive = 0;
				// $fields = array('Packag.title','Packag.id','Packag.numberofdays','Packag.numberofnights','Packag.active','Packag.price','Packag.travelagency_id');
				// $packag = $Packag->find('first',array('conditions'=>array('Packag.id'=>$packageid),'fields'=>$fields));
				//pr($packag);
			}
			// if (!empty($packag)){
			// 	$this->set(compact('packag'));
			// }else{
			// 	throw new NotFoundException(__('Package Not Found'));
			// }
		
		
		}
	}

	function admin_create($packageid=null,$admin=true){
		$this->create($packageid,$admin);
	}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null,$admin=null) {
		if (!$this->Itinerary->exists($id)) {
			throw new NotFoundException(__('Invalid package'));
		}
		if(!$this->Session->check("Auth.Travelagent")){
				$this->Flash->failure(__('The profile could not be saved. Please, try again.'));
				//$this->redirect($this->Auth->logoutRedirect);
		}else{
		
		$this->request->data['Itinerary']['travelagency_id'] = $this->Session->read("Auth.Travelagent.Travelagency.id");
		}
			//pr($this->request->data);exit;
		if ($this->request->is(array('post', 'put'))) {
			if(!array_key_exists('id',$this->request->data['Itinerary'])){
				
			}
			if ($this->Itinerary->saveAll($this->request->data)) {
				$this->Flash->success(__('Package has been saved.'));
				return $this->redirect(array('plugin'=>'','controller'=>'packags','action'=>'index','admin'=>$admin));
			} else {
				$this->Flash->failure(__('The package could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Itinerary.' . $this->Itinerary->primaryKey => $id));
			$this->request->data = $this->Packag->find('first', $options);
		}
		
	}

	function admin_edit($id = null,$admin=true){
		$this->edit($id, $admin);
	}
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Itinerary->id = $id;
		if (!$this->Itinerary->exists()) {
			throw new NotFoundException(__('Invalid Itinerary'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Itinerary->delete()) {
			$this->Session->setFlash(__('The Itinerary has been deleted.'));
		} else {
			$this->Session->setFlash(__('The Itinerary could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	
}
