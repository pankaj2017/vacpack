<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->Html->charset(); ?>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Vacdoo</title>

	<?php
	echo $this->Html->meta('favicon.png','favicon.png',array('type' => 'icon'));
	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->Html->css(['/assets/css/bootstrap.min','/assets/css/main']);
	// echo $this->Html->css(['/assets/css/bootstrap.min','/assets/css/owl.carousel.min','/assets/css/owl.theme.default.min','/assets/css/main']); 
	// echo $this->Html->script(['/assets/js/jquery.min','/assets/js/jquery-migrate-3.0.0','/assets/js/jquery-ui.min','/assets/js/jquery.nice-select.min','/assets/js/jquery.stellar.min','/assets/js/owl.carousel.min']);
	echo $this->Html->script(['/assets/js/jquery.min','/assets/js/popper.min','/assets/js/jquery-migrate-3.0.0','/assets/js/jquery-ui.min','/assets/js/jquery.nice-select.min','/assets/js/main','/assets/js/bootstrap.min']);
	echo $this->fetch('script');
	?>
	<script type="text/javascript">
	var wroot = '<?php echo Router::url('/');?>';
	</script>		
</head>
<body class="wrapper js">		
<?php 
	echo $this->element('tagent_default_header');
	echo $this->fetch('content');
	echo $this->element('tagent_default_footer');			
	echo $this->element('sql_dump');
	?>	
<?php echo $this->Html->script(['/assets/js/main']); ?>

</body>
</html>