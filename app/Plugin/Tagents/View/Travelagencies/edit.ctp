<?php echo $this->Html->script('ckeditor/ckeditor'); ?>

<div class="container inner_pages">
<div class="row">

<div class="col-md-12">
<?php echo $this->Html->script('bootstrap-filestyle.min',false); ?>
<?php echo $this->Html->css('jquery_datetimepicker'); //requried for bdate ?>

<h4 class="">Travel Agency Edit
	<div class="float-right btn btn-light"><?php echo $this->Html->link("Cancle update", array("plugin"=>"tagents","controller"=>"travelagents","action"=>"dashboard","admin"=>false));?>
		</div></h4><hr>
	<div class="col-right">
		<fieldset>
			<?php echo $this->Form->create('Travelagency',array("url"=>array("plugin"=>"tagents","controller"=>"travelagencies","action"=>"edit"),"type" =>"file")); ?>				
				<div class="row">
					<div class="form-group col-md-12"><label>Agency name</label>
						<span class="float-right">Write your travel agency name.</span>
						<?php echo $this->Form->text('Travelagency.name',array("class"=>"form-control ie7-margin ckeditor"));?>
						<?php echo $this->Form->error('Travelagency.name'); ?>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12"><label>About travel agency</label>
						<span class="float-right">Describe travel agency, your area of expertise.</span>
						<?php echo $this->Form->input('Travelagency.description',array("type"=>"textarea","label"=>false, "class"=>"form-control ie7-margin ckeditor"));?>
						<?php echo $this->Form->error('Travelagency.description'); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<label>Agency business email</label>
						<?php echo $this->Form->text('Travelagency.email_address',array("class"=>"form-control ie7-margin")); ?>
					</div>				
					<div class="form-group col-md-6">
						<label>Agency business phone</label><small class="float-right"></small>
						<?php echo $this->Form->text('Travelagency.phone',array("type"=>"text","class"=>"form-control ie7-margin")); ?>
						<?php echo $this->Form->error('Travelagency.phone'); ?>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>City</label>
						<?php echo $this->Form->input('Travelagency.city',array("type"=>"text","label"=>false, "class"=>"form-control ie7-margin")); ?>
					</div>
					<div class="form-group col-md-6">
						<label>State / region</label>
						<?php echo $this->Form->input('Travelagency.region',array("type"=>"text","label"=>false, "class"=>"form-control ie7-margin")); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<label>Travel agency country</label>
						<?php echo $this->Form->select('Travelagency.country_id',$appcountries,array("class"=>"form-control ","empty"=>"Select country"));?>
						<?php echo $this->Form->error('Travelagency.country_id'); ?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						<label>Team size of your agency</label>
						<?php  $options = array("<5"=>"less than 5","5-10"=>"between 5 to 10","10-15"=>"between 10 to 15","15>"=>"more than 15");
						echo $this->Form->select('Travelagency.teamsize',$options,array("type"=>"text","label"=>false, "class"=>"form-control ie7-margin","empty"=>'--Select team size--')); ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<label>Travel agency zipcode</label>
						<?php echo $this->Form->text('Travelagency.zipcode',array("type"=>"text","class"=>"form-control  form-control-sm","placeholder"=>"Zipcode"));?>
						</div>
					</div>
					<div class="form-group col-md-6">
						<label>Travel agency website</label>
						<?php echo $this->Form->text('Travelagency.website',array("type"=>"text","class"=>"form-control  form-control-sm","placeholder"=>"Website"));?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<label>Travel agency Image</label>
							<?php echo $this->Form->input("Travelagency.image", array('type'=>'file',"class"=>"filestyle","data-buttonName"=>"btn-default","label"=>false));?>
						</div>
					</div>
					<div class="col-md-6 form-group">
						<?php 
						if(!empty($this->request->data['Travelagency']['imagesmall'])){
							echo $this->Html->image($this->request->data['Travelagency']['imagesmall'],array('class'=>'img-fluid')); ?>
						<?php } ?>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group col-md-3"><?php echo $this->Form->submit('Update Agency information',array("class"=>"btn btn-primary")); ?></div>
				</div>
				
			<?php echo $this->Form->end(); ?>
		</fieldset>
	</div>
</div>
</div>
<script type="text/javascript">

CKEDITOR.replace('TravelagencyDescription', {
toolbar: [[ 'Source' ],[ 'Bold', 'Italic','Underline','Subscript','Superscript'],[ 'NumberedList','BulletedList' ]],

height: '200',
});
</script>

<script>$(":file").filestyle({buttonName: "btn-default",buttonText: "&nbsp;Image"});</script>
