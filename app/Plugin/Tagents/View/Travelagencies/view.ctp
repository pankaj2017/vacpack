<?php echo $this->Html->meta('canonical', Router::url('/', true).'Travelagency/'.Inflector::slug(strtolower($agency['Travelagency']['name']),'-'),array('rel'=>'canonical', 'type'=>null, 'title'=>null, 'inline' => false)); ?>
<?php $this->assign('title',$agency['Travelagency']['name']);?>
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<aside>
				<?php if(!empty($agency['Travelagency']['imagesmall'])){
					echo $this->Html->image($agency['Travelagency']['imagesmall'], array('class'=>'img-fluid'));
					echo $this->Html->meta(array('property'=>'og:image','type'=>'meta', 'content'=>substr(Router::url('/', true), 0, -1).'travelagency/'.$agency['Travelagency']['imagesmall'], 'rel' => null),NULL, array('inline'=>false));
					
					}else{
					echo $this->Html->image('company.png', array('class'=>'img-fluid'));
					}
					echo $this->Html->meta(array('property'=>'og:type','type'=>'meta', 'content'=>'website', 'rel' => null),NULL, array('inline'=>false));
					echo $this->Html->meta(array('property'=>'og:url','type'=>'meta', 'content'=>Router::url('/', true).$agency['Travelagency']['name'], 'rel' => null),NULL, array('inline'=>false));
				?>
				<div class="mt-3">
					<?php echo $this->element('Tagents.sidebar_dashboard');?>
				</div>
			</aside>
		</div>
		
		<div class="col-md-9">
			<div class="content_box">
				<div class="articles-details-header">
					<h3><?php echo $agency['Travelagency']['name'];?>

						
			
			<?php if($this->Session->check('Auth.Travelagent.id')){
					if($this->Session->read('Auth.Travelagent.id')){
				?>	
					<div class="float-right btn btn-light">
						<?php echo $this->Html->link("Update Agency", array('plugin'=>'tagents','controller'=>'travelagencies','action'=>'edit/'.$this->Session->read('Auth.Travelagent.travelagency_id'),"admin"=>false));?>
					</div>
				<?php }

			}?></h3>

					<hr/>
					<div class="articles-details-header-img"> 
						
					</div>					
				</div>
				
				
				<div id="tabs">
					
								<nav>
									<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
										<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">About Agency</a>
										
										<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Agency Team</a>
										
										<a class="nav-item nav-link" id="nav-package-tab" data-toggle="tab" href="#nav-package" role="tab" aria-controls="nav-profile" aria-selected="false">Packages</a>
										
										<a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">Inquiries</a>
									</div>
								</nav>
								<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

									<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
										<?php echo $agency['Travelagency']['description']; ?>
									</div>
									<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
										<?php echo $this->Html->link("Add New", array("plugin"=>"tagents","controller"=>"travelagents","action"=>"createteam","admin"=>false),array("class"=>"btn btn-sm btn-outline-info float-right")); ?>
										<table class="table table-bordered table-striped table-condensed">
										<thead>
											<tr>
												<th width="40%">Full Name</th>
												<th width="40%">Email</th>
												<th width="20%">Phone</th>
												
											</tr>
										</thead>
										<tbody>
										<?php foreach ($agency['Travelagent'] as $team){?>
											
											<tr>
												<td><strong><?php echo $team['firstname']." ".$team['lastname'];?></strong><br>
												</td>
												<td><?php echo $team['email_address'];?></td>
												<td><?php echo $team['phone'];?> </td>
												
											</tr>
											
										<?php }?>
										</tbody>
										</table>
									</div>
									
									<div class="tab-pane fade" id="nav-package" role="tabpanel" aria-labelledby="nav-package-tab">
										<table class="table table-bordered table-striped table-condensed">
											<thead>
												<tr>
													<th width="40%">Name</th>
													<th width="10%">#Days</th>
													<th width="10%">Price</th>
													<th width="14%">UpdatedOn</th>
													<th width="18%">Action</th>
												</tr>
											</thead>
											<tbody>
										
										<?php foreach ($agency['Packag'] as $package){?>
											
											<tr>
												<td><strong><?php echo $package['title'];?></strong><br>
												</td>
												<td><?php echo $package['numberofdays'];?>&nbsp;days </td>
												<td><?php echo $package['price'];?> </td>
												
												
												<td><?php echo $this->Time->format('M j, Y',$package['modified']); ?></td>
												<td><?php echo $this->Html->link('View',array('controller'=>'packags','action'=>'view/'.$package['id'],'admin'=>false),array('class'=>'badge badge-success')); ?> 
												<?php echo $this->Html->link('Edit',array('controller'=>'packags','action'=>'edit/'.$package['id'],'admin'=>false),array('class'=>'badge badge-info')); ?> 
												<?php echo $this->Html->link('Delete',array('controller'=>'packags','action'=>'delete/'.$package['id'],'admin'=>false),array('class'=>'badge badge-danger')); ?></td>
											</tr>

												<?php foreach ($package['Packageinquiry'] as $packageinquiry){?>
													
													<tr>
															<td><?php echo $packageinquiry['fname']." ".$packageinquiry['lname'];?></td>
															<td><?php echo $packageinquiry['email'];?></td>
															<td><?php echo $packageinquiry['phone'];?></td>
															<td><?php echo $packageinquiry['created'];?></td>
															<td>&nbsp;</td>
													</tr>

												<?php }?>
											
										<?php }?>
											</tbody>
										</table>
									</div>
									<div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
										<table class="table table-bordered table-striped table-condensed">
										<thead>
											<tr>
												<th width="20%">Full Name</th>
												<th width="30%">Email</th>
												<th width="20%">Phone</th>
												<th width="15%">Received</th>
												<th width="15%">View</th>
											</tr>
										</thead>
										<tbody>
										<?php foreach ($agency['Agencyinquiry'] as $inquiry){?>
											
											<tr>
												<td><strong><?php echo $inquiry['fname']." ".$inquiry['lname'];?></strong><br>
												</td>
												<td><?php echo $inquiry['email'];?></td>
												<td><?php echo $inquiry['phone'];?> </td>
												<td><?php echo $inquiry['created'];?> </td>
												<td><?php echo "View" ;?></td>
											</tr>
											
										<?php }?>
										</tbody>
										</table>
									</div>
								</div>
							
							
				</div><!-- ./Tabs -->
			</div>
		</div>
	</div>
</div>
		

<?php echo $this->fetch('script_execute'); //rating script ?>