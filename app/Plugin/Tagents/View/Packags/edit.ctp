<!--main content start-->
<style>
.checkbox input{
  margin: 0 5px 0 0px;
}
</style>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header"><h6>Update Package<?php echo $this->Html->link("Back to list", array("plugin"=>"tagents","controller"=>"packags","action"=>"index","admin"=>false),array("class"=>"btn btn-sm btn-outline-info float-right")); ?></h6>
					</div>
					<div class="row">
						<div class="col-md-7">						
							<div class="card-body">
								<?php echo $this->Form->create('Packag', array("url"=>array("plugin"=>"tagents","controller"=>"packags","action"=>"edit","admin"=>false),"role"=>"form","class"=>"",'type'=>'file'));?>
								
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
										<label>Package title</label>
										<?php echo $this->Form->text('Packag.title',array("type"=>"text","class"=>"form-control ie7-margin","placeholder"=>"Package title")); 
											 echo $this->Form->hidden('Packag.id'); 
											 echo $this->Form->hidden('Packag.travelagency_id'); ?>

										<!-- echo $this->Form->text('Packag.id');
										echo $this->Form->text('Packag.travelagency_id');?> -->
										<?php echo $this->Form->error('Packag.title'); ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
										<label>Package description</label>
										<?php echo $this->Form->textarea('Packag.description',array("rows"=>"4","class"=>"form-control ie7-margin","placeholder"=>"Describe about package")); ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label>Number of days</label>
										<?php echo $this->Form->text('Packag.numberofdays',array("type"=>"text","class"=>"form-control ie7-margin","placeholder"=>"Number of days")); ?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										<label>Number of nights</label>
										<?php echo $this->Form->text('Packag.numberofnights',array("type"=>"text","class"=>"form-control ie7-margin","placeholder"=>"Number of nights")); ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label>Package price</label>
										<?php echo $this->Form->text('Packag.price',array("type"=>"text","class"=>"form-control ie7-margin","placeholder"=>"Price of the package")); ?>
										</div>
									</div>
									<div class="col-md-3">
									<div class="form-group">
										<label>Discount</label>
										<?php echo $this->Form->text('Packag.discount',array("type"=>"text","class"=>"form-control ie7-margin","placeholder"=>"Discount")); ?>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
										<label>Discount type</label>
										<?php $discounttype = array("PC"=>"Percentage","FT"=>"Flat");
										echo $this->Form->select('Packag.discounttype',$discounttype,array("class"=>"form-control ie7-margin","empty"=>"Type")); ?>
										</div>
									</div>
								</div>
								<div class="row">
									<?php /*<div class="col-md-6">
										<div class="form-group">
										<label>Country</label>
										<?php $discounttype = array("PC"=>"Percentage","FT"=>"Flat");
										echo $this->Form->select('Packag.discounttype',$discounttype,array("class"=>"form-control ie7-margin","empty"=>"Type")); ?>
										</div>
									</div>*/?>
									<div class="col-md-12">
										<div class="form-group">
										<label>State</label>
										<?php echo $this->Form->select('Packag.state_id',$statelists,array("class"=>"form-control","empty"=>"Select State",)); ?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										<label>Image</label>
										<?php  
										echo $this->Form->input('Packag.pkgimage', array('type'=>'file',"class"=>"file", "data-max-file-count"=>"1", "label"=>false,'require')); 
										?>
										</div>
									</div>
									<div class="col-md-2 form-group">
										<!-- <label>Place Image<span class="">*</span></label> -->
										<?php 
										if(!empty($this->request->data['Packag']['pkgimage'])){
											echo $this->Html->image($this->request->data['Packag']['pkgimage'],array('class'=>'img-fluid')); ?>
										<?php } ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
										<label>Items included in this package</label>
										<?php echo $this->Form->textarea('Packag.inclusions',array("rows"=>"4","class"=>"form-control ie7-margin","placeholder"=>"Mention what is included in this package")); ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
										<label>Items excluded in this package</label>
										<?php echo $this->Form->textarea('Packag.exclusions',array("rows"=>"4","class"=>"form-control ie7-margin","placeholder"=>"Mention what is NOT included in this package")); ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<?php echo $this->Form->input('Packagetype', array('multiple'=>'checkbox', 'label'=>false, 'type'=>'select','separator'=>'&nbsp;','options'=>$apppackagetypes,'before' => '<label>Assign suitable package types</lablel>')); ?>
									</div>
								
									<div class="col-md-6">
										<?php echo $this->Form->input('Holidaytheme', array('multiple'=>'checkbox', 'label'=>false, 'type'=>'select','separator'=>'&nbsp;','options'=>$appholidaythemeslist,'before' => '<label>Assign suitable Holiday themes</lablel>')); ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<label>
									<?php echo $this->Form->checkbox('Packag.featured',array()); ?>&nbsp;Featured</label>
									</div>
									<div class="col-md-6">
										<label>
									<?php echo $this->Form->checkbox('Packag.sponsored',array()); ?>&nbsp;Sponsored</label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
									<label>
									<?php echo $this->Form->checkbox('Packag.active',array()); ?>&nbsp;Make this project active</label>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<button type="submit" class="btn btn-primary">Update Package</button>
									</div>
								</div>
								<?php echo $this->Form->end(); ?>
							</div>
						</div>
						<div class="col-md-5">
						
							
						</div>
					</div>
                </div>	
		
			</div><!-- end of id welcome -->
		</div><!--/#content.span10-->
	</div>