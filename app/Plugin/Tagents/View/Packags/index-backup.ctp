<?php //echo $this->element('sidebar_dashboard');
echo $this->Html->Script('jquery.tmpl');
?>
<!--main content start-->

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div><?php echo $this->Flash->render(); ?> </div>
			<div class="card">
				<div class="card-header"><h6>List of Packages<?php echo $this->Html->link("Add New", array("plugin"=>"tagents","controller"=>"packags","action"=>"create","admin"=>false),array("class"=>"btn btn-sm btn-outline-info float-right")); ?></h6>
				</div>
				<div class="card-body">
					<table class="table table-bordered table-striped table-condensed">
						<thead>
							<tr>
								<th width="37%">Name</th>
								<th width="6%">#Days</th>
								<th width="6%">#Nights</th>
								<th width="10%">Price</th>
								<th width="11%">Modified</th>
								<th width="12%">Action</th>									
							</tr>							
						</thead>
						<tbody>
							<?php 
							foreach($packages as $package){?>
							<tr>
								<td class="pckTitle"><?php echo $package['Packag']['title'];?> </td>
								<td class="pckDay"><?php echo $package['Packag']['numberofdays'];?> </td>
								<td><?php echo $package['Packag']['numberofnights'];?> </td>
								<td><?php echo $package['Packag']['price'];?> </td>
								<td><?php echo $this->Time->format('M jS, Y',$package['Packag']['modified']); ?></td>
								<td><?php echo $this->Html->link('View',array('plugin'=>'tagents','controller'=>'packags','action'=>'view/'.$package['Packag']['id'],'admin'=>false),array('class'=>'badge badge-success')); ?>
								<?php echo $this->Html->link('Edit',array('plugin'=>'tagents','controller'=>'packags','action'=>'edit/'.$package['Packag']['id'],'admin'=>false),array('class'=>'badge badge-info')); ?> 
								<?php echo $this->Html->link('Delete',array('plugin'=>'tagents','controller'=>'packags','action'=>'delete/'.$package['Packag']['id'],'admin'=>false),array('class'=>'badge badge-danger')); ?></td>
								<td><a href="#" id="itinerary" data-id="<?php echo $package['Packag']['id'];?>" class="badge badge-danger chirayu" data-toggle="modal" data-target="#exampleModal">Itinerary</a></td>
							</tr>
							<?php } ?>					
						</tbody>
					</table>
					
					<p>
					<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					));
					?>	</p>
					
					<ul class="pagination">
						<?php
							echo $this->Paginator->prev(__('Prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
							echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
						?>
					</ul>
				</div>
			</div><!-- panel -->
		</div><!--/col-lg-12-->
	</div>	
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">						
						<div class="card-body">
							<?php echo $this->Form->create('Itinerary', array("url"=>array("plugin"=>"tagents","controller"=>"itineraries","action"=>"create","admin"=>false),"role"=>"form","class"=>""));?>
							
							<div id="itineraryContainer"></div>
							
						</div><!--card-body-->
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-sm btn-primary">Create Itinerary</button>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>
<script>
$(document).on("click", ".chirayu", function () {
    var myPackagId = $(this).data('id');
	var pTitle = $(this).closest('tr').find('.pckTitle').text();
	var nDays = $(this).closest('tr').find('.pckDay').text();
    $(".modal-body #PackagId").val( myPackagId );
	$(".modal-header #modalTitle").html( pTitle );
	$("#itineraryContainer").empty();
	$("#itineraryTemplate").tmpl({days:new Array(parseInt(nDays))}).appendTo("#itineraryContainer");
	$("#itineraryTemplate").tmpl({pid:myPackagId})
});
</script>
<script id="itineraryTemplate" type="text/x-jQuery-tmpl">
{{each(i) days}}
    <div class="row">
		<div class="col-md-2">
			<div class="form-group">
			<label>Day ${i+1}</label>
			<input name="data[Itinerary][${i}][daynumber]" type="text" class="form-control form-control-sm" id="ItineraryDaynumber" value=${i+1} readonly>
			<input name="data[Itinerary][${i}][packag_id]" type="hidden" value=3 />
			</div>
		</div>
		<div class="col-md-10">
			<div class="form-group">
			<label>Title of the day</label>
			<input name="data[Itinerary][${i}][titleofday]" type="text" class="form-control form-control-sm" placeholder="" id="ItineraryTitleofday">
			<?php echo $this->Form->error('Itinerary.titleofday'); ?>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
			
			<textarea name="data[Itinerary][${i}][abouttheday]" rows="2" class="form-control form-control-sm" placeholder="" id="ItineraryAbouttheday"></textarea>
			
			</div>
		</div>
	</div>
{{/each}}
</script>