<section class="section-agency" id="agency-banner">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6 order-md-2 text-center">
            <?= $this->Html->image("/assets/images/agency-banner.png",array('class'=>'img-fluid my-4','alt'=>"agency-banner"), array('escape'=>false)); ?>
          </div>
          <div class="col-md-6 order-md-1">
            
			<h2 class="text-justify">Design best of your packages</h2>
            <p class="mb-3 text-justify">Vacdoo.com provides a platform to travel agencies to post their holiday packages. Define your agency information and your team expertise in various kind of vacation themes. Provide your team members contact details. Reach out to millions of vacationers and promot your packages that best suits to them. Get connected directly to travelers and have and opprtuinity to explain about each day of their vacation.</p>
			<p class="mb-3" >Well informed agency page attracts more travlers to contact your agency. Increase the frequency of vacation inquiries.</p>
			<?php echo $this->Html->link("Start getting leads", array("plugin"=>"tagents","controller"=>"travelagents","action"=>"login","admin"=>false), array("class"=>"btn btn-blue"));?>
          </div>
        </div>
      </div>
    </section>
    <section class="section-agency" id="how-works">
      <div class="container">
        <div class="row">
          <div class="col-12 py-3">
            <h2 class="agency-title c-white">HOW IT WORKS</h2>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="icon-block">
              <div class="icon">
                <svg id="Icon_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                  <g>
                    <g>
                      <g>
                        <path class="active-path" d="M225,0C150.561,0,90,60.561,90,135s60.561,135,135,135s135-60.561,135-135S299.439,0,225,0z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                      </g>
                    </g>
                    <g>
                      <g>
                        <path class="active-path" d="M407,302c-23.388,0-45.011,7.689-62.483,20.667C315.766,308.001,284.344,300,255,300h-60    c-52.009,0-101.006,20.667-137.966,58.195C20.255,395.539,0,444.834,0,497c0,8.284,6.716,15,15,15h392    c57.897,0,105-47.103,105-105C512,349.103,464.897,302,407,302z M407,482c-41.355,0-75-33.645-75-75    c0-21.876,9.418-41.591,24.409-55.313c0.052-0.048,0.103-0.098,0.154-0.147C369.893,339.407,387.597,332,407,332    c41.355,0,75,33.645,75,75C482,448.355,448.355,482,407,482z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                      </g>
                    </g>
                    <g>
                      <g>
                        <path class="active-path" d="M437,392h-15v-15c0-8.284-6.716-15-15-15s-15,6.716-15,15v15h-15c-8.284,0-15,6.716-15,15s6.716,15,15,15h15v15    c0,8.284,6.716,15,15,15s15-6.716,15-15v-15h15c8.284,0,15-6.716,15-15S445.284,392,437,392z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                      </g>
                    </g>
                  </g>
                </svg>
              </div>
              <h4>Create agency profile</h4>
            </div>
          </div>
		  <div class="col-sm-6 col-md-3">
            <div class="icon-block">
              <div class="icon">
                <svg id="Icon_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                  <g>
                    <g>
                      <g>
                        <path class="active-path" d="M225,0C150.561,0,90,60.561,90,135s60.561,135,135,135s135-60.561,135-135S299.439,0,225,0z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                      </g>
                    </g>
                    <g>
                      <g>
                        <path class="active-path" d="M407,302c-23.388,0-45.011,7.689-62.483,20.667C315.766,308.001,284.344,300,255,300h-60    c-52.009,0-101.006,20.667-137.966,58.195C20.255,395.539,0,444.834,0,497c0,8.284,6.716,15,15,15h392    c57.897,0,105-47.103,105-105C512,349.103,464.897,302,407,302z M407,482c-41.355,0-75-33.645-75-75    c0-21.876,9.418-41.591,24.409-55.313c0.052-0.048,0.103-0.098,0.154-0.147C369.893,339.407,387.597,332,407,332    c41.355,0,75,33.645,75,75C482,448.355,448.355,482,407,482z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                      </g>
                    </g>
                    <g>
                      <g>
                        <path class="active-path" d="M437,392h-15v-15c0-8.284-6.716-15-15-15s-15,6.716-15,15v15h-15c-8.284,0-15,6.716-15,15s6.716,15,15,15h15v15    c0,8.284,6.716,15,15,15s15-6.716,15-15v-15h15c8.284,0,15-6.716,15-15S445.284,392,437,392z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                      </g>
                    </g>
                  </g>
                </svg>
              </div>
              <h4>Define vaction packages</h4>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="icon-block">
              <div class="icon">
                <svg id="_30-leadership" xmlns="http://www.w3.org/2000/svg" height="512px" viewBox="0 0 192 192" width="512px" data-name="30-leadership">
                  <g>
                    <g id="Glyph">
                      <path class="active-path" d="m135.391 43.062a8 8 0 0 0 -1.734-8.719l-32-32a8 8 0 0 0 -11.314 0l-32 32a8 8 0 0 0 5.657 13.657h8v88a8 8 0 0 0 8 8h32a8 8 0 0 0 8-8v-88h8a8 8 0 0 0 7.391-4.938z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                      <path class="active-path" d="m72 176h48v16h-48z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                      <path class="active-path" d="m72 152h48v16h-48z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                      <path class="active-path" d="m167.438 49.344a8 8 0 0 0 -8.875 0l-24 16 8.875 13.312 11.562-7.708v33.052h16v-33.052l11.563 7.708 8.875-13.312z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                      <path class="active-path" d="m151.563 121.344-24 16 8.875 13.312 11.562-7.708v33.052h16v-33.052l11.563 7.708 8.875-13.312-24-16a8 8 0 0 0 -8.875 0z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                      <path class="active-path" d="m27.562 81.344-24 16 8.875 13.312 11.563-7.708v33.052h16v-33.052l11.563 7.708 8.875-13.312-24-16a8 8 0 0 0 -8.876 0z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                    </g>
                  </g>
                </svg>
              </div>
              <h4>Get Free Travel Leads</h4>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="icon-block">
              <div class="icon">
                <svg xmlns="http://www.w3.org/2000/svg" height="512px" viewBox="0 -61 512.00001 512" width="512px">
                  <g>
                    <path class="active-path" d="m220.960938 279.902344 170.949218 90.074218v-369.976562l-170.949218 90.074219zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                    <path class="active-path" d="m451.953125 169.988281h60.046875v30h-60.046875zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                    <path class="active-path" d="m486.78125 73.171875 11.480469 27.714844-55.480469 22.984375-11.480469-27.71875zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                    <path class="active-path" d="m486.769531 296.804688-55.480469-22.980469 11.480469-27.71875 55.480469 22.980469zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                    <path class="active-path" d="m190.960938 99.933594h-130.914063v30.023437h-5.015625c-30.34375 0-55.03125 24.6875-55.03125 55.03125s24.6875 55.03125 55.03125 55.03125h5.015625v30.027344h33.589844l33.027343 120.230469h97.324219l-33.027343-115.308594zm0 0" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                  </g>
                </svg>
              </div>
              <h4>Sell Your Package</h4>
            </div>
          </div>
          <!--div class="col-sm-6 col-md-3">
            <div class="icon-block">
              <div class="icon">
                <svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 448.941 448.941" height="512px" viewBox="0 0 448.941 448.941" width="512px">
                  <g>
                    <path class="active-path" d="m448.941 168.353h-161.338l-63.132-168.353-63.132 168.353h-161.339l121.478 106.293-65.36 174.295 168.353-84.176 168.353 84.176-65.361-174.296z" data-original="#000000" data-old_color="#000000" fill="#FFFFFF"></path>
                  </g>
                </svg>
              </div>
              <h4>Get reviews From Travellers</h4>
            </div>
          </div -->
        </div>
      </div>
    </section>
    <section class="section-agency">
      <div class="container">
        <div class="row">
          <div class="col-12 py-3">
            <h2 class="agency-title c-blue">WHY TO WORK WITH VACDOO</h2>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-4">
            <div class="why-block yellow">
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g>
                  <g>
                    <path d="M497.535,14.465c-19.569-19.568-51.395-19.241-70.557,0.726L322.092,124.488L66.131,39.781L12.4,93.513l213.352,131.365									L117.796,337.372l-69.231-11.366L0,374.571l101.78,35.649L137.429,512l48.565-48.565l-11.366-69.231l112.494-107.955									L418.487,499.6l53.732-53.732l-84.706-255.961L496.808,85.022C516.776,65.86,517.103,34.034,497.535,14.465z"></path>
                  </g>
                </g>
              </svg>
              <div class="clearfix"></div>
              <h4 class="why-block-title">Quick package posting</h4>
              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="why-block pink">
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g>
                  <g>
                    <path d="M481,332c0-16.5-13.5-30-30-30c16.5,0,30-13.5,30-30s-13.5-30-30-30H328.001C346.3,187.099,346,169.6,346,136									c0-24.961-20.466-45-45-45h-9c-5.7,0-12.9,4.501-14.401,11.4C262.758,160.181,239.555,226.484,181,239.914v217.062l51.899,17.223									c15.3,5.099,31.201,7.8,47.401,7.8H421C437.569,482,451,468.567,451,452c0-16.569-13.431-30-30-30h30c16.5,0,30-13.5,30-30									s-13.5-30-30-30C467.5,362,481,348.5,481,332z"></path>
                  </g>
                </g>
                <g>
                  <g>
                    <path d="M106,212H46c-8.291,0-15,6.709-15,15v270c0,8.291,6.709,15,15,15h60c24.814,0,45-20.186,45-45V257									C151,232.186,130.814,212,106,212z M106,452c-8.284,0-15-6.716-15-15c0-8.286,6.716-15,15-15s15,6.714,15,15									C121,445.284,114.284,452,106,452z"></path>
                  </g>
                </g>
                <g>
                  <g>
                    <path d="M237.353,66.142l-21.211-21.211c-5.859-5.859-15.352-5.859-21.211,0c-5.859,5.859-5.859,15.352,0,21.211l21.211,21.211									c5.859,5.859,15.351,5.859,21.211,0C243.212,81.494,243.212,72.001,237.353,66.142z"></path>
                  </g>
                </g>
                <g>
                  <g>
                    <path d="M407.069,44.931c-5.859-5.859-15.352-5.859-21.211,0l-21.211,21.211c-5.859,5.859-5.859,15.352,0,21.211									c5.859,5.859,15.352,5.859,21.211,0l21.211-21.211C412.928,60.283,412.928,50.79,407.069,44.931z"></path>
                  </g>
                </g>
                <g>
                  <g>
                    <path d="M301,0c-8.401,0-15,6.599-15,15v31c0,8.401,6.599,15,15,15s15-6.599,15-15V15C316,6.599,309.401,0,301,0z"></path>
                  </g>
                </g>
              </svg>
              <div class="clearfix"></div>
              <h4 class="why-block-title">Detailed itineraries</h4>
              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout long established fact that a reader will.</p>
            </div>
          </div>
          <div class="col-md-4 text-right">
            <div class="why-block green">
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 449.353 449.353" style="enable-background:new 0 0 449.353 449.353;" xml:space="preserve">
                <g>
                  <g>
                    <g>
                      <circle cx="224.676" cy="39.729" r="39.184"></circle>
                      <path d="M239.86,88.33c-0.185-0.005-0.37-0.01-0.555-0.013h-29.257c-30.003,0.564-53.867,25.344-53.303,55.347										c0.003,0.185,0.008,0.37,0.013,0.555v8.359c0,4.18,1.567,8.882,6.269,8.882h123.298c4.702,0,6.269-4.702,6.269-8.882v-8.359										C293.465,114.224,269.855,89.201,239.86,88.33z"></path>
                      <path d="M54.88,260.725c4.328,0,7.837-3.509,7.837-7.837c-0.109-49.175,22.201-95.718,60.604-126.433										c3.281-2.356,4.032-6.926,1.676-10.207c-0.193-0.269-0.404-0.524-0.631-0.764c-2.753-3.295-7.646-3.761-10.971-1.045										c-41.948,33.715-66.349,84.631-66.351,138.449C47.044,257.217,50.552,260.725,54.88,260.725z"></path>
                      <path d="M276.399,406.488L276.399,406.488c-33.431,10.967-69.491,10.967-102.922,0c-4.118-1.333-8.536,0.924-9.87,5.041										c-0.02,0.061-0.039,0.122-0.057,0.183c-1.067,4.16,1.192,8.451,5.224,9.927c18.054,5.869,36.918,8.866,55.902,8.882										c19.37,0.019,38.611-3.158,56.947-9.404c3.845-1.986,5.353-6.714,3.367-10.559C283.37,407.421,279.852,405.755,276.399,406.488z"></path>
                      <path d="M318.094,120.64c0.033,0.023,0.067,0.046,0.101,0.069c42.84,30.405,68.337,79.646,68.441,132.18										c0,4.328,3.509,7.837,7.837,7.837s7.837-3.509,7.837-7.837c-0.144-57.724-28.166-111.822-75.233-145.241										c-3.654-2.207-8.384-1.306-10.971,2.09C313.643,113.298,314.534,118.179,318.094,120.64z"></path>
                      <circle cx="67.941" cy="327.076" r="39.184"></circle>
                      <path d="M83.125,375.677c-0.185-0.005-0.37-0.01-0.555-0.013H53.313C23.31,376.228-0.555,401.008,0.01,431.011										c0.003,0.185,0.008,0.37,0.013,0.555v8.359c0,4.18,1.567,8.882,6.269,8.882H129.59c4.702,0,6.269-4.702,6.269-8.882v-8.359										C136.731,401.57,113.121,376.548,83.125,375.677z"></path>
                      <circle cx="381.411" cy="327.076" r="39.184"></circle>
                      <path d="M396.594,375.677c-0.185-0.005-0.37-0.01-0.555-0.013h-29.257c-30.003,0.564-53.867,25.344-53.303,55.347										c0.003,0.185,0.008,0.37,0.013,0.555v8.359c0,4.18,1.567,8.882,6.269,8.882H443.06c4.702,0,6.269-4.702,6.269-8.882v-8.359										C450.2,401.57,426.59,376.548,396.594,375.677z"></path>
                    </g>
                  </g>
                </g>
              </svg>
              <div class="clearfix"></div>
              <h4 class="why-block-title">Direct vation leads</h4>
              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="why-block lightblue">
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g>
                  <g>
                    <g>
                      <path d="M469.333,106.667H362.667V85.333c0-23.531-19.135-42.667-42.667-42.667H192c-23.531,0-42.667,19.135-42.667,42.667										v21.333H42.667C19.135,106.667,0,125.802,0,149.333v64C0,236.865,19.135,256,42.667,256h170.667v-10.667										c0-5.896,4.771-10.667,10.667-10.667h64c5.896,0,10.667,4.771,10.667,10.667V256h170.667C492.865,256,512,236.865,512,213.333										v-64C512,125.802,492.865,106.667,469.333,106.667z M320,106.667H192V85.333h128V106.667z"></path>
                      <path d="M506.083,267.51c-3.635-1.802-7.979-1.385-11.188,1.052c-7.583,5.74-16.417,8.771-25.563,8.771H298.667v32										c0,5.896-4.771,10.667-10.667,10.667h-64c-5.896,0-10.667-4.771-10.667-10.667v-32H42.667c-9.146,0-17.979-3.031-25.563-8.771										c-3.219-2.458-7.552-2.875-11.188-1.052C2.292,269.313,0,273.01,0,277.063v149.604c0,23.531,19.135,42.667,42.667,42.667h426.667										c23.531,0,42.667-19.135,42.667-42.667V277.063C512,273.01,509.708,269.313,506.083,267.51z"></path>
                    </g>
                  </g>
                </g>
              </svg>
              <div class="clearfix"></div>
              <h4 class="why-block-title">Free plans to start</h4>
              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="why-block cyne">
              <svg enable-background="new 0 0 511.334 511.334" height="512" viewBox="0 0 511.334 511.334" width="512" xmlns="http://www.w3.org/2000/svg">
                <path d="m0 224.667v132.333c0 38.108 30.892 69 69 69h373.334c38.108 0 69-30.892 69-69v-132.333c0-6.627-5.373-12-12-12h-487.334c-6.627 0-12 5.373-12 12zm127.667 84h-32c-11.598 0-21-9.402-21-21s9.402-21 21-21h32c11.598 0 21 9.402 21 21s-9.402 21-21 21z"></path>
                <path d="m511.334 158.667v-4.333c0-38.108-30.892-69-69-69h-373.334c-38.108 0-69 30.892-69 69v4.333c0 6.627 5.373 12 12 12h487.334c6.627 0 12-5.373 12-12z"></path>
              </svg>
              <div class="clearfix"></div>
              <h4 class="why-block-title">Detailed agency profile</h4>
              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
            </div>
          </div>
		  <div class="col-md-4 text-right">
            <div class="why-block orange">
              <svg enable-background="new 0 0 511.334 511.334" height="512" viewBox="0 0 511.334 511.334" width="512" xmlns="http://www.w3.org/2000/svg">
                <path d="m0 224.667v132.333c0 38.108 30.892 69 69 69h373.334c38.108 0 69-30.892 69-69v-132.333c0-6.627-5.373-12-12-12h-487.334c-6.627 0-12 5.373-12 12zm127.667 84h-32c-11.598 0-21-9.402-21-21s9.402-21 21-21h32c11.598 0 21 9.402 21 21s-9.402 21-21 21z"></path>
                <path d="m511.334 158.667v-4.333c0-38.108-30.892-69-69-69h-373.334c-38.108 0-69 30.892-69 69v4.333c0 6.627 5.373 12 12 12h487.334c6.627 0 12-5.373 12-12z"></path>
              </svg>
              <div class="clearfix"></div>
              <h4 class="why-block-title">Search engine friendly</h4>
              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section-agency" id="call-us">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h3>Agency and packages pages are designed attractive and industry efficient</h3>
            <p class="text-justify">At vacdoo.com we are continuously evaluating the presentation of your agency and packages information. We work hard for your pacakges to get noticed by more and more vacationes and leads to drop inquiries. We encourage you to provide as much information as possible for your prospective customers and take full advantage.</p><a class="btn btn-blue" href="javascript:;">Call Us Now</a>
          </div>
        </div>
      </div>
    </section>