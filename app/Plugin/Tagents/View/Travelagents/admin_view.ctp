<?php echo $this->element('admin/admin_sidebar');?>	
<div class="main-body">
	<div class="page-wrapper">
		<div class="page-header">
			<div class="page-header-title"><h4>Travel Agent</h4></div>
		</div>
	
			<div class="page-body">
                <div class="row">
                    <div class="col-md-12 col-xl-12">
                        <div class="card">
                            <div class="card-header">
                            	<div class="row">
                            		<div class="col-md-10">
	                                	<h5>Account Details Of <?php echo $agent['Travelagent']['firstname'].'&nbsp;&nbsp;'.$agent['Travelagent']['lastname'];?></h5>
	                            	</div>
	                                <div class="col-md-2 text-right"><?php echo $this->Html->link("Back to list",array('plugin'=>'tagents','controller'=>'travelagents','action'=>'index','admin'=>true),array('class'=>'btn btn-outline-info btn-sm'))?>
	                                </div>
                            	</div>
                            </div>
                            
                           	<div class="card-body">
								<div class="row">						
									<div class="col-md-6">
										<p><label>Name </label><?php echo $agent['Travelagent']['firstname'].'&nbsp;&nbsp;'.$agent['Travelagent']['lastname'];?></p>
										
										<p><label>Group </label><?php echo $agent['Travelagentgroup']['name']; ?></p>
										<p><label>Email </label><?php echo $agent['Travelagent']['email_address'];?></p>
										<p><label>Registered on </label><?php echo $agent['Travelagent']['created'];?></p>
										<p><label>Status </label><?php echo ($agent['Travelagent']['active']=='1')? "Active":"Disable";?></p>
										<p><label>Last LoggedIn </label><?php echo $agent['Travelagent']['lastlogin'];?></p>
									</div>
								</div>
							</div>
                        </div>
                        
                    </div>
                </div>
            </div>
    </div>
</div>