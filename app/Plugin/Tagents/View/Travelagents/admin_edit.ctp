<?php echo $this->element("admin/admin_sidebar"); ?>
	<!--main content start-->
<div class="main-body">
	<div class="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header"><h6>Update Agent<?php echo $this->Html->link("Back to list", array("plugin"=>"tagents","controller"=>"travelagents","action"=>"index","admin"=>true),array("class"=>"btn btn-sm btn-outline-info float-right")); ?></h6>
					</div>
					<div class="row">
						<div class="col-md-6">						
							<div class="card-body">
								<?php echo $this->Form->create('Travelagent', array("url"=>array("plugin"=>"tagents","controller"=>"travelagents","action"=>"edit","admin"=>true),"role"=>"form","class"=>""));?>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label for="firstname">First Name</label>
											<?php echo $this->Form->text('Travelagent.firstname',array("class"=>"form-control","placeholder"=>"First name"));echo $this->Form->hidden('Travelagent.id'); echo $this->Form->error('Travelagent.firstname');?>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
										<label for="lastname">Last Name</label>
											<?php echo $this->Form->text('Travelagent.lastname',array("class"=>"form-control","placeholder"=>"Last name")); echo $this->Form->error('Travelagent.lastname');?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label for="email_address">Email Id</label>
											<?php echo $this->Form->text('Travelagent.email_address',array("class"=>"form-control","placeholder"=>"Email Id")); echo $this->Form->error('Travelagent.email_address');?>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
										<label for="Phone">Phone</label>
											<?php echo $this->Form->text('Travelagent.phone',array("class"=>"form-control","placeholder"=>"Phone number")); echo $this->Form->error('Travelagent.phone');?>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label for="city">City</label>
											<?php echo $this->Form->text('Travelagent.city',array("class"=>"form-control","placeholder"=>"City name")); ?>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
										<label for="Zipcode">Zipcode</label>
											<?php echo $this->Form->text('Travelagent.zipcode',array("class"=>"form-control","placeholder"=>"Zipcode")); ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label for="Country">Country</label>
											<?php echo $this->Form->select('Travelagent.country_id',$appcountries,array("class"=>"form-control","empty"=>"--Country--")); ?>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
										<label for="Region">Region</label>
											<?php echo $this->Form->select('Travelagent.region_id','',array("class"=>"form-control","empty"=>"--Region--")); ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label for="group">Group</label>
											<?php echo $this->Form->select('Travelagent.travelagentgroup_id',$groups,array("class"=>"form-control","empty"=>"--Group--")); ?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="checkbox mt-5">
											<label>
											<?php echo $this->Form->checkbox('Travelagent.active',array()); ?>&nbsp;Make it active</label>
										</div>
									</div>									
								</div>
								<div class="row">
									<div class="col-md-6">
										<button type="submit" class="btn btn-success">Update</button>
									</div>
								</div>
								<?php echo $this->Form->end(); ?>
							</div>
						</div>
						<div class="col-md-6"> YET TO DECIDE FOR CONTENT</div>
					</div>
                </div>	
		
			</div><!-- end of id welcome -->
		</div><!--/#content.span10-->
	</div>
</div>
<script>
$("select#TravelagentCountryId").change(function()
	  {
		  $("select#TravelagentRegionId").html("<option value=''>Loading...</option>");
		    $.getJSON("http://<?php echo $_SERVER['SERVER_NAME']; ?>/acldemo/"+"regions/getregions/"+$(this).val(),{ajax: 'true'}, function(objData){
		      var options = '<option value="">--- Choose Region ---</option>';
		      if(objData!=null)
		      {
			      $.each(objData, function(i,item){
			    	  
			          options += '<option value="' +i+ '">' +item + '</option>';
			          
			        });
		      }
		      $("select#TravelagentRegionId").html(options);
		    });
	  });
</script>