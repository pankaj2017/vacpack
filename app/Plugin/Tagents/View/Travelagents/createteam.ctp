<?php echo $this->Html->script('ckeditor/ckeditor'); ?>
<?php echo $this->Html->script('bootstrap-filestyle.min',false); ?>
<?php echo $this->Html->css('jquery_datetimepicker'); //requried for bdate ?>

<div class="container inner_pages">
<div class="row">
<div class="col-md-3">

	<?php echo $this->element('Tagents.sidebar_dashboard');?>
	
</div>
<div class="col-md-9">

<h4 class="">Create my team
	<div class="float-right btn btn-light"><?php echo $this->Html->link("Cancel", array("plugin"=>"tagents","controller"=>"travelagents","action"=>"dashboard","admin"=>false));?>
		</div></h4><hr>
	<div class="col-right">
		<fieldset>
			<?php echo $this->Form->create('Travelagency',array("url"=>array("plugin"=>"tagents","controller"=>"travelagents","action"=>"createteam"),"type" =>"file")); ?>				
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<label>First name</label>
						<?php echo $this->Form->text('Travelagent.firstname',array("type"=>"text","class"=>"form-control form-control-sm","placeholder"=>"First name")); ?>
						<?php echo $this->Form->error('Travelagent.firstname'); ?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						<label>Last name</label>
						<?php echo $this->Form->text('Travelagent.lastname',array("type"=>"text","class"=>"form-control form-control-sm","placeholder"=>"Last name")); ?>
						</div>
					</div>						
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<label>Email Id</label>
						<?php echo $this->Form->text('Travelagent.email_address',array("type"=>"text","class"=>"form-control form-control-sm","placeholder"=>"Email address"));?>
						<?php echo $this->Form->error('Travelagent.email_address'); ?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						<label>Phone</label>
						<?php echo $this->Form->text('Travelagent.phone',array("type"=>"text","class"=>"form-control form-control-sm","placeholder"=>"Contact number"));?>
						<?php echo $this->Form->error('Travelagent.phone'); ?>
						</div>
					</div>
				</div>
				<div class="row">						
					<div class="col-md-6">
						<div class="form-group">
						<label>Password</label>
						<?php echo $this->Form->text('Travelagent.password',array("type"=>"password","class"=>"form-control form-control-sm","placeholder"=>"Password"));
						echo $this->Form->error('Travelagent.password'); ?>
						</div>
					</div>						
					<div class="col-md-6">
						<div class="form-group">
						<label>Confirm password</label>
						<?php echo $this->Form->text('Travelagent.confirm_password',array("type"=>"password","class"=>"form-control form-control-sm","placeholder"=>"Retype password"));
						echo $this->Form->error('confirm_password'); ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12"><label>About team</label>
					<span class="pull-right">Abstract about team,skills, interests and passions as short introduction.</span>
					<?php echo $this->Form->input('Travelagentprofile.aboutme',array("type"=>"textarea","label"=>false, "class"=>"form-control ie7-margin ckeditor"));?>
						<?php echo $this->Form->error('Travelagentprofile.aboutme'); ?></div>
				</div>
				
				<div class="row">
					<div class="form-group col-md-12">
						<label>Profile image</label>
						<?php echo $this->Form->input("Travelagentprofile.image", array('type'=>'file',"class"=>"filestyle","data-buttonName"=>"btn-default","label"=>false)); ?>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group col-md-3"><?php echo $this->Form->submit('Create',array("class"=>"btn btn-primary")); ?></div>
				</div>
				
			<?php echo $this->Form->end(); ?>
		</fieldset>
	</div>
</div>
</div>
</div>
<script type="text/javascript">

	CKEDITOR.replace( 'TravelagentprofileAboutme', {
	toolbar: [[ 'Source' ],[ 'Bold', 'Italic','Underline','Subscript','Superscript'],[ 'NumberedList','BulletedList' ]],

	height: '200',
	});

</script>
<script>$(":file").filestyle({buttonName: "btn-default",buttonText: "&nbsp;Image"});</script>

