<style>
/* Tabs*/
#tabs.nav-tabs { border-bottom: 2px solid #DDD; }
#tabs.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
#tabs.nav-tabs > li > a { border: none; color: #ffffff;background: #5a4080; }
#tabs.nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none;  color: #5a4080 !important; background: #fff; }
#tabs.nav-tabs > li > a::after { content: ""; background: #5a4080; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
#tabs.nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
#tabs.tab-nav > li > a::after { background: ##5a4080 none repeat scroll 0% 0%; color: #fff; }
#tabs.tab-pane { padding: 15px 0; }
#tabs.tab-content{padding:20px}
#tabs.nav-tabs > li  {width:20%; text-align:center;}
#tabs.card {background: #FFF none repeat scroll 0% 0%; box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3); margin-bottom: 30px; }
@media all and (max-width:724px){
#tabs.nav-tabs > li > a > span {display:none;}	
#tabs.nav-tabs > li > a {padding: 5px 5px;}
}
</style>

<div class="container">
	<div class="row">
		<div class="col-md-3">
			<?php echo $this->element('Tagents.travelagent_profile');?>
			<div class="mt-3">
			<?php echo $this->element('Tagents.sidebar_dashboard');?>
			</div>
		</div>
		<div class="col-md-9">
			<h4><?php 
			echo $currentuser['Travelagent']['firstname']." ".$currentuser['Travelagent']['lastname']; ?>
			<?php if($this->Session->check('Auth.Travelagent.id')){
					if($this->Session->read('Auth.Travelagent.id')==$currentuser['Travelagent']['id']){
				?>	
					<div class="float-right btn btn-light">
						<?php echo $this->Html->link("Update Profile", array("controller"=>"travelagentprofiles","action"=>"edit/".$currentuser['Travelagentprofile']['id'],"admin"=>false));?>
					</div>
				<?php } 
			}?></h4>
			<hr>
			<div class="text-justify">
			<?php echo $currentuser['Travelagentprofile']['aboutme'];?>
			</div>
		</div><!--end of col-md-9-->
	</div><!--row-->
</div><!--container-->



<?php echo $this->fetch('script_execute'); ?>
<script>$(":file").filestyle({buttonName: "btn-default",buttonText: "Change image"});</script>
<script>
$(document).ready(function() {
$(".btn-pref .btn").click(function () {
    $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).removeClass("btn-default").addClass("btn-primary");   
});
});</script>