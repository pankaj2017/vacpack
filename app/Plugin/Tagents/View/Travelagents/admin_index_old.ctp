<?php echo $this->element('admin/admin_sidebar');?>
<!--main content start-->
<section class="main-body">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				
				<div><?php echo $this->Session->flash(); ?> </div>
				<div class="">
			<div class="card mb-3">
				<div class="card-body">
				<?php echo $this->Form->create("Travelagent",array("url"=>array("plugin"=>"tagents","controller"=>"travelagents","action"=>"index","admin"=>true))); ?>
				<div class="form-inline">
					
					<?php echo $this->Form->select('Travelagentgroup.id', $travelagentgroups, array("empty"=>"--Select Group--","class"=>"form-control mx-sm-3"), false);?>
					<?php echo $this->Form->submit("Filter",array("class"=>"btn btn-primary btn-sm float-right")); ?>
					<?php echo $this->Form->error('Travelagentgroup.name');?>
					
				</div>
				<?php	echo $this->Form->end();?>
				</div>
				</div>
				</div>
				<div class="card">
					<div class="card-header"><h6>List of Agents<?php echo $this->Html->link("Add New", array("plugin"=>"tagents","controller"=>"travelagents","action"=>"create","admin"=>true),array("class"=>"btn btn-sm btn-outline-info float-right")); ?></h6>
					</div>
					<div class="card-body">
						
							<table class="table table-bordered table-striped table-condensed">
								<thead>
									<tr>
										<th width="10%">First Name</th>
										<th width="10%">Last Name</th>
										<th width="30%">Email</th>
										<th width="14%">phone</th>
										<th width="7%">Group</th>
										<th width="10%">Modified</th>
										<th width="14%" >Action</th>									
									</tr>							
								</thead>
								<tbody>
									<?php 
									foreach($aagents as $aagent){	?>
									<tr>
										<td><?php echo $aagent['Travelagent']['firstname']?> </td>
										<td><?php echo $aagent['Travelagent']['lastname']?> </td>
										<td><?php echo $aagent['Travelagent']['email_address'];?> </td>
										<td><?php echo $aagent['Travelagent']['phone']?></td>
										<td><?php echo $aagent['Travelagentgroup']['name']; ?></td>
										<td><?php echo $this->Time->format('M j, Y',$aagent['Travelagent']['modified']); ?></td>
										<td><?php echo $this->Html->link('View',array("plugin"=>"tagents","controller"=>"travelagents",'action'=>'view/'.$aagent['Travelagent']['id'],'admin'=>true),array('class'=>'badge badge-success')); ?> 
										<?php echo $this->Html->link('Edit',array("plugin"=>"tagents","controller"=>"travelagents",'action'=>'edit/'.$aagent['Travelagent']['id'],'admin'=>true),array('class'=>'badge badge-info')); ?> 
										<?php echo $this->Html->link('Delete',array("plugin"=>"tagents","controller"=>"travelagents",'action'=>'delete/'.$aagent['Travelagent']['id'],'admin'=>true),array('class'=>'badge badge-danger')); ?></td>
									</tr>
									<?php } ?>					
								</tbody>
							</table>
						
						<p>
						<?php
						echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
						));
						?>	</p>
						
						<ul class="pagination">
							<?php
								echo $this->Paginator->prev(__('Prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
								echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
								echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
							?>
						</ul>
					</div>
				</div><!-- panel -->
			</div><!--/col-lg-12-->
		</div>
	</div>
	</section>
</section>		