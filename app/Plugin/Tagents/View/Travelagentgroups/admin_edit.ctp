<?php echo $this->element("admin/admin_sidebar"); ?>
	<!--main content start-->
<div class="main-body">
	<div class="wrapper">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header"><h6>Update Travel Agent Group<?php echo $this->Html->link("Back to list", array("plugin"=>"tagents","controller"=>"travelagentgroups","action"=>"index","admin"=>true),array("class"=>"btn btn-outline-info btn-sm float-right")); ?></h6>
					</div>
					<div class="card-body">
						<?php echo $this->Form->create('Travelagentgroup', array("url"=>array("plugin"=>"tagents","controller"=>"travelagentgroups","action"=>"edit","admin"=>true),"role"=>"form","class"=>""));	?>
						<div class="row">
 						<div class="col-md-2">
							<div class="form-group">
								<?php echo $this->Form->text('Travelagentgroup.name',array("class"=>"form-control","placeholder"=>"Group name")); echo $this->Form->hidden('Travelagentgroup.id');?>
							</div>
						</div>
						<div class="col-md-7">
							<div class="form-group">
								<?php echo $this->Form->text('Travelagentgroup.description',array("class"=>"form-control","placeholder"=>"Group description")); ?>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox mt-2">
								<label>
								<?php echo $this->Form->checkbox('Travelagentgroup.active',array()); ?>&nbsp;Make it active</label>
							</div>
						</div>
						<div class="col-md-1">
							<button type="submit" class="btn btn-sm btn-success">Update</button>
						</div>
						</div><!--row-->
						<?php echo $this->Form->end(); ?>
					</div>
					
                </div>	
		
			</div><!-- end of id welcome -->
		</div><!--/#content.span10-->
	</div>
</div>