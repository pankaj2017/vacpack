<?php echo $this->element("admin/admin_sidebar"); ?>	
<!--main content start-->
<div class="main-body">
	<div class="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				
				<div class="card">
					
					<div class="card-header"><h6>Detail of <?php echo $travelagentgroup['Travelagentgroup']['name'];?>
					<?php echo $this->Html->link("Back to list", array("plugin"=>"tagents","controller"=>"travelagentgroups","action"=>"index","admin"=>true),array("class"=>"btn btn-sm btn-outline-info float-right")); ?></h6>
					</div>
					<div><?php echo $this->Session->flash(); ?> </div>
					<div class="row">
						<div class="col-md-6">
							<div class="card-body">
								<?php echo $travelagentgroup['Travelagentgroup']['description'];?>
							</div>
						</div>
					</div>
				</div><!-- panel -->
			</div><!--/col-lg-12-->
		</div>
	</div>
</div>