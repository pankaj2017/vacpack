<?php echo $this->element("admin/admin_sidebar"); ?>	
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				
				<div class="card">
					<div class="card-header"><h6>List of Agent groups<?php echo $this->Html->link("Add New", array("plugin"=>"tagents","controller"=>"travelagentgroups","action"=>"create","admin"=>true),array("class"=>"btn btn-outline-info btn-sm float-right")); ?></h6></div>
					<?php echo $this->Session->flash('auth');?>
					<div class="card-body">
						
							<table class="table table-bordered table-striped table-condensed">
								<thead>
								<tr>
									<th>Group</th>
									<th>Description</th>
									<th>Active</th>
									<th>Action</th>								  
								</tr>
								</thead>							
								<tbody>
									<?php //pr($admingroups); 
									foreach($travelagentgroups as $group){	?>
										<tr>
										<td><?php echo $group['Travelagentgroup']['name']; ?> </td>
										<td><?php echo $group['Travelagentgroup']['description']; ?> </td>
										<td><?php echo ($group['Travelagentgroup']['active'])?"Yes":"No"; ?></td>
										<td>
											<?php echo $this->Html->link('View',array('plugin'=>'tagents','controller'=>'travelagentgroups','action'=>'view/'.$group['Travelagentgroup']['id'],'admin'=>true),array('class'=>'badge badge-success btn-xs')); ?>
										
											<?php echo $this->Html->link('Edit',array('plugin'=>'tagents','controller'=>'travelagentgroups','action'=>'edit/'.$group['Travelagentgroup']['id'],'admin'=>true),array('class'=>'badge badge-info btn-xs')); ?>
										
											<?php echo $this->Html->link('Delete',array('plugin'=>'tagents','controller'=>'travelagentgroups','action'=>'delete/'.$group['Travelagentgroup']['id'],'admin'=>true),array('class'=>'badge badge-danger btn-xs'),"Are you sure you want to delete?"); ?>
										</td>										
										</tr>
									<?php }	?>
								</tbody>
							</table>						
						
					</div><!--/#content.span10-->
				</div><!-- panel -->
			</div><!--/col-lg-12-->
		</div>
	</section>
</section>