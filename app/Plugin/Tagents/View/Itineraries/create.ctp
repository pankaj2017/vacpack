<?php 
echo $this->Html->Script('jquery.tmpl');
?>
<!--main content start-->

<style>
.checkbox input{
  margin: 0 5px 0 0px;
}
</style>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header"><h6>Create an itinerary for <?php echo $pkgdata['Packag']['title'];?><?php echo $this->Html->link("Back to package", array("plugin"=>"tagents","controller"=>"packags","action"=>"index","admin"=>false,),array("class"=>"btn btn-sm btn-outline-info float-right")); ?></h6>

						
					</div>
					<div class="row">
						<div class="col-md-8">						
							<div class="card-body">
								<?php echo $this->Form->create('Itinerary', array("url"=>array("plugin"=>"tagents","controller"=>"itineraries","action"=>"create","admin"=>false),"role"=>"form","class"=>"",'type'=>'file'));?>
								<?php //echo $this->Form->text('Itinerary.packag_id',array('value'=>$packag['Packag']['id'])); ?>
								<?php 
								if(isset($itinerary1)) {

									$this->request->data = $itinerary1; 
								}
								// pr($this->request->data['Itinerary'][0]['id']);die;
								?>
								<?php for ($i = 0; $i < $pkgdata['Packag']['numberofdays']; $i++) { 
                                    
		                                if (isset($this->request->data['Itinerary'][$i]['id'])) {

		                                    echo $this->Form->hidden('Itinerary.' . $i . '.id', ['value' => $this->request->data['Itinerary'][$i]['id']]);
		                                	echo $this->Form->hidden('Itinerary.' . $i . '.packag_id', ['value' => $this->request->data['Itinerary'][$i]['packag_id']]);
		                                }
		                                echo $this->Form->hidden('Itinerary.' . $i . '.packag_id', ['value' => $packageid]);
		                                ?>

									<div class="col-md-12">
		                                <label class="control-label font-medium">Day Number<span class="text-danger">*</span></label>
		                                <?php
		                                echo $this->Form->text('Itinerary.' . $i . '.daynumber', array('title' => "Please enter license number.", 'class' => 'form-control', 'required' => 'required', 'autocomplete' => "off",'readonly','value'=> $i+1));                                
		                                ?>  
		                            </div>
		                            <div class="col-md-12">
		                                <label class="control-label font-medium">Title Of Day<span class="text-danger">*</span></label>
		                                <?php
		                                echo $this->Form->text('Itinerary.' . $i . '.titleofday', array('title' => "Please enter license number.", 'class' => 'form-control', 'required' => 'required', 'autocomplete' => "off"));                                
		                                ?>  
		                            </div>
		                            <div class="col-md-12">
		                                <label class="control-label font-medium">About The Day<span class="text-danger">*</span></label>
		                                <?php
		                                echo $this->Form->textarea('Itinerary.' . $i . '.abouttheday', array('title' => "Please enter license number.", 'class' => 'form-control', 'required' => 'required', 'autocomplete' => "off", 'pattern' => "^[A-Za-z0-9]{1,20}$"));                                
		                                ?>  
		                            </div>
		                            <div class="col-md-10 form-group">
										<label>Image<span class="">*</span></label>
										<?php echo $this->Form->input('Itinerary.' . $i . '.imageofday', array('type'=>'file',"class"=>"file", "data-max-file-count"=>"1", "label"=>false)); ?>
									</div>
									<div class="col-md-2 form-group">
										<!-- <label>Place Image<span class="">*</span></label> -->
										<?php 
										if(!empty($this->request->data['Itinerary'][$i]['imageofday'])){
											echo $this->Html->image($this->request->data['Itinerary'][$i]['imageofday'],array('class'=>'img-fluid')); ?>
										<?php } ?>
									</div>
		                            <br><hr>
								<?php } ?>
								<!-- <div id="itineraryContainer"></div> -->
								
								<div class="">
									<button type="submit" class="btn btn-sm btn-primary">Update Itinerary</button>
									<!-- <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button> -->
								</div>
								<?php echo $this->Form->end(); ?>
							
						</div><!--card-body-->
						</div>
						<div class="col-md-5">
						
							
						</div>
					</div>
                </div>	
		
			</div><!-- end of id welcome -->
		</div><!--/#content.span10-->
	</div>
	
<script>
$(document).ready(function () {
    var myPackagId = <?php echo $packag['Packag']['id'];?>;
	var nDays =<?php echo $packag['Packag']['numberofdays'];?>;
	$("#itineraryContainer").empty();
	$("#itineraryTemplate").tmpl({
		days:new Array(parseInt(nDays)),
		pid:myPackagId
	}).appendTo("#itineraryContainer");
});
</script>
<script id="itineraryTemplate" type="text/x-jQuery-tmpl">
{{each(i) days}}
    <div class="row">
		<div class="col-md-2">
			<div class="form-group">
			<label>Day ${i+1}</label>
			<input name="data[Itinerary][${i}][daynumber]" type="text" class="form-control form-control-sm" id="" value=${i+1} readonly>
			
			</div>
		</div>
		<div class="col-md-10">
			<div class="form-group">
			<label>Title of the day</label>
			<input name="data[Itinerary][${i}][titleofday]" type="text" class="form-control form-control-sm" value=[Itinerary][${i}][titleofday]>
			
			
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
			
			<textarea name="data[Itinerary][${i}][abouttheday]" rows="2" class="form-control form-control-sm" ></textarea>
			
			</div>
		</div>
	</div>
{{/each}}
</script>