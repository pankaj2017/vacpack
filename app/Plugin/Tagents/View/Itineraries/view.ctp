<?php //echo $this->element('sidebar_dashboard');?>	
<div class="container">
	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<header class="card-header"><?php echo $package['Packag']['title'];?> </header>
					<div><?php echo $this->Session->flash(); ?> </div>
					<div class="row">
						<div class="col-md-6">
							<div class="card-body">
								<p><label>By&nbsp; </label><?php echo $package['Travelagency']['name'];?></p>
								
								<p><?php echo $package['Travelagency']['description'];?></p>
								<p><label>Since&nbsp;</label><?php echo $package['Travelagency']['created'];?></p>
								<p><label>Status&nbsp;</label><?php echo ($package['Travelagency']['active']=='1')? "Active":"Disabled";?></p>
								<p><label>Last updated&nbsp;</label><?php echo $this->Time->format('M jS, Y h:i A',$package['Travelagency']['modified']); ?></p>
							</div>
						</div>
						<div class="col-md-6">
						<?php foreach($package['Travelagency']['Travelagent'] as $agent) {?>
							<p><?php echo $agent['firstname'].'&nbsp;'.$agent['lastname'];?></p>
							<p><?php echo $agent['email_address'];?></p>
							<p><?php echo $agent['phone'];?></p>
						<?php } ?>
						</div>
					</div>				
				</div>
			</div><!-- end of id welcome -->
		</div><!--/#content.span10-->
	
</div>