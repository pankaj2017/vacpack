<?php echo $this->Html->script('bootstrap-filestyle.min',false); ?>
<div class="card" style="">
	<div id="Profile">
	<?php echo $this->Html->image($currentuser['Travelagentprofile']['userimage'],array("alt"=>"","class"=>"img-fluid mb-2")); ?>
	</div>
	<?php if($currentuser['Travelagent']['id'] == $this->Session->read('Auth.Travelagent.id')){ ?>
	<?php echo $this->Form->create('Travelagentprofile',array('id'=>'imgForm','type' => 'file')); ?>
	<div class="form-group">
	<?php echo $this->Form->input("Travelagentprofile.image", array('type'=>'file',"class"=>"filestyle","data-buttonName"=>"btn-default","label"=>false));
	echo $this->Form->error('name',array('style'=>'color:red;'));				 
	echo $this->Form->hidden('Travelagentprofile.travelagent_id', array('value'=>$this->Session->read("Auth.Travelagent.id")));
	echo $this->Form->hidden('Travelagentprofile.id', array('value'=>$currentuser['Travelagentprofile']['id']));
	echo $this->Form->end();?>
	</div>
	<div id="reviewloader1" style="display:none; " class="btn btn-light">Image is being uploaded...</div>
	<div id='uploadError'>
		<?php if ($this->Session->check('Message.flash')):echo $this->Session->flash(); endif;  ?>
	</div>
	<?php } ?>
	
</div><!-- card -->
<ul class="list-group">
		<hr>
		<li class="list-group-item"><i class="fas fa-phone"></i>&nbsp;<?php echo $currentuser['Travelagent']['phone']; ?></li>
		<li class="list-group-item"><i class="far fa-envelope"></i>&nbsp;<?php echo $currentuser['Travelagent']['email_address']; ?></li>
		
			
	</ul>
<script type="text/javascript">
	$('#TravelagentprofileImage').on('change',(function(e) { 
        if($.trim($('#TravelagentprofileImage').val()) == ''){
			document.getElementById('uploadError').innerHTML = 'Please select file';
			document.getElementById('uploadError').style.display = 'block';
			return false;
		}else{
			document.getElementById('uploadError').style.display = 'none';
			document.getElementById('reviewloader1').style.display = 'block';
	        e.preventDefault();
	        
			var formData = new FormData($('#imgForm')[0]);
			
	        $.ajax({
	            type:'POST',
	            url: '<?php echo Router::url('/');?>' + 'tagents/travelagentprofiles/uploadimage/',
				data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            success:function(data){
	            	$('#Profile').html(data);
	               	document.getElementById('reviewloader1').style.display = 'none';
	            },
	            error: function(data){
	                console.log("error");
	                console.log(data);
	            }
	        });
	     }
    }));
</script>
<script>$(":file").filestyle({buttonName: "btn-default",buttonText: "Change image"});</script>