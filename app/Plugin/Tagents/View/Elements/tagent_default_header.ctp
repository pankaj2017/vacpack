<header>
  	<div class="agency-header mb-3">
	    <div class="container">
	      <div class="header">
	        <div class="logo">
	        	<?php $logo_img = $this->Html->image("/assets/images/logo.png",array('class'=>'','alt'=>"vacdoo.in"), array('escape'=>false)); 
                        echo $this->Html->link($logo_img, array('plugin'=>false,"controller"=>"pages","action"=>"display",),array("escape"=>false,'title'=>"vacdoo.in"));
                ?>
	        </div>
	        <ul class="right-navigation">
	        	<?php 
					if ($this->Session->check("Auth.Travelagent")){
						$travelagencyid = $this->Session->read("Auth.Travelagent.travelagency_id");
					}
				?>
				<?php if ($this->Session->check('Auth.Travelagent.id')){ ?>
				
				<li class="nav-item dropdown">
					<?php echo $this->Html->link("Welcome&nbsp;&nbsp;".$this->Session->read('Auth.Travelagent.firstname'),array('plugin'=>'',"controller"=>"travelagents","action"=>"dashboard","admin"=>false),array("class"=>"nav-link dropdown-toggle","data-toggle"=>"dropdown", "aria-haspopup"=>"true","aria-expanded"=>"false","escape"=>false)); ?>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<?php echo $this->Html->link("My Dashboard", array('plugin'=>'tagents','controller'=>'travelagents','action'=>'dashboard','admin'=>false),array("class"=>"dropdown-item")); ?>
					<?php echo $this->Html->link("My Travel Agency", array('plugin'=>'tagents','controller'=>'travelagencies','action'=>'view/'.$travelagencyid,'admin'=>false),array("class"=>"dropdown-item")); ?>
					<?php echo $this->Html->link("My Travel Packages", array('plugin'=>'tagents','controller'=>'packags','action'=>'index','admin'=>false),array("class"=>"dropdown-item")); ?>
					<?php echo $this->Html->link("My Package Inquiries", array('plugin'=>'tagents','controller'=>'packags','action'=>'index','admin'=>false),array("class"=>"dropdown-item")); ?>
					<?php echo $this->Html->link("Change Password", array('plugin'=>'tagents','controller'=>'travelagents','action'=>'changepassword/'.$this->Session->read('sessuserid'),'admin'=>false),array("class"=>"dropdown-item")); ?>
					<?php echo $this->Html->link("Logout", array("plugin"=>"tagents","controller"=>"travelagents","action"=>"logout","admin"=>false),array("class"=>"dropdown-item"));?>
					</div>
				</li>
				<?php }else { ?>

	        	<li><?php echo $this->Html->link("Sign up",array('plugin'=>'tagents','controller'=>'travelagents','action'=>'register','admin'=>false),array("class"=>"btn btn-orange-border btn-round"));?></li>
				<li><?php echo $this->Html->link("Sign in",array('plugin'=>'tagents','controller'=>'travelagents','action'=>'login','admin'=>false),array("class"=>"btn btn-orange-border btn-round"));?></li>
				
				<?php } ?>
	        </ul>
	      </div>
	    </div>
  	</div>
  </header>