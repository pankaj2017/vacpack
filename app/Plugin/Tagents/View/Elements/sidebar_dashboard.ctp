<aside>
	<?php 
		if ($this->Session->check("Auth.Travelagent")){
			$travelagencyid = $this->Session->read("Auth.Travelagent.travelagency_id");
		}
	?>
	<ul class="list-group">
		<li class="list-group-item vacdoo-bg-light-grey text-white"><h5>My Vacdoo</h5></li>
		<li class="list-group-item"><?php echo $this->Html->link("My Dashboard", array('plugin'=>'tagents','controller'=>'travelagents','action'=>'dashboard','admin'=>false),array("class"=>"")); ?></li>
					
		<li class="list-group-item"><?php echo $this->Html->link("My Travel Agency", array('plugin'=>'tagents','controller'=>'travelagencies','action'=>'view/'.$travelagencyid,'admin'=>false),array("class"=>"")); ?></li>
		
		<li class="list-group-item"><?php echo $this->Html->link("My Travel Packages", array('plugin'=>'tagents','controller'=>'packags','action'=>'index','admin'=>false),array("class"=>"")); ?></li>
		
		<li class="list-group-item"><?php echo $this->Html->link("My Package Inquiries", array('plugin'=>'tagents','controller'=>'packags','action'=>'index','admin'=>false),array("class"=>"")); ?></li>
	</ul>
</aside>